#ifndef _APP_LPR_H_
#define _APP_LPR_H_ 1

#include "basic_types.h"
#include "macro_tools.h"
#include "intrinsics.h"
#include "our_gl.h"

#define LPR_APP_NAME APP_NAME
#define LPR_SHADERS_FOLDER_PATH "../resources/shaders/lepre/"
#define LPR_INTERNAL 1
#include "lepre.h"

#endif /* ifndef _APP_LPR_H_ */
