#if 0
	echo $#
	echo $0 $1 $2

	#echo fullpath should be $(readlink -e $0)
	#export THIS_FILE_PATH=$0
	export THIS_FILE_PATH=$(readlink -e $0)
	#export THIS_FILE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
	export THIS_FILE_DIR=$(dirname $THIS_FILE_PATH)
	export THIS_FILE_NAME=${THIS_FILE_PATH##*/}
	export THIS_FILE_BASE_NAME=${THIS_FILE_NAME%%.*}

	exec /bin/bash -c 'exec "/bin/bash" <(tail -n +15 "$0") "$@"' "$0" "$@"
	exit $!

	# IFS= read -r -d '' BASH_TEXT <<BASH_SECTION
	# !/bin/bash
	echo -n "curr dir: "
	pwd

	APP_NAME='"app"'

	REL_ROOT_PROJ=./
	REL_DLIBS_DIR=../builds/hotlibs/
	DLIBS_DIR_FULL_PATH="$THIS_FILE_DIR/$REL_DLIBS_DIR"

	if [ ! -d $DLIBS_DIR_FULL_PATH ]
	then
		mkdir -p $DLIBS_DIR_FULL_PATH
	fi

	DLIB_NAME=$THIS_FILE_BASE_NAME
	DLIB_FILE_NAME=lib$DLIB_NAME.so
	if [ -z "${DLIB_VERSION}" ]
	then
		DLIB_VERSION=$(date +%Y.%m.%d.%H.%M.%S);
	fi
	DLIB_FILE_NAME_VERSIONED=$DLIB_FILE_NAME.$DLIB_VERSION

	DLIB_FULL_PATH="$DLIBS_DIR_FULL_PATH/$DLIB_FILE_NAME"
	DLIB_VERSIONED_FULL_PATH="$DLIBS_DIR_FULL_PATH/$DLIB_FILE_NAME_VERSIONED"

	echo "this file dir   = '$THIS_FILE_DIR'"
	echo "this file path  = '$THIS_FILE_PATH'"
	echo "this file name  = '$THIS_FILE_NAME'"
	echo "base file name  = '$THIS_FILE_BASE_NAME'"
	echo "executable name = '$EXECUTABLE_NAME'"
	echo "libs dir        = '$DLIBS_DIR_FULL_PATH'"
	echo "lib name        = '$DLIB_NAME'"
	echo "lib full path   = '$DLIB_FULL_PATH'"
	echo "lib v. fullpath = '$DLIB_VERSIONED_FULL_PATH'"
	echo "app name        = '$APP_NAME'"
	echo
	echo args count: $#
	i=0
	for arg in $*; do
		echo -e "\targ $i: $arg"
		((i++))
	done
	echo

	if [ -z "${CXX+x}" ]
	then
		#CXX=arm-linux-gnueabihf-g++
		CXX=clang++
		#CXX=g++
	fi

	if [[ "${CXX}" == @(g++*|gcc*) ]]
	then

	CXX_FLAGS="-std=c++20 \
	           -Wall \
	           -Wextra \
	           -Wno-missing-braces \
	           -Wno-unused-function \
	           -Wno-comment \
	           -Wl,-export-dynamic -rdynamic -fPIC -shared -fno-exceptions \
	           -feliminate-unused-debug-types \
	           -ffunction-sections -Wl,--gc-sections \
	           -fvisibility=hidden \
	           -O0 -ggdb3 \
	           -march=native \
	           -DAPP_NAME='$APP_NAME' \
	           -DLIB_NAME='\"$DLIB_NAME\"' \
	           -DLIB_VERSION=$DLIB_VERSION \
	           $CXX_FLAGS"

	elif [[ $CXX == clang* ]]
	then
	CXX_FLAGS="-std=c++20 \
	           -Wall \
	           -Wextra \
	           -Wno-missing-braces \
	           -Wno-unused-function \
	           -Wno-comment \
	           -Rpass-missed=loop-vectorize -Rpass-analysis=loop-vectorize \
	           -Wl,-export-dynamic -rdynamic -fdeclspec -fPIC -shared -fno-exceptions \
	           -feliminate-unused-debug-types \
	           -fvisibility=hidden \
	           -flto=thin \
	           -O0 -ggdb3 \
	           -march=native \
	           -DAPP_NAME='$APP_NAME' \
	           -DLIB_NAME='\"$DLIB_NAME\"' \
	           -DLIB_VERSION=$DLIB_VERSION \
	           $CXX_FLAGS"
	fi

	#          -fvisibility=hidden \ makes all functions internal to the compilation unit, and exported only if specified
	#          -fsave-optimization-record \ clang outputs text file for what it did
	#          -O3 \
	#          -O0 -ggdb3 \
	# -fwhole-program # ensures this CU represents the whole program, enabling more aggressive opts. Don t use if -flto is present
	# -flto=auto # link time optimization ( removes unused functions ) (g++ only)
	# -flto=thin # clang outputs small .so http://blog.llvm.org/2016/06/thinlto-scalable-and-incremental-lto.html
	# -ffunction-sections -Wl,--gc-sections # remove unused functions (this is also achieved with -flto), separating code from data
	# -fpermissive # downgrades some errors to warnings for non-conformant code
	#-fPIC # position independent code, useful for linked libraries

	declare -a DEFINES
	DEFINES[${#DEFINES[@]}]=DEBUG=1
	DEFINES[${#DEFINES[@]}]=LPR_DEBUG=1
	DEFINES[${#DEFINES[@]}]=WE_LOAD_OUR_GL=1

	declare -a INCLUDE_DIRS
	INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]=$THIS_FILE_DIR
	INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]=$THIS_FILE_DIR/$REL_ROOT_PROJ"libs"
	INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]=$THIS_FILE_DIR/$REL_ROOT_PROJ"libs/lepre"
	INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]=$THIS_FILE_DIR/$REL_ROOT_PROJ"libs/stb_truetype"

	declare -a LIBS_DIRS
	#LIBS_DIRS[${#LIBS_DIRS[@]}]=$THIS_FILE_DIR/$REL_ROOT_PROJ"libraries/"

	declare -a LIBS
	LIBS[${#LIBS[@]}]=pthread
	LIBS[${#LIBS[@]}]=dl
	#LIBS[${#LIBS[@]}]=rt

	function \
	defines()
	{
		for d in ${DEFINES[@]}
		do
			printf " -D$d "
		done
	}
	function \
	include_dirs()
	{
		for p in ${INCLUDE_DIRS[@]}
		do
			printf " -I$p "
		done
	}
	function \
	libs_dirs()
	{
		for p in ${LIBS_DIRS[@]}
		do
			printf " -L$p "
	#printf " -Wl,-rpath=$p "
		done
	}
	function \
	link_libs()
	{
		for l in ${LIBS[@]}
		do
			printf " -l$l "
		done
	}

	ret=3

	echo "calling compiler"

	CMD="$CXX $CXX_FLAGS `defines` `include_dirs` -Wl,-soname,lib$DLIB_NAME.so.$DLIB_VERSION -o $DLIB_FULL_PATH $THIS_FILE_PATH `libs_dirs` `link_libs`"
	echo CMD:
	echo $CMD
	RUNNABLE_CMD=$(echo $CMD | sed "s/'\"/\"/g" | sed "s/\"'/\"/g")
	echo RUNNABLE_CMD:
	echo $RUNNABLE_CMD

	#echo
	STRIPPED_CMD=$(echo $CMD | sed 's/"/\\"/g')
	#echo
	#echo $STRIPPED_CMD

	JSON="[\n{\n\t\"directory\":\"$THIS_FILE_DIR/$REL_ROOT_PROJ\",\n\t\"command\":\"$STRIPPED_CMD\",\n\t\"file\":\"$THIS_FILE_PATH\"\n},\n]";
	echo -e $JSON > compile_commands.json

	TIME="real %E | user %U | sys %S | max mem %M kB | avg mem %K kB" time $RUNNABLE_CMD

	ret=$?;

	if [ $ret -eq 0 ]
	then
		printf 'compilation succeeded\n'
		ln -s $DLIB_FULL_PATH $DLIB_VERSIONED_FULL_PATH
		ls -l $DLIB_FULL_PATH $DLIB_VERSIONED_FULL_PATH
		file $DLIB_FULL_PATH
		echo -n $DLIB_VERSION > "$DLIBS_DIR_FULL_PATH/lib$DLIB_NAME.version.txt"
		objdump -p $DLIB_FULL_PATH | grep SONAME

		#chmod +x ./$EXECUTABLE_NAME
	else
		printf 'compilation failed\n' >&2;
	fi

	echo "exiting $ret"
	exit $ret
	# BASH_SECTION

	# bash -c "exec <($BASH_TEXT)"
	# ret=$?
	# exit $ret
#endif

#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <locale.h>
#include <string.h>

#include "basic_types.h"
#include "macro_tools.h"
#include "intrinsics.h"
#include "our_gl.h"

#include "app_lepre.h"

#include "tgmath.h"

#include "tgogl.h"
#include "tgsys.h"

#define TGUI_DEF tg_internal
#include "tgui.h"

#include "utility.h"
#include "tghot.h"

#include "lpr_fonts.h"

#include "pool_allocator_generic_vm.h"

using namespace tg::sys;

TG_DL_EXPORTED
TG_SYS_EVENT_CALLBACK(tg_app_event_handler);

namespace tg::app
{
	struct
	Tables
	{
	};

	struct
	Glob
	{
		Tables tables;
		Inputs inputs;
		Impl * sys;
		struct
		Window
		{
			s32   window_id;
			bool  window_resized;
			V2u32 new_window_size;
		} window;
		Lpr_Draw_Data lpr_draw_data;
		tgui_Context tgui;
		fonts::Fonts fonts;
		String glyphs;
		String dbg_str;
		Pool_Allocator_Generic_VM fonts_mem_pool;
		bool just_booted;
		bool initialized;
		f32  swap_time_ms;
		tgui_Scrolling_Data container_scrolling_data;
	} glob;

	void
	on_sys_reload()
	{
		// NOTE(theGiallo): what if sys lib is reloaded and loses its data? We should re-set the callback
		glob.sys->set_event_callback( tg_app_event_handler, &glob );
	}

	void
	first_sys_use()
	{
		on_sys_reload();

		// NOTE(theGiallo): this has to be called after our boot but before our init. We get sys only in update
		tg::ogl::load_gl_functions( glob.sys->get_gl_func_loader() );
	}
	void
	init()
	{
		glob.initialized = true;
	}
	void
	on_gl_context_init()
	{
		lpr_init_draw_data( &glob.lpr_draw_data, 1 );

		glob.lpr_draw_data.viewport_origin = make_Lpr_V2u32( glob.window.new_window_size / 2 );
		glob.lpr_draw_data.batches[0].stencils_enabled   = true;
		glob.lpr_draw_data.batches[0].manage_stencils_rt = true;

		Lpr_Draw_Data * draw_data = &glob.lpr_draw_data;

		lpr_create_stencils_fbs_for_batches( draw_data );
		//lpr_set_draw_target_default_full( draw_data );

		struct _
		{
			static void * pool_allocate( void * data, u64 size )
			{
				auto * pool = (Pool_Allocator_Generic_VM*)data;
				tg_assert( size == pool->element_size );
				return pool->allocate();
			}
		};


		glob.glyphs.clear();
		for_mM( c, 1, 128 )
		{
			glob.glyphs.add( "%c", char(c) );
		}
		glob.glyphs.add( (const char *)u8"▼▲▶◀" );

		if ( glob.fonts.allocator.allocate_function )
		{
			glob.fonts.reset();
		} else
		{
			glob.fonts_mem_pool.init( sizeof ( fonts::Font_LL ), 128 );
			Allocator fonts_allocator { .allocate_function = _::pool_allocate, .data = (void*)&glob.fonts_mem_pool };

			glob.fonts.init( fonts_allocator );
		}

		u32 pixel_height = 32;
		u32 oversample_v = 2, oversample_h = 2;
		const char * font_path = "../resources/fonts/Hack-Regular.ttf";
		const fonts::Font_LL * font = tg::fonts::pack_single_font( glob.sys, font_path, &glob.fonts, pixel_height, (const u8 *)glob.glyphs.str, oversample_v, oversample_h );
		glob.fonts.game_font = font;

		glob.tgui.lepre.draw_data    = &glob.lpr_draw_data;
		glob.tgui.lepre.multi_font   = &glob.fonts.multi_font;
		glob.tgui.lepre.font_id      = font->font_id;
		glob.tgui.lepre.font_size_id = font->size_id;
		glob.tgui.lepre.batch_id     = 0;
		glob.tgui.lepre.bg_z         = 1;
		tgui_Context_Init( & glob.tgui, glob.sys );
	}
}; // namespace tg::app

TG_DL_EXPORTED
TG_SYS_EVENT_CALLBACK(tg_app_event_handler)
{
	using namespace tg::app;
	(void)sys_impl;
	(void)callback_data;
	glob.inputs.handle_event( event );

	if ( event->type == Event_Type::WINDOW )
	{
		switch ( event->window.type )
		{
			case Event_Window_Type::CLOSE:
				log_dbg( "window quit id: %d", event->window.window_id );
				sys_impl->close_window( event->window.window_id );
				((Glob*)callback_data)->window.window_id = -1;
				break;
			case Event_Window_Type::RESIZED:
				log_dbg( "window resized %ux%u -> %ux%u",
				         ((Glob*)callback_data)->window.new_window_size.x,
				         ((Glob*)callback_data)->window.new_window_size.y,
				         event->window.w, event->window.h
				       );
				((Glob*)callback_data)->window.window_resized = true;
				((Glob*)callback_data)->window.new_window_size =
				   V2u32{ event->window.w, event->window.h };
				break;
			default:
				break;
		}
	}
}

TG_DL_EXPORTED
bool
tg_app_update( tg::sys::Impl * sys )
{
	using namespace tg::app;
	using namespace tg::sys;
	Inputs & inputs = glob.inputs;

	bool ret = true;

	inputs.mouse_motions_during_frame_count = 0;
	if ( glob.just_booted )
	{
		log_dbg( "app version " TOSTRING(LIB_VERSION) );
		glob.sys = sys;
		first_sys_use();
		if ( ! glob.initialized )
		{
			init();
		}
		glob.just_booted = false;
	} else
	if ( ! glob.sys || glob.sys->set_event_callback != sys->set_event_callback )
	{
		glob.sys = sys;
		on_sys_reload();
	}
	glob.sys = sys;
	glob.tgui.sys_api = sys;

	inputs.frame_begin();
	u32 events_count = sys->poll_all_events_to_callback();
	(void)events_count;
	inputs.update_keyboard_state( sys );
	inputs.update_mouse_frame();

	#if 0
	for_0M( i, 512 )
	{
		if ( inputs.keyboard_is_down[i] )
		{
			log_dbg( "%3ld | key %d", sys->get_cached_time().frame_updates_counter, i );
		}
	}
	#endif

	if ( glob.window.window_id < 0 )
	{
		glob.window.window_id = sys->create_window( "app", {.major=4,.minor=6,.es=false}, true, true, Screen_Mode::WINDOWED, false, 800, 600 );
		if ( glob.window.window_id < 0 )
		{
			const char * en = sys->get_error_name( glob.window.window_id );
			const char * ed = sys->get_error_description( glob.window.window_id );
			log_err( "failed to create window: %s %s", en, ed );
		} else
		{
			log_dbg( "created window %d",glob.window.window_id );

			glob.lpr_draw_data = {};

			Lpr_V2u32 wsize;
			sys->get_window_size( glob.window.window_id, &wsize.w, &wsize.h );
			glob.lpr_draw_data.window_size = wsize;

			on_gl_context_init();
		}
	}

	//if ( inputs.keyboard_state )
	#if 1
	{
		if ( inputs.keyboard_is_down[(u8)PK::S   ] ) log_dbg( "S" );
		if ( inputs.keyboard_is_down[(u8)PK::F4  ] ) log_dbg( "F4" );
		if ( inputs.keyboard_is_down[(u8)PK::LALT] ) log_dbg( "LALT" );
		if ( inputs.keyboard_is_down[(u8)PK::RALT] ) log_dbg( "RALT" );

		if (   inputs.keyboard_is_down[(u8)PK::Q    ]
		  && ( inputs.keyboard_is_down[(u8)PK::LCTRL]
		    || inputs.keyboard_is_down[(u8)PK::RCTRL] ) )
		{
			log_dbg( "ctrl + Q" );
			ret = false;
			return ret;
		}
		if (   inputs.keyboard_is_down[(u8)PK::F4  ]
		  && ( inputs.keyboard_is_down[(u8)PK::LALT]
		    || inputs.keyboard_is_down[(u8)PK::RALT] ) )
		{
			log_dbg( "alt+F4 pressed" );
			if ( glob.window.window_id >= 0 )
			{
				sys->close_window( glob.window.window_id );
			}
			//ret = false;
			//return ret;
		}
	}
	#endif

	if ( glob.window.window_id >= 0 )
	{
		Lpr_Draw_Data * draw_data = &glob.lpr_draw_data;

		if ( glob.window.window_resized )
		{
			log_dbg( "resizing %dx%d -> %dx%d", glob.lpr_draw_data.window_size.x, glob.lpr_draw_data.window_size.y,
			                                    glob.window.new_window_size.x, glob.window.new_window_size.y );
			glob.window.window_resized = false;
			lpr_resize( draw_data, make_Lpr_V2u32( glob.window.new_window_size ) );

			glob.lpr_draw_data.viewport_origin = make_Lpr_V2u32( glob.window.new_window_size / 2 );
		}

		auto t = sys->get_cached_time();

		u32 ww = 0, wh = 0;
		sys->get_window_size( glob.window.window_id, &ww, &wh );
		draw_data->window_size.x = ww;
		draw_data->window_size.y = wh;
		V2 ws = V2( make_V2u32( draw_data->window_size ) );

		glob.dbg_str.clear();
		glob.dbg_str.add( APP_NAME " - %8.3f ms %dx%d [%d]", t.frames_delta_sys_time * 1000.0f, ww, wh,  glob.window.window_id );
		u64 start = ns_time();
		sys->set_window_name( glob.window.window_id, glob.dbg_str.str );
		u64 end = ns_time();
		u64 delta_ns = end - start;
		f32 set_window_name_ms = f64( delta_ns ) * 1e-4;
		//log_dbg( "set_window_name took %lu ns ( %f ms )", delta_ns, f64( delta_ns ) * 1e-4 );

		sys->make_window_current( glob.window.window_id );


		tgui_Context * tgui = &glob.tgui;
		const char * str = "Hello world!";
		glob.tgui = {};
		tgui->lepre.draw_data = draw_data;
		tgui->lepre.batch_id = 0;
		tgui->lepre.multi_font = &glob.fonts.multi_font;
		tgui->lepre.font_id = 0;
		tgui->lepre.font_size_id = 0,
		tgui->lepre.bg_z = 1;
		tgui_Context_Init( tgui, glob.sys );
		tgui_default_style( tgui );
		tgui_text_bg_color( tgui, lpr_C_colors_u8.gray_25 );

		lpr_reset_all_batches( draw_data );
		lpr_set_batch_camera( draw_data, tgui->lepre.batch_id,
		                      Lpr_V2{0.0f,0.0f}, 0.0f,
		                      Lpr_V2{0.0f,0.0f},
		                      lpr_V2_from_V2u32( draw_data->window_size ) );

		lpr_stencil_clear_all( tgui->lepre.draw_data, tgui->lepre.batch_id, false );
		//tgui->lepre.stencil_id = lpr_new_stencil( tgui->lepre.draw_data, tgui->lepre.batch_id );
		tgui->inputs           = &glob.inputs;
		tgui->window_id        = glob.window.window_id;

		Lpr_AA_Rect aa_rect = {};
		f32 container_h = 128.0f;//tgui_container_get_current( &tgui_context )->contextual_data.button.min_size.h;
		V2  text_padding {10, 10};
		f32 container_margin = 10;
		f32 text_h = glob.fonts.game_font->px_height + text_padding.y * 2;
		//V2 size = {ws.w, bar_h};
		aa_rect = lpr_AA_Rect_from_minmax( {container_margin, ws.h - container_h - container_margin}, {ws.w - container_margin, ws.h - container_margin} );

		//tgui_container_begin( tgui, aa_rect, 24.0f, false );
		tgui_container_background( tgui, lpr_C_colors_u8.purple );
		f32 scrollbar_size = 24;
#if 0
		tgui_container_begin( tgui, &glob.container_scrolling_data, V2{container_margin, ws.h - container_margin},
		                      tgui_Fixed_Point_Type::TGUI_TOP_LEFT,
		                      V2{ws.w - 2 * container_margin, max( 100.0f, glob.container_scrolling_data.old_extent.max.y - glob.container_scrolling_data.old_extent.min.y ) },
		                      ws - 2 * container_margin, scrollbar_size, true );
#else
		tgui_container_begin( tgui, aa_rect, scrollbar_size, true );
#endif
		tgui_container_dynamic_scrolling_directions(
		   tgui, TGUI_SCROLLING_H | TGUI_SCROLLING_V, &glob.container_scrolling_data );
		tgui_container_set_scrolling( tgui, glob.container_scrolling_data.scrolling );
		tgui_container_background( tgui, lpr_C_colors_u8.orange );

		tgui_line_start( tgui, V2 {00,container_h - text_h}, f32 {800}, f32 {64} );

		tgui_text_v_align( tgui, Lpr_Txt_VAlign::LPR_TXT_VALIGN_BOTTOM );
		tgui_text_padding( tgui, text_padding );
		glob.dbg_str.clear();
		glob.dbg_str.add( "sys %f perf %f\nswap time: %f ms\nset win. name: %f ms ",
		                  t.frames_delta_sys_time, t.frames_delta_perf_time, glob.swap_time_ms, set_window_name_ms );

		if ( inputs.mouse_left_down() )
		{
			glob.dbg_str.add( "\nmouse left down" );
		}
		tgui_text( tgui, glob.dbg_str.str, glob.dbg_str.occupancy );

		for_0M ( i, 10 )
		{
			tgui_line_carry( tgui );
			tgui_text( tgui, str, strlen( str ) );
		}

		tgui_line_stop( tgui );

		tgui_container_end( tgui, &glob.container_scrolling_data );


		// NOTE(theGiallo): is this to cleanup the gl state?
		glBindFramebuffer( GL_FRAMEBUFFER, 0 );

		lpr_set_draw_target_default_full( draw_data );

		lpr_clear_framebuffer_zero( draw_data, &lpr_C_colors_f32.orange );

		lpr_set_draw_target_default_full( draw_data );

		lpr_prep_batch_drawing( draw_data );

		#if 1
		lpr_sort_batch( draw_data, tgui->lepre.batch_id );
		glEnable( GL_DITHER );
		glEnable( GL_MULTISAMPLE );
		lpr_draw_all_batches( draw_data );
		#else

		lpr_draw_all_batches_sorted( draw_data );

		#endif

		lpr_finalize_draw( draw_data );
	}

	sys->update_time( 1 );
	if ( glob.window.window_id >= 0 )
	{
		u64 start = ns_time();
		sys->swap_window( glob.window.window_id );
		u64 end = ns_time();
		u64 dt_ns = end - start;
		glob.swap_time_ms = f64(dt_ns) * 1e-4;
	}

	return ret;
}

TG_DL_EXPORTED
void
hot_boot( tg::hot::Hot_Api * hot_api, tg::hot::Hot_Api_Lib_Data data )
{
	using namespace tg::app;
	(void)hot_api;
	(void)data;

	void * new_data = (void*)((u8*)&tg::app::glob /*+ sizeof ( tg::app::Tables )*/ );
	s64 new_data_size = sizeof tg::app::glob /*- sizeof ( tg::app::Tables )*/;

	bool there_is_old_data = data.data;
	if ( ! there_is_old_data )
	{
		glob.inputs.init();
		glob.window.window_id = -1;
	} else
	if ( data.size == new_data_size )
	{
		memcpy( new_data, data.data, (size_t)data.size );
	}

	hot_api->set_lib_data( new_data, new_data_size );

	tg::app::glob.just_booted = true;

	//if ( tg::app::glob.sys )
	//{
	//    first_sys_use();
	//}

	//tg::app::init();
}

TG_DL_EXPORTED
void
hot_outdated()
{
}


void
__attribute ((constructor))
init_function()
{
	//log_dbg( "constructor lib %s version %s", TOSTRING(LIB_NAME), TOSTRING(LIB_VERSION) );
}

void
__attribute ((destructor))
fini_function()
{
	//log_dbg( "destructor lib %s version %s", TOSTRING(LIB_NAME), TOSTRING(LIB_VERSION) );
}



#include "utility.cpp"
#include "tgmath.cpp"
#include "tgogl.cpp"

#define LPR_IMPLEMENTATION 1
#include "lepre.h"

#define TGUI_IMPLEMENTATION 1
#include "tgui.h"

#include "pool_allocator_generic_vm.cpp"
