#ifndef _TG_OGL_H_
#define _TG_OGL_H_ 1
#include "basic_types.h"
#include "macro_tools.h"
#include "our_gl.h"
#include "tgmath.h"
#include "utility.h"
#include "tgsys.h"

namespace tg::ogl
{
	using namespace tg::math;

struct
File
{
	const char * file_path;
	Time modified_time;
};

struct
Shader
{
	File geom, vert, frag;
	GLuint program;
	GLint VP_loc;
	GLint V_loc;
	GLint P_loc;
	GLint M_loc;
	GLint instance_M_loc;

	GLint color_loc;
	GLint instance_color_loc;
	GLint vertex_loc;
	GLint vertex_color_loc;

	GLint normal_loc;
	GLint uv_loc;

	static const s32 MAX_GENERIC_LOCS_COUNT = 8;
	GLint attr_float[MAX_GENERIC_LOCS_COUNT];
	GLint attr_vec2 [MAX_GENERIC_LOCS_COUNT];
	GLint attr_vec3 [MAX_GENERIC_LOCS_COUNT];
	GLint attr_vec4 [MAX_GENERIC_LOCS_COUNT];
	GLint unif_float[MAX_GENERIC_LOCS_COUNT];
	GLint unif_vec2 [MAX_GENERIC_LOCS_COUNT];
	GLint unif_vec3 [MAX_GENERIC_LOCS_COUNT];
	GLint unif_vec4 [MAX_GENERIC_LOCS_COUNT];

	void
	dynamically_update( tg::sys::Impl * sys, Mem_Stack * tmp_mem );
	void
	load_locations();
};

struct
Camera3D
{
	Mx4 V,P,VP;
	V3  pos, up, look;
	V2  vanishing_point;
	V2  size;
	f32 near;
	f32 far; // NOTE(theGiallo): this can be +inf
	f32 fov;
	f32 inverse_aspect_ratio; // NOTE(theGiallo): h / w

	void
	initialize_camera( f32 inverse_aspect_ratio, f32 near = 0.1f, f32 fov = 90.0f );

	void
	resize_camera( u32 width, u32 height );

	void
	update_view();

	void
	set_projection(  f32 near, V2 vanishing_point, V2 size, f32 far = f32_inf );

	void
	set_projection( f32 near, V2 vanishing_point, f32 fov, f32 inverse_aspect_ratio, f32 far = f32_inf );

	void
	set( Shader * shader );

	inline
	void
	check_up()
	{
		up =
		   ( look == V3{0.0f,0.0f,1.0f} ) ?
		   V3{0.0f,1.0f,0.0f} : V3{0.0f,0.0f,1.0f};
	}
};


struct
Colored_Vertex
{
	V3 vertex;
	V3 color;
};

struct
Packed_Vertex_PUVN
{
	V3 vertex;
	V3 normal;
	V2 uv;
};

struct
Frame_Buffer_With_Depth
{
	GLuint linear_fbo,
	       linear_color_rt,
	       depth_rt,
	       linear_fb_texture;
	V2u32 size;

	bool
	create( V2u32 px_size );
};

struct
Indexed_Mesh
{
	enum class
	Optionals_Flags
	{
		has_normals   = 0x1 << 0,
		has_uvs       = 0x1 << 1,
		has_u8_colors = 0x1 << 2,
	};

	V3   * vertices;
	u16  * indices;

	// NOTE(theGiallo): optionals
	V3   * normals;
	V2   * uvs;
	V3u8 * u8_colors;

	u32 vertices_count;
	u32 indices_count;

	struct
	OGL
	{
		u32 vertex_buffer;
		u32 index_buffer;
		u32 normals_buffer;
		u32 uvs_buffer;
		u32 u8_colors_buffer;
	} ogl;

	inline
	static
	u64
	compute_bytes_size( u32 vertices_count, u32 indices_count, u32 optionals_flags )
	{
		u64 ret = 0;

		ret += vertices_count * sizeof( ((Indexed_Mesh*)0)->vertices[0] );
		ret += indices_count  * sizeof( ((Indexed_Mesh*)0)->indices[0]);

		if ( optionals_flags & (u32)Optionals_Flags::has_normals )
		{
			ret += vertices_count * sizeof( ((Indexed_Mesh*)0)->normals[0] );
		}

		if ( optionals_flags & (u32)Optionals_Flags::has_uvs )
		{
			ret += vertices_count * sizeof( ((Indexed_Mesh*)0)->uvs[0] );
		}

		if ( optionals_flags & (u32)Optionals_Flags::has_u8_colors )
		{
			ret += vertices_count * sizeof( ((Indexed_Mesh*)0)->u8_colors[0] );
		}

		return ret;
	}

	inline
	u8 *
	init( u8 * mem, u32 vertices_count, u32 indices_count, u32 optionals_flags )
	{
		u8 * ret = mem;

		this->vertices = (V3*)ret;
		ret += vertices_count * sizeof( vertices[0] );

		this->indices = (u16*)ret;
		ret += indices_count  * sizeof( indices[0]);

		if ( optionals_flags & (u32)Optionals_Flags::has_normals )
		{
			this->normals = (V3*)ret;
			ret += vertices_count * sizeof( normals[0] );
		} else
		{
			this->normals = NULL;
		}

		if ( optionals_flags & (u32)Optionals_Flags::has_uvs )
		{
			this->uvs = (V2*)ret;
			ret += vertices_count * sizeof( uvs[0] );
		} else
		{
			this->uvs = NULL;
		}

		if ( optionals_flags & (u32)Optionals_Flags::has_u8_colors )
		{
			this->u8_colors = (V3u8*)ret;
			ret += vertices_count * sizeof( u8_colors[0] );
		} else
		{
			this->u8_colors = NULL;
		}

		this->vertices_count = vertices_count;
		this->indices_count  = indices_count;

		return ret;
	}

	void
	create_ogl_buffers( GLenum usage );
	void
	upload_to_gpu_1st_time( GLenum usage );

	void
	upload_to_gpu_update( GLenum usage );

	void
	render_single( Shader * shader,
	               Mx4 model_matrix, V4 mesh_color );

	void
	render_multiple( u32 model_matrices_buffer, u32 mesh_colors_buffer,
	                 u32 count,
	                 Shader * shader );

	bool
	load_obj( const u8 * file_path, Allocator allocator, Mem_Stack * tmp_mem_stack, tg::sys::Impl * sys );
};

struct
Grid_Planes
{
	V3    * points;
	V3    * normals;
	V3    * ups;
	float * grid_cell_sides;
	float * grid_sides;
	V4    * grid_colors;
	V4    * grid_bg_colors;
	u32 count;
	u32 points_vb;
	u32 normals_vb;
	u32 ups_vb;
	u32 grid_cell_sides_vb;
	u32 grid_sides_vb;
	u32 grid_colors_vb;
	u32 grid_bg_colors_vb;

	void
	init();

	void
	render( Shader * shader );
};

namespace debug
{
	inline
	bool
	check_and_log_framebuffer_status()
	{
		// NOTE(theGiallo): check FBO status
		GLenum status = glCheckFramebufferStatus( GL_FRAMEBUFFER );
		switch ( status )
		{
			case GL_FRAMEBUFFER_COMPLETE:
				log_info( "Framebuffer complete." );
				return true;

			case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
				log_err( "Framebuffer incomplete: Attachment is NOT complete." );
				return false;

			case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
				log_err( "Framebuffer incomplete: No image is attached to FBO." );
				return false;
			#if 0
			case GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS:
				log_err( "Framebuffer incomplete: Attached images have different dimensions." );
				return false;

			case GL_FRAMEBUFFER_INCOMPLETE_FORMATS:
				log_err( "Framebuffer incomplete: Color attached images have different internal formats." );
				return false;
			#endif
			case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
				log_err( "Framebuffer incomplete: Draw buffer." );
				return false;

			case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
				log_err( "Framebuffer incomplete: Read buffer." );
				return false;

			case GL_FRAMEBUFFER_UNSUPPORTED:
				log_err( "Framebuffer incomplete: Unsupported by FBO implementation." );
				return false;

			default:
				log_err( "Framebuffer incomplete: Unknown error." );
				return false;
		}
	}

	char *
	convert_internal_format_to_string( GLenum format, char * buf, size_t buf_size )
	{
		const char * format_name;

		switch ( format )
		{
			case GL_STENCIL_INDEX: // 0x1901
				format_name = "GL_STENCIL_INDEX";
				break;
			case GL_DEPTH_COMPONENT: // 0x1902
				format_name = "GL_DEPTH_COMPONENT";
				break;
			case GL_ALPHA: // 0x1906
				format_name = "GL_ALPHA";
				break;
			case GL_RGB: // 0x1907
				format_name = "GL_RGB";
				break;
			case GL_RGBA: // 0x1908
				format_name = "GL_RGBA";
				break;
			case GL_LUMINANCE: // 0x1909
				format_name = "GL_LUMINANCE";
				break;
			case GL_LUMINANCE_ALPHA: // 0x190A
				format_name = "GL_LUMINANCE_ALPHA";
				break;
			case GL_R3_G3_B2: // 0x2A10
				format_name = "GL_R3_G3_B2";
				break;
			case GL_ALPHA4: // 0x803B
				format_name = "GL_ALPHA4";
				break;
			case GL_ALPHA8: // 0x803C
				format_name = "GL_ALPHA8";
				break;
			case GL_ALPHA12: // 0x803D
				format_name = "GL_ALPHA12";
				break;
			case GL_ALPHA16: // 0x803E
				format_name = "GL_ALPHA16";
				break;
			case GL_LUMINANCE4: // 0x803F
				format_name = "GL_LUMINANCE4";
				break;
			case GL_LUMINANCE8: // 0x8040
				format_name = "GL_LUMINANCE8";
				break;
			case GL_LUMINANCE12: // 0x8041
				format_name = "GL_LUMINANCE12";
				break;
			case GL_LUMINANCE16: // 0x8042
				format_name = "GL_LUMINANCE16";
				break;
			case GL_LUMINANCE4_ALPHA4: // 0x8043
				format_name = "GL_LUMINANCE4_ALPHA4";
				break;
			case GL_LUMINANCE6_ALPHA2: // 0x8044
				format_name = "GL_LUMINANCE6_ALPHA2";
				break;
			case GL_LUMINANCE8_ALPHA8: // 0x8045
				format_name = "GL_LUMINANCE8_ALPHA8";
				break;
			case GL_LUMINANCE12_ALPHA4: // 0x8046
				format_name = "GL_LUMINANCE12_ALPHA4";
				break;
			case GL_LUMINANCE12_ALPHA12: // 0x8047
				format_name = "GL_LUMINANCE12_ALPHA12";
				break;
			case GL_LUMINANCE16_ALPHA16: // 0x8048
				format_name = "GL_LUMINANCE16_ALPHA16";
				break;
			case GL_INTENSITY: // 0x8049
				format_name = "GL_INTENSITY";
				break;
			case GL_INTENSITY4: // 0x804A
				format_name = "GL_INTENSITY4";
				break;
			case GL_INTENSITY8: // 0x804B
				format_name = "GL_INTENSITY8";
				break;
			case GL_INTENSITY12: // 0x804C
				format_name = "GL_INTENSITY12";
				break;
			case GL_INTENSITY16: // 0x804D
				format_name = "GL_INTENSITY16";
				break;
			case GL_RGB4: // 0x804F
				format_name = "GL_RGB4";
				break;
			case GL_RGB5: // 0x8050
				format_name = "GL_RGB5";
				break;
			case GL_RGB8: // 0x8051
				format_name = "GL_RGB8";
				break;
			case GL_RGB10: // 0x8052
				format_name = "GL_RGB10";
				break;
			case GL_RGB12: // 0x8053
				format_name = "GL_RGB12";
				break;
			case GL_RGB16: // 0x8054
				format_name = "GL_RGB16";
				break;
			case GL_RGBA2: // 0x8055
				format_name = "GL_RGBA2";
				break;
			case GL_RGBA4: // 0x8056
				format_name = "GL_RGBA4";
				break;
			case GL_RGB5_A1: // 0x8057
				format_name = "GL_RGB5_A1";
				break;
			case GL_RGBA8: // 0x8058
				format_name = "GL_RGBA8";
				break;
			case GL_RGB10_A2: // 0x8059
				format_name = "GL_RGB10_A2";
				break;
			case GL_RGBA12: // 0x805A
				format_name = "GL_RGBA12";
				break;
			case GL_RGBA16: // 0x805B
				format_name = "GL_RGBA16";
				break;
			case GL_DEPTH_COMPONENT16: // 0x81A5
				format_name = "GL_DEPTH_COMPONENT16";
				break;
			case GL_DEPTH_COMPONENT24: // 0x81A6
				format_name = "GL_DEPTH_COMPONENT24";
				break;
			case GL_DEPTH_COMPONENT32: // 0x81A7
				format_name = "GL_DEPTH_COMPONENT32";
				break;
			case GL_DEPTH_STENCIL: // 0x84F9
				format_name = "GL_DEPTH_STENCIL";
				break;
			case GL_RGBA32F: // 0x8814
				format_name = "GL_RGBA32F";
				break;
			case GL_RGB32F: // 0x8815
				format_name = "GL_RGB32F";
				break;
			case GL_RGBA16F: // 0x881A
				format_name = "GL_RGBA16F";
				break;
			case GL_RGB16F: // 0x881B
				format_name = "GL_RGB16F";
				break;
			case GL_RGBA32UI: // 0x8D70
				format_name = "GL_RGBAUI32";
				break;
			case GL_DEPTH24_STENCIL8: // 0x88F0
				format_name = "GL_DEPTH24_STENCIL8";
				break;
			case GL_SRGB: // 0x8C40
				format_name = "GL_SRGB";
				break;
			case GL_SRGB8: // 0x8C41
				format_name = "GL_SRGB8";
				break;
			case GL_SRGB_ALPHA: // 0x8C42
				format_name = "GL_SRGB_ALPHA";
				break;
			case GL_SRGB8_ALPHA8: // 0x8C43
				format_name = "GL_SRGB8_ALPHA8";
				break;
			case GL_COMPRESSED_SRGB: // 0x8C48
				format_name = "GL_COMPRESSED_SRGB";
				break;
			case GL_COMPRESSED_SRGB_ALPHA: // 0x8C49
				format_name = "GL_COMPRESSED_SRGB_ALPHA";
				break;
			case GL_COMPRESSED_SLUMINANCE: // 0x8C4A
				format_name = "GL_COMPRESSED_SLUMINANCE";
				break;
			case GL_COMPRESSED_SLUMINANCE_ALPHA: // 0x8C4B
				format_name = "GL_COMPRESSED_SLUMINANCE_ALPHA";
				break;

			default:
				snprintf( buf, buf_size, "Unknown Format ( 0x%x )", format );
				return buf;
		}

		snprintf( buf, buf_size, "%s", format_name );
		return buf;
	}


	char *
	get_texture_parameters( GLuint id, char * buf, size_t buf_size )
	{
		if ( glIsTexture( id ) == GL_FALSE )
		{
			snprintf( buf, buf_size, "Not texture object" );
			return buf;
		}

		int width, height, format;
		char format_name[1024];
		glBindTexture( GL_TEXTURE_2D, id );
		glGetTexLevelParameteriv( GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &width);
		glGetTexLevelParameteriv( GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &height);
		glGetTexLevelParameteriv( GL_TEXTURE_2D, 0, GL_TEXTURE_INTERNAL_FORMAT,
		                          &format );
		glBindTexture( GL_TEXTURE_2D, 0 );

		convert_internal_format_to_string( format, format_name,
		                                         sizeof(format_name) );

		snprintf( buf, buf_size, "%d x %d, %s", width, height, format_name );

		return buf;
	}

	char *
	get_renderbuffer_parameters( GLuint id, char * buf, size_t buf_size )
	{
		if ( glIsRenderbuffer(id) == GL_FALSE )
		{
			snprintf( buf, buf_size, "Not Renderbuffer object" );
			return buf;
		}

		int width, height, format;
		char format_name[1024];
		glBindRenderbuffer( GL_RENDERBUFFER, id );
		glGetRenderbufferParameteriv( GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width );
		glGetRenderbufferParameteriv( GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height );
		glGetRenderbufferParameteriv( GL_RENDERBUFFER, GL_RENDERBUFFER_INTERNAL_FORMAT,
		                              &format );
		glBindRenderbuffer( GL_RENDERBUFFER, 0 );

		convert_internal_format_to_string( format, format_name, sizeof(format_name) );

		snprintf( buf, buf_size, "%d x %d, %s", width, height, format_name );

		return buf;
	}
	void
	log_framebuffer_info()
	{
		log_info( "\n===== FBO STATUS =====\n" );

		// NOTE(theGiallo): print max # of colorbuffers supported by FBO
		int color_buffer_count = 0;
		glGetIntegerv( GL_MAX_COLOR_ATTACHMENTS, &color_buffer_count );
		log_info( "Max Number of Color Buffer Attachment Points: %d",
		          color_buffer_count );

		int object_type;
		int object_id;
		char string_buf[2048];

		// NOTE(theGiallo): print info of the colorbuffer attachable image
		for_0M( i, color_buffer_count )
		{
			glGetFramebufferAttachmentParameteriv( GL_FRAMEBUFFER,
			                                       GL_COLOR_ATTACHMENT0 + i,
			                                       GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE,
			                                       & object_type );

			if ( object_type != GL_NONE )
			{
				glGetFramebufferAttachmentParameteriv( GL_FRAMEBUFFER,
				                                       GL_COLOR_ATTACHMENT0 + i,
				                                       GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME,
				                                       & object_id );

				log_info( "Color Attachment %d: ", i );
				if ( object_type == GL_TEXTURE )
				{
					log_info( "\tGL_TEXTURE, %s",
					              get_texture_parameters(
					                 object_id,
					                 string_buf,
					                 sizeof( string_buf ) ) );
				} else
				if ( object_type == GL_RENDERBUFFER )
				{
					log_info( "\tGL_RENDERBUFFER, %s",
					                get_renderbuffer_parameters(
					                   object_id,
					                   string_buf,
					                   sizeof( string_buf ) ) );
				}
			}
		}

		// NOTE(theGiallo): print info of the depthbuffer attachable image
		glGetFramebufferAttachmentParameteriv( GL_FRAMEBUFFER,
		                                       GL_DEPTH_ATTACHMENT,
		                                       GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE,
		                                       &object_type );
		if ( object_type != GL_NONE )
		{
			glGetFramebufferAttachmentParameteriv( GL_FRAMEBUFFER,
			                                       GL_DEPTH_ATTACHMENT,
			                                       GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME,
			                                       &object_id );

			log_info( "Depth Attachment: " );
			switch(object_type)
			{
				case GL_TEXTURE:
					log_info( "\tGL_TEXTURE, %s",
					                get_texture_parameters(
					                   object_id,
					                   string_buf,
					                   sizeof( string_buf ) ) );
					break;
				case GL_RENDERBUFFER:
					log_info( "\tGL_RENDERBUFFER, %s",
					                get_renderbuffer_parameters(
					                   object_id,
					                   string_buf,
					                   sizeof( string_buf ) ) );
					break;
			}
		}

		// NOTE(theGiallo): print info of the stencilbuffer attachable image
		glGetFramebufferAttachmentParameteriv( GL_FRAMEBUFFER,
		                                       GL_STENCIL_ATTACHMENT,
		                                       GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE,
		                                       &object_type );
		if ( object_type != GL_NONE )
		{
			glGetFramebufferAttachmentParameteriv( GL_FRAMEBUFFER,
			                                       GL_STENCIL_ATTACHMENT,
			                                       GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME,
			                                       &object_id );

			log_info( "Stencil Attachment: " );
			switch( object_type )
			{
				case GL_TEXTURE:
					log_info( "\tGL_TEXTURE, %s",
					                get_texture_parameters(
					                   object_id,
					                   string_buf,
					                   sizeof( string_buf ) ) );
					break;
				case GL_RENDERBUFFER:
					log_info( "tGL_RENDERBUFFER, %s",
					                get_renderbuffer_parameters(
					                   object_id,
					                   string_buf,
					                   sizeof( string_buf ) ) );
					break;
			}
		}

		log_info( "\n===== /FBO STATUS =====\n" );
	}
}; // namespace debug


inline
void
upload_matrices_buffer_static( Mx4 * matrices, u32 count, u32 buffer )
{
	glBindBuffer( GL_ARRAY_BUFFER, buffer );
	glBufferData( GL_ARRAY_BUFFER, sizeof(Mx4) * count, matrices, GL_STATIC_DRAW );
}

inline
void
upload_mesh_color_buffer_static( u8 * matrices, u32 count, u32 buffer )
{
	glBindBuffer( GL_ARRAY_BUFFER, buffer );
	glBufferData( GL_ARRAY_BUFFER, sizeof(u8) * 3 * count, matrices, GL_STATIC_DRAW );
}

inline
void
update_matrices_buffer_stream( Mx4 * matrices, u32 count, u32 buffer )
{
	glBindBuffer( GL_ARRAY_BUFFER, buffer );
	glBufferData( GL_ARRAY_BUFFER, sizeof(Mx4) * count, matrices, GL_STREAM_DRAW );
}

inline
void
update_mesh_color_buffer_stream( u8 * matrices, u32 count, u32 buffer )
{
	glBindBuffer( GL_ARRAY_BUFFER, buffer );
	glBufferData( GL_ARRAY_BUFFER, sizeof(u8) * 3 * count, matrices, GL_STREAM_DRAW );
}


// NOTE(theGiallo): h in [0,1)
V4
rgba_from_hsva( V4 hsva );
V3
rgb_from_hsv( V3 hsv );
V3
rgb_from_v2( V2 v );

inline
void
create_and_fill_ogl_buffer( GLuint * buffer, GLenum target, GLenum usage, void * data, GLsizeiptr byte_size )
{
	glGenBuffers( 1, buffer );
	glBindBuffer( target, *buffer );
	glBufferData( target, byte_size,
	              data,  usage );
	glBindBuffer( target, 0 );
}

bool
create_program_from_text( GLuint *     gl_program,
                          const char * geometry_shader_text,
                          GLint        geometry_shader_text_length,
                          const char * vertex_shader_text,
                          GLint        vertex_shader_text_length,
                          const char * fragment_shader_text,
                          GLint        fragment_shader_text_length );

bool
create_program( const char * geom, const char * vert, const char * frag,
                tg::sys::Impl * sys, Mem_Stack * tmp_mem,
                GLuint * out_program );
void
dynamically_update_shader( tg::sys::Impl * sys, Mem_Stack * tmp_mem, Shader * shader );

void
render_colored_vertices( Shader * shader,
                         Colored_Vertex * vertices, u32 vertices_in_buffer,
                         u32 vertices_count,
                         V4 * default_color,
                         GLuint vbo, GLenum draw_mode, GLenum buffer_usage );



}; // namespace tg::ogl
#endif /* ifndef _TG_OGL_H_ */
