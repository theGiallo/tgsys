#ifndef _TG_SYS_H_
#define _TG_SYS_H_ 1

#include "basic_types.h"
#include "tghot.h"

#define TG_SYS_RET_IS_ERROR(r) ((r)<0)

#if DEBUG
#define DEBUG_ONLY(e) e
#else
#define DEBUG_ONLY(e)
#endif

namespace tg::sys
{
	using namespace tg::math;

	struct Impl;

struct
GL_Version
{
	s32  major, minor;
	bool es;
};

struct
GL_Func_Loader
{
	void * _data;
	void * ( * _loader ) ( void * data, const char * f_name );

	void *
	load( const char * f_name )
	{
		return _loader( _data, f_name );
	}
};

enum class
Screen_Mode : u8
{
	WINDOWED           = 0x0,
	FULLSCREEN,
	FULLSCREEN_DESKTOP,
	// ---
	__COUNT
};

struct
Thread;

enum class
List_Directory_Option
{
	NONE       = 0x0,
	FULL_PATHS = 0x1, // NOTE(theGiallo): if not it returns only the file names
};
// NOTE(theGiallo): TG_SYS_LIST_DIRECTORY writes in the mem buffer an array of
// u8* of the returned length plus one, that is pointing to the first free byte.
// NOTE(theGiallo): TG_SYS_LIST_DIRECTORY_FAST writes in the mem buffer a u32
// containing the files count and after it a Sys_Listed_Directory that is the
// first node of a list.
struct
Listed_Directory
{
	u8               * name;
	Listed_Directory * next;
};

#define TG_SYS_UNKNOWN_THREAD_ID 255
#define TG_SYS_MAIN_THREAD_ID    0

struct
Cached_Time
{
	Time run_perf_time;
	Time run_sys_time;
	Time base_perf_time;
	Time base_sys_time;
	Time delta_perf_time;
	Time delta_sys_time;

	Time frames_delta_perf_time;
	Time frames_delta_sys_time;
	Time frames_perf_time;
	Time frames_sys_time;

	u64  time_updates_counter;
	u64  frame_updates_counter;
	u32  delta_frames;
};

enum class
Event_Type : u32 // NOTE(theGiallo): for padding matters
{
	WINDOW,
	KEYBOARD,
	MOUSE,
	PAD,
	GAME_SYSTEM,
	// ---
	__COUNT,
};

struct
Event_Common
{
	Event_Type type;
	Time       time_fired;
};

#define TG_SYS_WINDOW_EVENT_EXPAND_AS_ENUM(a) a,
#define TG_SYS_WINDOW_EVENT_EXPAND_AS_NAME_TABLE(a) TOSTRING(a),
#define TG_SYS_WINDOW_EVENT_TABLE(ENTRY) \
	ENTRY( CLOSE        ) \
	ENTRY( RESIZED      ) \
	ENTRY( SHOWN        ) \
	ENTRY( HIDDEN       ) \
	ENTRY( MOVED        ) \
	ENTRY( EXPOSED      ) \
	ENTRY( SIZE_CHANGED ) \
	ENTRY( MINIMIZED    ) \
	ENTRY( MAXIMIZED    ) \
	ENTRY( RESTORED     ) \
	ENTRY( ENTER        ) \
	ENTRY( LEAVE        ) \
	ENTRY( FOCUS_GAINED ) \
	ENTRY( FOCUS_LOST   ) \



#if 0
#define WINDOW_EVENT_TABLE(ENTRY) \
	ENTRY( EVENT_WINDOW_CLOSE,       SDL_WINDOWEVENT_CLOSE        ) \
	ENTRY( EVENT_WINDOW_RESIZED,      SDL_WINDOWEVENT_RESIZED      ) \
	ENTRY( EVENT_WINDOW_SHOWN,        SDL_WINDOWEVENT_SHOWN        ) \
	ENTRY( EVENT_WINDOW_HIDDEN,       SDL_WINDOWEVENT_HIDDEN       ) \
	ENTRY( EVENT_WINDOW_MOVED,        SDL_WINDOWEVENT_MOVED        ) \
	ENTRY( EVENT_WINDOW_EXPOSED,      SDL_WINDOWEVENT_EXPOSED      ) \
	ENTRY( EVENT_WINDOW_SIZE_CHANGED, SDL_WINDOWEVENT_SIZE_CHANGED ) \
	ENTRY( EVENT_WINDOW_MINIMIZED,    SDL_WINDOWEVENT_MINIMIZED    ) \
	ENTRY( EVENT_WINDOW_MAXIMIZED,    SDL_WINDOWEVENT_MAXIMIZED    ) \
	ENTRY( EVENT_WINDOW_RESTORED,     SDL_WINDOWEVENT_RESTORED     ) \
	ENTRY( EVENT_WINDOW_ENTER,        SDL_WINDOWEVENT_ENTER        ) \
	ENTRY( EVENT_WINDOW_LEAVE,        SDL_WINDOWEVENT_LEAVE        ) \
	ENTRY( EVENT_WINDOW_FOCUS_GAINED, SDL_WINDOWEVENT_FOCUS_GAINED ) \
	ENTRY( EVENT_WINDOW_FOCUS_LOST,   SDL_WINDOWEVENT_FOCUS_LOST   ) \


#endif

enum class
Event_Window_Type : u8
{
	TG_SYS_WINDOW_EVENT_TABLE( TG_SYS_WINDOW_EVENT_EXPAND_AS_ENUM )
	// ---
	__COUNT,
};

struct
Event_Window
{
	Event_Common common;
	s32 window_id;
	Event_Window_Type type;
	u8 _padding1;
	u8 _padding2;
	u8 _padding3;
	union
	{
		struct
		{
			s32 x, y;
		};
		struct
		{
			u32 w,h;
		};
	};
};


#define TG_SYS_KEYBOARD_EVENT_EXPAND_AS_ENUM(a) a,
#define TG_SYS_KEYBOARD_EVENT_EXPAND_AS_NAME_TABLE(a) TOSTRING(a),
#define TG_SYS_KEYBOARD_EVENT_TABLE(ENTRY) \
	ENTRY( KEY_PRESSED ) \
	ENTRY( KEY_RELEASED ) \


enum class
Event_Keyboard_Type : u8
{
	TG_SYS_KEYBOARD_EVENT_TABLE( TG_SYS_KEYBOARD_EVENT_EXPAND_AS_ENUM )
	// ---
	__COUNT,
};

enum class
Key_Mod : u16
{
	NONE     = 0x0,
	LSHIFT   = 0x1 << 0,
	RSHIFT   = 0x1 << 1,
	LCTRL    = 0x1 << 6,
	RCTRL    = 0x1 << 7,
	LALT     = 0x1 << 8,
	RALT     = 0x1 << 9,
	LGUI     = 0x1 << 10,
	RGUI     = 0x1 << 11,
	NUM      = 0x1 << 12,
	CAPS     = 0x1 << 13,
	MODE     = 0x1 << 14,
	RESERVED = 0x1 << 15,
	// ---
	CTRL     = (Key_Mod::LCTRL |Key_Mod::RCTRL ),
	SHIFT    = (Key_Mod::LSHIFT|Key_Mod::RSHIFT),
	ALT      = (Key_Mod::LALT  |Key_Mod::RALT  ),
	GUI      = (Key_Mod::LGUI  |Key_Mod::RGUI  ),
};

// TODO(theGiallo, 2021-04-18): add string tables for all events
typedef
enum class
Phys_Key : s32
{
	UNKNOWN = 0,

	/**
	 *  \name Usage page 0x07
	 *
	 *  These values are from usage page 0x07 (USB keyboard page).
	 */
	/* @{ */

	A = 4,
	B = 5,
	C = 6,
	D = 7,
	E = 8,
	F = 9,
	G = 10,
	H = 11,
	I = 12,
	J = 13,
	K = 14,
	L = 15,
	M = 16,
	N = 17,
	O = 18,
	P = 19,
	Q = 20,
	R = 21,
	S = 22,
	T = 23,
	U = 24,
	V = 25,
	W = 26,
	X = 27,
	Y = 28,
	Z = 29,

	_1 = 30,
	_2 = 31,
	_3 = 32,
	_4 = 33,
	_5 = 34,
	_6 = 35,
	_7 = 36,
	_8 = 37,
	_9 = 38,
	_0 = 39,

	RETURN = 40,
	ESCAPE = 41,
	BACKSPACE = 42,
	TAB = 43,
	SPACE = 44,

	MINUS = 45,
	EQUALS = 46,
	LEFTBRACKET = 47,
	RIGHTBRACKET = 48,
	BACKSLASH = 49, /**< Located at the lower left of the return
	                    *   key on ISO keyboards and at the right end
	                    *   of the QWERTY row on ANSI keyboards.
	                    *   Produces REVERSE SOLIDUS (backslash) and
	                    *   VERTICAL LINE in a US layout, REVERSE
	                    *   SOLIDUS and VERTICAL LINE in a UK Mac
	                    *   layout, NUMBER SIGN and TILDE in a UK
	                    *   Windows layout, DOLLAR SIGN and POUND SIGN
	                    *   in a Swiss German layout, NUMBER SIGN and
	                    *   APOSTROPHE in a German layout, GRAVE
	                    *   ACCENT and POUND SIGN in a French Mac
	                    *   layout, and ASTERISK and MICRO SIGN in a
	                    *   French Windows layout.
	                    */
	NONUSHASH = 50, /**< ISO USB keyboards actually use this code
	                    *   instead of 49 for the same key, but all
	                    *   OSes I've seen treat the two codes
	                    *   identically. So, as an implementor, unless
	                    *   your keyboard generates both of those
	                    *   codes and your OS treats them differently,
	                    *   you should generate BACKSLASH
	                    *   instead of this code. As a user, you
	                    *   should not rely on this code because SDL
	                    *   will never generate it with most (all?)
	                    *   keyboards.
	                    */
	SEMICOLON = 51,
	APOSTROPHE = 52,
	GRAVE = 53, /**< Located in the top left corner (on both ANSI
	                *   and ISO keyboards). Produces GRAVE ACCENT and
	                *   TILDE in a US Windows layout and in US and UK
	                *   Mac layouts on ANSI keyboards, GRAVE ACCENT
	                *   and NOT SIGN in a UK Windows layout, SECTION
	                *   SIGN and PLUS-MINUS SIGN in US and UK Mac
	                *   layouts on ISO keyboards, SECTION SIGN and
	                *   DEGREE SIGN in a Swiss German layout (Mac:
	                *   only on ISO keyboards), CIRCUMFLEX ACCENT and
	                *   DEGREE SIGN in a German layout (Mac: only on
	                *   ISO keyboards), SUPERSCRIPT TWO and TILDE in a
	                *   French Windows layout, COMMERCIAL AT and
	                *   NUMBER SIGN in a French Mac layout on ISO
	                *   keyboards, and LESS-THAN SIGN and GREATER-THAN
	                *   SIGN in a Swiss German, German, or French Mac
	                *   layout on ANSI keyboards.
	                */
	COMMA  = 54,
	PERIOD = 55,
	SLASH  = 56,

	CAPSLOCK = 57,

	F1  = 58,
	F2  = 59,
	F3  = 60,
	F4  = 61,
	F5  = 62,
	F6  = 63,
	F7  = 64,
	F8  = 65,
	F9  = 66,
	F10 = 67,
	F11 = 68,
	F12 = 69,

	PRINTSCREEN = 70,
	SCROLLLOCK  = 71,
	PAUSE       = 72,
	INSERT      = 73, // insert on PC, help on some Mac keyboards (but does send code 73, not 117)
	HOME        = 74,
	PAGEUP      = 75,
	DELETE      = 76,
	END         = 77,
	PAGEDOWN    = 78,
	RIGHT       = 79,
	LEFT        = 80,
	DOWN        = 81,
	UP          = 82,

	NUMLOCKCLEAR = 83, // num lock on PC, clear on Mac keyboards
	KP_DIVIDE    = 84,
	KP_MULTIPLY  = 85,
	KP_MINUS     = 86,
	KP_PLUS      = 87,
	KP_ENTER     = 88,
	KP_1         = 89,
	KP_2         = 90,
	KP_3         = 91,
	KP_4         = 92,
	KP_5         = 93,
	KP_6         = 94,
	KP_7         = 95,
	KP_8         = 96,
	KP_9         = 97,
	KP_0         = 98,
	KP_PERIOD    = 99,

	NONUSBACKSLASH = 100, /**< This is the additional key that ISO
	                          *   keyboards have over ANSI ones,
	                          *   located between left shift and Y.
	                          *   Produces GRAVE ACCENT and TILDE in a
	                          *   US or UK Mac layout, REVERSE SOLIDUS
	                          *   (backslash) and VERTICAL LINE in a
	                          *   US or UK Windows layout, and
	                          *   LESS-THAN SIGN and GREATER-THAN SIGN
	                          *   in a Swiss German, German, or French
	                          *   layout. */
	APPLICATION = 101, /**< windows contextual menu, compose */
	POWER = 102, /**< The USB document says this is a status flag,
	                           *   not a physical key - but some Mac keyboards
	                           *   do have a power key. */
	KP_EQUALS  = 103,
	F13        = 104,
	F14        = 105,
	F15        = 106,
	F16        = 107,
	F17        = 108,
	F18        = 109,
	F19        = 110,
	F20        = 111,
	F21        = 112,
	F22        = 113,
	F23        = 114,
	F24        = 115,
	EXECUTE    = 116,
	HELP       = 117,
	MENU       = 118,
	SELECT     = 119,
	STOP       = 120,
	AGAIN      = 121,   /**< redo */
	UNDO       = 122,
	CUT        = 123,
	COPY       = 124,
	PASTE      = 125,
	FIND       = 126,
	MUTE       = 127,
	VOLUMEUP   = 128,
	VOLUMEDOWN = 129,
	/* not sure whether there's a reason to enable these */
	/*     LOCKINGCAPSLOCK = 130,  */
	/*     LOCKINGNUMLOCK = 131, */
	/*     LOCKINGSCROLLLOCK = 132, */
	KP_COMMA       = 133,
	KP_EQUALSAS400 = 134,

	INTERNATIONAL1 = 135, /**< used on Asian keyboards, see
	                                      footnotes in USB doc */
	INTERNATIONAL2 = 136,
	INTERNATIONAL3 = 137, /**< Yen */
	INTERNATIONAL4 = 138,
	INTERNATIONAL5 = 139,
	INTERNATIONAL6 = 140,
	INTERNATIONAL7 = 141,
	INTERNATIONAL8 = 142,
	INTERNATIONAL9 = 143,
	LANG1 = 144, /**< Hangul/English toggle */
	LANG2 = 145, /**< Hanja conversion */
	LANG3 = 146, /**< Katakana */
	LANG4 = 147, /**< Hiragana */
	LANG5 = 148, /**< Zenkaku/Hankaku */
	LANG6 = 149, /**< reserved */
	LANG7 = 150, /**< reserved */
	LANG8 = 151, /**< reserved */
	LANG9 = 152, /**< reserved */

	ALTERASE   = 153, /**< Erase-Eaze */
	SYSREQ     = 154,
	CANCEL     = 155,
	CLEAR      = 156,
	PRIOR      = 157,
	RETURN2    = 158,
	SEPARATOR  = 159,
	OUT        = 160,
	OPER       = 161,
	CLEARAGAIN = 162,
	CRSEL      = 163,
	EXSEL      = 164,

	KP_00              = 176,
	KP_000             = 177,
	THOUSANDSSEPARATOR = 178,
	DECIMALSEPARATOR   = 179,
	CURRENCYUNIT       = 180,
	CURRENCYSUBUNIT    = 181,
	KP_LEFTPAREN       = 182,
	KP_RIGHTPAREN      = 183,
	KP_LEFTBRACE       = 184,
	KP_RIGHTBRACE      = 185,
	KP_TAB             = 186,
	KP_BACKSPACE       = 187,
	KP_A               = 188,
	KP_B               = 189,
	KP_C               = 190,
	KP_D               = 191,
	KP_E               = 192,
	KP_F               = 193,
	KP_XOR             = 194,
	KP_POWER           = 195,
	KP_PERCENT         = 196,
	KP_LESS            = 197,
	KP_GREATER         = 198,
	KP_AMPERSAND       = 199,
	KP_DBLAMPERSAND    = 200,
	KP_VERTICALBAR     = 201,
	KP_DBLVERTICALBAR  = 202,
	KP_COLON           = 203,
	KP_HASH            = 204,
	KP_SPACE           = 205,
	KP_AT              = 206,
	KP_EXCLAM          = 207,
	KP_MEMSTORE        = 208,
	KP_MEMRECALL       = 209,
	KP_MEMCLEAR        = 210,
	KP_MEMADD          = 211,
	KP_MEMSUBTRACT     = 212,
	KP_MEMMULTIPLY     = 213,
	KP_MEMDIVIDE       = 214,
	KP_PLUSMINUS       = 215,
	KP_CLEAR           = 216,
	KP_CLEARENTRY      = 217,
	KP_BINARY          = 218,
	KP_OCTAL           = 219,
	KP_DECIMAL         = 220,
	KP_HEXADECIMAL     = 221,

	LCTRL  = 224,
	LSHIFT = 225,
	LALT   = 226, /**< alt, option */
	LGUI   = 227, /**< windows, command (apple), meta */
	RCTRL  = 228,
	RSHIFT = 229,
	RALT   = 230, /**< alt gr, option */
	RGUI   = 231, /**< windows, command (apple), meta */

	MODE   = 257, /**< I'm not sure if this is really not covered
	               *   by any of the above, but since there's a
	               *   special KEY_MOD_MODE for it I'm adding it
	               *   here */

	/* @} *//* Usage page 0x07 */

	/**
	 *  \name Usage page 0x0C
	 *
	 *  These values are mapped from usage page 0x0C (USB consumer page).
	 */
	/* @{ */

	AUDIONEXT    = 258,
	AUDIOPREV    = 259,
	AUDIOSTOP    = 260,
	AUDIOPLAY    = 261,
	AUDIOMUTE    = 262,
	MEDIASELECT  = 263,
	WWW          = 264,
	MAIL         = 265,
	CALCULATOR   = 266,
	COMPUTER     = 267,
	AC_SEARCH    = 268,
	AC_HOME      = 269,
	AC_BACK      = 270,
	AC_FORWARD   = 271,
	AC_STOP      = 272,
	AC_REFRESH   = 273,
	AC_BOOKMARKS = 274,

	/* @} *//* Usage page 0x0C */

	/**
	 *  \name Walther keys
	 *
	 *  These are values that Christian Walther added (for mac keyboard?).
	 */
	/* @{ */

	BRIGHTNESSDOWN = 275,
	BRIGHTNESSUP   = 276,
	DISPLAYSWITCH  = 277, /**< display mirroring/dual display switch, video mode switch */
	KBDILLUMTOGGLE = 278,
	KBDILLUMDOWN   = 279,
	KBDILLUMUP     = 280,
	EJECT          = 281,
	SLEEP          = 282,

	APP1 = 283,
	APP2 = 284,

	/* @} *//* Walther keys */

	/* Add any other keys here. */

	COUNT = 512, /**< not a key, just marks the number of scancodes
	                    for array bounds */

	// ---

	MASK = (1<<30),
} PK;
#define PK_TO_VK(pk) ( (s32)(pk) | (s32)PK::MASK )

typedef
enum class
Virt_Key : s32
{
	UNKNOWN = 0,

	RETURN     = '\r',
	ESCAPE     = '\033',
	BACKSPACE  = '\b',
	TAB        = '\t',
	SPACE      = ' ',
	EXCLAIM    = '!',
	QUOTEDBL   = '"',
	HASH       = '#',
	PERCENT    = '%',
	DOLLAR     = '$',
	AMPERSAND  = '&',
	QUOTE      = '\'',
	LEFTPAREN  = '(',
	RIGHTPAREN = ')',
	ASTERISK   = '*',
	PLUS       = '+',
	COMMA      = ',',
	MINUS      = '-',
	PERIOD     = '.',
	SLASH      = '/',
	_0 = '0',
	_1 = '1',
	_2 = '2',
	_3 = '3',
	_4 = '4',
	_5 = '5',
	_6 = '6',
	_7 = '7',
	_8 = '8',
	_9 = '9',
	COLON     = ':',
	SEMICOLON = ';',
	LESS      = '<',
	EQUALS    = '=',
	GREATER   = '>',
	QUESTION  = '?',
	AT        = '@',
	// NOTE(theGiallo): Skip uppercase letters
	LEFTBRACKET  = '[',
	BACKSLASH    = '\\',
	RIGHTBRACKET = ']',
	CARET        = '^',
	UNDERSCORE   = '_',
	BACKQUOTE    = '`',
	a = 'a',
	b = 'b',
	c = 'c',
	d = 'd',
	e = 'e',
	f = 'f',
	g = 'g',
	h = 'h',
	i = 'i',
	j = 'j',
	k = 'k',
	l = 'l',
	m = 'm',
	n = 'n',
	o = 'o',
	p = 'p',
	q = 'q',
	r = 'r',
	s = 's',
	t = 't',
	u = 'u',
	v = 'v',
	w = 'w',
	x = 'x',
	y = 'y',
	z = 'z',

	CAPSLOCK = PK_TO_VK(PK::CAPSLOCK),

	F1  = PK_TO_VK(PK::F1),
	F2  = PK_TO_VK(PK::F2),
	F3  = PK_TO_VK(PK::F3),
	F4  = PK_TO_VK(PK::F4),
	F5  = PK_TO_VK(PK::F5),
	F6  = PK_TO_VK(PK::F6),
	F7  = PK_TO_VK(PK::F7),
	F8  = PK_TO_VK(PK::F8),
	F9  = PK_TO_VK(PK::F9),
	F10 = PK_TO_VK(PK::F10),
	F11 = PK_TO_VK(PK::F11),
	F12 = PK_TO_VK(PK::F12),

	PRINTSCREEN = PK_TO_VK(PK::PRINTSCREEN),
	SCROLLLOCK  = PK_TO_VK(PK::SCROLLLOCK),
	PAUSE       = PK_TO_VK(PK::PAUSE),
	INSERT      = PK_TO_VK(PK::INSERT),
	HOME        = PK_TO_VK(PK::HOME),
	PAGEUP      = PK_TO_VK(PK::PAGEUP),
	DELETE      = '\177',
	END         = PK_TO_VK(PK::END),
	PAGEDOWN    = PK_TO_VK(PK::PAGEDOWN),
	RIGHT       = PK_TO_VK(PK::RIGHT),
	LEFT        = PK_TO_VK(PK::LEFT),
	DOWN        = PK_TO_VK(PK::DOWN),
	UP          = PK_TO_VK(PK::UP),

	NUMLOCKCLEAR = PK_TO_VK(PK::NUMLOCKCLEAR),
	KP_DIVIDE    = PK_TO_VK(PK::KP_DIVIDE),
	KP_MULTIPLY  = PK_TO_VK(PK::KP_MULTIPLY),
	KP_MINUS     = PK_TO_VK(PK::KP_MINUS),
	KP_PLUS      = PK_TO_VK(PK::KP_PLUS),
	KP_ENTER     = PK_TO_VK(PK::KP_ENTER),
	KP_1         = PK_TO_VK(PK::KP_1),
	KP_2         = PK_TO_VK(PK::KP_2),
	KP_3         = PK_TO_VK(PK::KP_3),
	KP_4         = PK_TO_VK(PK::KP_4),
	KP_5         = PK_TO_VK(PK::KP_5),
	KP_6         = PK_TO_VK(PK::KP_6),
	KP_7         = PK_TO_VK(PK::KP_7),
	KP_8         = PK_TO_VK(PK::KP_8),
	KP_9         = PK_TO_VK(PK::KP_9),
	KP_0         = PK_TO_VK(PK::KP_0),
	KP_PERIOD    = PK_TO_VK(PK::KP_PERIOD),

	APPLICATION    = PK_TO_VK(PK::APPLICATION),
	POWER          = PK_TO_VK(PK::POWER),
	KP_EQUALS      = PK_TO_VK(PK::KP_EQUALS),
	F13            = PK_TO_VK(PK::F13),
	F14            = PK_TO_VK(PK::F14),
	F15            = PK_TO_VK(PK::F15),
	F16            = PK_TO_VK(PK::F16),
	F17            = PK_TO_VK(PK::F17),
	F18            = PK_TO_VK(PK::F18),
	F19            = PK_TO_VK(PK::F19),
	F20            = PK_TO_VK(PK::F20),
	F21            = PK_TO_VK(PK::F21),
	F22            = PK_TO_VK(PK::F22),
	F23            = PK_TO_VK(PK::F23),
	F24            = PK_TO_VK(PK::F24),
	EXECUTE        = PK_TO_VK(PK::EXECUTE),
	HELP           = PK_TO_VK(PK::HELP),
	MENU           = PK_TO_VK(PK::MENU),
	SELECT         = PK_TO_VK(PK::SELECT),
	STOP           = PK_TO_VK(PK::STOP),
	AGAIN          = PK_TO_VK(PK::AGAIN),
	UNDO           = PK_TO_VK(PK::UNDO),
	CUT            = PK_TO_VK(PK::CUT),
	COPY           = PK_TO_VK(PK::COPY),
	PASTE          = PK_TO_VK(PK::PASTE),
	FIND           = PK_TO_VK(PK::FIND),
	MUTE           = PK_TO_VK(PK::MUTE),
	VOLUMEUP       = PK_TO_VK(PK::VOLUMEUP),
	VOLUMEDOWN     = PK_TO_VK(PK::VOLUMEDOWN),
	KP_COMMA       = PK_TO_VK(PK::KP_COMMA),
	KP_EQUALSAS400 = PK_TO_VK(PK::KP_EQUALSAS400),

	ALTERASE   = PK_TO_VK(PK::ALTERASE),
	SYSREQ     = PK_TO_VK(PK::SYSREQ),
	CANCEL     = PK_TO_VK(PK::CANCEL),
	CLEAR      = PK_TO_VK(PK::CLEAR),
	PRIOR      = PK_TO_VK(PK::PRIOR),
	RETURN2    = PK_TO_VK(PK::RETURN2),
	SEPARATOR  = PK_TO_VK(PK::SEPARATOR),
	OUT        = PK_TO_VK(PK::OUT),
	OPER       = PK_TO_VK(PK::OPER),
	CLEARAGAIN = PK_TO_VK(PK::CLEARAGAIN),
	CRSEL      = PK_TO_VK(PK::CRSEL),
	EXSEL      = PK_TO_VK(PK::EXSEL),

	KP_00              = PK_TO_VK(PK::KP_00),
	KP_000             = PK_TO_VK(PK::KP_000),
	THOUSANDSSEPARATOR = PK_TO_VK(PK::THOUSANDSSEPARATOR),
	DECIMALSEPARATOR   = PK_TO_VK(PK::DECIMALSEPARATOR),
	CURRENCYUNIT       = PK_TO_VK(PK::CURRENCYUNIT),
	CURRENCYSUBUNIT    = PK_TO_VK(PK::CURRENCYSUBUNIT),
	KP_LEFTPAREN       = PK_TO_VK(PK::KP_LEFTPAREN),
	KP_RIGHTPAREN      = PK_TO_VK(PK::KP_RIGHTPAREN),
	KP_LEFTBRACE       = PK_TO_VK(PK::KP_LEFTBRACE),
	KP_RIGHTBRACE      = PK_TO_VK(PK::KP_RIGHTBRACE),
	KP_TAB             = PK_TO_VK(PK::KP_TAB),
	KP_BACKSPACE       = PK_TO_VK(PK::KP_BACKSPACE),
	KP_A               = PK_TO_VK(PK::KP_A),
	KP_B               = PK_TO_VK(PK::KP_B),
	KP_C               = PK_TO_VK(PK::KP_C),
	KP_D               = PK_TO_VK(PK::KP_D),
	KP_E               = PK_TO_VK(PK::KP_E),
	KP_F               = PK_TO_VK(PK::KP_F),
	KP_XOR             = PK_TO_VK(PK::KP_XOR),
	KP_POWER           = PK_TO_VK(PK::KP_POWER),
	KP_PERCENT         = PK_TO_VK(PK::KP_PERCENT),
	KP_LESS            = PK_TO_VK(PK::KP_LESS),
	KP_GREATER         = PK_TO_VK(PK::KP_GREATER),
	KP_AMPERSAND       = PK_TO_VK(PK::KP_AMPERSAND),
	KP_DBLAMPERSAND    = PK_TO_VK(PK::KP_DBLAMPERSAND),
	KP_VERTICALBAR     = PK_TO_VK(PK::KP_VERTICALBAR),
	KP_DBLVERTICALBAR  = PK_TO_VK(PK::KP_DBLVERTICALBAR),
	KP_COLON           = PK_TO_VK(PK::KP_COLON),
	KP_HASH            = PK_TO_VK(PK::KP_HASH),
	KP_SPACE           = PK_TO_VK(PK::KP_SPACE),
	KP_AT              = PK_TO_VK(PK::KP_AT),
	KP_EXCLAM          = PK_TO_VK(PK::KP_EXCLAM),
	KP_MEMSTORE        = PK_TO_VK(PK::KP_MEMSTORE),
	KP_MEMRECALL       = PK_TO_VK(PK::KP_MEMRECALL),
	KP_MEMCLEAR        = PK_TO_VK(PK::KP_MEMCLEAR),
	KP_MEMADD          = PK_TO_VK(PK::KP_MEMADD),
	KP_MEMSUBTRACT     = PK_TO_VK(PK::KP_MEMSUBTRACT),
	KP_MEMMULTIPLY     = PK_TO_VK(PK::KP_MEMMULTIPLY),
	KP_MEMDIVIDE       = PK_TO_VK(PK::KP_MEMDIVIDE),
	KP_PLUSMINUS       = PK_TO_VK(PK::KP_PLUSMINUS),
	KP_CLEAR           = PK_TO_VK(PK::KP_CLEAR),
	KP_CLEARENTRY      = PK_TO_VK(PK::KP_CLEARENTRY),
	KP_BINARY          = PK_TO_VK(PK::KP_BINARY),
	KP_OCTAL           = PK_TO_VK(PK::KP_OCTAL),
	KP_DECIMAL         = PK_TO_VK(PK::KP_DECIMAL),
	KP_HEXADECIMAL     = PK_TO_VK(PK::KP_HEXADECIMAL),

	LCTRL  = PK_TO_VK(PK::LCTRL),
	LSHIFT = PK_TO_VK(PK::LSHIFT),
	LALT   = PK_TO_VK(PK::LALT),
	LGUI   = PK_TO_VK(PK::LGUI),
	RCTRL  = PK_TO_VK(PK::RCTRL),
	RSHIFT = PK_TO_VK(PK::RSHIFT),
	RALT   = PK_TO_VK(PK::RALT),
	RGUI   = PK_TO_VK(PK::RGUI),

	MODE = PK_TO_VK(PK::MODE),

	AUDIONEXT    = PK_TO_VK(PK::AUDIONEXT),
	AUDIOPREV    = PK_TO_VK(PK::AUDIOPREV),
	AUDIOSTOP    = PK_TO_VK(PK::AUDIOSTOP),
	AUDIOPLAY    = PK_TO_VK(PK::AUDIOPLAY),
	AUDIOMUTE    = PK_TO_VK(PK::AUDIOMUTE),
	MEDIASELECT  = PK_TO_VK(PK::MEDIASELECT),
	WWW          = PK_TO_VK(PK::WWW),
	MAIL         = PK_TO_VK(PK::MAIL),
	CALCULATOR   = PK_TO_VK(PK::CALCULATOR),
	COMPUTER     = PK_TO_VK(PK::COMPUTER),
	AC_SEARCH    = PK_TO_VK(PK::AC_SEARCH),
	AC_HOME      = PK_TO_VK(PK::AC_HOME),
	AC_BACK      = PK_TO_VK(PK::AC_BACK),
	AC_FORWARD   = PK_TO_VK(PK::AC_FORWARD),
	AC_STOP      = PK_TO_VK(PK::AC_STOP),
	AC_REFRESH   = PK_TO_VK(PK::AC_REFRESH),
	AC_BOOKMARKS = PK_TO_VK(PK::AC_BOOKMARKS),

	BRIGHTNESSDOWN = PK_TO_VK(PK::BRIGHTNESSDOWN),
	BRIGHTNESSUP   = PK_TO_VK(PK::BRIGHTNESSUP),
	DISPLAYSWITCH  = PK_TO_VK(PK::DISPLAYSWITCH),
	KBDILLUMTOGGLE = PK_TO_VK(PK::KBDILLUMTOGGLE),
	KBDILLUMDOWN   = PK_TO_VK(PK::KBDILLUMDOWN),
	KBDILLUMUP     = PK_TO_VK(PK::KBDILLUMUP),
	EJECT          = PK_TO_VK(PK::EJECT),
	SLEEP          = PK_TO_VK(PK::SLEEP),
} VK;

#if 0
struct
Key_Data
{
	Phys_Key phys_key;
	Virt_Key virt_key;
	u16 mods;
	u16 _padding2;
};
#endif

struct
Event_Keyboard
{
	Event_Common        common;
	s32                 window_id;
	Phys_Key            phys_key;
	Virt_Key            virt_key;
	u16                 mods;
	Event_Keyboard_Type type;
	u8                  repeat;
};

enum class
Mouse_Button : u32
{
	_1  = 0,
	_2  = 1,
	_3  = 2,
	_4  = 3,
	_5  = 4,
	_6  = 5,
	_7  = 6,
	_8  = 7,
	_9  = 8,
	_10 = 9,
	_11 = 10,
	_12 = 11,
	_13 = 12,
	_14 = 13,
	_15 = 14,
	_16 = 15,
	_17 = 16,
	_18 = 17,
	_19 = 18,
	_20 = 19,
	_21 = 20,
	_22 = 21,
	_23 = 22,
	_24 = 23,
	_25 = 24,
	_26 = 25,
	_27 = 26,
	_28 = 27,
	_29 = 28,
	_30 = 29,
	_31 = 30,
	_32 = 31,
	// ---
	 __COUNT,
	LEFT   = _1,
	MIDDLE = _2,
	RIGHT  = _3,
};


enum class
Mouse_State : u32
{
	_1  = 0x1u << 0,
	_2  = 0x1u << 1,
	_3  = 0x1u << 2,
	_4  = 0x1u << 3,
	_5  = 0x1u << 4,
	_6  = 0x1u << 5,
	_7  = 0x1u << 6,
	_8  = 0x1u << 7,
	_9  = 0x1u << 8,
	_10 = 0x1u << 9,
	_11 = 0x1u << 10,
	_12 = 0x1u << 11,
	_13 = 0x1u << 12,
	_14 = 0x1u << 13,
	_15 = 0x1u << 14,
	_16 = 0x1u << 15,
	_17 = 0x1u << 16,
	_18 = 0x1u << 17,
	_19 = 0x1u << 18,
	_20 = 0x1u << 19,
	_21 = 0x1u << 20,
	_22 = 0x1u << 21,
	_23 = 0x1u << 22,
	_24 = 0x1u << 23,
	_25 = 0x1u << 24,
	_26 = 0x1u << 25,
	_27 = 0x1u << 26,
	_28 = 0x1u << 27,
	_29 = 0x1u << 28,
	_30 = 0x1u << 29,
	_31 = 0x1u << 30,
	_32 = 0x1u << 31,
	// ---
	LEFT_BUTTON   = _1,
	MIDDLE_BUTTON = _2,
	RIGHT_BUTTON  = _3,
};

#define TG_SYS_MOUSE_STATE_BUTTON(i) (0x1<<(i))

#define TG_SYS_MOUSE_EVENT_EXPAND_AS_ENUM(a) a,
#define TG_SYS_MOUSE_EVENT_EXPAND_AS_NAME_TABLE(a) TOSTRING(a),
#define TG_SYS_MOUSE_EVENT_TABLE(ENTRY) \
	ENTRY( BUTTON_PRESSED  ) \
	ENTRY( BUTTON_RELEASED ) \
	ENTRY( MOTION          ) \
	ENTRY( WHEEL           ) \

enum class
Event_Mouse_Type : u8
{
	TG_SYS_MOUSE_EVENT_TABLE( TG_SYS_MOUSE_EVENT_EXPAND_AS_ENUM )
	// ---
	__COUNT,
};

struct
Event_Mouse
{
	Event_Common common;
	s32 windows_id;
	Event_Mouse_Type type;
	u8 _padding1;
	u8 _padding2;
	u8 _padding3;
	u32 state;
	union
	{
		struct
		{
			s32 pos_x, pos_y;
			s32 motion_x, motion_y;
		} motion;
		struct
		{
			s32 pos_x, pos_y;
			Mouse_Button button;
			u8 clicks_count;
			u8 _padding1;
			u8 _padding2;
		} button;
		struct
		{
			s32 scrolled_x, scrolled_y;
		} wheel;
	};
};


#define TG_SYS_CONTROLLER_EVENT_EXPAND_AS_ENUM(a) a,
#define TG_SYS_CONTROLLER_EVENT_EXPAND_AS_NAME_TABLE(a) TOSTRING(a),
#define TG_SYS_CONTROLLER_EVENT_TABLE(ENTRY) \
	ENTRY( AXIS ) \
	ENTRY( BUTTON_PRESSED ) \
	ENTRY( BUTTON_RELEASED ) \
	ENTRY( DEVICE_ADDED ) \
	ENTRY( DEVICE_REMOVED ) \
	ENTRY( DEVICE_REMAPPED ) \

enum class
Event_Controller_Type : u8
{
	TG_SYS_CONTROLLER_EVENT_TABLE( TG_SYS_CONTROLLER_EVENT_EXPAND_AS_ENUM )
	// ---
	__COUNT,
};

enum class
Controller_Button : u8
{
	INVALID,
	A,
	B,
	X,
	Y,
	BACK,
	GUIDE,
	START,
	LEFTSTICK,
	RIGHTSTICK,
	LEFTSHOULDER,
	RIGHTSHOULDER,
	DPAD_UP,
	DPAD_DOWN,
	DPAD_LEFT,
	DPAD_RIGHT,
	// ---
	__COUNT
};

enum class
Controller_Axis : u8
{
	INVALID,
	LEFTX,
	LEFTY,
	RIGHTX,
	RIGHTY,
	TRIGGERLEFT,
	TRIGGERRIGHT,
	// ---
	__COUNT
};

struct
Event_Controller
{
	Event_Common          common;
	u32                   controller_id;
	s32                   motion;
	Event_Controller_Type type;
	Controller_Axis       axis;
	Controller_Button     button;
	u8                    _padding1;
};

// TODO(theGiallo, 2021-04-08): remove this enum?
#define TG_SYS_GAME_SYSTEM_EVENT_EXPAND_AS_ENUM(a) a,
#define TG_SYS_GAME_SYSTEM_EVENT_EXPAND_AS_NAME_TABLE(a) TOSTRING(a),
#define TG_SYS_GAME_SYSTEM_EVENT_TABLE(ENTRY) \
	ENTRY( GAME_API_DL_RELOADED ) \

enum class
Event_Game_System_Type : u8
{
	TG_SYS_GAME_SYSTEM_EVENT_TABLE( TG_SYS_GAME_SYSTEM_EVENT_EXPAND_AS_ENUM )
	// ---
	__COUNT,
};

struct
Event_Game_System
{
	Event_Common           common;
	Event_Game_System_Type type;
	u8                     _padding1;
	u8                     _padding2;
	u8                     _padding3;
};

union
Event
{
	Event_Type        type;
	Event_Common      common;
	Event_Window      window;
	Event_Keyboard    keyboard;
	Event_Mouse       mouse;
	Event_Controller  controller;
	Event_Game_System game_system;
};

typedef s32 Thread_Function( void * data );

// TODO(theGiallo, 2016-02-27): make all sys api functions that have fail cases to return a s32?

#define TG_SYS_GET_ERROR_NAME( name ) const char * name( s64 error )
typedef TG_SYS_GET_ERROR_NAME( Get_Error_Name );
#define TG_SYS_GET_ERROR_DESCRIPTION( name ) const char * name( s64 error )
typedef TG_SYS_GET_ERROR_DESCRIPTION( Get_Error_Description );
#define TG_SYS_SET_VSYNC( name ) bool name( bool vsync )
typedef TG_SYS_SET_VSYNC( Set_VSync );
#define TG_SYS_SET_SCREEN_MODE( name ) bool name( s32 window_id, tg::sys::Screen_Mode mode )
typedef TG_SYS_SET_SCREEN_MODE( Set_Screen_Mode );
#define TG_SYS_GET_SCREEN_MODE( name ) tg::sys::Screen_Mode name( s32 window_id )
typedef TG_SYS_GET_SCREEN_MODE( Get_Screen_Mode );
#define TG_SYS_GET_WINDOW_SIZE( name ) void name( s32 window_id, \
                                                   u32 * width, u32 * height )
typedef TG_SYS_GET_WINDOW_SIZE( Get_Window_Size );
#define TG_SYS_SET_WINDOW_SIZE( name ) void name( s32 window_id, \
                                                   u32 width, u32 height )
typedef TG_SYS_SET_WINDOW_SIZE( Set_Window_Size );
#define TG_SYS_MAX_WINDOW_NAME_CHARS 256
#define TG_SYS_GET_WINDOW_NAME( name ) s32 name( s32 window_id, char * out_window_name_256 )
typedef TG_SYS_GET_WINDOW_NAME( Get_Window_Name );
#define TG_SYS_SET_WINDOW_NAME( name ) s32 name( s32 window_id, const char * window_name_256 )
typedef TG_SYS_SET_WINDOW_NAME( Set_Window_Name );
#define TG_SYS_CREATE_WINDOW( name ) s32 name( const char * win_name,       \
                                               tg::sys::GL_Version gl_version,   \
                                               bool share_context,          \
                                               bool vsync,                  \
                                               tg::sys::Screen_Mode screen_mode, \
                                               bool max_screen_size,        \
                                               u32 width, u32 height )
typedef TG_SYS_CREATE_WINDOW( Create_Window );
#define TG_SYS_MAKE_WINDOW_CURRENT( name ) s32 name( s32 window_id )
typedef TG_SYS_MAKE_WINDOW_CURRENT( Make_Window_Current );
#define TG_SYS_SWAP_WINDOW( name ) void name( s32 window_id )
typedef TG_SYS_SWAP_WINDOW( Swap_Window );
#define TG_SYS_CLOSE_WINDOW( name ) void name( s32 window_id )
typedef TG_SYS_CLOSE_WINDOW( Close_Window );
#define TG_SYS_EXIT( name ) void name( )
typedef TG_SYS_EXIT( Exit );
#define TG_SYS_GET_FILE_MODIFIED_TIME( name ) s64 name( const char * file_path, Time * modified_time )
typedef TG_SYS_GET_FILE_MODIFIED_TIME( Get_File_Modified_Time );
#define TG_SYS_GET_FILE_SIZE( name ) s64 name( const char * file_path )
typedef TG_SYS_GET_FILE_SIZE( Get_File_Size );
#define TG_SYS_READ_ENTIRE_FILE( name ) s64 name( const char * file_path, void * mem, s64 mem_size )
typedef TG_SYS_READ_ENTIRE_FILE( Read_Entire_File );
#define TG_SYS_LIST_DIRECTORY_FAST( name ) s64 name( const char * dir_path, u8 * mem, s64 mem_size, u32 options_flags )
typedef TG_SYS_LIST_DIRECTORY_FAST( List_Directory_Fast );
#define TG_SYS_LIST_DIRECTORY( name ) s64 name( const char * dir_path, u8 * mem, s64 mem_size, u32 options_flags )
typedef TG_SYS_LIST_DIRECTORY( List_Directory );
#define TG_SYS_FILE_IS_DIRECTORY( name ) s64 name( const char * file_path )
typedef TG_SYS_FILE_IS_DIRECTORY( File_Is_Directory );
#define TG_SYS_THREAD_CREATE( f_name ) tg::sys::Thread * f_name( tg::sys::Thread_Function fun, const char * name, void * data )
typedef TG_SYS_THREAD_CREATE( Thread_Create );
#define TG_SYS_THREAD_WAIT( name ) void name( tg::sys::Thread * thread, s32 * status )
typedef TG_SYS_THREAD_WAIT( Thread_Wait );
#define TG_SYS_THREAD_DETACH( name ) void name( tg::sys::Thread * thread )
typedef TG_SYS_THREAD_DETACH( Thread_Detach );
#define TG_SYS_THREAD_ID( name ) u32 name( )
typedef TG_SYS_THREAD_ID( Thread_ID );
#define TG_SYS_WARP_MOUSE_IN_WINDOW( name ) void name( s32 window_id, u32 px_pos_x, u32 px_pos_y )
typedef TG_SYS_WARP_MOUSE_IN_WINDOW( Warp_Mouse_In_Window );
// NOTE(theGiallo): physical keys
#define TG_SYS_GET_KEYBOARD_STATE( name ) const u8 * name( s32 * bytes_size )
typedef TG_SYS_GET_KEYBOARD_STATE( Get_Keyboard_State );

#define TG_SYS_AUDIO_CALLBACK( name ) void name( tg::sys::Impl * sys_impl, void * callback_data, f32 * buf, u32 len )
typedef TG_SYS_AUDIO_CALLBACK( Audio_Callback );

#define TG_SYS_SET_AUDIO_CALLBACK( name ) void name( tg::sys::Audio_Callback * callback, void * callback_data )
typedef TG_SYS_SET_AUDIO_CALLBACK( Set_Audio_Callback );

#define TG_SYS_EVENT_CALLBACK( name ) void name( tg::sys::Impl * sys_impl, void * callback_data, Event * event )
typedef TG_SYS_EVENT_CALLBACK( Event_Callback );

// TODO(theGiallo, 2021-04-18): make this add/remove (and internally use an array)
#define TG_SYS_SET_EVENT_CALLBACK( name ) void name( tg::sys::Event_Callback * callback, void * callback_data )
typedef TG_SYS_SET_EVENT_CALLBACK( Set_Event_Callback );

#define TG_SYS_POLL_EVENT( name ) bool name( tg::sys::Event * out_event )
typedef TG_SYS_POLL_EVENT( Poll_Event );

#define TG_SYS_POLL_ALL_EVENTS_TO_CALLBACK( name ) u32 name()
typedef TG_SYS_POLL_ALL_EVENTS_TO_CALLBACK( Poll_All_Events_To_Callback );

#define TG_SYS_UPDATE_TIME( name ) void name( u32 delta_frames )
typedef TG_SYS_UPDATE_TIME( Update_Time );
#define TG_SYS_GET_CACHED_TIME( name ) Cached_Time name()
typedef TG_SYS_GET_CACHED_TIME( Get_Cached_Time );
#define TG_SYS_GET_PERF_SECS( name ) Time name()
typedef TG_SYS_GET_PERF_SECS( Get_Perf_Secs );
#define TG_SYS_GET_SYS_SECS( name ) Time name()
typedef TG_SYS_GET_SYS_SECS( Get_Sys_Secs );
#define TG_SYS_GET_PERF_RUN_SECS( name ) Time name()
typedef TG_SYS_GET_PERF_RUN_SECS( Get_Perf_Run_Secs );
#define TG_SYS_GET_SYS_RUN_SECS( name ) Time name()
typedef TG_SYS_GET_SYS_RUN_SECS( Get_Sys_Run_Secs );

#define TG_SYS_GET_GL_FUNC_LOADER( name ) tg::sys::GL_Func_Loader name()
typedef TG_SYS_GET_GL_FUNC_LOADER( Get_GL_Func_Loader );

#define TG_SYS_API_F_NAME_FROM_NAME(name) GLUE( tg_sys_, name )
#define TG_SYS_API_F_NAME_FROM_NAME_STR(name) TOSTRING( TG_SYS_API_F_NAME_FROM_NAME( name ) )
#define TG_SYS_API_AS_IMPL( T, name ) T * name;
#define TG_SYS_API_AS_IMPL_SELF_INIT( T, name ) .name = TG_SYS_API_F_NAME_FROM_NAME( name ),
#define TG_SYS_API_AS_DL_F_NAMES_STR( T, name ) TG_SYS_API_F_NAME_FROM_NAME_STR( name ),
#define TG_SYS_API_AS_ENUM( T, name ) name,
#define TG_SYS_API_AS_DL_LOAD( T, name ) impl->name = (T*)dl->functions.array[(u8)Impl::Index::name];
#define TG_SYS_API( ENTRY ) \
	ENTRY( Get_Error_Name              , get_error_name              ) \
	ENTRY( Get_Error_Description       , get_error_description       ) \
	ENTRY( Set_VSync                   , set_vsync                   ) \
	ENTRY( Set_Screen_Mode             , set_screen_mode             ) \
	ENTRY( Get_Screen_Mode             , get_screen_mode             ) \
	ENTRY( Get_Window_Size             , get_window_size             ) \
	ENTRY( Set_Window_Size             , set_window_size             ) \
	ENTRY( Get_Window_Name             , get_window_name             ) \
	ENTRY( Set_Window_Name             , set_window_name             ) \
	ENTRY( Create_Window               , create_window               ) \
	ENTRY( Make_Window_Current         , make_window_current         ) \
	ENTRY( Swap_Window                 , swap_window                 ) \
	ENTRY( Close_Window                , close_window                ) \
	ENTRY( Exit                        , exit                        ) \
	ENTRY( Get_File_Modified_Time      , get_file_modified_time      ) \
	ENTRY( Get_File_Size               , get_file_size               ) \
	ENTRY( Read_Entire_File            , read_entire_file            ) \
	ENTRY( List_Directory_Fast         , list_directory_fast         ) \
	ENTRY( List_Directory              , list_directory              ) \
	ENTRY( File_Is_Directory           , file_is_directory           ) \
	ENTRY( Thread_Create               , thread_create               ) \
	ENTRY( Thread_Wait                 , thread_wait                 ) \
	ENTRY( Thread_Detach               , thread_detach               ) \
	ENTRY( Thread_ID                   , thread_id                   ) \
	ENTRY( Warp_Mouse_In_Window        , warp_mouse_in_window        ) \
	ENTRY( Get_Keyboard_State          , get_keyboard_state          ) \
	ENTRY( Set_Audio_Callback          , set_audio_callback          ) \
	ENTRY( Set_Event_Callback          , set_event_callback          ) \
	ENTRY( Poll_Event                  , poll_event                  ) \
	ENTRY( Poll_All_Events_To_Callback , poll_all_events_to_callback ) \
	ENTRY( Update_Time                 , update_time                 ) \
	ENTRY( Get_Cached_Time             , get_cached_time             ) \
	ENTRY( Get_Perf_Secs               , get_perf_secs               ) \
	ENTRY( Get_Sys_Secs                , get_sys_secs                ) \
	ENTRY( Get_Perf_Run_Secs           , get_perf_run_secs           ) \
	ENTRY( Get_Sys_Run_Secs            , get_sys_run_secs            ) \
	ENTRY( Get_GL_Func_Loader          , get_gl_func_loader          ) \

// NOTE(theGiallo): we are not exposing the real dl functions declarations
struct
Impl
{
	enum class
	Index : u8
	{
		TG_SYS_API(TG_SYS_API_AS_ENUM)
	};

	TG_SYS_API(TG_SYS_API_AS_IMPL)

	tg::hot::DL dl;
	void ( * callback )( Impl * loaded_impl );
	void * custom_data;

	static
	void
	hot_callback( tg::hot::DL * dl, void * _impl )
	{
		Impl * impl = (Impl*) _impl;
		tg_assert( dl == &impl->dl );

		TG_SYS_API(TG_SYS_API_AS_DL_LOAD)

		if ( impl->callback ) impl->callback( impl );
	}
};

struct
Inputs
{
	V2 mouse_pos;
	V2 mouse_motion_total_in_frame;
	V2s32 mouse_motions_during_frame  [1024];
	V2s32 mouse_positions_during_frame[1024];
	u32 mouse_motions_during_frame_count;

	bool mouse_clicked        [(s32)Mouse_Button::__COUNT];
	bool mouse_button_released[(s32)Mouse_Button::__COUNT];
	bool mouse_button_pressed [(s32)Mouse_Button::__COUNT];
	bool mouse_button_down    [(s32)Mouse_Button::__COUNT];
	bool mouse_left_clicked()  { return mouse_clicked        [ (s32)Mouse_Button::LEFT   ]; }
	bool mouse_left_down()     { return mouse_button_down    [ (s32)Mouse_Button::LEFT   ]; }
	bool mouse_left_pressed()  { return mouse_button_pressed [ (s32)Mouse_Button::LEFT   ]; }
	bool mouse_left_released() { return mouse_button_released[ (s32)Mouse_Button::LEFT   ]; }
	bool mouse_right_clicked() { return mouse_clicked        [ (s32)Mouse_Button::RIGHT  ]; }
	bool mouse_right_down()    { return mouse_button_down    [ (s32)Mouse_Button::RIGHT  ]; }
	bool mouse_right_pressed() { return mouse_clicked        [ (s32)Mouse_Button::RIGHT  ]; }
	bool mouse_right_released(){ return mouse_button_released[ (s32)Mouse_Button::RIGHT  ]; }
	bool mouse_wheel_clicked() { return mouse_clicked        [ (s32)Mouse_Button::MIDDLE ]; }
	bool mouse_wheel_down()    { return mouse_button_down    [ (s32)Mouse_Button::MIDDLE ]; }
	bool mouse_wheel_pressed() { return mouse_button_pressed [ (s32)Mouse_Button::MIDDLE ]; }
	bool mouse_wheel_released(){ return mouse_button_released[ (s32)Mouse_Button::MIDDLE ]; }
	s32 mouse_wheel_x;
	s32 mouse_wheel_y;
	s32 mouse_window_id;
	u8 keyboard_pressions           [(u32)PK::COUNT];
	u8 keyboard_releases            [(u32)PK::COUNT];
	u8 keyboard_is_down             [(u32)PK::COUNT];
	u8 keyboard_pressions_with_reps [(u32)PK::COUNT];
	u8 keyboard_releases_with_reps  [(u32)PK::COUNT];
	const u8 * keyboard_state;
	s32 keyboard_state_byte_size;

	// ---
	bool game_api_dl_reloaded;

	inline
	void
	init()
	{
		mouse_wheel_x = 0;
		mouse_wheel_y = 0;
		mouse_motion_total_in_frame = mouse_pos;
		mouse_motions_during_frame_count = 0;
		memset( mouse_clicked               , 0x0, sizeof ( mouse_clicked                ) );
		memset( mouse_button_released       , 0x0, sizeof ( mouse_button_released        ) );
		memset( mouse_button_pressed        , 0x0, sizeof ( mouse_button_pressed         ) );
		memset( mouse_button_down           , 0x0, sizeof ( mouse_button_down            ) );
		memset( keyboard_pressions          , 0x0, sizeof ( keyboard_pressions           ) );
		memset( keyboard_releases           , 0x0, sizeof ( keyboard_releases            ) );
		memset( keyboard_is_down            , 0x0, sizeof ( keyboard_is_down             ) );
		memset( keyboard_pressions_with_reps, 0x0, sizeof ( keyboard_pressions_with_reps ) );
		memset( keyboard_releases_with_reps , 0x0, sizeof ( keyboard_releases_with_reps  ) );

		game_api_dl_reloaded = false;
	}

	inline
	void
	frame_begin()
	{
		mouse_wheel_x = 0;
		mouse_wheel_y = 0;
		mouse_motion_total_in_frame = {};
		mouse_motions_during_frame_count = 0;
		memset( mouse_clicked               , 0x0, sizeof ( mouse_clicked                ) );
		memset( mouse_button_released       , 0x0, sizeof ( mouse_button_released        ) );
		memset( mouse_button_pressed        , 0x0, sizeof ( mouse_button_pressed         ) );
		//memset( mouse_button_down           , 0x0, sizeof ( mouse_button_down            ) );
		memset( keyboard_pressions          , 0x0, sizeof ( keyboard_pressions           ) );
		memset( keyboard_releases           , 0x0, sizeof ( keyboard_releases            ) );
		//memset( keyboard_is_down            , 0x0, sizeof ( keyboard_is_down             ) );
		memset( keyboard_pressions_with_reps, 0x0, sizeof ( keyboard_pressions_with_reps ) );
		memset( keyboard_releases_with_reps , 0x0, sizeof ( keyboard_releases_with_reps  ) );

		game_api_dl_reloaded = false;
	}

	inline
	void
	update_keyboard_state( Impl * impl )
	{
		keyboard_state = impl->get_keyboard_state( &keyboard_state_byte_size );
	}

	inline
	void
	update_mouse_frame()
	{
		mouse_motion_total_in_frame = mouse_pos - mouse_motion_total_in_frame;
	}

	inline
	bool
	handle_event( Event * e )
	{
		bool ret = false;

		switch ( e->type )
		{
			case Event_Type::KEYBOARD:
				switch ( e->keyboard.type )
				{
					case Event_Keyboard_Type::KEY_PRESSED:
						if ( !e->keyboard.repeat )
						{
							++keyboard_pressions[(s32)e->keyboard.phys_key];
							++keyboard_is_down  [(s32)e->keyboard.phys_key];
							//log_dbg( "event keyboard pressed %d", e->keyboard.phys_key );
						} else
						{
							//log_dbg( "event keyboard repeat pressed %d", e->keyboard.phys_key );
							//log_dbg( "mods: %u", (u32)e->keyboard.mods );
							//u16 mods = e->keyboard.mods;
							//if ( mods == (u16)Key_Mod::NONE     ) log_dbg( "NONE    " );
							//if ( mods  & (u16)Key_Mod::LSHIFT   ) log_dbg( "LSHIFT  " );
							//if ( mods  & (u16)Key_Mod::RSHIFT   ) log_dbg( "RSHIFT  " );
							//if ( mods  & (u16)Key_Mod::LCTRL    ) log_dbg( "LCTRL   " );
							//if ( mods  & (u16)Key_Mod::RCTRL    ) log_dbg( "RCTRL   " );
							//if ( mods  & (u16)Key_Mod::LALT     ) log_dbg( "LALT    " );
							//if ( mods  & (u16)Key_Mod::RALT     ) log_dbg( "RALT    " );
							//if ( mods  & (u16)Key_Mod::LGUI     ) log_dbg( "LGUI    " );
							//if ( mods  & (u16)Key_Mod::RGUI     ) log_dbg( "RGUI    " );
							//if ( mods  & (u16)Key_Mod::NUM      ) log_dbg( "NUM     " );
							//if ( mods  & (u16)Key_Mod::CAPS     ) log_dbg( "CAPS    " );
							//if ( mods  & (u16)Key_Mod::MODE     ) log_dbg( "MODE    " );
							//if ( mods  & (u16)Key_Mod::RESERVED ) log_dbg( "RESERVED" );
							//if ( mods  & (u16)Key_Mod::CTRL     ) log_dbg( "CTRL    " );
							//if ( mods  & (u16)Key_Mod::SHIFT    ) log_dbg( "SHIFT   " );
							//if ( mods  & (u16)Key_Mod::ALT      ) log_dbg( "ALT     " );
							//if ( mods  & (u16)Key_Mod::GUI      ) log_dbg( "GUI     " );
						}
						++keyboard_pressions_with_reps[(s32)e->keyboard.phys_key];
						ret = true;
						break;
					case Event_Keyboard_Type::KEY_RELEASED:
						if ( !e->keyboard.repeat )
						{
							++keyboard_releases[(s32)e->keyboard.phys_key];
							--keyboard_is_down [(s32)e->keyboard.phys_key];
						} else
						{
							//log_dbg( "event keyboard repeat released %d", e->keyboard.phys_key );
						}
						++keyboard_releases_with_reps[(s32)e->keyboard.phys_key];
						ret = true;
						break;
					default:
						break;
				}
				break;
			case Event_Type::MOUSE:
				switch ( e->mouse.type )
				{
					case Event_Mouse_Type::MOTION:
						mouse_window_id = e->mouse.windows_id;
						mouse_pos =
						   {(f32)e->mouse.motion.pos_x,
						    (f32)e->mouse.motion.pos_y};
						if ( mouse_motions_during_frame_count
						   < ARRAY_COUNT( mouse_motions_during_frame ) )
						{
							u32 id = mouse_motions_during_frame_count;
							mouse_motions_during_frame  [id] =
							   {e->mouse.motion.motion_x, e->mouse.motion.motion_y};
							mouse_positions_during_frame[id] =
							   {e->mouse.motion.pos_x,    e->mouse.motion.pos_y};
							++mouse_motions_during_frame_count;
						} else
						{
							log_err( "reached max capacity for mouse motions buffering per frame!" );
						}
						ret = true;
						break;
					case Event_Mouse_Type::WHEEL:
						mouse_wheel_x += e->mouse.wheel.scrolled_x;
						mouse_wheel_y += e->mouse.wheel.scrolled_y;
						ret = true;
						break;
					case Event_Mouse_Type::BUTTON_PRESSED:
						mouse_button_down   [(s32)e->mouse.button.button] = true;
						mouse_button_pressed[(s32)e->mouse.button.button] = true;
						ret = true;
						break;
					case Event_Mouse_Type::BUTTON_RELEASED:
						mouse_clicked        [(s32)e->mouse.button.button] = true;
						mouse_button_released[(s32)e->mouse.button.button] = true;
						mouse_button_down    [(s32)e->mouse.button.button] = false;
						ret = true;
						break;
					default:
						break;
				}
				break;
			case Event_Type::GAME_SYSTEM:
				switch ( e->game_system.type )
				{
					case Event_Game_System_Type::GAME_API_DL_RELOADED:
						game_api_dl_reloaded = true;
						ret = true;
						break;
					default:
						break;
				}
				break;
			default:
				break;
		}

		return ret;
	}
};




inline
void
request_impl( Impl * in_impl, const char * impl_name, const char * requester_name, void ( * callback )( Impl * loaded_impl ) )
{
	in_impl->callback = callback;
	in_impl->dl = tg::hot::DL::Make( impl_name, { TG_SYS_API(TG_SYS_API_AS_DL_F_NAMES_STR) } );
	tg::hot::request_dl( in_impl->dl, Impl::hot_callback, requester_name, in_impl );
}

}; // namespace tg::sys
#endif /* ifndef _TG_SYS_H_ */
