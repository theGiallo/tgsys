#ifndef _LPR_FONTS_H_
#define _LPR_FONTS_H_ 1

#include "app_lepre.h"
#include "basic_types.h"
#include "macro_tools.h"
#include "tgsys.h"

#define STBTT_STATIC
#define STB_TRUETYPE_IMPLEMENTATION
#include "stb_truetype.h"

namespace tg::fonts
{

#define TG_FONT_ATLAS_W 512
#define TG_FONT_ATLAS_H TG_FONT_ATLAS_W
#define TG_MAX_FONT_FILE_SIZE (1024*1024)



struct
Font_LL
{
	Font_LL * next;
	u32 key;
	u32 font_id;
	f32 px_height;
	u32 size_id;
	u32 atlas_id;
};

struct
Fonts
{
	#define MAX_FONTS_COUNT 8
	#define MAX_SIZES_PER_FONT 32
	Font_LL * fonts_ht[1<<10];
	//Font_LL_MH list_nodes_hm;
	Allocator allocator;

	const Font_LL *
	find_or_record( const char * path, f32 pixel_height );

	Lpr_Multi_Font   multi_font;
	Lpr_Texture      font_atlases   [MAX_FONTS_COUNT * MAX_SIZES_PER_FONT];
	Lpr_Font         fonts          [MAX_FONTS_COUNT];
	Lpr_Font_Of_Size font_of_sizes  [MAX_FONTS_COUNT][MAX_SIZES_PER_FONT];
	float            font_scales    [MAX_FONTS_COUNT][MAX_SIZES_PER_FONT];
	float            font_px_heights[MAX_FONTS_COUNT][MAX_SIZES_PER_FONT];

	// TODO(theGiallo, 2016-06-02): better refer to fonts
	const Font_LL * game_font;
	const Font_LL * debug_font;

	void
	init( Allocator allocator );

	void
	reset();
};

const Font_LL *
Fonts::
find_or_record( const char * path, f32 pixel_height )
{
	const Font_LL * ret = NULL;

	u32 font_id;
	bool font_id_found = false;
	u32 key = u32_FNV1a( path );
	u32 mask = ( 1 << 10 ) - 1;
	u32 h = ( ( key >>  0 ) & mask ) ^
	        ( ( key >> 10 ) & mask ) ^
	        ( ( key >> 20 ) & mask );
	Font_LL ** n = this->fonts_ht + h;
	for ( ;
	      *n;
	      *n = (*n)->next )
	{
		if ( (*n)->key )
		{
			font_id_found = true;
			font_id = (*n)->font_id;
		}
		if ( (*n)->key       == key &&
		     (*n)->px_height == pixel_height )
		{
			ret = (*n);
			return ret;
		}
	}

	if ( ( !font_id_found && this->multi_font.fonts_count == MAX_FONTS_COUNT ) ||
	     ( font_id_found &&
	       this->fonts[font_id].sizes_count == MAX_SIZES_PER_FONT ) )
	{
		return ret;
	}

	*n = (Font_LL*) allocator.allocate( sizeof ( Font_LL ) );
	ret = *n;
	if ( *n )
	{
		if ( !font_id_found )
		{
			font_id = this->multi_font.fonts_count++;
		}
		(*n)->font_id   = font_id;
		(*n)->atlas_id  = this->multi_font.atlases_count++;
		(*n)->size_id   = this->fonts[(*n)->font_id].sizes_count++;
		(*n)->px_height = pixel_height;
		(*n)->key       = key;
	}

	return ret;
}

void
Fonts::
init( Allocator allocator )
{
	this->allocator = allocator;
	Lpr_Multi_Font * mf = &this->multi_font;
	mf->atlases = this->font_atlases;
	mf->fonts   = this->fonts;
	Lpr_Font * f = mf->fonts;
	for ( u32 i = 0; i < MAX_FONTS_COUNT; ++i, ++f )
	{
		f->of_size    = this->font_of_sizes  [i];
		f->scales     = this->font_scales    [i];
		f->px_heights = this->font_px_heights[i];
	}
}

void
Fonts::
reset()
{
	Lpr_Multi_Font * mf = &this->multi_font;
	mf->atlases = this->font_atlases;
	mf->fonts   = this->fonts;
	Lpr_Font * f = mf->fonts;

	for ( u32 fi = 0; fi < MAX_FONTS_COUNT   ; ++fi, ++f )
	for ( u32 si = 0; si < MAX_SIZES_PER_FONT; ++si )
	{
		f->of_size   [si] = {};
		f->scales    [si] = {};
		f->px_heights[si] = {};
	}
}



tg_internal
const Font_LL *
pack_single_font( tg::sys::Impl * sys_api,
                  const char * font_path,
                  Fonts * fonts, f32 pixel_height,
                  const u8 * utf8_glyphs, u32 oversample_v, u32 oversample_h );

tg_internal
const Font_LL *
pack_single_font( tg::sys::Impl * sys_api,
                  const char * font_path,
                  Fonts * fonts, f32 pixel_height,
                  const u8 * utf8_glyphs, u32 oversample_v, u32 oversample_h )
{
	const Font_LL * ret = NULL;
	u8 atlas_blob[TG_FONT_ATLAS_W * TG_FONT_ATLAS_H];
	u8 font_file_blob[TG_MAX_FONT_FILE_SIZE];

	s32 i_res =
	sys_api->read_entire_file( font_path, font_file_blob,
	                           TG_MAX_FONT_FILE_SIZE );
	if ( TG_SYS_RET_IS_ERROR( i_res ) )
	{
		lpr_log_err( "failed to load font file '%s' [%d]. %s:%s",
		             font_path, i_res, sys_api->get_error_name( i_res ),
		             sys_api->get_error_description( i_res ) );
		return ret;
	}

	const s32 glyphs_count =
	   lpr_utf8_char_count( utf8_glyphs, UINT_MAX, true );
	s32 ucs4_codepoints[glyphs_count];
	const u8 * curr_char =  utf8_glyphs;
	u32 bytes_size = 0;
	while ( *(curr_char++) ){ ++bytes_size; }
	curr_char =  utf8_glyphs;
	for ( s32 i = 0; i < glyphs_count; ++i )
	{
		Lpr_UTF8_Fullchar fullchar = lpr_utf8_full_char( curr_char );
		lpr_assert( fullchar.all );
		ucs4_codepoints[i] = lpr_utf8_fullchar_to_ucs4( fullchar );
		curr_char =
		lpr_utf8_next_char_ptr( curr_char, utf8_glyphs, bytes_size );
	}
	{
		u32 tmp[glyphs_count];
		lpr_radix_sort_mc_32_keys_only( (u32*)ucs4_codepoints, tmp,
		                                glyphs_count );
	}
	s32 unique_glyphs_count = 0;
	for ( s32 i = 0; i < glyphs_count; ++i )
	{
		s32 j;
		for ( j = 0; j < unique_glyphs_count; ++j )
		{
			if ( ucs4_codepoints[j] == ucs4_codepoints[i] )
			{
				break;
			}
		}
		if ( j == unique_glyphs_count )
		{
			ucs4_codepoints[unique_glyphs_count++] = ucs4_codepoints[i];
		}
	}
	stbtt_packedchar char_data[unique_glyphs_count];
	stbtt_pack_range font_range;
	font_range.num_chars = unique_glyphs_count;
	font_range.array_of_unicode_codepoints = ucs4_codepoints;
	font_range.chardata_for_range = char_data;
	font_range.font_size = pixel_height;
	font_range.v_oversample = oversample_v;
	font_range.h_oversample = oversample_h;

	const s32 padding = 1;

	stbtt_pack_context pack_context{};
	i_res =
	stbtt_PackBegin( &pack_context, atlas_blob,
	                 TG_FONT_ATLAS_W, TG_FONT_ATLAS_H, 0, padding, NULL );
	if ( !i_res )
	{
		lpr_log_err( "stbtt_PackBegin failed!" );
		return ret;
	}

	stbtt_PackSetOversampling( &pack_context, oversample_v, oversample_h );
	oversample_v = pack_context.v_oversample;
	oversample_h = pack_context.h_oversample;

	//pack_context.v_oversample = oversample_v;
	//pack_context.h_oversample = oversample_h;
	//font_range.v_oversample = oversample_v;
	//font_range.h_oversample = oversample_h;

	i_res =
	stbtt_PackFontRanges( &pack_context, font_file_blob, 0, &font_range, 1 );
	if ( !i_res )
	{
		lpr_log_err( "stbtt_PackFontRanges failed! (font='%s' pixel height %f)", font_path, pixel_height );
		return ret;
	}

	stbtt_PackEnd( &pack_context );

	stbtt_fontinfo font_info;
	i_res =
	stbtt_InitFont( &font_info, font_file_blob,
	                stbtt_GetFontOffsetForIndex( font_file_blob, 0 ) );
	if ( !i_res )
	{
		lpr_log_err( "stbtt_InitFont failed!" );
		return ret;
	}

	// TODO(theGiallo, 2016-03-15): check if the font is already there, maybe
	// hash the filename concatenated with the TTF font id; also check for size
	// to be already there
	const Font_LL * f =
	fonts->find_or_record( font_path, pixel_height );
	if ( !f )
	{
		lpr_log_err( "cannot store another font!" );
		LPR_ILLEGAL_PATH();
	}

	u32 font_id  = f->font_id;
	u32 atlas_id = f->atlas_id;
	u32 size_id  = f->size_id;

	Lpr_Multi_Font * multi_font = &fonts->multi_font;
	Lpr_Font * font = multi_font->fonts + font_id;
	font->scales[size_id] =
	   stbtt_ScaleForPixelHeight( &font_info, pixel_height );
	font->px_heights[size_id] = pixel_height;
	Lpr_Font_Of_Size * of_size = font->of_size + size_id;
	of_size->oversampling_scale.x = 1.0f / oversample_h;
	of_size->oversampling_scale.y = 1.0f / oversample_v;

	stbtt_GetFontVMetrics( &font_info,
	                       &font->metrics.ascent,
	                       &font->metrics.descent,
	                       &font->metrics.line_gap );
	for ( s32 i = 0; i < unique_glyphs_count; ++i )
	{
		s32 codepoint = ucs4_codepoints[i];
		Lpr_UTF8_Fullchar fullchar =
		   lpr_utf8_fullchar_from_ucs4( (u32)codepoint );
		s32 h =  lpr_hash_fullchar( fullchar );
		s32 glyph_id =
		stbtt_FindGlyphIndex( &font_info, codepoint );

		if ( of_size->glyphs_indexes_ht[h] )
		{
			lpr_log_err( "codepoint -> glyph hash map collision! (codepoint = %d, fullchar = %.4s, h=%d)", codepoint, fullchar.subchars, h );
			LPR_ILLEGAL_PATH();
		}
		of_size->glyphs_indexes_ht[h] = glyph_id;

		Lpr_Glyph_Metrics * cm = font->glyphs_metrics + glyph_id;
		stbtt_GetGlyphHMetrics( &font_info, glyph_id,
		                        &cm->advance_width,
		                        &cm->left_side_bearing );
	}

	for ( s32 i = 0; i < unique_glyphs_count; ++i )
	{
		s32 codepoint_i = ucs4_codepoints[i];
		Lpr_UTF8_Fullchar fullchar_i =
		   lpr_utf8_fullchar_from_ucs4( (u32)codepoint_i );
		s32 glyph_id_i = lpr_glyph_id_from_fullchar( of_size, fullchar_i );

		for ( s32 j = 0; j < unique_glyphs_count; ++j )
		{
			s32 codepoint_j = ucs4_codepoints[j];
			Lpr_UTF8_Fullchar fullchar_j =
			   lpr_utf8_fullchar_from_ucs4( (u32)codepoint_j );
			s32 glyph_id_j = lpr_glyph_id_from_fullchar( of_size, fullchar_j );

			font->kerning_table[glyph_id_i][glyph_id_j] =
			stbtt_GetGlyphKernAdvance( &font_info, glyph_id_i, glyph_id_j );
		}
	}
	for ( s32 i = 0; i < unique_glyphs_count; ++i )
	{
		s32 codepoint = ucs4_codepoints[i];
		Lpr_UTF8_Fullchar fullchar =
		   lpr_utf8_fullchar_from_ucs4( (u32)codepoint );
		s32 glyph_id = lpr_glyph_id_from_fullchar( of_size, fullchar );
		of_size->atlases_of_glyphs[glyph_id] = atlas_id;
		Lpr_Glyph_In_Atlas * gia = of_size->glyphs_in_atlas + glyph_id;

		char_data[i].x0 -= padding;
		char_data[i].y0 -= padding;
		char_data[i].x1 += padding;
		char_data[i].y1 += padding;

		gia->px_size =
		   {(u32)(char_data[i].x1 - char_data[i].x0),
		    (u32)(char_data[i].y1 - char_data[i].y0)};

		gia->pos_offset_px =
		   lpr_V2_mult_f32(
		      lpr_V2_add_V2( lpr_V2( char_data[i].xoff,
		                            -char_data[i].yoff),
		                     lpr_V2( char_data[i].xoff2,
		                            -char_data[i].yoff2) ),
		      0.5f );

		gia->bl_uv.x = char_data[i].x0 / (f32)TG_FONT_ATLAS_W;
		gia->bl_uv.y = 1.0f - char_data[i].y1 / (f32)TG_FONT_ATLAS_H;
		gia->uv_size.x =
		   ( char_data[i].x1 - char_data[i].x0 ) /
		   (f32)TG_FONT_ATLAS_W;
		gia->uv_size.y =
		   ( char_data[i].y1 - char_data[i].y0 ) /
		   (f32)TG_FONT_ATLAS_H;

		gia->x_advance_px = char_data[i].xadvance;
	}

	bool b_ret =
	lpr_create_texture( atlas_blob,
	                    {TG_FONT_ATLAS_W, TG_FONT_ATLAS_H},
	                    LPR_GRAYSCALE8_ALPHA,
	                    false,
	                    false,
	                    false,
	                    multi_font->atlases + atlas_id,
	                    Lpr_Texture_Filter::LPR_BILINEAR,
	                    Lpr_Texture_Filter::LPR_BILINEAR,
	                    LPR_CLAMP_TO_EDGE,
	                    LPR_CLAMP_TO_EDGE,
	                    Lpr_Scale_Filter::LPR_SCALE_LINEAR,
	                    {1,1},
	                    NULL );
	if ( !b_ret )
	{
		lpr_log_err( "failed to create texture for font atlas!" );
	}

	ret = f;

	return ret;
}

} // namespace tg::fonts

#endif /* ifndef _LPR_FONTS_H_ */
