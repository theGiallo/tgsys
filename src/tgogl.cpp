#include "tgogl.h"

namespace tg::ogl
{
using namespace tg::math;

void
tg::ogl::Shader::
dynamically_update( tg::sys::Impl * sys, Mem_Stack * tmp_mem )
{
	Time mod_time_geom, mod_time_vert, mod_time_frag;
	mod_time_geom = mod_time_vert = mod_time_frag = 0.0;
	sys->get_file_modified_time( geom.file_path, &mod_time_geom );
	if ( sys->get_file_modified_time( vert.file_path, &mod_time_vert ) >= 0 &&
	     sys->get_file_modified_time( frag.file_path, &mod_time_frag ) >= 0 )
	{
		if ( geom.modified_time < mod_time_geom ||
		     vert.modified_time < mod_time_vert ||
		     frag.modified_time < mod_time_frag )
		{
			geom.modified_time = mod_time_geom;
			vert.modified_time = mod_time_vert;
			frag.modified_time = mod_time_frag;

			log_dbg( "shader files modified, going to make a new program (%s)", frag.file_path );

			if ( ! create_program( geom.file_path,
			                       vert.file_path,
			                       frag.file_path,
			                       sys, tmp_mem,
			                       &program ) )
			{
				log_err( "failed to load or compile the new shader" );
			} else
			{
				load_locations();
			}
		}
	}
}

void
tg::ogl::Shader::
load_locations()
{
	s32 i_ret;
	#define LOAD_GL_LOC( gl_func, shader_variable, client_variable ) \
	   i_ret = gl_func( program, shader_variable );                                                    \
	   if ( i_ret == -1 )                                                                              \
	   {                                                                                               \
	      log_dbg( "no '" shader_variable "' found. gl error = %u, i_ret = %d", glGetError(), i_ret ); \
	   }                                                                                               \
	   client_variable = i_ret;                                                                        \

	LOAD_GL_LOC( glGetAttribLocation, "vertex", vertex_loc );
	LOAD_GL_LOC( glGetAttribLocation, "vertex_color", vertex_color_loc );
	LOAD_GL_LOC( glGetAttribLocation, "normal", normal_loc );
	LOAD_GL_LOC( glGetAttribLocation, "uv", uv_loc );
	LOAD_GL_LOC( glGetAttribLocation, "instance_M", instance_M_loc );
	LOAD_GL_LOC( glGetAttribLocation, "instance_color", instance_color_loc );
	LOAD_GL_LOC( glGetUniformLocation, "VP", VP_loc );
	LOAD_GL_LOC( glGetUniformLocation, "V", V_loc );
	LOAD_GL_LOC( glGetUniformLocation, "P", P_loc );
	LOAD_GL_LOC( glGetUniformLocation, "M", M_loc );
	LOAD_GL_LOC( glGetUniformLocation, "color", color_loc );

	#define LOAD_GL_LOCS( gl_func, prefix_type ) \
	for_0M ( i, Shader::MAX_GENERIC_LOCS_COUNT )                 \
	{                                                            \
		char shader_variable[256] = {};                          \
		snprintf( shader_variable, sizeof( shader_variable ),    \
		          TOSTRING(prefix_type) "_%d", i );              \
		i_ret = gl_func( program, shader_variable );             \
		if ( i_ret == -1 )                                       \
		{                                                        \
			log_dbg( "no '%s' found. gl error = %u, i_ret = %d", \
			             shader_variable, glGetError(), i_ret ); \
		}                                                        \
		prefix_type[i] = i_ret;                                  \
	}                                                            \

	LOAD_GL_LOCS( glGetAttribLocation,  attr_float )
	LOAD_GL_LOCS( glGetAttribLocation,  attr_vec2  )
	LOAD_GL_LOCS( glGetAttribLocation,  attr_vec3  )
	LOAD_GL_LOCS( glGetAttribLocation,  attr_vec4  )
	LOAD_GL_LOCS( glGetUniformLocation, unif_float )
	LOAD_GL_LOCS( glGetUniformLocation, unif_vec2  )
	LOAD_GL_LOCS( glGetUniformLocation, unif_vec3  )
	LOAD_GL_LOCS( glGetUniformLocation, unif_vec4  )

	#undef LOAD_GL_LOC
	#undef LOAD_GL_LOCS
}

void
tg::ogl::Camera3D::
initialize_camera( f32 inverse_aspect_ratio, f32 near /*= 0.1f*/, f32 fov /*= 90.0f*/ )
{
	this->pos  = V3{};
	this->look = V3{1.0f,0.0f,0.0f};
	this->up   = V3{0.0f,0.0f,1.0f};
	update_view();
	set_projection( near, V2{0.5f,0.5f}, fov, inverse_aspect_ratio );
}

void
tg::ogl::Camera3D::
resize_camera( u32 width, u32 height )
{
	f32 inverse_aspect_ratio = (f32)height / (f32)width;
	set_projection( this->near, this->vanishing_point, this->fov, inverse_aspect_ratio );
}

void
tg::ogl::Camera3D::
update_view()
{
	V = view_mx4( look, up, pos );
}

void
check_up( tg::ogl::Camera3D * camera )
{
	camera->up = ( camera->look == V3{0,0,1} ) ? V3{0,1,0} : V3{0,0,1};
}

void
tg::ogl::Camera3D::
set_projection(  f32 near, V2 vanishing_point, V2 size, f32 far )
{
	if ( far == f32_inf )
	{
		P = perspective_infinite_mx4( near, vanishing_point, size );
	} else
	{
		P = perspective_mx4( near, far, vanishing_point, size );
	}

	this->fov = fov_from_near_plane_size( near, size.w );

	this->near                 = near;
	this->far                  = far;
	this->size                 = size;
	this->vanishing_point      = vanishing_point;
	this->inverse_aspect_ratio = size.h / size.w;
}

void
tg::ogl::Camera3D::
set_projection( f32 near, V2 vanishing_point, f32 fov, f32 inverse_aspect_ratio, f32 far )
{
	if ( far == f32_inf )
	{
		P = perspective_infinite_mx4( near, vanishing_point, fov, inverse_aspect_ratio, & size );
	} else
	{
		P = perspective_mx4( near, far, vanishing_point, fov, inverse_aspect_ratio, & size );
	}

	this->near                 = near;
	this->far                  = far;
	this->vanishing_point      = vanishing_point;
	this->fov                  = fov;
	this->inverse_aspect_ratio = inverse_aspect_ratio;
}

void
tg::ogl::Camera3D::
set( Shader * shader )
{
	glUseProgram( shader->program );
	if ( shader->VP_loc != -1 )
	{
		glUniformMatrix4fv( shader->VP_loc, 1, GL_FALSE,
		                    this  ->VP.arr );
	}
	if ( shader->V_loc != -1 )
	{
		glUniformMatrix4fv( shader->V_loc, 1, GL_FALSE,
		                    this  ->V.arr );
	}
	if ( shader->P_loc != -1 )
	{
		glUniformMatrix4fv( shader->P_loc, 1, GL_FALSE,
		                    this  ->P.arr );
	}
}

bool
tg::ogl::Frame_Buffer_With_Depth::
create( V2u32 px_size )
{
	if ( ! glIsFramebuffer( linear_fbo ) )
	{
		glGenFramebuffers( 1, &linear_fbo );
	}
	glBindFramebuffer( GL_FRAMEBUFFER, linear_fbo );
	if ( ! glIsRenderbuffer( linear_color_rt ) )
	{
		glGenRenderbuffers( 1, &linear_color_rt );
	}
	glBindRenderbuffer(    GL_RENDERBUFFER, linear_color_rt );
	glRenderbufferStorage( GL_RENDERBUFFER, GL_RGBA16F, px_size.w, px_size.h );
	glFramebufferRenderbuffer( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
	                           GL_RENDERBUFFER, linear_color_rt );

	glGenRenderbuffers( 1, &depth_rt );
	glBindRenderbuffer(    GL_RENDERBUFFER, depth_rt );
	glRenderbufferStorage( GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, px_size.w, px_size.h );
	glFramebufferRenderbuffer( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
	                           GL_RENDERBUFFER, depth_rt );

	// NOTE(theGiallo): Generate and bind the OGL texture for diffuse
	if ( ! glIsTexture( linear_fb_texture ) )
	{
		glGenTextures( 1, &linear_fb_texture );
	}
	glBindTexture( GL_TEXTURE_2D, linear_fb_texture );
	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA16F, px_size.w, px_size.h, 0,
	              GL_RGBA, GL_HALF_FLOAT, NULL );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
	// NOTE(theGiallo): Attach the texture to the FBO
	glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
	                        GL_TEXTURE_2D, linear_fb_texture, 0 );

	// NOTE(theGiallo): Check if all worked fine and unbind the FBO
	if( ! debug::check_and_log_framebuffer_status() )
	{
		log_err( "Something failed during FBO initialization. FBO is not complete." );
		// TODO(theGiallo, 2015/09/28): proper clean
		glDeleteFramebuffers( 1, &linear_fbo );
		glDeleteRenderbuffers( 1, &linear_color_rt );
		glDeleteRenderbuffers( 1, &depth_rt );
		ILLEGAL_PATH();
		return false;
	}
	debug::log_framebuffer_info();

	// NOTE(theGiallo): OpenGL cleanup
	glBindTexture( GL_TEXTURE_2D, 0 );
	glBindFramebuffer( GL_FRAMEBUFFER, 0 );
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
	glBindBuffer( GL_ARRAY_BUFFER, 0 );

	size = px_size;

	return true;
}

void
tg::ogl::Indexed_Mesh::
create_ogl_buffers( GLenum usage )
{
	create_and_fill_ogl_buffer(
	   &ogl.vertex_buffer,
	   GL_ARRAY_BUFFER, usage, vertices,
	   sizeof(vertices[0]) * vertices_count );

	#if 0
	glGenBuffers( 1, &ogl.index_buffer );
	#else
	create_and_fill_ogl_buffer(
	   &ogl.index_buffer,
	   GL_ARRAY_BUFFER, usage, indices,
	   sizeof(indices[0]) * indices_count );
	#endif

	if ( normals )
	{
		create_and_fill_ogl_buffer(
		   &ogl.normals_buffer,
		   GL_ARRAY_BUFFER, usage, normals,
		   sizeof(normals[0]) * vertices_count );
	}
	if ( uvs )
	{
		create_and_fill_ogl_buffer(
		   &ogl.uvs_buffer,
		   GL_ARRAY_BUFFER, usage, uvs,
		   sizeof(uvs[0]) * vertices_count );
	}
	if ( u8_colors )
	{
		create_and_fill_ogl_buffer(
		   &ogl.u8_colors_buffer,
		   GL_ARRAY_BUFFER, usage, u8_colors,
		   sizeof(u8) * 3 * vertices_count );
	}
}

void
tg::ogl::Indexed_Mesh::
upload_to_gpu_1st_time( GLenum usage )
{
	glBindBuffer( GL_ARRAY_BUFFER, ogl.vertex_buffer );
	glBufferData( GL_ARRAY_BUFFER, sizeof(V3) * vertices_count, vertices, usage );

	glBindBuffer( GL_ARRAY_BUFFER, ogl.index_buffer );
	glBufferData( GL_ARRAY_BUFFER, sizeof(u16) * indices_count, indices, usage );

	if ( normals )
	{
		glBindBuffer( GL_ARRAY_BUFFER, ogl.normals_buffer );
		glBufferData( GL_ARRAY_BUFFER, sizeof(V3) * vertices_count, normals, usage );
	}
	if ( uvs )
	{
		glBindBuffer( GL_ARRAY_BUFFER, ogl.uvs_buffer );
		glBufferData( GL_ARRAY_BUFFER, sizeof(V2) * vertices_count, uvs, usage );
	}
	if ( u8_colors )
	{
		glBindBuffer( GL_ARRAY_BUFFER, ogl.u8_colors_buffer );
		glBufferData( GL_ARRAY_BUFFER, sizeof(u8) * 3 * vertices_count, u8_colors, usage );
	}
}

void
tg::ogl::Indexed_Mesh::
upload_to_gpu_update( GLenum usage )
{
	glBindBuffer( GL_ARRAY_BUFFER, ogl.vertex_buffer );
	glBufferData( GL_ARRAY_BUFFER, 0, NULL, usage );
	glBufferData( GL_ARRAY_BUFFER, sizeof(V3) * vertices_count, vertices, usage );

	glBindBuffer( GL_ARRAY_BUFFER, ogl.index_buffer );
	glBufferData( GL_ARRAY_BUFFER, 0, NULL, usage );
	glBufferData( GL_ARRAY_BUFFER, sizeof(u16) * indices_count, indices, usage );

	if ( normals )
	{
		glBindBuffer( GL_ARRAY_BUFFER, ogl.normals_buffer );
		glBufferData( GL_ARRAY_BUFFER, 0, NULL, usage );
		glBufferData( GL_ARRAY_BUFFER, sizeof(V3) * vertices_count, normals, usage );
	}
	if ( uvs )
	{
		glBindBuffer( GL_ARRAY_BUFFER, ogl.uvs_buffer );
		glBufferData( GL_ARRAY_BUFFER, 0, NULL, usage );
		glBufferData( GL_ARRAY_BUFFER, sizeof(V2) * vertices_count, uvs, usage );
	}
	if ( u8_colors )
	{
		glBindBuffer( GL_ARRAY_BUFFER, ogl.u8_colors_buffer );
		glBufferData( GL_ARRAY_BUFFER, 0, NULL, usage );
		glBufferData( GL_ARRAY_BUFFER, sizeof(u8) * 3 * vertices_count, u8_colors, usage );
	}
}

void
tg::ogl::Indexed_Mesh::
render_single( Shader * shader,
               Mx4 model_matrix, V4 mesh_color )
{
	glUseProgram( shader->program );

	glUniformMatrix4fv( shader->M_loc, 1, GL_FALSE, model_matrix.arr );
	glUniform4fv( shader->color_loc, 1, mesh_color.rgba );

	glBindBuffer( GL_ARRAY_BUFFER, ogl.vertex_buffer );
	glEnableVertexAttribArray( shader->vertex_loc );
	glVertexAttribPointer( shader->vertex_loc, 3, GL_FLOAT, GL_FALSE, 0, 0 );

	if ( normals &&
	     shader->normal_loc >= 0 )
	{
		glBindBuffer( GL_ARRAY_BUFFER, ogl.normals_buffer );
		glEnableVertexAttribArray( shader->normal_loc );
		glVertexAttribPointer( shader->normal_loc, 3, GL_FLOAT, GL_FALSE, 0, 0 );
	}
	if ( uvs &&
	     shader->uv_loc >= 0 )
	{
		glBindBuffer( GL_ARRAY_BUFFER, ogl.uvs_buffer );
		glEnableVertexAttribArray( shader->uv_loc );
		glVertexAttribPointer( shader->uv_loc, 2, GL_FLOAT, GL_FALSE, 0, 0 );
	}
	if ( u8_colors &&
	     shader->vertex_color_loc >= 0 )
	{
		glBindBuffer( GL_ARRAY_BUFFER, ogl.u8_colors_buffer );
		glEnableVertexAttribArray( shader->vertex_color_loc );
		glVertexAttribPointer( shader->vertex_color_loc, 3, GL_UNSIGNED_BYTE, GL_TRUE, 0, 0 );
	}

	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, ogl.index_buffer );
	glDrawElements( GL_TRIANGLES, indices_count,
	                GL_UNSIGNED_SHORT, 0 );


	glDisableVertexAttribArray( shader->vertex_loc );
	glVertexAttribPointer( shader->vertex_loc, 3, GL_FLOAT, GL_FALSE, 0, 0 );

	if ( normals &&
	     shader->normal_loc >= 0 )
	{
		glDisableVertexAttribArray( shader->normal_loc );
	}
	if ( uvs &&
	     shader->uv_loc >= 0 )
	{
		glDisableVertexAttribArray( shader->uv_loc );
	}
	if ( u8_colors &&
	     shader->vertex_color_loc >= 0 )
	{
		glDisableVertexAttribArray( shader->vertex_color_loc );
	}
}

void
tg::ogl::Indexed_Mesh::
render_multiple( u32 model_matrices_buffer, u32 mesh_colors_buffer,
                 u32 count,
                 Shader * shader )
{
	glUseProgram( shader->program );

	glBindBuffer( GL_ARRAY_BUFFER, ogl.vertex_buffer );
	glEnableVertexAttribArray( shader->vertex_loc );
	glVertexAttribDivisor( shader->vertex_loc, 0 );
	glVertexAttribPointer( shader->vertex_loc, 3, GL_FLOAT, GL_FALSE, 0, 0 );

	glBindBuffer( GL_ARRAY_BUFFER, model_matrices_buffer );
	for ( u32 i = 0; i != 4; ++i )
	{
		glEnableVertexAttribArray( shader->instance_M_loc + i);
		glVertexAttribDivisor( shader->instance_M_loc + i, 1 );
		glVertexAttribPointer( shader->instance_M_loc + i, 4, GL_FLOAT, GL_FALSE,
		                       sizeof(f32) * 16, (void*)( sizeof(f32) * 4 * (u64)i ) );
	}

	glBindBuffer( GL_ARRAY_BUFFER, mesh_colors_buffer );
	glEnableVertexAttribArray( shader->instance_color_loc );
	glVertexAttribDivisor( shader->instance_color_loc, 1 );
	glVertexAttribPointer( shader->instance_color_loc, 4, GL_FLOAT, GL_FALSE, 0, 0 );

	if ( normals &&
	     shader->normal_loc >= 0 )
	{
		glBindBuffer( GL_ARRAY_BUFFER, ogl.normals_buffer );
		glEnableVertexAttribArray( shader->normal_loc );
		glVertexAttribDivisor( shader->normal_loc, 0 );
		glVertexAttribPointer( shader->normal_loc, 3, GL_FLOAT, GL_FALSE, 0, 0 );
	}
	if ( uvs &&
	     shader->uv_loc >= 0 )
	{
		glBindBuffer( GL_ARRAY_BUFFER, ogl.uvs_buffer );
		glEnableVertexAttribArray( shader->uv_loc );
		glVertexAttribDivisor( shader->uv_loc, 0 );
		glVertexAttribPointer( shader->uv_loc, 2, GL_FLOAT, GL_FALSE, 0, 0 );
	}
	if ( u8_colors &&
	     shader->vertex_color_loc >= 0 )
	{
		glBindBuffer( GL_ARRAY_BUFFER, ogl.u8_colors_buffer );
		glEnableVertexAttribArray( shader->vertex_color_loc );
		glVertexAttribDivisor( shader->vertex_color_loc, 0 );
		glVertexAttribPointer( shader->vertex_color_loc, 3, GL_UNSIGNED_BYTE, GL_TRUE, 0, 0 );
	}

	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, ogl.index_buffer );
	glDrawElementsInstanced( GL_TRIANGLES, indices_count,
	                         GL_UNSIGNED_SHORT, 0, count );


	glDisableVertexAttribArray( shader->vertex_loc );
	glVertexAttribDivisor( shader->vertex_loc, 0 );
	for ( u32 i = 0; i != 4; ++i )
	{
		glDisableVertexAttribArray( shader->instance_M_loc + i);
		glVertexAttribDivisor( shader->instance_M_loc + i, 0 );
	}

	glDisableVertexAttribArray( shader->instance_color_loc );
	glVertexAttribDivisor( shader->instance_color_loc, 0 );

	if ( normals &&
	     shader->normal_loc >= 0 )
	{
		glDisableVertexAttribArray( shader->normal_loc );
		glVertexAttribDivisor( shader->normal_loc, 0 );
	}
	if ( uvs &&
	     shader->uv_loc >= 0 )
	{
		glDisableVertexAttribArray( shader->uv_loc );
		glVertexAttribDivisor( shader->uv_loc, 0 );
	}
	if ( u8_colors &&
	     shader->vertex_color_loc >= 0 )
	{
		glDisableVertexAttribArray( shader->vertex_color_loc );
		glVertexAttribDivisor( shader->vertex_color_loc, 0 );
	}
}
bool
tg::ogl::Indexed_Mesh::
load_obj( const u8 * file_path, Allocator allocator, Mem_Stack * tmp_mem_stack, tg::sys::Impl * sys )
{
	bool ret = false;

	s64 file_size = sys->get_file_size( (const char*)file_path );
	if ( TG_SYS_RET_IS_ERROR( file_size ) )
	{
		log_err( "couldn't load obj file '%s'.\n"
		         "   Error: %s\n"
		         "   Description: %s\n",
		         file_path,
		         sys->get_error_name( file_size ),
		         sys->get_error_description( file_size ) );
		return ret;
	}

	u64 pop = tmp_mem_stack->first_free;

	u8 * file_mem = tmp_mem_stack->push( file_size + 1 );
	if ( !file_mem )
	{
		log_err( "not enough tmp memory to load obj file '%s' of size %lu",
		         file_path, file_size );
		return ret;
	}

	s64 read = sys->read_entire_file( (const char *)file_path, file_mem, file_size );
	if ( TG_SYS_RET_IS_ERROR( read ) )
	{
		log_err( "couldn't load obj file '%s'.\n"
		         "   Error: %s\n"
		         "   Description: %s\n",
		         file_path,
		         sys->get_error_name( read ),
		         sys->get_error_description( read ) );

		tmp_mem_stack->first_free = pop;
		return ret;
	}

	file_mem[file_size] = 0;

	s64 remaining_size = file_size;
	u8 * into_file = file_mem;
	u32 line_number = 0;

	u32 file_vertices_count = 0;
	u32 file_uvs_count      = 0;
	u32 file_normals_count  = 0;
	u32 file_faces_count    = 0;
	u32 file_vertices_indices_count = 0;
	u32 file_uvs_indices_count      = 0;
	u32 file_normals_indices_count  = 0;

	bool got_an_error = false;
	for (;;)
	{
		u32 read_length = str_bytes_to_char_or_end( (char*)into_file, remaining_size, ' ' );
		if ( !read_length )
		{
			break;
		}

		into_file[read_length] = 0;

		if ( string_min( "v", (char*)into_file ) == 0 )
		{
			into_file[read_length] = ' ';
			into_file += read_length;

			u32 line_length = str_bytes_to_char_or_end( (char*)into_file, remaining_size, '\n' );
			if ( !line_length )
			{
				log_err( "malformed obj file '%s'. Expected vertex coordinates at line %u",
				         file_path, line_number );
				got_an_error = true;
				break;
			}
			if ( into_file[line_length] == '\n' )
			{
				++line_length;
			}
			++file_vertices_count;
			into_file += line_length;
			remaining_size -= line_length;
			++line_number;
		} else
		if ( string_min( "vt", (char*)into_file ) == 0 )
		{
			into_file[read_length] = ' ';
			into_file += read_length;

			u32 line_length = str_bytes_to_char_or_end( (char*)into_file, remaining_size, '\n' );
			if ( !line_length )
			{
				log_err( "malformed obj file '%s'. Expected uv coordinates at line %u",
				         file_path, line_number );
				got_an_error = true;
				break;
			}
			if ( into_file[line_length] == '\n' )
			{
				++line_length;
			}
			++file_uvs_count;
			into_file += line_length;
			remaining_size -= line_length;
			++line_number;
		} else
		if ( string_min( "vn", (char*)into_file ) == 0 )
		{
			into_file[read_length] = ' ';
			into_file += read_length;

			u32 line_length = str_bytes_to_char_or_end( (char*)into_file, remaining_size, '\n' );
			if ( !line_length )
			{
				log_err( "malformed obj file '%s'. Expected normal vector at line %u",
				         file_path, line_number );
				got_an_error = true;
				break;
			}
			if ( into_file[line_length] == '\n' )
			{
				++line_length;
			}
			++file_normals_count;
			into_file += line_length;
			remaining_size -= line_length;
			++line_number;
		} else
		if ( string_min( "f", (char*)into_file ) == 0 )
		{
			into_file[read_length] = ' ';
			into_file += read_length;

			u32 line_length = str_bytes_to_char_or_end( (char*)into_file, remaining_size, '\n' );
			if ( !line_length )
			{
				log_err( "malformed obj file '%s'. Expected face indices at line %u",
				         file_path, line_number );
				got_an_error = true;
				break;
			}

			if ( into_file[line_length] == '\n' )
			{
				++line_length;
			}

			u32 foo[9];
			s32 read_elements = sscanf( (char*)into_file, "%u/%u/%u %u/%u/%u %u/%u/%u \n",
			                            foo + 0, foo + 1, foo + 2, foo + 3, foo + 4, foo + 5,
			                            foo + 6, foo + 7, foo + 8 );
			if ( read_elements != 9 )
			{
				read_elements = sscanf( (char*)into_file, "%u/%u %u/%u %u/%u \n",
				                foo + 0, foo + 1, foo + 2, foo + 3, foo + 4, foo + 5 );
				if ( read_elements != 6 )
				{
					read_elements = sscanf( (char*)into_file, "%u//%u %u//%u %u//%u \n",
					                        foo + 0, foo + 1, foo + 2, foo + 3, foo + 4, foo + 5 );
					if ( read_elements != 6 )
					{
						log_err( "malformed obj. Error reading face indices at lien %u", line_number );
						got_an_error = true;
						break;
					}
					file_vertices_indices_count += 3;
					file_normals_indices_count  += 3;
				} else
				{
					file_vertices_indices_count += 3;
					file_uvs_indices_count      += 3;
				}
			} else
			{
				file_vertices_indices_count += 3;
				file_uvs_indices_count      += 3;
				file_normals_indices_count  += 3;
			}

			++file_faces_count;
			into_file      += line_length;
			remaining_size -= line_length;
			++line_number;
		} else
		{
			into_file[read_length] = ' ';

			u32 line_length = str_bytes_to_char_or_end( (char*)into_file, remaining_size, '\n' );
			if ( !line_length )
			{
				break;
			}
			u8 old = into_file[line_length];
			into_file[line_length] = 0;
			log_dbg( "assuming this line is a comment: '%s'", into_file );
			into_file[line_length] = old;

			if ( into_file[line_length] == '\n' )
			{
				++line_length;
			}

			into_file += line_length;
			remaining_size -= line_length;
			++line_number;
		}
	}

	if ( got_an_error )
	{
		tmp_mem_stack->first_free = pop;

		return ret;
	}

	log_dbg( "found %4u vertices, %4u normals, %4u uvs, %4u faces",
	         file_vertices_count, file_normals_count, file_uvs_count, file_faces_count );

	V3  * vertices       = ( V3*) tmp_mem_stack->push( sizeof ( V3  ) * file_vertices_count         ) ;
	V2  * uvs            = ( V2*) tmp_mem_stack->push( sizeof ( V2  ) * file_uvs_count              ) ;
	V3  * normals        = ( V3*) tmp_mem_stack->push( sizeof ( V3  ) * file_normals_count          ) ;
	u32 * vertex_indices = (u32*) tmp_mem_stack->push( sizeof ( u32 ) * file_vertices_indices_count ) ;
	u32 * uv_indices     = (u32*) tmp_mem_stack->push( sizeof ( u32 ) * file_uvs_indices_count      ) ;
	u32 * normal_indices = (u32*) tmp_mem_stack->push( sizeof ( u32 ) * file_normals_indices_count  ) ;

	if ( !vertices || !uvs || !normals || !vertex_indices || !uv_indices || !normal_indices )
	{
		log_err( "not enough tmp memory to load obj '%s'", file_path );

		tmp_mem_stack->first_free = pop;
		return ret;
	}

	u32 read_vertices_count = 0;
	u32 read_uvs_count      = 0;
	u32 read_normals_count  = 0;
	u32 read_vertices_indices_count  = 0;
	u32 read_uvs_indices_count       = 0;
	u32 read_normals_indices_count   = 0;

	remaining_size = file_size;
	into_file = file_mem;
	line_number = 0;
	for (;;)
	{
		u32 read_length = str_bytes_to_char_or_end( (char*)into_file, remaining_size, ' ' );
		if ( !read_length )
		{
			break;
		}

		into_file[read_length] = 0;

		if ( string_min( "v", (char*)into_file ) == 0 )
		{
			into_file[read_length] = ' ';
			into_file += read_length;

			u32 line_length = str_bytes_to_char_or_end( (char*)into_file, remaining_size, '\n' );
			if ( !line_length )
			{
				log_err( "malformed obj file '%s'. Expected vertex coordinates at line %u",
				         file_path, line_number );
				got_an_error = true;
				break;
			}
			if ( into_file[line_length] == '\n' )
			{
				++line_length;
			}
			s32 read_elements =
			sscanf( (char*)into_file, "%f %f %f\n",
			        &vertices[read_vertices_count].x,
			        &vertices[read_vertices_count].y,
			        &vertices[read_vertices_count].z );
			if ( read_elements != 3 )
			{
				log_err( "malformed obj file '%s'. Expected 3 vertices dimensions, %d found.",
				         file_path, read_elements );
				got_an_error = true;
				break;
			}
			++read_vertices_count;
			into_file += line_length;
			remaining_size -= line_length;
			++line_number;
		} else
		if ( string_min( "vt", (char*)into_file ) == 0 )
		{
			into_file[read_length] = ' ';
			into_file += read_length;

			u32 line_length = str_bytes_to_char_or_end( (char*)into_file, remaining_size, '\n' );
			if ( !line_length )
			{
				log_err( "malformed obj file '%s'. Expected uv coordinates at line %u",
				         file_path, line_number );
				got_an_error = true;
				break;
			}
			if ( into_file[line_length] == '\n' )
			{
				++line_length;
			}
			s32 read_elements =
			sscanf( (char*)into_file, "%f %f\n",
			        &uvs[read_uvs_count].u,
			        &uvs[read_uvs_count].v );
			if ( read_elements != 2 )
			{
				log_err( "malformed obj file '%s'. Expected 2 uv dimensions, %d found.",
				         file_path, read_elements );
				got_an_error = true;
				break;
			}
			++read_uvs_count;
			into_file += line_length;
			remaining_size -= line_length;
			++line_number;
		} else
		if ( string_min( "vn", (char*)into_file ) == 0 )
		{
			into_file[read_length] = ' ';
			into_file += read_length;

			u32 line_length = str_bytes_to_char_or_end( (char*)into_file, remaining_size, '\n' );
			if ( !line_length )
			{
				log_err( "malformed obj file '%s'. Expected normal vector at line %u",
				         file_path, line_number );
				got_an_error = true;
				break;
			}
			if ( into_file[line_length] == '\n' )
			{
				++line_length;
			}
			s32 read_elements =
			sscanf( (char*)into_file, "%f %f %f\n",
			        &normals[read_normals_count].x,
			        &normals[read_normals_count].y,
			        &normals[read_normals_count].z );
			if ( read_elements != 3 )
			{
				log_err( "malformed obj file '%s'. Expected 3 normal dimensions, %d found.",
				         file_path, read_elements );
				got_an_error = true;
				break;
			}
			++read_normals_count;
			into_file += line_length;
			remaining_size -= line_length;
			++line_number;
		} else
		if ( string_min( "f", (char*)into_file ) == 0 )
		{
			into_file[read_length] = ' ';
			into_file += read_length;

			u32 line_length = str_bytes_to_char_or_end( (char*)into_file, remaining_size, '\n' );
			if ( !line_length )
			{
				log_err( "malformed obj file '%s'. Expected face indices at line %u",
				         file_path, line_number );
				got_an_error = true;
				break;
			}
			if ( into_file[line_length] == '\n' )
			{
				++line_length;
			}

			s32 read_elements =
			sscanf( (char*)into_file, "%u/%u/%u %u/%u/%u %u/%u/%u \n",
			        vertex_indices + read_vertices_indices_count + 0,
			        uv_indices     + read_uvs_indices_count      + 0,
			        normal_indices + read_normals_indices_count  + 0,
			        vertex_indices + read_vertices_indices_count + 1,
			        uv_indices     + read_uvs_indices_count      + 1,
			        normal_indices + read_normals_indices_count  + 1,
			        vertex_indices + read_vertices_indices_count + 2,
			        uv_indices     + read_uvs_indices_count      + 2,
			        normal_indices + read_normals_indices_count  + 2 );
			if ( read_elements != 9 )
			{
				read_elements =
				sscanf( (char*)into_file, "%u/%u %u/%u %u/%u \n",
				        vertex_indices + read_vertices_indices_count + 0,
				        uv_indices     + read_uvs_indices_count      + 0,
				        vertex_indices + read_vertices_indices_count + 1,
				        uv_indices     + read_uvs_indices_count      + 1,
				        vertex_indices + read_vertices_indices_count + 2,
				        uv_indices     + read_uvs_indices_count      + 2 );
				if ( read_elements != 6 )
				{
					read_elements =
					sscanf( (char*)into_file, "%u//%u %u//%u %u//%u \n",
					        vertex_indices + read_vertices_indices_count + 0,
					        normal_indices + read_normals_indices_count  + 0,
					        vertex_indices + read_vertices_indices_count + 1,
					        normal_indices + read_normals_indices_count  + 1,
					        vertex_indices + read_vertices_indices_count + 2,
					        normal_indices + read_normals_indices_count  + 2 );
					if ( read_elements != 6 )
					{
						log_err( "malformed obj. Error reading face indices at line %u", line_number );
						got_an_error = true;
						break;
					}
					read_vertices_indices_count += 3;
					read_normals_indices_count  += 3;
				} else
				{
					read_vertices_indices_count += 3;
					read_uvs_indices_count      += 3;
				}
			} else
			{
				read_vertices_indices_count += 3;
				read_uvs_indices_count      += 3;
				read_normals_indices_count  += 3;
			}
			into_file += line_length;
			remaining_size -= line_length;
			++line_number;
		} else
		{
			into_file[read_length] = ' ';

			u32 line_length = str_bytes_to_char_or_end( (char*)into_file, remaining_size, '\n' );
			if ( !line_length )
			{
				break;
			}
			u8 old = into_file[line_length];
			into_file[line_length] = 0;
			log_dbg( "assuming this line is a comment: '%s'", into_file );
			into_file[line_length] = old;

			if ( into_file[line_length] == '\n' )
			{
				++line_length;
			}

			into_file += line_length;
			remaining_size -= line_length;
			++line_number;
		}
	}

	if ( read_vertices_indices_count != read_uvs_indices_count ||
	     read_vertices_indices_count != read_normals_indices_count )
	{
		log_err( "malformed obj file '%s'. Different number of indices for vertices, uvs, normals. ( %u, %u, %u )",
		         file_path, read_vertices_count, read_uvs_indices_count, read_normals_indices_count );
		got_an_error = true;
	}

	if ( got_an_error )
	{
		tmp_mem_stack->first_free = pop;

		return ret;
	}


	// TODO(theGiallo, 2017-02-11): optimize vertices de-duplication
	// TODO(theGiallo, 2017-02-11): make the loaded mesh into an opengl compatible mesh
	// TODO(theGiallo, 2017-02-11): split the mesh in case it's more than 65336 indices
	// INCOMPLETE IMPORTANT WORKING ON THIS
	log_err( "WTF r u doing?! Go back coding and finish this function!" );

	Packed_Vertex_PUVN * triangle_vertices =
	   (Packed_Vertex_PUVN*)tmp_mem_stack->push( sizeof(Packed_Vertex_PUVN) * read_vertices_indices_count );
	u32 * nonduplicate_ogl_indices = (u32*) tmp_mem_stack->push( sizeof(u32) * read_vertices_indices_count );
	u32 nonduplicate_ogl_indices_count  = 0;
	u32 nonduplicate_ogl_vertices_count = 0;
	if ( !triangle_vertices || !nonduplicate_ogl_indices )
	{
		log_err( "not enough tmp memory to load obj file '%s'", file_path );

		tmp_mem_stack->first_free = pop;
		return ret;
	}

	for ( u32 i = 0; i != read_vertices_indices_count; ++i )
	{
		triangle_vertices[nonduplicate_ogl_vertices_count].vertex =           vertices[vertex_indices[i] - 1];
		triangle_vertices[nonduplicate_ogl_vertices_count].normal = normals ? normals [normal_indices[i] - 1] : V3{};
		triangle_vertices[nonduplicate_ogl_vertices_count].uv     = uvs     ? uvs     [    uv_indices[i] - 1] : V2{};
		bool found = false;
		u32 found_index = U32_MAX;
		for ( u32 j = 0; j != nonduplicate_ogl_vertices_count; ++j )
		{
			if ( triangle_vertices[j].vertex ==
			     triangle_vertices[nonduplicate_ogl_vertices_count].vertex &&
			     triangle_vertices[j].normal ==
			     triangle_vertices[nonduplicate_ogl_vertices_count].normal &&
			     triangle_vertices[j].uv ==
			     triangle_vertices[nonduplicate_ogl_vertices_count].uv )
			{
				found = true;
				found_index = j;
			}
		}
		if ( found )
		{
			nonduplicate_ogl_indices[nonduplicate_ogl_indices_count] = found_index;
			++nonduplicate_ogl_indices_count;
		} else
		{
			nonduplicate_ogl_indices[nonduplicate_ogl_indices_count] = nonduplicate_ogl_vertices_count;
			++nonduplicate_ogl_indices_count;
			++nonduplicate_ogl_vertices_count;
		}
	}

	log_dbg( "nonduplicate_ogl_vertices_count %4u", nonduplicate_ogl_vertices_count );
	log_dbg( "nonduplicate_ogl_indices_count %4u",  nonduplicate_ogl_indices_count  );

	u32 optionals_flags = 0;
	if ( normals )
	{
		optionals_flags |= (u32)Optionals_Flags::has_normals;
	}
	if ( uvs )
	{
		optionals_flags |= (u32)Optionals_Flags::has_uvs;
	}
	u64 indexed_mesh_bytes_size =
	   compute_bytes_size( nonduplicate_ogl_vertices_count,
	                       nonduplicate_ogl_indices_count,
	                       optionals_flags );
	u8 * indexed_mesh_mem = (u8*) allocator.allocate( indexed_mesh_bytes_size );
	if ( !indexed_mesh_mem )
	{
		log_err( "not enough memory to store read indexed mesh loaded from obj '%s'."
		         " Required mesh size is %luB. It has %u vertices and %u indices.",
		         file_path, indexed_mesh_bytes_size, nonduplicate_ogl_vertices_count,
		         nonduplicate_ogl_indices_count );

		tmp_mem_stack->first_free = pop;
		return ret;
	}

	init( indexed_mesh_mem, nonduplicate_ogl_vertices_count,
	      nonduplicate_ogl_indices_count, optionals_flags );

	for ( u32 i = 0; i != nonduplicate_ogl_vertices_count; ++i )
	{
		this->vertices[i] = triangle_vertices[i].vertex;
		if ( uvs )
		{
			this->uvs[i]     = triangle_vertices[i].uv;
		}
		if ( normals )
		{
			this->normals[i] = triangle_vertices[i].normal;
		}
	}
	for ( u32 i = 0; i != nonduplicate_ogl_indices_count; ++i )
	{
		this->indices[i] = nonduplicate_ogl_indices[i];
	}

	ret = true;
	tmp_mem_stack->first_free = pop;

	return ret;
}


void
tg::ogl::Grid_Planes::
init()
{
	create_and_fill_ogl_buffer( & points_vb,          GL_ARRAY_BUFFER, GL_STREAM_DRAW, NULL, 0 );
	create_and_fill_ogl_buffer( & normals_vb,         GL_ARRAY_BUFFER, GL_STREAM_DRAW, NULL, 0 );
	create_and_fill_ogl_buffer( & ups_vb,             GL_ARRAY_BUFFER, GL_STREAM_DRAW, NULL, 0 );
	create_and_fill_ogl_buffer( & grid_sides_vb,      GL_ARRAY_BUFFER, GL_STREAM_DRAW, NULL, 0 );
	create_and_fill_ogl_buffer( & grid_cell_sides_vb, GL_ARRAY_BUFFER, GL_STREAM_DRAW, NULL, 0 );
	create_and_fill_ogl_buffer( & grid_colors_vb,     GL_ARRAY_BUFFER, GL_STREAM_DRAW, NULL, 0 );
	create_and_fill_ogl_buffer( & grid_bg_colors_vb,  GL_ARRAY_BUFFER, GL_STREAM_DRAW, NULL, 0 );
}

void
tg::ogl::Grid_Planes::
render( Shader * shader )
{
	glUseProgram( shader->program );

	glBindBuffer( GL_ARRAY_BUFFER, points_vb );
	glBufferData( GL_ARRAY_BUFFER, 0, NULL, GL_STREAM_DRAW );
	glBufferData( GL_ARRAY_BUFFER, sizeof(V3) * count, points, GL_STREAM_DRAW );

	glEnableVertexAttribArray( shader->vertex_loc );
	glVertexAttribPointer( shader->vertex_loc, 3, GL_FLOAT, GL_FALSE, 0, 0 );

	glBindBuffer( GL_ARRAY_BUFFER, normals_vb );
	glBufferData( GL_ARRAY_BUFFER, 0, NULL, GL_STREAM_DRAW );
	glBufferData( GL_ARRAY_BUFFER, sizeof(V3) * count, normals, GL_STREAM_DRAW );

	glEnableVertexAttribArray( shader->normal_loc );
	glVertexAttribPointer( shader->normal_loc, 3, GL_FLOAT, GL_FALSE, 0, 0 );

	glBindBuffer( GL_ARRAY_BUFFER, ups_vb );
	glBufferData( GL_ARRAY_BUFFER, 0, NULL, GL_STREAM_DRAW );
	glBufferData( GL_ARRAY_BUFFER, sizeof(V3) * count, ups, GL_STREAM_DRAW );

	glEnableVertexAttribArray( shader->attr_vec3[0] );
	glVertexAttribPointer( shader->attr_vec3[0], 3, GL_FLOAT, GL_FALSE, 0, 0 );

	glBindBuffer( GL_ARRAY_BUFFER, grid_cell_sides_vb );
	glBufferData( GL_ARRAY_BUFFER, 0, NULL, GL_STREAM_DRAW );
	glBufferData( GL_ARRAY_BUFFER, sizeof(float) * count, grid_cell_sides, GL_STREAM_DRAW );

	glEnableVertexAttribArray( shader->attr_float[0] );
	glVertexAttribPointer( shader->attr_float[0], 1, GL_FLOAT, GL_FALSE, 0, 0 );

	glBindBuffer( GL_ARRAY_BUFFER, grid_sides_vb );
	glBufferData( GL_ARRAY_BUFFER, 0, NULL, GL_STREAM_DRAW );
	glBufferData( GL_ARRAY_BUFFER, sizeof(float) * count, grid_sides, GL_STREAM_DRAW );

	glEnableVertexAttribArray( shader->attr_float[1] );
	glVertexAttribPointer( shader->attr_float[1], 1, GL_FLOAT, GL_FALSE, 0, 0 );

	glBindBuffer( GL_ARRAY_BUFFER, grid_colors_vb );
	glBufferData( GL_ARRAY_BUFFER, 0, NULL, GL_STREAM_DRAW );
	glBufferData( GL_ARRAY_BUFFER, sizeof(V4) * count, grid_colors, GL_STREAM_DRAW );

	glEnableVertexAttribArray( shader->attr_vec4[0] );
	glVertexAttribPointer( shader->attr_vec4[0], 4, GL_FLOAT, GL_FALSE, 0, 0 );

	glBindBuffer( GL_ARRAY_BUFFER, grid_bg_colors_vb );
	glBufferData( GL_ARRAY_BUFFER, 0, NULL, GL_STREAM_DRAW );
	glBufferData( GL_ARRAY_BUFFER, sizeof(V4) * count, grid_bg_colors, GL_STREAM_DRAW );

	glEnableVertexAttribArray( shader->attr_vec4[1] );
	glVertexAttribPointer( shader->attr_vec4[1], 4, GL_FLOAT, GL_FALSE, 0, 0 );

	glDrawArrays( GL_POINTS, 0, count );

	glEnableVertexAttribArray( shader->vertex_loc );
	glEnableVertexAttribArray( shader->normal_loc );
	glEnableVertexAttribArray( shader->attr_vec3[0] );
	glEnableVertexAttribArray( shader->attr_float[0] );
	glEnableVertexAttribArray( shader->attr_float[1] );
	glEnableVertexAttribArray( shader->attr_vec4[0] );
	glEnableVertexAttribArray( shader->attr_vec4[1] );
}


// NOTE(theGiallo): h in [0,1)
V4
rgba_from_hsva( V4 hsva )
{
	V4 ret;
	ret.a = hsva.a;

	ret.xyz = rgb_from_hsv( hsva.xyz );

	return ret;
}
V3
rgb_from_hsv( V3 hsv )
{
	V3 ret;
	f32 c = hsv.g * hsv.b;
	f32 hh = hsv.r * 6.0;

	f32 x = c * ( 1.0 - abs( hh - 2.0f * floorf( hh / 2.0f ) - 1.0 ) );

	if ( hh < 1.0 || hh >= 6.0 )
	{
		ret.r = c;
		ret.g = x;
		ret.b = 0;
	} else
	if ( hh < 2.0 )
	{
		ret.r = x;
		ret.g = c;
		ret.b = 0;
	} else
	if ( hh < 3.0 )
	{
		ret.r = 0;
		ret.g = c;
		ret.b = x;
	} else
	if ( hh < 4.0 )
	{
		ret.r = 0;
		ret.g = x;
		ret.b = c;
	} else
	if ( hh < 5.0 )
	{
		ret.r = x;
		ret.g = 0;
		ret.b = c;
	} else
	if ( hh < 6.0 )
	{
		ret.r = c;
		ret.g = 0;
		ret.b = x;
	}

	f32 m = hsv.b - c;
	ret.r += m;
	ret.g += m;
	ret.b += m;

	return ret;
}
V3
rgb_from_v2( V2 v )
{
	f32 hue = angle_rad( v ) / TAU_F;
	V3 ret = rgb_from_hsv( V3{hue, 0.7f, 1.0f} );
	return ret;
}


bool
create_program_from_text( GLuint *     gl_program,
                          const char * geometry_shader_text,
                          GLint        geometry_shader_text_length,
                          const char * vertex_shader_text,
                          GLint        vertex_shader_text_length,
                          const char * fragment_shader_text,
                          GLint        fragment_shader_text_length )
{
	*gl_program = glCreateProgram( );
	GLuint gl_p = *gl_program;

	GLuint gl_shader_geometry,
	       gl_shader_vertex,
	       gl_shader_fragment;

	if ( geometry_shader_text )
	{
		gl_shader_geometry = glCreateShader( GL_GEOMETRY_SHADER   );
	}
	gl_shader_vertex   = glCreateShader( GL_VERTEX_SHADER );
	gl_shader_fragment = glCreateShader( GL_FRAGMENT_SHADER );

	GLint compiled;
	s32 info_log_length;

	if ( ( geometry_shader_text && !gl_shader_geometry ) ||
	     !gl_shader_vertex || !gl_shader_fragment )
	{
		log_err( "Error creating shader objects." );

		glDeleteProgram( gl_p );

		if ( geometry_shader_text )
		{
			glDetachShader( gl_p, gl_shader_geometry );
			glDeleteShader( gl_shader_geometry );
		}

		glDetachShader( gl_p, gl_shader_vertex );
		glDeleteShader( gl_shader_vertex );

		glDetachShader( gl_p, gl_shader_fragment );
		glDeleteShader( gl_shader_fragment );
		return false;
	}

	if ( geometry_shader_text )
	{
		// NOTE(theGiallo): compile and attach geometry shader
		glShaderSource( gl_shader_geometry,                    // shader
		                1,                                     // count
		                (const GLchar**)&geometry_shader_text, // string
		                &geometry_shader_text_length );        // length
		glCompileShader( gl_shader_geometry );

		// NOTE(theGiallo): check geometry shader compilation
		compiled = GL_FALSE;
		info_log_length = 0;
		glGetShaderiv( gl_shader_geometry, GL_COMPILE_STATUS,  &compiled );
		glGetShaderiv( gl_shader_geometry, GL_INFO_LOG_LENGTH, &info_log_length );
		if ( info_log_length > 0 )
		{
			char shader_info_log[info_log_length+1];
			glGetShaderInfoLog( gl_shader_geometry, info_log_length, NULL,
			                    shader_info_log );
			log_info( "\ngeometry shader compilation infos (%dB):\n" \
			          "----------------------------------------\n" \
			          "%s\n" \
			          "----------------------------------------\n",
			          info_log_length,
			          shader_info_log );
		}
		if ( compiled == GL_FALSE )
		{
			log_err( "Error compiling geometry shader." );

			glDeleteProgram( gl_p );

			glDetachShader( gl_p, gl_shader_geometry );
			glDeleteShader( gl_shader_geometry );

			glDetachShader( gl_p, gl_shader_vertex );
			glDeleteShader( gl_shader_vertex );

			glDetachShader( gl_p, gl_shader_fragment );
			glDeleteShader( gl_shader_fragment );
			return false;
		}
		// NOTE(theGiallo): compilation successfull => attach geometry shader
		glAttachShader( gl_p, gl_shader_geometry );
	}

	gl_shader_vertex   = glCreateShader( GL_VERTEX_SHADER   );
	gl_shader_fragment = glCreateShader( GL_FRAGMENT_SHADER );
	if ( !gl_shader_vertex || !gl_shader_fragment )
	{
		log_err( "Error creating shader objects." );

		glDeleteProgram( gl_p );

		if ( geometry_shader_text )
		{
			glDetachShader( gl_p, gl_shader_geometry );
			glDeleteShader( gl_shader_geometry );
		}

		glDetachShader( gl_p, gl_shader_vertex );
		glDeleteShader( gl_shader_vertex );

		glDetachShader( gl_p, gl_shader_fragment );
		glDeleteShader( gl_shader_fragment );
		return false;
	}

	// NOTE(theGiallo): compile and attach vertex shader
	glShaderSource( gl_shader_vertex,                    // shader
	                1,                                   // count
	                (const GLchar**)&vertex_shader_text, // string
	                &vertex_shader_text_length );        // length
	glCompileShader( gl_shader_vertex );

	// NOTE(theGiallo): check vertex shader compilation
	compiled = GL_FALSE;
	info_log_length = 0;
	glGetShaderiv( gl_shader_vertex, GL_COMPILE_STATUS,  &compiled );
	glGetShaderiv( gl_shader_vertex, GL_INFO_LOG_LENGTH, &info_log_length );
	if ( info_log_length > 0 )
	{
		char shader_info_log[info_log_length+1];
		glGetShaderInfoLog( gl_shader_vertex, info_log_length, NULL,
		                    shader_info_log );
		log_info( "\nVertex shader compilation infos (%dB):\n" \
		          "----------------------------------------\n" \
		          "%s\n" \
		          "----------------------------------------\n",
		          info_log_length,
		          shader_info_log );
	}
	if ( compiled == GL_FALSE )
	{
		log_err( "Error compiling vertex shader." );

		glDeleteProgram( gl_p );

		if ( geometry_shader_text )
		{
			glDetachShader( gl_p, gl_shader_geometry );
			glDeleteShader( gl_shader_geometry );
		}

		glDetachShader( gl_p, gl_shader_vertex );
		glDeleteShader( gl_shader_vertex );

		glDetachShader( gl_p, gl_shader_fragment );
		glDeleteShader( gl_shader_fragment );

		return false;
	}
	// NOTE(theGiallo): compilation successfull => attach vertex shader
	glAttachShader( gl_p, gl_shader_vertex );


	// NOTE(theGiallo): compile and attach fragment shader
	glShaderSource( gl_shader_fragment,                    // shader
	                1,                                     // count
	                (const GLchar**)&fragment_shader_text, // string
	                &fragment_shader_text_length );        // length
	glCompileShader( gl_shader_fragment );

	// NOTE(theGiallo): check fragment shader compilation
	compiled = GL_FALSE;
	info_log_length = 0;
	glGetShaderiv( gl_shader_fragment, GL_COMPILE_STATUS,  &compiled );
	glGetShaderiv( gl_shader_fragment, GL_INFO_LOG_LENGTH, &info_log_length );
	if ( info_log_length > 0 )
	{
		char shader_info_log[info_log_length+1];
		glGetShaderInfoLog( gl_shader_fragment, info_log_length, NULL,
		                    shader_info_log );
		log_info( "\nFragment shader compilation infos (%dB):\n" \
		          "----------------------------------------\n" \
		          "%s\n" \
		          "----------------------------------------\n",
		          info_log_length,
		          shader_info_log );
	}
	if ( compiled == GL_FALSE )
	{
		log_err( "Error compiling vertex shader." );

		glDeleteProgram( gl_p );

		if ( geometry_shader_text )
		{
			glDetachShader( gl_p, gl_shader_geometry );
			glDeleteShader( gl_shader_geometry );
		}

		glDetachShader( gl_p, gl_shader_vertex );
		glDeleteShader( gl_shader_vertex );

		glDetachShader( gl_p, gl_shader_fragment );
		glDeleteShader( gl_shader_fragment );
		return false;
	}
	// NOTE(theGiallo): compilation successfull => attach fragment shader
	glAttachShader( gl_p, gl_shader_fragment );


	// NOTE(theGiallo): link the program
	glLinkProgram( gl_p );

	// NOTE(theGiallo): check the program linking
	{
		GLint link_status = GL_FALSE;
		int info_log_length;
		glGetProgramiv(gl_p, GL_INFO_LOG_LENGTH, &info_log_length);
		if ( info_log_length > 0 )
		{
			char program_info_log[info_log_length+1];
			glGetProgramInfoLog( gl_p, info_log_length, NULL,
			                     program_info_log );
			log_info( "\nProgram linking infos:\n\n%s\n", program_info_log );
		}
		glGetProgramiv( gl_p, GL_LINK_STATUS, &link_status );
		if ( link_status == GL_FALSE )
		{
			log_err( "Error linking the program." );

			glDeleteProgram( gl_p );

			if ( geometry_shader_text )
			{
				glDetachShader( gl_p, gl_shader_geometry );
				glDeleteShader( gl_shader_geometry );
			}

			glDetachShader( gl_p, gl_shader_vertex );
			glDeleteShader( gl_shader_vertex );

			glDetachShader( gl_p, gl_shader_fragment );
			glDeleteShader( gl_shader_fragment );
			return false;
		}
	}

	glValidateProgram( gl_p );

	// NOTE(theGiallo): check the program validation
	{
		GLint validation_status = GL_FALSE;
		int info_log_length;
		glGetProgramiv(gl_p, GL_INFO_LOG_LENGTH, &info_log_length);
		if ( info_log_length > 0 )
		{
			char program_info_log[info_log_length+1];
			glGetProgramInfoLog( gl_p, info_log_length, NULL,
			                     program_info_log );
			log_info( "\nProgram validation infos:\n\n%s\n", program_info_log );
		}
		glGetProgramiv( gl_p, GL_VALIDATE_STATUS, &validation_status );
		if ( validation_status == GL_FALSE )
		{
			log_err( "Error validating the program." );

			glDeleteProgram( gl_p );

			if ( geometry_shader_text )
			{
				glDetachShader( gl_p, gl_shader_geometry );
				glDeleteShader( gl_shader_geometry );
			}

			glDetachShader( gl_p, gl_shader_vertex );
			glDeleteShader( gl_shader_vertex );

			glDetachShader( gl_p, gl_shader_fragment );
			glDeleteShader( gl_shader_fragment );
			return false;
		}
	}

	// NOTE(theGiallo): we will never modify or reuse the shader objects, so we can detach
	// and delete them.

	if ( geometry_shader_text )
	{
		glDetachShader( gl_p, gl_shader_geometry );
		glDeleteShader( gl_shader_geometry );
	}

	glDetachShader( gl_p, gl_shader_vertex );
	glDeleteShader( gl_shader_vertex );

	glDetachShader( gl_p, gl_shader_fragment );
	glDeleteShader( gl_shader_fragment );

	return true;
}

bool
create_program( const char * geom, const char * vert, const char * frag,
                tg::sys::Impl * sys, Mem_Stack * tmp_mem,
                GLuint * out_program )
{
	bool ret = false;

	s64 size_geom = {};
	u8 * mem_geom = {};
	if ( geom )
	{

		size_geom = sys->get_file_size( geom );
		if ( size_geom < 0 )
		{
			return ret;
		}

		u64 old_top = tmp_mem->first_free;
		mem_geom = tmp_mem->push( size_geom );
		if ( !mem_geom )
		{
			return ret;
		}

		s64 res = sys->read_entire_file( geom, mem_geom, size_geom );
		if ( TG_SYS_RET_IS_ERROR( res ) )
		{
			log_err( "failed to read geometry shader file '%s', error:\n%s\n%s",
			         geom, sys->get_error_name( res ),
			         sys->get_error_description( res ) );
			tmp_mem->first_free = old_top;
			return ret;
		}
	}

	s64 size_vert = sys->get_file_size( vert );
	if ( size_vert < 0 )
	{
		return ret;
	}

	u64 old_top = tmp_mem->first_free;
	u8 * mem_vert = tmp_mem->push( size_vert );
	if ( !mem_vert )
	{
		return ret;
	}

	s64 res = sys->read_entire_file( vert, mem_vert, size_vert );
	if ( TG_SYS_RET_IS_ERROR( res ) )
	{
		log_err( "failed to read vertex shader file '%s', error:\n%s\n%s",
		         vert, sys->get_error_name( res ),
		         sys->get_error_description( res ) );
		tmp_mem->first_free = old_top;
		return ret;
	}

	s64 size_frag = sys->get_file_size( frag );
	if ( size_frag < 0 )
	{
		tmp_mem->first_free = old_top;
		return ret;
	}

	u8 * mem_frag = tmp_mem->push( size_frag );
	if ( !mem_frag )
	{
		return ret;
	}
	res = sys->read_entire_file( frag, mem_frag, size_frag );
	if ( TG_SYS_RET_IS_ERROR( res ) )
	{
		log_err( "failed to read fragment shader file '%s', error:\n%s\n%s",
		         vert, sys->get_error_name( res ),
		         sys->get_error_description( res ) );
		tmp_mem->first_free = old_top;
		return ret;
	}

	GLuint program = {};
	if ( create_program_from_text( &program,
	                               (const char*)mem_geom, size_geom,
	                               (const char*)mem_vert, size_vert,
	                               (const char*)mem_frag, size_frag ) )
	{
		ret = true;
		if ( glIsProgram( *out_program ) )
		{
			glDeleteProgram( *out_program );
		}
		*out_program = program;
	}

	tmp_mem->first_free = old_top;
	return ret;
}

void
render_colored_vertices( Shader * shader,
                         Colored_Vertex * vertices, u32 vertices_in_buffer,
                         u32 vertices_count,
                         V4 * default_color,
                         GLuint vbo, GLenum draw_mode, GLenum buffer_usage )
{
	glUseProgram( shader->program );
	if ( shader->color_loc != -1 && default_color )
	{
		glUniform4f( shader->color_loc, default_color->r, default_color->g, default_color->b, default_color->a );// 0.0f, 0.05f, 0.4f, 0.5f );
	}

	if ( shader->M_loc != -1 )
	{
		glUniformMatrix4fv( shader->M_loc, 1, GL_FALSE, mx4_identity().arr );
	}

	u32 buffer_byte_size = vertices_in_buffer * sizeof( Colored_Vertex );
	glBindBuffer( GL_ARRAY_BUFFER, vbo );
	glBufferData( GL_ARRAY_BUFFER, buffer_byte_size,
	              NULL,  buffer_usage );
	glBufferData( GL_ARRAY_BUFFER, buffer_byte_size,
	              vertices,  buffer_usage );

	u32 stride = sizeof( Colored_Vertex );
	glEnableVertexAttribArray( shader->vertex_loc );
	glVertexAttribPointer( shader->vertex_loc, // location
	                       3,        // size
	                       GL_FLOAT, // type
	                       GL_FALSE, // normalized
	                       stride,   // stride
	                       (void*)0  // array buffer offset
	                     );

	glEnableVertexAttribArray( shader->vertex_color_loc );
	glVertexAttribPointer( shader->vertex_color_loc, // location
	                       3,        // size
	                       GL_FLOAT, // type
	                       GL_FALSE, // normalized
	                       stride,   // stride
	                       (void*)sizeof( V3 ) // array buffer offset
	                     );
	glDrawArrays( draw_mode, 0, vertices_count );

	glDisableVertexAttribArray( shader->vertex_loc );
	glDisableVertexAttribArray( shader->vertex_color_loc );
	glBindBuffer( GL_ARRAY_BUFFER, 0 );
	glUseProgram( 0 );
}

}; // namespace tg::ogl
