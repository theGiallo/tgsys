#if 0
	echo $#
	echo $0 $1 $2

	#echo fullpath should be $(readlink -e $0)
	#export THIS_FILE_PATH=$0
	export THIS_FILE_PATH=$(readlink -e $0)
	#export THIS_FILE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
	export THIS_FILE_DIR=$(dirname $THIS_FILE_PATH)
	export THIS_FILE_NAME=${THIS_FILE_PATH##*/}
	export THIS_FILE_BASE_NAME=${THIS_FILE_NAME%%.*}

	exec /bin/bash -c 'exec "/bin/bash" <(tail -n +15 "$0") "$@"' "$0" "$@"
	exit $!

	# IFS= read -r -d '' BASH_TEXT <<BASH_SECTION
	# !/bin/bash
	echo -n "curr dir: "
	pwd

	APP_NAME='"tgsyssdl_test"'

	REL_ROOT_PROJ=../../
	REL_BUILDS_DIR=../../builds/
	REL_DLIBS_DIR=../../builds/hotlibs/
	DLIBS_DIR_FULL_PATH="$THIS_FILE_DIR/$REL_DLIBS_DIR"
	DLIBS_DIR_EXEC_REL_PATH=./hotlibs/
	EXECUTABLE_FOLDER_FULL_PATH="$THIS_FILE_DIR/$REL_BUILDS_DIR"

	LIB_NAME=$THIS_FILE_BASE_NAME
	EXECUTABLE_NAME=$THIS_FILE_BASE_NAME
	LIB_FILE_NAME=lib${THIS_FILE_NAME%%.*}.so
	if [ -z "${LIB_VERSION}" ]
	then
		LIB_VERSION=$(date +%Y.%m.%d.%H.%M.%S);
	fi

	EXECUTABLE_FULL_PATH="$EXECUTABLE_FOLDER_FULL_PATH/$EXECUTABLE_NAME"

	echo "this file dir   = '$THIS_FILE_DIR'"
	echo "this file path  = '$THIS_FILE_PATH'"
	echo "this file name  = '$THIS_FILE_NAME'"
	echo "base file name  = '$THIS_FILE_BASE_NAME'"
	echo "executable name = '$EXECUTABLE_NAME'"
	echo "lib name        = '$LIB_NAME'"
	echo "app name        = '$APP_NAME'"
	echo
	echo args count: $#
	i=0
	for arg in $*; do
		echo -e "\targ $i: $arg"
		((i++))
	done
	echo

	if [ -z "${CXX+x}" ]
	then
		#CXX=arm-linux-gnueabihf-g++
		CXX=clang++
		#CXX=g++
	fi

	if [[ "${CXX}" == @(g++*|gcc*) ]]
	then

	CXX_FLAGS="-std=c++20 \
	           -Wall \
	           -Wextra \
	           -Wno-missing-braces \
	           -Wno-unused-function \
	           -Wno-comment \
	           -fno-exceptions \
	           -feliminate-unused-debug-types \
	           -rdynamic \
	           -ffunction-sections -Wl,--gc-sections \
	           -fvisibility=hidden \
	           -O0 -ggdb3 \
	           -march=native \
	           -DAPP_NAME='$APP_NAME' \
	           -DEXECUTABLE_NAME='\"$EXECUTABLE_NAME\"' \
	           -DTGHOT_DLIBS_DIR='\"$DLIBS_DIR_EXEC_REL_PATH\"' \
	           $CXX_FLAGS"

	elif [[ $CXX == clang* ]]
	then
	CXX_FLAGS="-std=c++20 \
	           -Wall \
	           -Wextra \
	           -Wno-missing-braces \
	           -Wno-unused-function \
	           -Wno-comment \
	           -Rpass-missed=loop-vectorize -Rpass-analysis=loop-vectorize \
	           -fno-exceptions \
	           -feliminate-unused-debug-types \
	           -Wl,--export-dynamic \
	           -fvisibility=hidden \
	           -flto=thin \
	           -O0 -ggdb3 \
	           -march=native \
	           -DAPP_NAME='$APP_NAME' \
	           -DEXECUTABLE_NAME='\"$EXECUTABLE_NAME\"' \
	           -DTGHOT_DLIBS_DIR='\"$DLIBS_DIR_EXEC_REL_PATH\"' \
	           $CXX_FLAGS"
	fi

	#          -fvisibility=hidden \ makes all functions internal to the compilation unit, and exported only if specified
	#          -fsave-optimization-record \ clang outputs text file for what it did
	#          -O3 \
	#          -O0 -ggdb3 \
	# -fwhole-program # ensures this CU represents the whole program, enabling more aggressive opts. Don t use if -flto is present
	# -flto=auto # link time optimization ( removes unused functions ) (g++ only)
	# -flto=thin # clang outputs small .so http://blog.llvm.org/2016/06/thinlto-scalable-and-incremental-lto.html
	# -ffunction-sections -Wl,--gc-sections # remove unused functions (this is also achieved with -flto), separating code from data
	# -fpermissive # downgrades some errors to warnings for non-conformant code
	#-fPIC # position independent code, useful for linked libraries

	declare -a DEFINES
	DEFINES[${#DEFINES[@]}]=DEBUG=1

	declare -a INCLUDE_DIRS
	INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]=$THIS_FILE_DIR
	INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]=$THIS_FILE_DIR/../
	INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]=$THIS_FILE_DIR/$REL_ROOT_PROJ"libs"
	INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]=$THIS_FILE_DIR/$REL_ROOT_PROJ"src"
	INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]=$THIS_FILE_DIR/$REL_ROOT_PROJ"src/libs"

	declare -a LIBS_DIRS
	LIBS_DIRS[${#LIBS_DIRS[@]}]=$THIS_FILE_DIR/$REL_ROOT_PROJ"src/"
	LIBS_DIRS[${#LIBS_DIRS[@]}]=$THIS_FILE_DIR/$REL_ROOT_PROJ"src/libs/SDL2/linux/lib/"
	#LIBS_DIRS[${#LIBS_DIRS[@]}]=$THIS_FILE_DIR/$REL_ROOT_PROJ"libraries/"

	declare -a LIBS
	LIBS[${#LIBS[@]}]=pthread
	LIBS[${#LIBS[@]}]=dl
	#LIBS[${#LIBS[@]}]=rt

	function \
	defines()
	{
		for d in ${DEFINES[@]}
		do
			printf " -D$d "
		done
	}
	function \
	include_dirs()
	{
		for p in ${INCLUDE_DIRS[@]}
		do
			printf " -I$p "
		done
	}
	function \
	libs_dirs()
	{
		for p in ${LIBS_DIRS[@]}
		do
			printf " -L$p "
	#printf " -Wl,-rpath=$p "
		done
	}
	function \
	link_libs()
	{
		for l in ${LIBS[@]}
		do
			printf " -l$l "
		done
	}

	ret=3

	echo "calling compiler"

	# -Wl,-z,origin libfoo.so -Wl,-rpath,./
	CMD="$CXX $CXX_FLAGS `defines` `include_dirs` -o "$EXECUTABLE_FULL_PATH" $THIS_FILE_PATH    `libs_dirs` `link_libs` -Wl,--disable-new-dtags "
	echo CMD:
	echo $CMD
	RUNNABLE_CMD=$(echo $CMD | sed "s/'\"/\"/g" | sed "s/\"'/\"/g")
	echo RUNNABLE_CMD:
	echo $RUNNABLE_CMD

	#echo
	STRIPPED_CMD=$(echo $CMD | sed 's/"/\\"/g')
	#echo
	#echo $STRIPPED_CMD

	JSON="[\n{\n\t\"directory\":\"$THIS_FILE_DIR/$REL_ROOT_PROJ\",\n\t\"command\":\"$STRIPPED_CMD\",\n\t\"file\":\"$THIS_FILE_PATH\"\n},\n]";
	echo -e $JSON > compile_commands.json

	TIME="real %E | user %U | sys %S | max mem %M kB | avg mem %K kB" time $RUNNABLE_CMD

	ret=$?;

	if [ $ret -eq 0 ]
	then
		printf 'compilation succeeded\n'
		file $EXECUTABLE_FULL_PATH
		ls -l $EXECUTABLE_FULL_PATH

		echo "changing pwd to $EXECUTABLE_FOLDER_FULL_PATH"
		cd "$EXECUTABLE_FOLDER_FULL_PATH"
		echo "running ./$EXECUTABLE_NAME $@"
		echo
		chmod +x ./$EXECUTABLE_NAME
		#./$EXECUTABLE_NAME "$@"
		gdb --args ./$EXECUTABLE_NAME "$@"
		#gdb -ex run ./$EXECUTABLE_NAME --args "$@"

		ret=$?

		if [ ! $ret -eq 0 ]
		then
			printf "run failed\n"
		fi
		#trash "$EXECUTABLE_NAME"
		#trash "$EXECUTABLE_NAME.o"
	else
		printf 'compilation failed\n' >&2;
	fi

	echo "exiting $ret"
	exit $ret
	# BASH_SECTION

	# bash -c "exec <($BASH_TEXT)"
	# ret=$?
	# exit $ret
#endif


#include "basic_types.h"
#include "macro_tools.h"
#include "tghot.h"
#include "tgsys.h"
#include "tgapp.h"

#include "argparse.h"

using namespace tg::hot;
using namespace tg::sys;

struct
Context
{
	bool running;
};

void
tg_sys_impl_loaded( Impl * sys_impl )
{
	*((bool*)sys_impl->custom_data) = true;
}

#if 0
// void name( tg::sys::Impl * sys_impl, void * callback_data, Event * event )
TG_SYS_EVENT_CALLBACK( event_callback )
{
	Context * context = (Context*)callback_data;
	switch ( event->type )
	{
		case Event_Type::KEYBOARD:
			if ( event->keyboard.type == Event_Keyboard_Type::KEY_PRESSED
			  && event->keyboard.phys_key == PK::F4
			  && ( ( event->keyboard.mods & (u16)Key_Mod::LALT ) || ( event->keyboard.mods & (u16)Key_Mod::RALT ) )
			)
			{
				// TODO(theGiallo, 2021-04-15): this does not work
				log_dbg( "asdfkjsd;flkjs;a" );
				context->running = false;
			}
			break;
		case Event_Type::GAME_SYSTEM:
		case Event_Type::MOUSE:
		case Event_Type::PAD:
		case Event_Type::WINDOW:
			break;
		default: break;
	}
}
#endif

s32
main( s32 argc, const char * argv[] )
{
	(void)argc;
	(void)argv;

	s32 ret = 1;

	const char * app_name = "app";


	static const char *const usage[] = {
	    EXECUTABLE_NAME " [options] [[--] args]",
	    NULL,
	};

	argparse_option options[] = {
		OPT_HELP(),
		OPT_STRING('a', "app", &app_name, "lib name of the app to run", NULL, NULL, NULL),
		//OPT_STRING('w', "weights", &load_from_file_path, "file path to load weights from", NULL, NULL, NULL),
		//OPT_INTEGER('L', "layer", &target_layer, "target layer index", NULL, NULL, OPT_NONEG ),
		//OPT_FLOAT('o', "ovalue", &other_nodes_values, "other nodes value", NULL, NULL, NULL),
		//OPT_BIT('W', "viz-weights", &viz_weights, "generate an image for each W vector", NULL, 1, NULL ),
		OPT_END(),
	};

	struct argparse argparse;
	argparse_init( &argparse, options, usage, 0 );
	argparse_describe( &argparse, "\nno description", "\n" );
	argc = argparse_parse( &argparse, argc, argv );


	bool tg_sys_impl_is_loaded = false;
	tg::sys::Impl sys_impl = {};
	sys_impl.custom_data = (void *)&tg_sys_impl_is_loaded;
	Context context = {};

	tg::sys::request_impl( &sys_impl, "tgsyssdl", "main", tg_sys_impl_loaded );
	bool b_res;
	b_res = tg::app::request_dl( app_name, "main" );

	while ( ! tg_sys_impl_is_loaded ) {};

	log_dbg( "tg sys sdl loaded" );

	//sys_impl.set_event_callback( event_callback, (void*)&context );

	#if 0
	s32 window_id = sys_impl.create_window( "Hello world", {3,3}, false, true, Screen_Mode::WINDOWED, false, 800, 600 );
	if ( window_id < 0 )
	{
		log_err( "failed to create window: %s", sys_impl.get_error_name( window_id ) );
		return ret;
	}
	#endif

	context.running = true;
	while ( context.running )
	{
		//log_dbg( "#  #  #  #  #  #  #  #  #  #  iteration %d", it );
		u64 ns_start = ns_CPU_time();
		tg::hot::reload_if_you_want();
		u64 ns_end   = ns_CPU_time();
		u64 reload_if_you_want_ns = ns_end - ns_start;

		if ( tg::app::update )
		{
			context.running = tg::app::update( &sys_impl );
		}

		//sys_impl.poll_all_events_to_callback();
		//sys_impl.update_time( 1 );

		#if 0
		fd_set rfds = {};
		FD_ZERO( &rfds );
		FD_SET( STDIN_FILENO, &rfds );

		struct timeval tv = {};
		tv.tv_usec = 16666;
		s32 retval = select(1, &rfds, NULL, NULL, &tv);

		if ( FD_ISSET( STDIN_FILENO, &rfds ) )
		{
			log_dbg( "stdin is set" );
		}

		if ( retval == -1 )
		{
			perror( "select()" );
		} else
		if (retval)
		{
			log_dbg( "Data is available now.\n" );
			char buf[1024] = {};
			char line[1024] = {};
			char * res = fgets( buf, sizeof ( buf ) - 1, stdin );
			if ( res )
			{
				tg_assert( buf[sizeof( buf ) - 1] == 0 );
				s32 read = 0;
				s32 el_read = 0;
				do
				{
					s32 line_len = 0;
					el_read = sscanf( buf + read, "%s\n%n", line, &line_len );
					if ( el_read == 1 )
					{
						read += line_len;
						log_dbg( "read (%d): %s", read, buf );
						if ( string_equal( line, "exit" )
						  || string_equal( line, "quit" ) )
						{
							run = false;
							break;
						} else
						if ( string_equal( line, "help" ) )
						{
							log_info( "type 'exit' or 'quit' to exit the program\ntype 'perf' to get tg::hot timings" );
						}
						else
						if ( string_equal( line, "perf" ) )
						{
							log_info( "reload_if_you_want took %lu ns that is %f ms",
							          reload_if_you_want_ns, reload_if_you_want_ns / (f64)1e6 );
						}
					}
				} while ( el_read == 1 );
			}
		} else
		{
			//printf("No data within five seconds.\n");
		}
		#endif
	}

	#if 0
	b_res = tg::hot::request_dl( Some_Other_Library::dl, dl_load_callback_some_other_library, "main" );
	if ( b_res )
	{
		log_err( "request_dl failed" );
		return 1;
	}
	#endif

	ret = 0;

	return ret;
}

#include "argparse.c"
#include "tghot.cpp"
#include "pool_allocator_generic_vm.cpp"
#include "hash_table_generic_vm.cpp"
#include "hash_table_generic_multi_vm.cpp"
#include "utility.cpp"
#include "system.cpp"
#include "tgmath.cpp"
