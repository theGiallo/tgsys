#ifndef _CONTROLS_UTILITIES_H_
#define _CONTROLS_UTILITIES_H_ 1

#include "tgsys.h"
#include "tgogl.h"
#include "tgmath.h"

namespace tg::controls_utilities
{
	using namespace tg::math;
	using namespace tg::ogl;
	using namespace tg::sys;

	void
	warp_mouse_to_center( Impl * sys, s32 window_id );

	union
	Controlled_Motion
	{
		struct
		{
			f32 side;
			f32 straight;
			f32 vertical;
		};
		V3 v3;
	};
	struct
	Distributed_Speed
	{
		Controlled_Motion positive, negative;
	};


	struct
	First_Person
	{
		enum class
		Mouse_Motion
		{
			COMPUTE_PER_FRAME_MOTION,
			COMPUTE_EVERY_MOTION,
		};

		struct
		Mapping
		{
			PK ahead;
			PK back;
			PK left;
			PK right;
			PK up;
			PK down;
			PK speed_up;
			PK rot_left;
			PK rot_right;

			static
			inline
			Mapping
			DEFAULT()
			{
				Mapping ret = {
					.ahead     = PK::W,
					.back      = PK::S,
					.left      = PK::A,
					.right     = PK::D,
					.up        = PK::SPACE,
					.down      = PK::C,
					.speed_up  = PK::LSHIFT,
					.rot_left  = PK::Q,
					.rot_right = PK::E,
				};
				return ret;
			}
		};

		Mapping           mapping;

		Controlled_Motion _motion;
		Distributed_Speed _distributed_speed;


		// NOTE(theGiallo):
		// dir should be positive if the key is ahead or right
		// dir should be negative if the key is back or left
		inline
		void
		_manage_motion_key( Inputs * inputs, Phys_Key key, s32 dir, f32 * var )
		{
			//s32 is_pressed = (s32)game_mem->inputs.keyboard_state[key];
			s32 pre_rel =
			(s32)inputs->keyboard_pressions[(s32)key] -
			(s32)inputs->keyboard_releases [(s32)key];
			if ( pre_rel > 0 )
			{
				*var += 1.0f * sign( dir );
			} else
			if ( pre_rel < 0 )
			{
				*var -= 1.0f * sign( dir );
			}
		}

		// NOTE(theGiallo): ahead, right and up are supposed to be normalized
		// motion values are supposed to be +1, 0 or-1
		inline
		void
		_apply_controlled_motion( V3 ahead, V3 right, V3 up, f32 speed_mod, Time delta_time, V3 * pos )
		{
			*pos +=
			(
			   ahead *
			      ( _motion.straight *
			         ( _motion.straight > 0.0f ?
			            _distributed_speed.positive.straight :
			            _distributed_speed.negative.straight ) ) +
			   right *
			      ( _motion.side     *
			         ( _motion.side > 0.0f ?
			            _distributed_speed.positive.side :
			            _distributed_speed.negative.side )     ) +
			   up    *
			      ( _motion.vertical *
			         ( _motion.vertical > 0.0f ?
			            _distributed_speed.positive.vertical :
			            _distributed_speed.negative.vertical ) )
			) * ( delta_time * speed_mod );
		}

		inline
		void
		_apply_controlled_motion( Camera3D * camera, f32 speed_mod, Time delta_time )
		{
			_apply_controlled_motion( normalize( camera->look ),
			                          normalize( camera->look ^ camera->up ),
			                          camera->up,
			                          speed_mod,
			                          delta_time, &camera->pos );
		}

		inline
		bool
		_mouse_motion( Impl * sys, s32 window_id,
		               s32 pos_x, s32 pos_y,
		               s32 motion_x, s32 motion_y, Camera3D * camera, bool warp_to_center )
		{
			bool ret = false;
			V2u32 window_size = {};
			V2u32 window_center = {};
			sys->get_window_size( window_id, &window_size.x, &window_size.y );
			window_center = window_size / 2;

			if ( pos_x != (s32)window_center.x ||
			     pos_y != (s32)window_center.y )
			{
				ret = true;
				V2  win_size     = (V2)window_size;
				f32 angle_per_px = camera->fov / win_size.w;
				f32 u_a          = -angle_per_px * motion_x;
				f32 r_a          =  angle_per_px * motion_y;
				V3  right        = normalize( camera->look ^ camera->up );
				Mx4 rot_right    = rotation_mx4( right, r_a );
				Mx4 rot_up       = rotation_mx4( camera->up, u_a );
				Mx4 rot          = rot_up * rot_right;

				camera->look = normalize( rot * camera->look );
				camera->update_view();

				if ( warp_to_center )
				{
					sys->warp_mouse_in_window(
					   window_id,
					   window_center.x, window_center.y );
				}
			}

			return ret;
		}

		inline
		void
		per_frame_update( Impl * sys, Inputs * inputs, s32 window_id, Camera3D * camera, f32 delta_time, Mouse_Motion mouse_motion_options, f32 speed_up_mod = 10.0f )
		{
			_manage_motion_key( inputs, mapping.ahead,  1, & _motion.straight );
			_manage_motion_key( inputs, mapping.back , -1, & _motion.straight );
			_manage_motion_key( inputs, mapping.right,  1, & _motion.side     );
			_manage_motion_key( inputs, mapping.left , -1, & _motion.side     );
			_manage_motion_key( inputs, mapping.up   ,  1, & _motion.vertical );
			_manage_motion_key( inputs, mapping.down , -1, & _motion.vertical );

			f32 speed_mod = 1.0f;
			if ( inputs->keyboard_state[(s32)mapping.speed_up] )
			{
				speed_mod = speed_up_mod;
			}

			_apply_controlled_motion( camera, speed_mod, delta_time );

			f32 a = 5.0f * delta_time * speed_mod;

			if (  inputs->keyboard_state[(s32)mapping.rot_left ] &&
			     !inputs->keyboard_state[(s32)mapping.rot_right] )
			{
				Mx4 rot_up = rotation_mx4( camera->up, a );
				camera->look = normalize( rot_up * camera->look );
			}

			if ( !inputs->keyboard_state[(s32)mapping.rot_left ] &&
			      inputs->keyboard_state[(s32)mapping.rot_right] )
			{
				Mx4 rot_up = rotation_mx4( camera->up, -a );
				camera->look = normalize( rot_up * camera->look );
			}

			camera->check_up();
			camera->update_view();

			switch ( mouse_motion_options )
			{
				case Mouse_Motion::COMPUTE_PER_FRAME_MOTION:
				{
					V2 pos    = inputs->mouse_pos;
					V2 motion = inputs->mouse_motion_total_in_frame;
					_mouse_motion( sys,  window_id, pos.x, pos.y, motion.x, motion.y, camera, true );
					break;
				}
				case Mouse_Motion::COMPUTE_EVERY_MOTION:
				{
					for_0M( i, inputs->mouse_motions_during_frame_count )
					{
						V2s32 pos    = inputs->mouse_positions_during_frame[i];
						V2s32 motion = inputs->mouse_motions_during_frame  [i];
						_mouse_motion( sys,  window_id, pos.x, pos.y, motion.x, motion.y, camera, i == i__count - 1 );
					}
					break;
				}
			}
		}
	};
}; // namespace tg::controls_utilities

#endif /* ifndef _CONTROLS_UTILITIES_H_ */
