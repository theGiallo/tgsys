#ifndef SDL_PLATFORM_H
#define SDL_PLATFORM_H 1

#include "sdl_platform_lepre.h"

#include "generic_system.h"

// NOTE(theGiallo): return codes
#define SYS_ERR_EXPAND_AS_ENUM(a,b,c) a = b,
#define SYS_ERR_EXPAND_AS_TABLE_ENTRY(a,b,c) { LPR_TOSTRING( a ), c },
#define SYS_ERR_TABLE(ENTRY) \
        ENTRY( SYS_ERR_TOO_MANY_WINDOWS,        1, "" ) \
        ENTRY( SYS_ERR_CREATING_WINDOW ,        2, "" ) \
        ENTRY( SYS_ERR_CREATING_CONTEXT,        3, "" ) \
        ENTRY( SYS_ERR_NO_DISPLAYS,             4, "" ) \
        ENTRY( SYS_ERR_MAKING_WINDOW_CURRENT,   5, "" ) \
        ENTRY( SYS_ERR_LOADING_GL_FUNCTIONS,    6, "" ) \
        ENTRY( SYS_ERR_FILE_OPEN_FAILED,        7, "" ) \
        ENTRY( SYS_ERR_MEMORY_BUFFER_TOO_SMALL, 8, "" )

#define SYS_ERR_TO_TABLE_ID(e) (-(e)-1)
enum
Sys_Err_Codes : u8
{
	SYS_ERR_TABLE( SYS_ERR_EXPAND_AS_ENUM )
	// ---
	SYS_ERR_COUNT_PLUS_ONE
};

#define SYS_SCREEN_MODE_EXPAND_AS_SWITCH_SDL_TO_SYS(a,b) case b: mode = a; break;
#define SYS_SCREEN_MODE_EXPAND_AS_ENUM(a,b) a,
#define SYS_SCREEN_MODE_EXPAND_AS_TABLE_ENTRY(a,b) { LPR_TOSTRING(a), (b) },
#define SYS_SCREEN_MODE_TABLE(ENTRY) \
        ENTRY( SYS_SCREEN_MODE_WINDOWED,           0 ) \
        ENTRY( SYS_SCREEN_MODE_FULLSCREEN,         SDL_WINDOW_FULLSCREEN ) \
        ENTRY( SYS_SCREEN_MODE_FULLSCREEN_DESKTOP, SDL_WINDOW_FULLSCREEN_DESKTOP )
// NOTE(theGiallo): SDL_WINDOW_FULLSCREEN_DESKTOP ignores given res and uses
// native one, but it's a fake one, does not change videomode
#if 0
enum
Sys_Screen_Mode : u8
{
	SYS_SCREEN_MODE_TABLE( SYS_SCREEN_MODE_EXPAND_AS_ENUM )
	// ---
	SYS_SCREEN_MODE_COUNT
};
#endif
struct System_API_Screen_Mode_Table_Row
{
	const char * str;
	u32 sdl_flag;
};

struct System_API_Err_Table_Row
{
	const char * name,
	           * description;
};

struct System_API_Tables
{
	System_API_Screen_Mode_Table_Row screen_modes[SYS_SCREEN_MODE_COUNT];
	System_API_Err_Table_Row         errors[SYS_ERR_COUNT_PLUS_ONE-1];
};


#endif /* ifndef SDL_PLATFORM_H */
