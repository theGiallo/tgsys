#ifndef __POOL_ALLOCATOR_GENERIC_VM_H__
#define __POOL_ALLOCATOR_GENERIC_VM_H__ 1

#include "basic_types.h"

struct
Pool_Allocator_Generic_VM;

// NOTE(theGiallo): if initial_capacity == -1 its set to the real max_capacity
// NOTE(theGiallo): if capacity_increment == 0 its set to `page_size / element_size` or 8 if page size fails to be read
// NOTE(theGiallo): capacity_increment and initial_capacity when used are grown up to fill a page
bool
pool_allocator_generic_vm_init( Pool_Allocator_Generic_VM * pa, u64 element_size, s64 max_capacity, s64 initial_capacity = 0, s64 capacity_increment = 0 );

void *
pool_allocator_generic_vm_allocate( Pool_Allocator_Generic_VM * pa );

void *
pool_allocator_generic_vm_allocate_clean( Pool_Allocator_Generic_VM * pa );

void
pool_allocator_generic_vm_deallocate( Pool_Allocator_Generic_VM * pa, void * p );

bool
pool_allocator_generic_vm_is_current_capacity_full( Pool_Allocator_Generic_VM * pa );

bool
pool_allocator_generic_vm_is_max_capacity_full( Pool_Allocator_Generic_VM * pa );

bool
pool_allocator_generic_vm_release_unused_pm( Pool_Allocator_Generic_VM * pa );

bool
pool_allocator_generic_vm_release_unused_pm_to_capacity( Pool_Allocator_Generic_VM * pa, u64 target_capacity );

bool
pool_allocator_generic_vm_up_capacity_to( Pool_Allocator_Generic_VM * pa, s64 target_capacity );

void
pool_allocator_generic_vm_destroy( Pool_Allocator_Generic_VM * pa );

struct
Pool_Allocator_Generic_VM
{
	void * vm_start;
	void * touched_end;
	u64    reserved_size;
	s64    max_capacity;
	s64    current_capacity;
	s64    capacity_increment;
	u64    element_size;
	s64    id_of_first_free;
	s64    id_of_last_free;
	s64    occupancy;

	// NOTE(theGiallo): if initial_capacity == -1 its set to the real max_capacity
	// NOTE(theGiallo): if capacity_increment == 0 its set to `page_size / element_size` or 8 if page size fails to be read
	// NOTE(theGiallo): capacity_increment and initial_capacity when used are grown up to fill a page
	bool
	init( u64 element_size, s64 max_capacity, s64 initial_capacity = 0, s64 capacity_increment = 0 )
	{
		return pool_allocator_generic_vm_init( this, element_size, max_capacity, initial_capacity, capacity_increment );
	}

	void *
	allocate()
	{
		return pool_allocator_generic_vm_allocate( this );
	}

	void *
	allocate_clean()
	{
		return pool_allocator_generic_vm_allocate_clean( this );
	}

	void
	deallocate( void * p )
	{
		pool_allocator_generic_vm_deallocate( this, p );
	}

	bool
	is_current_capacity_full()
	{
		return pool_allocator_generic_vm_is_current_capacity_full( this );
	}

	bool
	is_max_capacity_full()
	{
		return pool_allocator_generic_vm_is_max_capacity_full( this );
	}

	bool
	release_unused_pm()
	{
		return pool_allocator_generic_vm_release_unused_pm( this );
	}

	bool
	release_unused_pm_to_capacity( u64 target_capacity )
	{
		return pool_allocator_generic_vm_release_unused_pm_to_capacity( this, target_capacity );
	}

	bool
	up_capacity_to( s64 target_capacity )
	{
		return pool_allocator_generic_vm_up_capacity_to( this, target_capacity );
	}

	void
	destroy()
	{
		pool_allocator_generic_vm_destroy( this );
	}
};

inline
bool
operator == ( const Pool_Allocator_Generic_VM & l, const Pool_Allocator_Generic_VM & r )
{
	bool ret = l.vm_start           == r.vm_start
	        && l.touched_end        == r.touched_end
	        && l.reserved_size      == r.reserved_size
	        && l.max_capacity       == r.max_capacity
	        && l.current_capacity   == r.current_capacity
	        && l.capacity_increment == r.capacity_increment
	        && l.element_size       == r.element_size
	        && l.id_of_first_free   == r.id_of_first_free
	        && l.id_of_last_free    == r.id_of_last_free
	        && l.occupancy          == r.occupancy;
	return ret;
}

#endif /* ifndef __POOL_ALLOCATOR_GENERIC_VM_H__ */
