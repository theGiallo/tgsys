#ifndef _TG_MATH__LPR_H_
#define _TG_MATH__LPR_H_ 1

#include "app_lepre.h"
#include "tgmath.h"

namespace tg::math
{
	inline
	Lpr_V2
	make_Lpr_V2( V2 lv )
	{
		Lpr_V2 ret { lv.x, lv.y };
		return ret;
	}
	inline
	V2
	make_V2( Lpr_V2 lv )
	{
		V2 ret { lv.x, lv.y };
		return ret;
	}
	inline
	Lpr_V2u32
	make_Lpr_V2u32( V2u32 lv )
	{
		Lpr_V2u32 ret { lv.x, lv.y };
		return ret;
	}
	inline
	V2u32
	make_V2u32( Lpr_V2u32 lv )
	{
		V2u32 ret { lv.x, lv.y };
		return ret;
	}
	inline
	Lpr_Rgb_f32
	make_Lpr_Rgb_f32( V3 v )
	{
		Lpr_Rgb_f32 ret { v.r, v.g, v.b };
		return ret;
	}
	inline
	Lpr_Rgb_u8
	make_Lpr_Rgb_u8( V3u8 v )
	{
		Lpr_Rgb_u8 ret { v.r, v.g, v.b };
		return ret;
	}
	inline
	Lpr_Rgba_f32
	make_Lpr_Rgba_f32( V4 v )
	{
		Lpr_Rgba_f32 ret { v.r, v.g, v.b, v.a };
		return ret;
	}
	inline
	Lpr_Rgba_u8
	make_Lpr_Rgba_u8( V4u8 v )
	{
		Lpr_Rgba_u8 ret { v.r, v.g, v.b, v.a };
		return ret;
	}
}
#endif /* ifndef _TG_MATH__LPR_H_ */
