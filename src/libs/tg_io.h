#ifndef _TG_IO_H
#define _TG_IO_H 1

#include <cstdio>
#include "basic_types.h"
#include "macro_tools.h"

namespace tg
{
namespace IO
{
	#define tg_io_read_one( f, ptr ) tg::IO::read( f, ptr, sizeof( * (ptr) ) )
	#define tg_io_read_n( f, ptr, n ) tg::IO::read( f, ptr, sizeof( * (ptr) ), n )
	#define tg_io_write_one( f, ptr ) tg::IO::write( f, ptr, sizeof( * (ptr) ) )
	#define tg_io_write_n( f, ptr, n ) tg::IO::write( f, ptr, sizeof( * (ptr) ), n )

	inline
	bool
	read( FILE * f, void * out, size_t el_size, size_t el_count = 1 )
	{
		bool ret = false;

		size_t n_read = fread( out, el_size, el_count, f );

		ret = el_count == n_read;

		return ret;
	}
	inline
	bool
	write( FILE * f, void * buf, size_t el_size, size_t el_count = 1 )
	{
		bool ret = false;

		size_t n_written = fwrite( buf, el_size, el_count, f );

		ret = el_count == n_written;

		return ret;
	}

	inline
	void
	invert_endianess( s64 * ptr )
	{
		s64 o = *ptr;
		*ptr = ( ( o << 40 ) & 0xff00000000000000 )
		     | ( ( o << 32 ) & 0x00ff000000000000 )
		     | ( ( o << 24 ) & 0x0000ff0000000000 )
		     | ( ( o <<  8 ) & 0x000000ff00000000 )
		     | ( ( o >>  8 ) & 0x00000000ff000000 )
		     | ( ( o >> 24 ) & 0x0000000000ff0000 )
		     | ( ( o >> 32 ) & 0x000000000000ff00 )
		     | ( ( o >> 40 ) & 0x00000000000000ff );
	}
	inline
	void
	invert_endianess( u64 * ptr )
	{
		invert_endianess( (s64*)ptr );
	}
	inline
	void
	invert_endianess( s32 * ptr )
	{
		s32 o = *ptr;
		*ptr = ( ( o << 24 ) & 0xff000000 )
		     | ( ( o <<  8 ) & 0x00ff0000 )
		     | ( ( o >>  8 ) & 0x0000ff00 )
		     | ( ( o >> 24 ) & 0x000000ff );
	}
	inline
	void
	invert_endianess( u32 * ptr )
	{
		invert_endianess( (s32*)ptr );
	}
	inline
	void
	invert_endianess( s16 * ptr )
	{
		s16 o = *ptr;
		*ptr = ( ( o <<  8 ) & 0xff00 )
		     | ( ( o >>  8 ) & 0x00ff );
	}
	inline
	void
	invert_endianess( u16 * ptr )
	{
		invert_endianess( (s16*)ptr );
	}
};
};

#endif /* ifndef _TG_IO_H */
