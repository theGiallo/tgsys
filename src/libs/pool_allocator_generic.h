#ifndef __POOL_ALLOCATOR_GENERIC_H__
#define __POOL_ALLOCATOR_GENERIC_H__ 1

#include "basic_types.h"

struct
Pool_Allocator_Generic
{
	void * memory;
	s64 capacity;
	u64 element_size;
	s64 id_of_first_free;
	s64 occupancy;
};

void
pool_allocator_generic_init( Pool_Allocator_Generic * pa, void * memory, s64 capacity, u64 element_size );

void *
pool_allocator_generic_allocate( Pool_Allocator_Generic * pa );

void *
pool_allocator_generic_allocate_clean( Pool_Allocator_Generic * pa );

void
pool_allocator_generic_deallocate( Pool_Allocator_Generic * pa, void * p );

bool
pool_allocator_generic_is_full( Pool_Allocator_Generic * pa );

#endif /* ifndef __POOL_ALLOCATOR_GENERIC_H__ */
