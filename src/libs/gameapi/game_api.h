#ifndef GAME_API_H
#define GAME_API_H 1

#if DEBUG
#define DEBUG_GUI_ENABLED 1
#endif

#include "generic_system.h"

struct System_API;
struct Game_Memory;

#define GAME_API_CYCLE_BEGINNING( name ) void name( System_API * sys_api, Game_Memory * game_mem )
typedef GAME_API_CYCLE_BEGINNING( Game_API_Cycle_Beginning );
extern "C" GAME_API_CYCLE_BEGINNING( game_api_cycle_beginning );

#define GAME_API_INPUT( name ) void name( System_API * sys_api, Game_Memory * game_mem, Event * e )
typedef GAME_API_INPUT( Game_API_Input );
extern "C" GAME_API_INPUT( game_api_input );

#define GAME_API_RENDER( name ) void name( System_API * sys_api, Game_Memory * game_mem )
typedef GAME_API_RENDER( Game_API_Render );
extern "C" GAME_API_RENDER( game_api_render );

#define GAME_API_UPDATE( name ) void name( System_API * sys_api, Game_Memory * game_mem, Time game_time, Time delta_time )
typedef GAME_API_UPDATE( Game_API_Update );
extern "C" GAME_API_UPDATE( game_api_update );

#define GAME_API_AUDIO_CALLBACK( name ) void name( System_API * sys_api, Game_Memory * game_mem, f32 * buf, u32 len )
typedef GAME_API_AUDIO_CALLBACK( Game_API_Audio_Callback );
extern "C" GAME_API_AUDIO_CALLBACK( game_api_audio_callback );

#ifndef LPR_APP_NAME
	#error Why is LEPRE_APP_NAME not defined?!
#endif

#include "game_api_lepre.h"
#include "intrinsics.h"
#include "tgmath.h"
#include "game_api_utility.h"
#include "game_api_fft.h"

struct
Inputs
{
	V2 mouse_pos;
	V2 mouse_motion_total_in_frame;
	V2s32 mouse_motions_during_frame  [1024];
	V2s32 mouse_positions_during_frame[1024];
	u32 mouse_motions_during_frame_count;
	bool mouse_clicked_left;
	bool mouse_clicked_right;
	bool mouse_clicked_wheel;
	bool mouse_button_left_down;
	bool mouse_button_left_pressed;
	bool mouse_button_left_released;
	bool mouse_button_right_down;
	bool mouse_button_right_pressed;
	bool mouse_button_right_released;
	bool mouse_button_wheel_down;
	bool mouse_button_wheel_pressed;
	bool mouse_button_wheel_released;
	s32 mouse_wheel_x;
	s32 mouse_wheel_y;
	s32 mouse_window_id;
	u8 keyboard_pressions[PK_COUNT];
	u8 keyboard_releases [PK_COUNT];
	u8 keyboard_pressions_with_reps[PK_COUNT];
	u8 keyboard_releases_with_reps [PK_COUNT];
	const u8 * keyboard_state;
	s32 keyboard_state_byte_size;

	// ---
	bool game_api_dl_reloaded;

	inline
	void
	init()
	{
		mouse_wheel_x = 0;
		mouse_wheel_y = 0;
		mouse_motion_total_in_frame = mouse_pos;
		mouse_motions_during_frame_count = 0;
		mouse_clicked_left  = false;
		mouse_clicked_right = false;
		mouse_clicked_wheel = false;
		mouse_button_left_pressed  = false;
		mouse_button_right_pressed = false;
		mouse_button_wheel_pressed = false;
		mouse_button_left_released  = false;
		mouse_button_right_released = false;
		mouse_button_wheel_released = false;
		memset( keyboard_pressions, 0,
		        sizeof( keyboard_pressions ) );
		memset( keyboard_releases, 0,
		        sizeof( keyboard_releases ) );
		memset( keyboard_pressions_with_reps, 0,
		        sizeof( keyboard_pressions_with_reps ) );
		memset( keyboard_releases_with_reps, 0,
		        sizeof( keyboard_releases_with_reps ) );

		game_api_dl_reloaded = false;
	}

	inline
	bool
	handle_event( Event * e )
	{
		bool ret = false;

		switch ( e->type )
		{
			case EVENT_KEYBOARD:
				switch ( e->keyboard.type )
				{
					case EVENT_KEYBOARD_KEY_PRESSED:
						if ( !e->keyboard.repeat )
						{
							++keyboard_pressions[e->keyboard.phys_key];
						}
						++keyboard_pressions_with_reps[e->keyboard.phys_key];
						ret = true;
						break;
					case EVENT_KEYBOARD_KEY_RELEASED:
						if ( !e->keyboard.repeat )
						{
							++keyboard_releases[e->keyboard.phys_key];
						}
						++keyboard_releases_with_reps[e->keyboard.phys_key];
						ret = true;
						break;
					default:
						break;
				}
				break;
			case EVENT_MOUSE:
				switch ( e->mouse.type )
				{
					case EVENT_MOUSE_MOTION:
						mouse_window_id = e->mouse.windows_id;
						mouse_pos =
						   {(f32)e->mouse.motion.pos_x,
						    (f32)e->mouse.motion.pos_y};
						if ( mouse_motions_during_frame_count
						   < ARRAY_COUNT( mouse_motions_during_frame ) )
						{
							u32 id = mouse_motions_during_frame_count;
							mouse_motions_during_frame  [id] =
							   {e->mouse.motion.motion_x, e->mouse.motion.motion_y};
							mouse_positions_during_frame[id] =
							   {e->mouse.motion.pos_x,    e->mouse.motion.pos_y};
							++mouse_motions_during_frame_count;
						} else
						{
							lpr_log_err( "reached max capacity for mouse motions buffering per frame!" );
						}
						ret = true;
						break;
					case EVENT_MOUSE_WHEEL:
						mouse_wheel_x += e->mouse.wheel.scrolled_x;
						mouse_wheel_y += e->mouse.wheel.scrolled_y;
						ret = true;
						break;
					case EVENT_MOUSE_BUTTON_PRESSED:
						switch( e->mouse.button.button )
						{
							case MOUSE_LEFT_BUTTON:
								mouse_button_left_down    = true;
								mouse_button_left_pressed = true;
								ret = true;
								break;
							case MOUSE_RIGHT_BUTTON:
								mouse_button_right_down    = true;
								mouse_button_right_pressed = true;
								ret = true;
								break;
							case MOUSE_MIDDLE_BUTTON:
								mouse_button_wheel_down    = true;
								mouse_button_wheel_pressed = true;
								ret = true;
								break;
							default:
								break;
						}
						break;
					case EVENT_MOUSE_BUTTON_RELEASED:
						switch( e->mouse.button.button )
						{
							case MOUSE_LEFT_BUTTON:
								mouse_clicked_left     = true;
								mouse_button_left_released = true;
								mouse_button_left_down = false;
								ret = true;
								break;
							case MOUSE_RIGHT_BUTTON:
								mouse_clicked_right     = true;
								mouse_button_right_released = true;
								mouse_button_right_down = false;
								ret = true;
								break;
							case MOUSE_MIDDLE_BUTTON:
								mouse_clicked_wheel     = true;
								mouse_button_wheel_released = true;
								mouse_button_wheel_down = false;
								ret = true;
								break;
							case MOUSE_BUTTON_4:
								// TODO(theGiallo, 2020-12-06): manage all mouse buttons
								//ret = true;
								break;
							default:
								break;
						}
						break;
					default:
						break;
				}
				break;
			case EVENT_GAME_SYSTEM:
				switch ( e->game_system.type )
				{
					case EVENT_GAME_API_DL_RELOADED:
						game_api_dl_reloaded = true;
						ret = true;
						break;
					default:
						break;
				}
				break;
			default:
				break;
		}

		return ret;
	}
};

#include "tgui.h"

#define AUDIO_SAMPLING_FREQUENCY 44100.0
#define AUDIO_SAMPLES_IN_BUF (512*1)

#define WORLD_BATCH_ID 0

struct
Camera
{
	V2 pos;
	V2 vanishing_point;
	V2 world_span;
	f32 angle;
};

struct
Window
{
	s32 window_id;
	bool resized;
	V2u32 new_size;
	Lpr_Draw_Data lpr_draw_data;
};

#define FONT_ATLAS_W 512
#define FONT_ATLAS_H FONT_ATLAS_W
#define MAX_FONT_FILE_SIZE (1024*1024)

struct
Sound
{
	f32 * samples;
	u32 tot_samples_count;
	u8 channels_count;

	// NOTE(theGiallo): used in the copy of the Player, it has to be 0 outside
	u8 ever_played;

	u8 padding[2];
};

struct
Kalman_Rot
{
	Mx<f32,2,2> P;
	Mx<f32,2,1> x;
};

struct
Kalman_Pos
{
	Mx<f32,2,2> P[2];
	Mx<f32,2,1> x[2];
};

struct
Playable_Sound
{
	Sound sound;

	// NOTE(theGiallo): 0 for immediate,
	// negative for postponed,
	// positive to skip some
	s32 played_samples;

	// NOTE(theGiallo): negative: infinite; 0: none; positive: loops and stops
	s32 loops;

	// NOTE(theGiallo): the pointed memory must be 64bit aligned! IMPORTANT
	V2 * source_pos;
	V2 last_source_pos;
	V2 last_sampled_source_pos;
	// NOTE(theGiallo): data for Kalman
	Kalman_Pos kalman;

	s32 last_played_sample_left;
	s32 last_played_sample_right;
};

#define MAX_ENQUEUABLE_SOUNDS 128
PRAGMA_ALIGN_PRE(64)
struct
Sounds_Queue
{
	// NOTE(theGiallo): circular buffer, single producer, single reader
	// cb[f] is always unused, the real first element is cb[f+1]
	// if f == l then it's empty
	// if f == l+1 || (f==0 && l == MAX_ENQUEUABLE_SOUNDS-1) then it's full
	volatile u64 first;
	volatile u64 last;
	Playable_Sound cb_sounds[MAX_ENQUEUABLE_SOUNDS];
} PRAGMA_ALIGN_POST(64);
internal
bool
enqueue_sound( Sounds_Queue * sq, Playable_Sound * in_p_sound );
internal
bool
dequeue_sound( Sounds_Queue * sq, Playable_Sound * out_p_sound );

#define MAX_MIXABLE_SOUNDS 1024
struct
Audio_Player
{
	// NOTE(theGiallo): these are individually accessed atomically
	// this is not really correct, but it should not be so terrible
	// NOTE(theGiallo): the pointed memory must be 64bit aligned! IMPORTANT
	struct
	{
		V2  * position;
		f32 * rotation;
		Kalman_Pos kalman_pos;
		Kalman_Rot kalman_rot;
	} head;

	Time audio_time;
	s64 played_samples;
	PRAGMA_ALIGN_PRE(64) f32 volume PRAGMA_ALIGN_POST(64);
	PRAGMA_ALIGN_PRE(64) bool mute PRAGMA_ALIGN_POST(64);

	u32 sounds_count;
	Playable_Sound sounds[MAX_MIXABLE_SOUNDS];

	Sounds_Queue new_sounds_queue;
};

struct
Image
{
	u8 * raw;
	V2u32 size;
};

struct
Resource_Image
{
	u32 guid;
	Image image;
	Lpr_Texture * lpr_texture;
};

struct
Test_2D_Entity
{
	Resource_Image * image;
	V2 sprite_size;
	V2 center;
	V3 pos;
	f32 rot;
	Lpr_Rgba_u8 color;
};

struct
Font_LL
{
	Font_LL * next;
	u32 key;
	u32 font_id;
	f32 px_height;
	u32 size_id;
	u32 atlas_id;
};


#include "game_api_mem_hotels.h"

struct
Fonts
{
#define MAX_FONTS_COUNT 8
#define MAX_SIZES_PER_FONT 32
	Font_LL * fonts_ht[1<<10];
	Font_LL_MH list_nodes_hm;

	const Font_LL *
	find_or_record( const char * path, f32 pixel_height );

	Lpr_Multi_Font   multi_font;
	Lpr_Texture      font_atlases   [MAX_FONTS_COUNT * MAX_SIZES_PER_FONT];
	Lpr_Font         fonts          [MAX_FONTS_COUNT];
	Lpr_Font_Of_Size font_of_sizes  [MAX_FONTS_COUNT][MAX_SIZES_PER_FONT];
	float            font_scales    [MAX_FONTS_COUNT][MAX_SIZES_PER_FONT];
	float            font_px_heights[MAX_FONTS_COUNT][MAX_SIZES_PER_FONT];

	// TODO(theGiallo, 2016-06-02): better refer to fonts
	const Font_LL * game_font;
	const Font_LL * debug_font;

	void
	init();
};

#define MH_ROOMS (256*255)
struct
Resource_Image_Storage
{
	u32_u32_LL * guids_hm[MH_ROOMS];
	u32_u32_LL_MH list_nodes_hm;
	Resource_Image_MH storage;

	Resource_Image *
	allocate( u32 guid, bool * already_there );

	void
	deallocate( Resource_Image * ri );

	Resource_Image *
	find( u32 guid, u32_u32_LL ** guid_node = NULL );
};

struct
Resources_GUIDs_Table
{
	u32_u32_LL * guids_hm[MH_ROOMS];
	u32_u32_LL_MH list_nodes_hm;
	charp_MH paths;
	Mem_Pool paths_pool;

	// NOTE(theGiallo): returns GUID
	u32
	record( const char * path );
	bool
	_record( u32 guid, const char * path );

	// NOTE(theGiallo): returns GUID
	u32
	find_or_record( const char * path );

	void
	remove( u32 guid );

	const char *
	find( u32 guid, u32_u32_LL *** guid_node = NULL );

	bool
	find( const char * path, u32 * guid_out );

	u32
	compute_guid( const char * path );
};

#define GAME_API_MAX_THREADS_NUM 16
// NOTE(theGiallo): IMPORTANT TGPROF_COPY_STRINGS feature is not complete
// TODO(theGiallo, 2016-08-16): would need a series of non-locking queues
// to prevent dll reloading in the middle of const static pointer usage
//#define TGPROF_COPY_STRINGS 1
#include "tgprof_ds.h"

enum
Prof_Graph_Mode : u8
{
	PROF_GRAPH_MODE_FRAME_PILES,
	PROF_GRAPH_MODE_TIME_FLOW,
	PROF_GRAPH_MODE_TOP,
};
#define PROFTOP_EXPAND_COLUMN_AS_STRING( e, s ) s,
#define PROFTOP_EXPAND_COLUMN_AS_ENUM( e, s ) e,
#define PROFTOP_SORTS( ACTION ) \
           ACTION( PROF_TOP_SORT_THREAD_ID,                            "thread id" ) \
           ACTION( PROF_TOP_SORT_NON_RECURSIVE_TOT_DURATION,           "non-recursive tot duration" ) \
           ACTION( PROF_TOP_SORT_NON_RECURSIVE_AVG_DURATION,           "non-recursive avg duration" ) \
           ACTION( PROF_TOP_SORT_NON_RECURSIVE_MIN_DURATION,           "non-recursive min duration" ) \
           ACTION( PROF_TOP_SORT_NON_RECURSIVE_MAX_DURATION,           "non-recursive max duration" ) \
           ACTION( PROF_TOP_SORT_NON_RECURSIVE_SD_DURATION,            "non-recursive sd duration" ) \
           ACTION( PROF_TOP_SORT_NON_RECURSIVE_VAR_DURATION,           "non-recursive var duration" ) \
           ACTION( PROF_TOP_SORT_NON_RECURSIVE_AVG_DURATION_PER_FRAME, "non-recursive avg duration per frame" ) \
           ACTION( PROF_TOP_SORT_NON_RECURSIVE_CALLS_COUNT_TOT,        "non-recursive calls count tot" ) \
           ACTION( PROF_TOP_SORT_NON_RECURSIVE_CALLS_PER_FRAME_AVG,    "non-recursive calls per frame avg" ) \
           ACTION( PROF_TOP_SORT_NON_RECURSIVE_CALLS_PER_FRAME_MIN,    "non-recursive calls per frame min" ) \
           ACTION( PROF_TOP_SORT_NON_RECURSIVE_CALLS_PER_FRAME_MAX,    "non-recursive calls per frame max" ) \
           ACTION( PROF_TOP_SORT_RECURSIVE_AVG_DURATION,               "recursive avg duration" ) \
           ACTION( PROF_TOP_SORT_RECURSIVE_MIN_DURATION,               "recursive min duration" ) \
           ACTION( PROF_TOP_SORT_RECURSIVE_MAX_DURATION,               "recursive max duration" ) \
           ACTION( PROF_TOP_SORT_RECURSIVE_SD_DURATION,                "recursive sd duration" ) \
           ACTION( PROF_TOP_SORT_RECURSIVE_VAR_DURATION,               "recursive var duration" ) \
           ACTION( PROF_TOP_SORT_RECURSIVE_TOT_DURATION,               "recursive tot duration" ) \
           ACTION( PROF_TOP_SORT_RECURSIVE_AVG_DURATION_PER_FRAME,     "recursive avg duration per frame" ) \
           ACTION( PROF_TOP_SORT_RECURSIVE_CALLS_COUNT_TOT,            "recursive calls count tot" ) \
           ACTION( PROF_TOP_SORT_RECURSIVE_CALLS_PER_FRAME_AVG,        "recursive calls per frame avg" ) \
           ACTION( PROF_TOP_SORT_RECURSIVE_CALLS_PER_FRAME_MIN,        "recursive calls per frame min" ) \
           ACTION( PROF_TOP_SORT_RECURSIVE_CALLS_PER_FRAME_MAX,        "recursive calls per frame max" ) \
           ACTION( PROF_TOP_SORT_NAME,                                 "name" ) \
           ACTION( PROF_TOP_SORT_FUNCTION,                             "function" ) \
           ACTION( PROF_TOP_SORT_FILE,                                 "file" ) \
           ACTION( PROF_TOP_SORT_LINE,                                 "line" ) \


enum
Prof_Top_Sort : u8
{
	PROFTOP_SORTS( PROFTOP_EXPAND_COLUMN_AS_ENUM )
	//---
	PROF_TOP_SORTS_COUNT
};
extern const char * prof_top_sorts_name_table[];
struct
Prof_Drawing_Data
{
	u64 color_shift;
	u32 max_depth;
	u32 time_flow_scrolling;
	u8 prof_graph_mode;
	struct
	{
		Prof_Top_Sort sort_whats [PROF_TOP_SORTS_COUNT];
		Sort_Order    sort_orders[PROF_TOP_SORTS_COUNT];
		V2 scrolling;
		s32 dragging_id;
		bool is_dragging;
		V2 dragging_mouse_pos_rel_to_center;
		bool sort_orders_contracted;
	} top;
};

struct
Game_Memory
{
	bool initialized;
	Audio_Player audio_player;
	Camera camera;
	Window main_window;
	Time game_time;
	Time delta_time;

#if 0
	f32 y_sorting_values[LPR_MAX_QUADS_PER_BATCH];

	Test_2D_Entity_MH entities_mh;

	Lpr_Texture_MH lpr_textures_mh;
	struct
	{
		Resources_GUIDs_Table guids;
		Resource_Image_Storage images;
	} resources;

	Resource_Image * metal_man_ri = {};
	Lpr_Texture metal_man_texture;
	Image test_image;

	Resource_Image_MH resource_images_mh;

	Sound test_vorbis_sound;
	Sound sound_campfire;

	PRAGMA_ALIGN_PRE(64)
	V2 campfire_pos PRAGMA_ALIGN_POST(64);

	// NOTE(theGiallo): test spatial sound
	#define TEST_SOUND_FREQ 440
	f32 test_sound_samples[(u32)AUDIO_SAMPLING_FREQUENCY*11];
	Sound test_sound;
	PRAGMA_ALIGN_PRE(64)
	V3 sound_source_pos PRAGMA_ALIGN_POST(64);
	PRAGMA_ALIGN_PRE(64)
	V3  player_pos PRAGMA_ALIGN_POST(64);
	PRAGMA_ALIGN_PRE(64)
	f32 player_rot PRAGMA_ALIGN_POST(64);
	f32 sound_source_dist;
	Test_2D_Entity * player_entity;
#endif

	Inputs inputs;

	Fonts fonts;

	V2 txt_pos;
	f32 txt_rot;
	DEBUG_ONLY(
	Camera DEBUG_camera;
	Window DEBUG_window;
	)

	Prof_Data prof;
	Prof_Drawing_Data prof_drawing_data;

	struct
	Texture_Gen_Data
	{
		struct
		GUI
		{
			tgui_Scrolling_Data scrolling_data;
			tgui_File_Select_Data file_select_data;
			bool selecting_dir;
			u8 * dir_path;
		} gui;

		#define TGEN_UP   0
		#define TGEN_DOWN 1
		u32 files_ids[2];
		u32 folder_files_count;

		// NOTE(theGiallo): a memory blob used to store a u8 * array of length
		// folder_files_count and after the strings as u8 arrays
		u8  folder_files_paths[8 * 1024];
		u32 folder_files_paths_bytes_to_null[4096];
		Lpr_Texture images[2];
		Lpr_Texture images_channels[2][3];
		Lpr_Texture power_spectrum_images[2][3];
		s32 * rgbs[2][3];
		kiss_fft_cpx * freqs[2];

		u32 loading_top_or_bottom_id;
		u8 * separate_rgb_u8[2][3];
		u8 * power_spectrums_u8[2][3];
		f32 mssim_rgb[3];
		f32 mssim_fft_rgb[3];
		f32 mstsim_rgb[3];
		f32 mstsim_fft_rgb[3];

		u8 canary;
	} texture_gen_data;

	Mem_Pool assets_mem_pool;
	u8 assets_mem_pool_canary;
	u8 mem_for_assets_mem_pool[256L*1024L*1024L];

	Mem_Stack mem_stack_for_asset_loading;
	u8 mem_for_asset_loading[20L*1024L*1024L];

	Mem_Stack mem_stack_tmp;
	u8 mem_for_tmp[256L*1024L*1024L];

	u8 mem_for_paths_pool[1L*1024L*1024L];

	#if 0
	u8 a_lot_of_mem_for_testing[3L*1024L*1024L*1024L];
	#endif
};

#endif /* ifndef GAME_API_H */
