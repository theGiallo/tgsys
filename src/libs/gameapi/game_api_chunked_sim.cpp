#include "game_api_chunked_sim.h"
#include "game_api_utility.h"
#include "game_api_lepre.h"

bool
add_entity( Entity * entity, Entity_List * list, Entity_List_Node_MH * list_mh )
{
	bool ret = false;
	Entity_List_Node * new_node = allocate( list_mh );
	if ( new_node )
	{
		new_node->entity = entity;
		new_node->next = *list;
		*list = new_node;
		ret = true;
	}
	return ret;
}

bool
remove_entity( Entity * entity, Entity_List * list, Entity_List_Node_MH * list_mh )
{
	bool ret = false;

	u64 identifier = entity->identifier;

	for ( Entity_List * curr_list = list;
	      *curr_list;
	      curr_list = &(*curr_list)->next )
	{
		if ( (*curr_list)->entity->identifier == identifier )
		{
			deallocate( list_mh, *curr_list );
			*curr_list = (*curr_list)->next;
			ret = true;
			break;
		}
	}

	return ret;
}

inline
internal
u32
get_hash_entity_id( Entity * entity, Entity_Hash_Table * ht )
{
	u64 identifier = entity->identifier;
#if 0
	u32 ret = ( identifier & 0xFFFFFFFF ) ^ ( ( identifier >> 32 ) & 0xFFFFFFFF );
#else
	u32 ret = identifier;
#endif
	ret = ret % ARRAY_COUNT( ht->table );

	return ret;
}

bool
remove_entity( Entity * entity, Entity_Hash_Table * ht )
{
	bool ret;

	u32 id = get_hash_entity_id( entity, ht );

	ret =
	remove_entity( entity, ht->table + id, &ht->list_mh );

	return ret;
}

bool
add_entity( Entity * entity, Entity_Hash_Table * ht )
{
	bool ret;

	u32 id = get_hash_entity_id( entity, ht );

	ret =
	add_entity( entity, ht->table + id, &ht->list_mh );

	return ret;
}

void
simulate_entity( Sim_Chunk * sim_chunk, Entity * entity )
{
	if ( entity->owner_chunk_x != sim_chunk->idx_x ||
	     entity->owner_chunk_y != sim_chunk->idx_y )
	{
		return;
	}

	switch ( entity->type )
	{
		// NOTE(theGiallo): here we have the code for the compile time known
		// entities. In the default the dl ones.
		case ENTITY_TYPE_EXAMPLE:
			break;
		default:
			// TODO(theGiallo, 2016-10-17): here find the proper function to
			// call. The entity could be loaded from a dl, the type name and
			// function inserted into hash-tables, accessed here.
			break;
	}
}

void
simulate_entity_fixed_timestep_hypothetically( Sim_Chunk * sim_chunk, Entity * entity )
{
	switch ( entity->type )
	{
		// NOTE(theGiallo): here we have the code for the compile time known
		// entities. In the default the dl ones.
		case ENTITY_TYPE_EXAMPLE:
			break;
		default:
			// TODO(theGiallo, 2016-10-17): here find the proper function to
			// call. The entity could be loaded from a dl, the type name and
			// function inserted into hash-tables, accessed here.
			break;
	}
}

void
simulate_entity_fixed_timestep_check_hypothesis( Sim_Chunk * sim_chunk, Entity * entity )
{
	switch ( entity->type )
	{
		// NOTE(theGiallo): here we have the code for the compile time known
		// entities. In the default the dl ones.
		case ENTITY_TYPE_EXAMPLE:
			break;
		default:
			// TODO(theGiallo, 2016-10-17): here find the proper function to
			// call. The entity could be loaded from a dl, the type name and
			// function inserted into hash-tables, accessed here.
			break;
	}
}

void
simulate_entity_fixed_timestep( Sim_Chunk * sim_chunk, Entity * entity )
{
	switch ( entity->type )
	{
		// NOTE(theGiallo): here we have the code for the compile time known
		// entities. In the default the dl ones.
		case ENTITY_TYPE_EXAMPLE:
			break;
		default:
			// TODO(theGiallo, 2016-10-17): here find the proper function to
			// call. The entity could be loaded from a dl, the type name and
			// function inserted into hash-tables, accessed here.
			break;
	}
}

void
simulate_chunk( Sim_Chunk * sim_chunk )
{
	Entity_List * table = sim_chunk->entities.table;
	for ( int i = 0; i!= ARRAY_COUNT( sim_chunk->entities.table ); ++i )
	{
		for ( Entity_List list = table[i];
		      list;
		      list = list->next )
		{
			simulate_entity( sim_chunk, list->entity );
		}
	}
}

void
simulate_chunk_fixed_timestep( Sim_Chunk * sim_chunk )
{
	Entity_List * table = sim_chunk->entities.table;
	for ( int i = 0; i!= ARRAY_COUNT( sim_chunk->entities.table ); ++i )
	{
		for ( Entity_List list = table[i];
		      list;
		      list = list->next )
		{
			simulate_entity_fixed_timestep( sim_chunk, list->entity );
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

inline
internal
u32
get_hash_entity_id( u64 identifier, Entities_By_Id * ht )
{
#if 0
	u32 ret = ( identifier & 0xFFFFFFFF ) ^ ( ( identifier >> 32 ) & 0xFFFFFFFF );
#else
	u32 ret = identifier;
#endif
	ret = ret % ARRAY_COUNT( ht->table );

	return ret;
}

internal
bool
add( Entities_By_Id * entities_by_id, Entity * entity )
{
	bool ret = false;

	u32 id = get_hash_entity_id( entity->identifier, entities_by_id );
	Entity_List * list = entities_by_id->table + id;
	Entity_List_Node * new_node = allocate( &entities_by_id->list_mh );
	if ( new_node )
	{
		new_node->entity = entity;
		new_node->next   = *list;
		*list = new_node;
		ret = true;
	}

	return ret;
}

internal
bool
remove( Entities_By_Id * entities_by_id, u64 identifier )
{
	bool ret = false;

	u32 id = get_hash_entity_id( identifier, entities_by_id );
	Entity_List * list = entities_by_id->table + id;

	for ( Entity_List * curr_list = list;
	      *curr_list;
	      curr_list = &(*curr_list)->next )
	{
		if ( (*curr_list)->entity->identifier == identifier )
		{
			deallocate( &entities_by_id->list_mh, *curr_list );
			*curr_list = (*curr_list)->next;
			ret = true;
			break;
		}
	}

	return ret;
}

internal
inline
void
index_entity_by_identifier( Entity_Storage * storage, Entity * entity )
{
	add( &storage->entities_by_id, entity );
}

internal
inline
void
unindex_entity_by_identifier( Entity_Storage * storage, Entity * entity )
{
	remove( &storage->entities_by_id, entity->identifier );
}

Entity *
recreate_entity_header( Entity_Storage * storage, u32 type, u64 identifier )
{
	Entity * ret = allocate_clean( &storage->mh );

	if ( ret )
	{
		ret->type       = type;
		ret->identifier = identifier;
		index_entity_by_identifier( storage, ret );
	}

	return ret;

}

Entity *
create_entity_header( Entity_Storage * storage )
{
	Entity * ret = allocate_clean( &storage->mh );

	if ( ret )
	{
		ret->identifier = lpr_xorshift128plus( storage->seeds );
		index_entity_by_identifier( storage, ret );
	}

	return ret;
}

void
destroy_entity_header( Entity_Storage * storage, Entity * entity )
{
	unindex_entity_by_identifier( storage, entity );
	deallocate( &storage->mh, entity );
}

Entity *
find_entity_by_identifier( Entity_Storage * storage, u64 identifier )
{
	Entity * ret = NULL;

	Entities_By_Id * entities_by_id = &storage->entities_by_id;
	u32 id = get_hash_entity_id( identifier, entities_by_id );
	Entity_List * list = entities_by_id->table + id;

	for ( Entity_List * curr_list = list;
	      *curr_list;
	      curr_list = &(*curr_list)->next )
	{
		Entity * entity = (*curr_list)->entity;
		if ( entity->identifier == identifier )
		{
			ret = entity;
			break;
		}
	}

	return ret;
}

////////////////////////////////////////////////////////////////////////////////

Entity *
create_entity_example( Entity_Storage * entity_storage, Entity_Example_Storage * storage )
{
	Entity * ret = create_entity_header( entity_storage );
	Entity_Example * entity_example = allocate_clean( &storage->mh );
	ret->body = entity_example;
	ret->type = ENTITY_TYPE_EXAMPLE;

	return ret;
}

#define MH_EMIT_BODY 1

#define MH_TYPE Entity_Example
#include "mem_hotel.inc.h"

#define MH_TYPE Entity
// NOTE(theGiallo): 255 * 4096 = 1,044,480
#define MH_FLOORS_COUNT ENTITY_FLOORS_COUNT
#include "mem_hotel.inc.h"

#define MH_TYPE Entity_List_Node
// NOTE(theGiallo): 255 * 4096 = 1,044,480
#define MH_FLOORS_COUNT ENTITY_FLOORS_COUNT
#include "mem_hotel.inc.h"

#define MH_TYPE Entity_List_Node
#include "mem_hotel.inc.h"

#undef MH_EMIT_BODY

////////////////////////////////////////////////////////////////////////////////
// NOTE(theGiallo): COD

#include "game_api_cod.h"

const V3s32 neighbors_3d_v3s32[COD_NEIGHBOR_3D_COUNT] =
{
	COD_NEIGHBORS_3D_IDS(COD_NEIGHBORS_3D_IDS_AS_VECTORS)
};
const char * neighbors_3d_strings[COD_NEIGHBOR_3D_COUNT] =
{
	COD_NEIGHBORS_3D_IDS(COD_NEIGHBORS_3D_IDS_AS_STRINGS)
};


void
cod_global_tables_status_init( COD_Global_Tables_Status * global_tables_status,
                               u8 * mem, u64 mem_bytes_size )
{
	u64 remaining_size = mem_bytes_size;
	u64 ht_size = B12_to_u32_hash_table_compute_memory_size( MAX_CHUNKS_COUNT );
	lpr_assert( ht_size < remaining_size );
	remaining_size -= ht_size;
	u8* available_mem = memory_to_next_64bits_alignment( mem );
	available_mem =
	   B12_to_u32_hash_table_init( &global_tables_status->chunks_ht,
	                               available_mem, MAX_CHUNKS_COUNT );
	available_mem = memory_to_next_64bits_alignment( available_mem );
	u64 ids_ht_size = u64_to_B12_hash_table_compute_memory_size( MAX_TOTAL_ENTITIES_COUNT );
	remaining_size -= ids_ht_size;
	available_mem =
	   u64_to_B12_hash_table_init( &global_tables_status->ids_to_chunks_ht,
	                               available_mem, MAX_TOTAL_ENTITIES_COUNT );
	available_mem = memory_to_next_64bits_alignment( available_mem );
	remaining_size = mem_bytes_size - ( (u64)available_mem - (u64)mem );
	global_tables_status->chunks_tables_mem_pool.init( available_mem, remaining_size );
}

// NOTE(theGiallo): returns the id of the table in the tables array of chunks.
// Returns negative on error.
s32
register_data_table(
   COD_Global_Tables_Status * global_status,
   COD_Generic_Table_Multicolumn_Init_Data table_init_data,
   const u8 * name )
{
	s32 ret = -1;

	u32 max_tables = ARRAY_COUNT( global_status->tables_init_data );
	if ( global_status->registered_tables_count >= max_tables )
	{
		return ret;
	}

	u32 max_data_elements_sizes_storage =
	   ARRAY_COUNT( global_status->data_elements_sizes_storage );
	if ( global_status->data_elements_sizes_storage_count + table_init_data.data_count >=
	     max_data_elements_sizes_storage )
	{
		return ret;
	}

	COD_Generic_Table_Multicolumn_Init_Data *
	stored_init_data = global_status->tables_init_data + global_status->registered_tables_count;
	ret = global_status->registered_tables_count;
	stored_init_data->data_count = table_init_data.data_count;
	stored_init_data->capacity = table_init_data.capacity;
	stored_init_data->data_elements_sizes =
	   global_status->data_elements_sizes_storage + global_status->data_elements_sizes_storage_count;
	global_status->data_elements_sizes_storage_count += stored_init_data->data_count;
	for ( u32 i = 0; i != table_init_data.data_count; ++i )
	{
		stored_init_data->data_elements_sizes[i] = table_init_data.data_elements_sizes[i];
	}

	global_status->tables_names[ret] = name;

	++global_status->registered_tables_count;

	return ret;
}

bool
initialize_chunk( COD_Global_Tables_Status * global_status,
                  COD_Chunk * chunk )
{
	bool ret = false;
	for ( u32 i = 0; i != global_status->registered_tables_count; ++i )
	{
		u64 table_mem_size =
		generic_table_multicolumn_compute_memory_size(
		   global_status->tables_init_data[i].data_elements_sizes,
		   global_status->tables_init_data[i].data_count,
		   global_status->tables_init_data[i].capacity );
		u8 * chunk_mem = (u8*)global_status->chunks_tables_mem_pool.allocate_clean( table_mem_size );
		if ( !chunk_mem )
		{
			return ret;
		}
		generic_table_multicolumn_init(
		   chunk->tables + i,
		   chunk_mem,
		   global_status->tables_init_data[i].data_elements_sizes,
		   global_status->tables_init_data[i].data_count,
		   global_status->tables_init_data[i].capacity );
	}
	ret = true;
	return ret;
}

u32_bool
spawn_chunk( COD_Global_Tables_Status * global_status,
             V3s32 xyz )
{
	u32_bool ret = {};

	u32_bool res = search_chunk( global_status, xyz );

	lpr_assert( !res.bool_v );
	if ( res.bool_v )
	{
		lpr_log_err( "ERROR: asked to spawn chunk (%d,%d,%d) but one is already there", xyz.x, xyz.y, xyz.z );
		ret = res;
		return ret;
	}

	s32 new_chunk_id =
	allocate_clean_get_index( &global_status->chunks_mem_hotel );

	if ( new_chunk_id < 0 )
	{
		return ret;
	}

	if ( !B12_to_u32_hash_table_insert( &global_status->chunks_ht, B12{.v3s32 = xyz }, new_chunk_id ) )
	{
		deallocate_index( &global_status->chunks_mem_hotel, new_chunk_id );
		return ret;
	}

	COD_Chunk * new_chunk = global_status->chunks_mem_hotel.mem + new_chunk_id;
	initialize_chunk( global_status, new_chunk );

	new_chunk->idx_x = xyz.x;
	new_chunk->idx_y = xyz.y;
	new_chunk->idx_z = xyz.z;

	for ( s32 i = 0; i != COD_NEIGHBOR_3D_COUNT; ++i )
	{
		res = B12_to_u32_hash_table_retrieve( &global_status->chunks_ht,
		                                      B12{ .v3s32 = xyz + neighbors_3d_v3s32[i] } );
		if ( res.bool_v )
		{
			COD_Chunk * neighbor = global_status->chunks_mem_hotel.mem + res.u32_v;
			new_chunk->neighbors[i] = neighbor;
			neighbor->neighbors[neighbor_3d_id_inverse((COD_Neighbor_3D_ID)i)] = new_chunk;
		} // NOTE(theGiallo): else defaults to 0 thanks to clean allocation
	}

	ret.u32_v  = new_chunk_id;
	ret.bool_v = true;
	return ret;
}


#define MH_EMIT_BODY 1

#define MH_FLOORS_COUNT COD_CHUNK_MH_FLOORS_COUNT
#define MH_TYPE COD_Chunk
#include "mem_hotel.inc.h"
#undef MH_EMIT_BODY

////////////////////////////////////////////////////////////////////////////////

#define MH_EMIT_BODY 1

#define MH_TYPE x128_fw_list
#include "mem_hotel.inc.h"

#undef MH_EMIT_BODY

u32 dynamic_physics_data_elements_sizes[(u32)Dynamic_Physics_Data_enum::_enum_count] = {
   DYNAMIC_PHYSICS_DATA(STRUCT_XMACRO_AS_SIZES)
};

u32 collision_sphere_elements_sizes[(u32)Collision_Sphere_enum::_enum_count] = {
   COLLISION_SPHERE(STRUCT_XMACRO_AS_SIZES)
};

u32 collisions_lists_table_entry_elements_sizes[(u32)Collisions_Lists_Table_Entry_enum::_enum_count] = {
   COLLISIONS_LISTS_TABLE_ENTRY(STRUCT_XMACRO_AS_SIZES)
};

