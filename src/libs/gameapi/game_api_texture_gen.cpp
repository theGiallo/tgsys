#include "game_api.h"
#include "game_api_lodepng.h"
#include "game_api_fft.h"
#include "game_api_opengl_tools.h"
#include "tgui.h"



////////////////////////////////////////////////////////////////////////////////
// NOTE(theGiallo): experimenting on similarity measures

struct
Image_Gray
{
	u8 * pixels;
	V2u32 size;
};
struct
Pixels_Window
{
	V2u32 bottom_left, size;
};
// NOTE(theGiallo): I'm not using an integral image for precision concerns
f32
mean( Image_Gray image, Pixels_Window window )
{
	f32 ret, sum = 0.0f;
	V2u32 top_right = window.bottom_left + window.size;
	for ( u32 y = window.bottom_left.y; y != top_right.y; ++y )
	{
		u8 * row = image.pixels + y * image.size.w;
		for ( u32 x = window.bottom_left.x; x != top_right.x; ++x )
		{
			sum += row[x];
		}
	}
	ret = sum / f32( window.size.w * window.size.h );
	return ret;
}
f32
variance( Image_Gray image, Pixels_Window window, f32 mean )
{
	f32 ret, sum = 0.0f;
	V2u32 top_right = window.bottom_left + window.size;
	for ( u32 y = window.bottom_left.y; y != top_right.y; ++y )
	{
		u8 * row = image.pixels + y * image.size.w;
		for ( u32 x = window.bottom_left.x; x != top_right.x; ++x )
		{
			sum += u32( row[x] ) * u32( row[x] );
		}
	}
	ret = sum / f32( window.size.w * window.size.h ) - square( mean );
	return ret;
}
V2
mean_and_variance( Image_Gray image, Pixels_Window window )
{
	V2 ret;
	f64 sum = 0.0, sum_sq = 0.0;
	V2u32 top_right = window.bottom_left + window.size;
	for ( u32 y = window.bottom_left.y; y != top_right.y; ++y )
	{
		u8 * row = image.pixels + y * image.size.w;
		for ( u32 x = window.bottom_left.x; x != top_right.x; ++x )
		{
			sum_sq += u32( row[x] ) * u32( row[x] );
			sum    += row[x];
		}
	}
	ret.x = sum    / f64( window.size.w * window.size.h );
	ret.y = sum_sq / f64( window.size.w * window.size.h ) - square( ret.x );
	return ret;
}
f32
covariance( Image_Gray image0, Pixels_Window window0, f32 mean0,
            Image_Gray image1, Pixels_Window window1, f32 mean1 )
{
	lpr_assert( window0.size == window1.size );

	f32 ret, sum = 0.0f;
	for ( u32 ry = 0; ry != window0.size.y; ++ry )
	{
		u8 * row0 = image0.pixels + ( window0.bottom_left.y + ry ) * image0.size.w;
		u8 * row1 = image1.pixels + ( window1.bottom_left.y + ry ) * image1.size.w;
		for ( u32 rx = 0; rx != window0.size.x; ++rx )
		{
			u32 x0 = window0.bottom_left.x + rx;
			u32 x1 = window1.bottom_left.x + rx;
			sum += ( row0[x0] - mean0 ) * ( row1[x1] - mean1 );
		}
	}
	ret = sum / f32( window0.size.w * window0.size.h );
	return ret;
}
f32
luminance( Image_Gray image0, Pixels_Window window0,
           Image_Gray image1, Pixels_Window window1 )
{
	f32 ret;
	f32 mean0 = mean( image0, window0 );
	f32 mean1 = mean( image1, window1 );
	constexpr f32 K1 = 1.0f / 128.0f;
	constexpr f32 C1 = ( 255.0f * K1 ) * ( 255.0f * K1 );
	ret = ( 2.0f * mean0 * mean1 + C1 )
	    / ( square( mean0 ) + square( mean1 ) + C1 );
	return ret;
}
f32
contrast( Image_Gray image0, Pixels_Window window0,
          Image_Gray image1, Pixels_Window window1 )
{
	f32 ret;
	V2 m_v0 = mean_and_variance( image0, window0 );
	V2 m_v1 = mean_and_variance( image1, window1 );
	f32 sd0 = sqrtf( m_v0.y );
	f32 sd1 = sqrtf( m_v1.y );
	constexpr f32 K2 = 1.0f / 128.0f;
	constexpr f32 C2 = ( 255.0f * K2 ) * ( 255.0f * K2 );
	ret = ( 2.0f * sd0 * sd1 + C2 )
	    / ( m_v0.y + m_v1.y + C2 );
	return ret;
}
f32
structure( Image_Gray image0, Pixels_Window window0,
           Image_Gray image1, Pixels_Window window1 )
{
	f32 ret;
	V2 m_v0 = mean_and_variance( image0, window0 );
	V2 m_v1 = mean_and_variance( image1, window1 );
	f32 sd0 = sqrtf( m_v0.y );
	f32 sd1 = sqrtf( m_v1.y );
	f32 cov = covariance( image0, window0, m_v0.x, image1, window1, m_v1.x );

	constexpr f32 K3 = 1.0f / 128.0f;
	constexpr f32 C3 = ( 255.0f / K3 ) * ( 255.0f / K3 );
	ret = ( cov + C3 )
	    / ( sd0 * sd1 + C3 );
	return ret;
}
V3
luminance_contrast_structure( Image_Gray image0, Pixels_Window window0,
                              Image_Gray image1, Pixels_Window window1 )
{
	V3 ret;
	V2 m_v0 = mean_and_variance( image0, window0 );
	V2 m_v1 = mean_and_variance( image1, window1 );
	f32 sd0 = sqrtf( m_v0.y );
	f32 sd1 = sqrtf( m_v1.y );
	f32 cov = covariance( image0, window0, m_v0.x, image1, window1, m_v1.x );

	constexpr f32 K1 = 1.0f / 128.0f;
	constexpr f32 C1 = ( 255.0f * K1 ) * ( 255.0f * K1 );
	ret.x = ( 2.0f * m_v0.x * m_v1.x + C1 )
	      / ( square( m_v0.x ) + square( m_v1.x ) + C1 );

	constexpr f32 K2 = 1.0f / 128.0f;
	constexpr f32 C2 = ( 255.0f * K2 ) * ( 255.0f * K2 );
	ret.y = ( 2.0f * sd0 * sd1 + C2 )
	      / ( m_v0.y + m_v1.y + C2 );

	//constexpr f32 K3 = 1.0f / 128.0f;
	//constexpr f32 C3 = ( 255.0f / K3 ) * ( 255.0f / K3 );
	constexpr f32 C3 = C2 * 0.5f;
	ret.z = ( cov + C3 )
	      / ( sd0 * sd1 + C3 );

	return ret;
}
f32
autocovariance_horizontal( Image_Gray image, Pixels_Window window,
                           f32 mean, f32 variance )
{
	lpr_assert( isfinite( variance ) );
	lpr_assert( isfinite( mean ) );
	//lpr_assert( 0.0f != variance || mean == 0.0f );

	f32 ret;

	if ( variance == 0.0f )
	{
		constexpr f32 K1 = 1.0f / 128.0f;
		constexpr f32 C1 = ( 255.0f * K1 ) * ( 255.0f * K1 );
		variance = C1;
	}

	f64 sum = 0.0;
	for ( u32 ry = 0; ry != window.size.y; ++ry )
	{
		u8 * row = image.pixels + ( window.bottom_left.y + ry ) * image.size.w;
		for ( u32 rx = 0; rx != window.size.x - 1; ++rx )
		{
			u32 xl = window.bottom_left.x + rx;
			u32 xr = window.bottom_left.x + rx + 1;
			sum += ( row[xl] - mean ) * ( row[xr] - mean );
		}
	}
	ret = ( sum / f64( window.size.w * window.size.h ) ) / variance;

	return ret;
}
f32
autocovariance_vertical( Image_Gray image, Pixels_Window window,
                         f32 mean, f32 variance )
{
	lpr_assert( isfinite( variance ) );
	lpr_assert( isfinite( mean ) );
	//lpr_assert( 0.0f != variance || mean == 0.0f );

	f32 ret;

	if ( variance == 0.0f )
	{
		constexpr f32 K1 = 1.0f / 128.0f;
		constexpr f32 C1 = ( 255.0f * K1 ) * ( 255.0f * K1 );
		variance = C1;
	}

	f64 sum = 0.0;
	for ( u32 ry = 0; ry != window.size.y - 1; ++ry )
	{
		u8 * rowb = image.pixels + ( window.bottom_left.y + ry ) * image.size.w;
		u8 * rowt = image.pixels + ( window.bottom_left.y + ry + 1 ) * image.size.w;
		for ( u32 rx = 0; rx != window.size.x; ++rx )
		{
			u32 x = window.bottom_left.x + rx;
			sum += ( rowb[x] - mean ) * ( rowt[x] - mean );
		}
	}
	ret = ( sum / f64( window.size.w * window.size.h ) ) / variance;

	return ret;
}
f32
zhao_c01( Image_Gray image0, Pixels_Window window0, f32 mean0, f32 variance0,
          Image_Gray image1, Pixels_Window window1, f32 mean1, f32 variance1 )
{
	f32 ret;
	f32 d =  autocovariance_horizontal( image0, window0, mean0, variance0 )
	       - autocovariance_horizontal( image1, window1, mean1, variance1 );
	ret = 1.0f - 0.5f * abs( d );
	return ret;
}
f32
zhao_c10( Image_Gray image0, Pixels_Window window0, f32 mean0, f32 variance0,
          Image_Gray image1, Pixels_Window window1, f32 mean1, f32 variance1 )
{
	f32 ret;
	f32 d =  autocovariance_vertical( image0, window0, mean0, variance0 )
	       - autocovariance_vertical( image1, window1, mean1, variance1 );
	ret = 1.0f - 0.5f * abs( d );
	return ret;
}
f32
STSIM( Image_Gray image0, Pixels_Window window0,
       Image_Gray image1, Pixels_Window window1 )
{
	f32 ret;

	V2 m_v0 = mean_and_variance( image0, window0 );
	V2 m_v1 = mean_and_variance( image1, window1 );
	f32 sd0 = sqrtf( m_v0.y );
	f32 sd1 = sqrtf( m_v1.y );

	constexpr f32 K1 = 1.0f / 128.0f;
	constexpr f32 C1 = ( 255.0f * K1 ) * ( 255.0f * K1 );
	f32 l = ( 2.0f * m_v0.x * m_v1.x + C1 )
	      / ( square( m_v0.x ) + square( m_v1.x ) + C1 );

	constexpr f32 K2 = 1.0f / 128.0f;
	constexpr f32 C2 = ( 255.0f * K2 ) * ( 255.0f * K2 );
	f32 c = ( 2.0f * sd0 * sd1 + C2 )
	      / ( m_v0.y + m_v1.y + C2 );

	f32 c01 = zhao_c01( image0, window0, m_v0.x, m_v0.y, image1, window1, m_v1.x, m_v1.y );
	f32 c10 = zhao_c10( image0, window0, m_v0.x, m_v0.y, image1, window1, m_v1.x, m_v1.y );

	lpr_assert( isfinite( l ) );
	lpr_assert( isfinite( c ) );
	lpr_assert( isfinite( c01 ) );
	lpr_assert( isfinite( c10 ) );

	#define P1_4( x ) sqrtf( sqrtf( x ) )
	ret = P1_4( l ) * P1_4( c) * P1_4( c01 ) * P1_4( c10 );
	#undef P1_4

	return ret;
}
f32
MSTSIM( Image_Gray image0, Image_Gray image1, u32 window_side = 7 )
{
	lpr_assert( image0.size == image1.size );
	f32 ret = 0.0f;
	f64 sum = 0.0;

	Pixels_Window window0 = {
	   .bottom_left = {},
	   .size        = {window_side, window_side}
	};
	Pixels_Window window1 = {
	   .bottom_left = {},
	   .size        = {window_side, window_side}
	};
	u32 x_max = image0.size.w - window_side;
	u32 y_max = image0.size.h - window_side;
	for ( u32 y = 0; y != y_max; ++y )
	{
		for ( u32 x = 0; x != x_max; ++x )
		{
			window0.bottom_left = {x,y};
			window1.bottom_left = {x,y};
			sum += STSIM( image0, window0, image1, window1 );
		}
	}

	ret = sum / f64( x_max * y_max );

	return ret;
}



f32
SSIM( Image_Gray image0, Pixels_Window window0,
      Image_Gray image1, Pixels_Window window1 )
{
	f32 ret;
	V2 m_v0 = mean_and_variance( image0, window0 );
	V2 m_v1 = mean_and_variance( image1, window1 );
	f32 sd0 = sqrtf( m_v0.y );
	f32 sd1 = sqrtf( m_v1.y );
	f32 cov = covariance( image0, window0, m_v0.x, image1, window1, m_v1.x );

	constexpr f32 K1 = 1.0f / 1024.0f;
	constexpr f32 C1 = ( 255.0f * K1 ) * ( 255.0f * K1 );
	constexpr f32 K2 = 1.0f / 1024.0f;
	constexpr f32 C2 = ( 255.0f * K2 ) * ( 255.0f * K2 );

	ret = ( ( 2.0f * m_v0.x * m_v1.x + C1 ) * ( 2.0f * cov + C2 ) )
	      / ( ( square( m_v0.x ) + square( m_v1.x ) + C1 ) * ( m_v0.y + m_v1.y + C2 ) );

	return ret;
}
f32
MSSIM( Image_Gray image0, Image_Gray image1, u32 window_side = 7 )
{
	lpr_assert( image0.size == image1.size );
	f32 ret = 0.0f;
	f64 sum = 0.0;

	Pixels_Window window0 = {
	   .bottom_left = {},
	   .size        = {window_side, window_side}
	};
	Pixels_Window window1 = {
	   .bottom_left = {},
	   .size        = {window_side, window_side}
	};
	u32 x_max = image0.size.w - window_side;
	u32 y_max = image0.size.h - window_side;
	for ( u32 y = 0; y != y_max; ++y )
	{
		for ( u32 x = 0; x != x_max; ++x )
		{
			window0.bottom_left = {x,y};
			window1.bottom_left = {x,y};
			sum += SSIM( image0, window0, image1, window1 );
		}
	}

	ret = sum / f64( x_max * y_max );

	return ret;
}

// NOTE(theGiallo): experimenting on similarity measures
////////////////////////////////////////////////////////////////////////////////

void
separate_image_channel_u8_red( u8 * rgba, u32 width, u32 height, u8 * separate_r )
{
	u32 bytes_size = height * width * 4;
	for ( u32 i = 0, pxid = 0; i != bytes_size; i += 4, ++pxid )
	{
		separate_r[pxid] = rgba[i    ];
	}
}
void
separate_image_channels_u8( u8 * rgba, u32 width, u32 height, u8 * separate_rgb[3] )
{
	u32 bytes_size = height * width * 4;
	for ( u32 i = 0, pxid = 0; i != bytes_size; i += 4, ++pxid )
	{
		separate_rgb[0][pxid] = rgba[i    ];
		separate_rgb[1][pxid] = rgba[i + 1];
		separate_rgb[2][pxid] = rgba[i + 2];
	}
}
void
separate_image_channels_s32_linear( u8 * rgba, u32 width, u32 height, s32 * separate_rgb[3], bool from_srgb )
{
	u32 bytes_size = height * width * 4;
	if ( from_srgb )
	{
		for ( u32 i = 0, pxid = 0; i != bytes_size; i += 4, ++pxid )
		{
			separate_rgb[0][pxid] = (s32)( (f64)powf( (f32)rgba[i    ] / 255.0f,  1.0f / 2.2f ) * (f64)(S32_MAX) );
			separate_rgb[1][pxid] = (s32)( (f64)powf( (f32)rgba[i + 1] / 255.0f,  1.0f / 2.2f ) * (f64)(S32_MAX) );
			separate_rgb[2][pxid] = (s32)( (f64)powf( (f32)rgba[i + 2] / 255.0f,  1.0f / 2.2f ) * (f64)(S32_MAX) );
		}
	} else
	{
		for ( u32 i = 0, pxid = 0; i != bytes_size; i += 4, ++pxid )
		{
			separate_rgb[0][pxid] = rgba[i    ];
			separate_rgb[1][pxid] = rgba[i + 1];
			separate_rgb[2][pxid] = rgba[i + 2];
		}
	}
}
void fftshift( Lpr_Rgb_u8 * rgb, u32 width, u32 height )
{
	s32 xas[] = {0,0};
	s32 yas[] = {0,(s32)height/2};
	s32 xbs[] = {(s32)width/2,(s32)width/2};
	s32 ybs[] = {(s32)height/2,0};
	for ( s32 i = 0; i != 2; ++i )
	{
		for ( s32 y = 0; y != height / 2; ++y )
		{
			for ( s32 x = 0; x != width / 2; ++x )
			{
				s32 x_a = x + xas[i];
				s32 y_a = y + yas[i];
				s32 x_b = x + xbs[i];
				s32 y_b = y + ybs[i];
				Lpr_Rgb_u8 tmp = rgb[ y_a * width + x_a ];
				rgb[ y_a * width + x_a ] = rgb[ y_b * width + x_b ];
				rgb[ y_b * width + x_b ] = tmp;
			}
		}
	}
}
void fftshift( u8 * g, u32 width, u32 height )
{
	s32 xas[] = {0,0};
	s32 yas[] = {0,(s32)height/2};
	s32 xbs[] = {(s32)width/2,(s32)width/2};
	s32 ybs[] = {(s32)height/2,0};
	for ( s32 i = 0; i != 2; ++i )
	{
		for ( s32 y = 0; y != height / 2; ++y )
		{
			for ( s32 x = 0; x != width / 2; ++x )
			{
				s32 x_a = x + xas[i];
				s32 y_a = y + yas[i];
				s32 x_b = x + xbs[i];
				s32 y_b = y + ybs[i];
				u8 tmp = g[ y_a * width + x_a ];
				g[ y_a * width + x_a ] = g[ y_b * width + x_b ];
				g[ y_b * width + x_b ] = tmp;
			}
		}
	}
}
void
separate_image_channels_cpx( u8 * rgba, u32 width, u32 height, kiss_fft_cpx * separate_rgb[3] )
{
	u32 bytes_size = height * width * 4;
	for ( u32 i = 0, pxid = 0; i != bytes_size; i += 4, ++pxid )
	{
		separate_rgb[0][pxid].r = rgba[i    ]; separate_rgb[0][pxid].i = 0;
		separate_rgb[1][pxid].r = rgba[i + 1]; separate_rgb[1][pxid].i = 0;
		separate_rgb[2][pxid].r = rgba[i + 2]; separate_rgb[2][pxid].i = 0;
	}
}
#if 0
#if REALOPT
void
texture_gen_load_images( Game_Memory * game_mem, System_API * sys_api )
{
	Game_Memory::Texture_Gen_Data * data = &game_mem->texture_gen_data;
	const char * file_paths[2] = {
	   "./resources/imgs/pixar128library/brick/Red-orange_glazed_pxr128.png",
	   "./resources/imgs/pixar128library/brick/Standard_red_pxr128.png" };
	u8 * img_blob;
	V2u32 dims;
	for ( s32 i = 0; i != 2; ++i )
	{
		{
			u64 s = data->rgbs[1][0] ? ((u64*)((u8*)(data->rgbs[1][0]) - 8))[0] : 0;
			lpr_assert( s <= game_mem->assets_mem_pool.size );
		}
		game_mem->mem_stack_for_asset_loading.first_free = 0;
		load_png( sys_api, file_paths[i],
		          &game_mem->mem_stack_tmp,
		          &game_mem->mem_stack_for_asset_loading,
		          &img_blob, &dims.w, &dims.h );

		lpr_log_dbg( "loaded image '%s' %u x %u", file_paths[i], dims.w, dims.h );

		{
			u64 s = data->rgbs[1][0] ? ((u64*)((u8*)(data->rgbs[1][0]) - 8))[0] : 0;
			lpr_assert( s <= game_mem->assets_mem_pool.size );
		}

		Lpr_Texture * image = data->images + i;
		lpr_create_texture( img_blob, dims, LPR_RGBA8, true, true, true, image, LPR_MIPMAP_TRILINEAR, LPR_NEAREST, LPR_CLAMP_TO_EDGE, LPR_CLAMP_TO_EDGE, LPR_SCALE_LINEAR, {1,1}, 0 );

		lpr_assert( !data->canary );
		lpr_assert( !game_mem->assets_mem_pool_canary );
		{
			u64 s = data->rgbs[1][0] ? ((u64*)((u8*)(data->rgbs[1][0]) - 8))[0] : 0;
			lpr_assert( s <= game_mem->assets_mem_pool.size );
		}

		Mem_Pool * mp = &game_mem->assets_mem_pool;

		s32 ** rgb = data->rgbs[i];
		for ( s32 c = 0; c!= 3; ++c )
		{
			if ( rgb[c] )
			{
				mp->deallocate( rgb[c] );
			}
			lpr_log_dbg( "mp occupied = %lu/%lu (%04.01f%%)", mp->occupied, mp->size, (f32)mp->occupied / (f32)mp->size );
			rgb[c] = (s32*)mp->allocate( dims.w * dims.h * sizeof( s32 ) );
			lpr_log_dbg( "mp occupied = %lu/%lu (%04.01f%%)", mp->occupied, mp->size, (f32)mp->occupied / (f32)mp->size );
			lpr_assert( rgb[c] );
		}
		separate_image_channels_s32( img_blob, dims.w, dims.h, rgb, true );

		lpr_assert( !data->canary );
		lpr_assert( !game_mem->assets_mem_pool_canary );

		{
			u8 * rgbu8[3];
			Mem_Stack * ms = &game_mem->mem_stack_tmp;
			u64 pop = ms->first_free;
			for ( s32 c = 0; c != 3; ++c )
			{
				rgbu8[c] = ms->push( dims.w * dims.h );
			}
			separate_image_channels_u8( img_blob, dims.w, dims.h, rgbu8 );
			for ( s32 c = 0; c != 3; ++c )
			{
				lpr_create_texture( rgbu8[c], dims, LPR_GRAYSCALE8, true, true, false, data->images_channels[i] + c, LPR_MIPMAP_TRILINEAR, LPR_NEAREST, LPR_CLAMP_TO_EDGE, LPR_CLAMP_TO_EDGE, LPR_SCALE_LINEAR, {1,1}, 0 );
			}
			ms->first_free = pop;
			s32 area = dims.w * dims.h;
			for ( s32 c = 0; c != 3; ++c )
			{
				for ( s32 j = 0; j != area; ++j )
				{
					//rgb[c][j] <<= 24;
					rgb[c][j] *= U32_MAX / U8_MAX;
				}
			}
		}

		lpr_assert( !data->canary );
		lpr_assert( !game_mem->assets_mem_pool_canary );

		#if 0
		V2u32 freqsn = V2u32{dims.x / 2 + 1, dims.y};
		#else
		V2u32 freqsn = V2u32{dims.x, dims.y / 2 + 1};
		#endif
		for ( s32 c = 0; c!= 3; ++c )
		{
			Mem_Stack * ms = &game_mem->mem_stack_tmp;
			u64 pop = ms->first_free;
			size_t lenmem = 0;
			void * mem = 0;
			kiss_fftndr_alloc( make_V2s32(dims).arr, 2, 0, mem, &lenmem );
			mem = ms->push( lenmem );
			memset( mem, 0, lenmem );
			kiss_fftndr_cfg cfg = kiss_fftndr_alloc( make_V2s32(dims).arr, 2, 0, mem, &lenmem );

			lpr_assert( !data->canary );
			lpr_assert( !game_mem->assets_mem_pool_canary );

			kiss_fft_cpx ** freqs = data->freqs + i;
			if ( *freqs )
			{
				mp->deallocate( *freqs );
			}
			lpr_log_dbg( "cfg: %d, %d", cfg->dimReal, cfg->dimOther );
			lpr_log_dbg( "mp occupied = %lu/%lu (%04.01f%%)", mp->occupied, mp->size, (f32)mp->occupied / (f32)mp->size );
			*freqs = (kiss_fft_cpx*) mp->allocate( freqsn.w * freqsn.h * sizeof( kiss_fft_cpx ) );
			lpr_log_dbg( "mp occupied = %lu/%lu (%04.01f%%)", mp->occupied, mp->size, (f32)mp->occupied / (f32)mp->size );
			lpr_assert( *freqs );

			lpr_assert( !data->canary );
			lpr_assert( !game_mem->assets_mem_pool_canary );
			{
				u64 s = data->rgbs[1][0] ? ((u64*)((u8*)(data->rgbs[1][0]) - 8))[0] : 0;
				lpr_assert( s <= game_mem->assets_mem_pool.size );
			}

			kiss_fftndr( cfg, rgb[c], *freqs);

			lpr_assert( !data->canary );
			lpr_assert( !game_mem->assets_mem_pool_canary );
			{
				u64 s = data->rgbs[1][0] ? ((u64*)((u8*)(data->rgbs[1][0]) - 8))[0] : 0;
				lpr_assert( s <= game_mem->assets_mem_pool.size );
			}

			u32 fntot = freqsn.w * freqsn.h;
			u8 * power_spectrum = (u8*)ms->push( sizeof( power_spectrum[0] ) * fntot );
			for ( u32 f = 0; f != fntot; ++f )
			{
				s64 ps = squareu64((s64)freqs[0][f].r) - ( (s64)freqs[0][f].i * (s64)( -freqs[0][f].i ) );
				lpr_assert( ps >= 0 );
				power_spectrum[f] = (u8)( ( (u64)( ps > U32_MAX ? U32_MAX : ps ) * (u64)U8_MAX ) / (u64)U32_MAX );
				//lpr_log_dbg( "freqs[0][%u] = %u, %u", f, freqs[0][f].r, freqs[0][f].i );
				//lpr_log_dbg( "ps[%u] = %u", f, (u32)power_spectrum[f] );
			}

			lpr_assert( !data->canary );
			lpr_assert( !game_mem->assets_mem_pool_canary );
			{
				u64 s = data->rgbs[1][0] ? ((u64*)((u8*)(data->rgbs[1][0]) - 8))[0] : 0;
				lpr_assert( s <= game_mem->assets_mem_pool.size );
			}

			lpr_create_texture( power_spectrum, freqsn, LPR_GRAYSCALE8, true, true, false, data->power_spectrum_images[i] + c, LPR_MIPMAP_TRILINEAR, LPR_NEAREST, LPR_CLAMP_TO_EDGE, LPR_CLAMP_TO_EDGE, LPR_SCALE_LINEAR, {1,1}, 0 );

			lpr_assert( !data->canary );
			lpr_assert( !game_mem->assets_mem_pool_canary );
			{
				u64 s = data->rgbs[1][0] ? ((u64*)((u8*)(data->rgbs[1][0]) - 8))[0] : 0;
				lpr_assert( s <= game_mem->assets_mem_pool.size );
			}

			ms->first_free = pop;
		}
	}
}
#else
void
texture_gen_load_images( Game_Memory * game_mem, System_API * sys_api )
{
	Game_Memory::Texture_Gen_Data * data = &game_mem->texture_gen_data;
	const char * file_paths[2] = {
	   "./resources/imgs/pixar128library/brick/Red-orange_glazed_pxr128.png",
	   "./resources/imgs/pixar128library/brick/Standard_red_pxr128.png" };
	u8 * img_blob;
	V2u32 dims;
	for ( s32 i = 0; i != 2; ++i )
	{
		{
			u64 s = data->rgbs[1][0] ? ((u64*)((u8*)(data->rgbs[1][0]) - 8))[0] : 0;
			lpr_assert( s <= game_mem->assets_mem_pool.size );
		}
		game_mem->mem_stack_for_asset_loading.first_free = 0;
		load_png( sys_api, file_paths[i],
		          &game_mem->mem_stack_tmp,
		          &game_mem->mem_stack_for_asset_loading,
		          &img_blob, &dims.w, &dims.h );

		lpr_log_dbg( "loaded image '%s' %u x %u", file_paths[i], dims.w, dims.h );

		{
			u64 s = data->rgbs[1][0] ? ((u64*)((u8*)(data->rgbs[1][0]) - 8))[0] : 0;
			lpr_assert( s <= game_mem->assets_mem_pool.size );
		}

		Lpr_Texture * image = data->images + i;
		lpr_create_texture( img_blob, dims, LPR_RGBA8, true, true, true, image, LPR_MIPMAP_TRILINEAR, LPR_NEAREST, LPR_CLAMP_TO_EDGE, LPR_CLAMP_TO_EDGE, LPR_SCALE_LINEAR, {1,1}, 0 );

		lpr_assert( !data->canary );
		lpr_assert( !game_mem->assets_mem_pool_canary );
		{
			u64 s = data->rgbs[1][0] ? ((u64*)((u8*)(data->rgbs[1][0]) - 8))[0] : 0;
			lpr_assert( s <= game_mem->assets_mem_pool.size );
		}

		Mem_Pool * mp = &game_mem->assets_mem_pool;

		static kiss_fft_cpx * rgbc[2][3] = {};
		for ( s32 c = 0; c!= 3; ++c )
		{
			if ( rgbc[i][c] )
			{
				mp->deallocate( rgbc[i][c] );
			}
			lpr_log_dbg( "mp occupied = %lu/%lu (%04.01f%%)", mp->occupied, mp->size, (f32)mp->occupied / (f32)mp->size );
			rgbc[i][c] = (kiss_fft_cpx*)mp->allocate( dims.w * dims.h * sizeof( kiss_fft_cpx ) );
			lpr_log_dbg( "mp occupied = %lu/%lu (%04.01f%%)", mp->occupied, mp->size, (f32)mp->occupied / (f32)mp->size );
			lpr_assert( rgbc[i][c] );
		}
		separate_image_channels_cpx( img_blob, dims.w, dims.h, rgbc[i] );

		lpr_assert( !data->canary );
		lpr_assert( !game_mem->assets_mem_pool_canary );

		{
			u8 * rgbu8[3];
			Mem_Stack * ms = &game_mem->mem_stack_tmp;
			u64 pop = ms->first_free;
			for ( s32 c = 0; c != 3; ++c )
			{
				rgbu8[c] = ms->push( dims.w * dims.h );
			}
			separate_image_channels_u8( img_blob, dims.w, dims.h, rgbu8 );
			for ( s32 c = 0; c != 3; ++c )
			{
				lpr_create_texture( rgbu8[c], dims, LPR_GRAYSCALE8, false, true, false, data->images_channels[i] + c, LPR_MIPMAP_TRILINEAR, LPR_NEAREST, LPR_CLAMP_TO_EDGE, LPR_CLAMP_TO_EDGE, LPR_SCALE_LINEAR, {1,1}, 0 );
			}
			ms->first_free = pop;
			s32 area = dims.w * dims.h;
			for ( s32 c = 0; c != 3; ++c )
			{
				for ( s32 j = 0; j != area; ++j )
				{
					//rgbi[i][c][j].r <<= 24;
					rgbc[i][c][j].r *= U32_MAX / U8_MAX;
				}
			}
		}

		lpr_assert( !data->canary );
		lpr_assert( !game_mem->assets_mem_pool_canary );

		V2u32 freqsn = V2u32{dims.x, dims.y};
		for ( s32 c = 0; c!= 3; ++c )
		{
			Mem_Stack * ms = &game_mem->mem_stack_tmp;
			u64 pop = ms->first_free;
			size_t lenmem = 0;
			void * mem = 0;
			kiss_fftnd_alloc( make_V2s32(dims).arr, 2, 0, mem, &lenmem );
			mem = ms->push( lenmem );
			memset( mem, 0, lenmem );
			kiss_fftnd_cfg cfg = kiss_fftnd_alloc( make_V2s32(dims).arr, 2, 0, mem, &lenmem );

			lpr_assert( !data->canary );
			lpr_assert( !game_mem->assets_mem_pool_canary );

			kiss_fft_cpx ** freqs = data->freqs + i;
			if ( *freqs )
			{
				mp->deallocate( *freqs );
			}
			//lpr_log_dbg( "cfg: %d, %d", cfg->dimReal, cfg->dimOther );
			lpr_log_dbg( "mp occupied = %lu/%lu (%04.01f%%)", mp->occupied, mp->size, (f32)mp->occupied / (f32)mp->size );
			*freqs = (kiss_fft_cpx*) mp->allocate( freqsn.w * freqsn.h * sizeof( kiss_fft_cpx ) );
			lpr_log_dbg( "mp occupied = %lu/%lu (%04.01f%%)", mp->occupied, mp->size, (f32)mp->occupied / (f32)mp->size );
			lpr_assert( *freqs );

			lpr_assert( !data->canary );
			lpr_assert( !game_mem->assets_mem_pool_canary );
			{
				u64 s = data->rgbs[1][0] ? ((u64*)((u8*)(data->rgbs[1][0]) - 8))[0] : 0;
				lpr_assert( s <= game_mem->assets_mem_pool.size );
			}

			kiss_fftnd( cfg, rgbc[i][c], *freqs);

			lpr_assert( !data->canary );
			lpr_assert( !game_mem->assets_mem_pool_canary );
			{
				u64 s = data->rgbs[1][0] ? ((u64*)((u8*)(data->rgbs[1][0]) - 8))[0] : 0;
				lpr_assert( s <= game_mem->assets_mem_pool.size );
			}

			u32 fntot = freqsn.w * freqsn.h;
			Lpr_Rgb_u8 * power_spectrum = (Lpr_Rgb_u8*)ms->push( sizeof( power_spectrum[0] ) * fntot );
			u64 * power_spectrum_u64 = (u64*)ms->push( sizeof( power_spectrum_u64[0] ) * fntot );
			for ( u32 f = 0; f != fntot; ++f )
			{
				freqs[0][f].r /= (f32)fntot;
				freqs[0][f].i /= (f32)fntot;
				u64 ps = squares64((s64)freqs[0][f].r) + squares64( (s64)freqs[0][f].i );
				power_spectrum_u64[f] = ps;
			}

			u64 psmin = U64_MAX, psmax = 0;
			for ( u32 f = 0; f != fntot; ++f )
			{
				psmin = min( power_spectrum_u64[f], psmin );
				psmax = max( power_spectrum_u64[f], psmax );
			}
			u64 psspan = psmax - psmin;
			f32 max_v = 0.0, min_v = F64_MAX;
			for ( u32 f = 0; f != fntot; ++f )
			{
				f32 v = log10f( 1.0f + 9.0f * log10f( 1.0f + 9.0f * (f32)((f64)(power_spectrum_u64[f] - psmin) / (f64)psspan) ) );
				min_v = min( v, min_v );
				max_v = max( v, max_v );
				power_spectrum[f] =
				   lpr_Rgb_u8_from_f32(
				      lpr_Rgb_f32_sRGB_from_linear(
				         rgb_from_hsv( {0.333333f, 1.0f, v } ) ) );
						 //rgb_from_hsv( {mod1( 10.0f * log10( 1.0 + (f64)(power_spectrum_u64[f] - psmin) / (f64)psspan ) ), 1.0f, 1.0f } ) ) );
			}
			lpr_log_dbg( "min_v = %f max_v = %f", min_v, max_v );

			fftshift( power_spectrum, freqsn.w, freqsn.h );

			lpr_assert( !data->canary );
			lpr_assert( !game_mem->assets_mem_pool_canary );
			{
				u64 s = data->rgbs[1][0] ? ((u64*)((u8*)(data->rgbs[1][0]) - 8))[0] : 0;
				lpr_assert( s <= game_mem->assets_mem_pool.size );
			}

			lpr_create_texture( power_spectrum, freqsn, LPR_RGB8, false, true, false, data->power_spectrum_images[i] + c, LPR_MIPMAP_TRILINEAR, LPR_NEAREST, LPR_CLAMP_TO_EDGE, LPR_CLAMP_TO_EDGE, LPR_SCALE_LINEAR, {1,1}, 0 );

			lpr_assert( !data->canary );
			lpr_assert( !game_mem->assets_mem_pool_canary );
			{
				u64 s = data->rgbs[1][0] ? ((u64*)((u8*)(data->rgbs[1][0]) - 8))[0] : 0;
				lpr_assert( s <= game_mem->assets_mem_pool.size );
			}

			ms->first_free = pop;
		}
	}
}
#endif
#endif
void
texture_gen_load_image( Game_Memory * game_mem, System_API * sys_api, u32 id )
{
	Game_Memory::Texture_Gen_Data * data = &game_mem->texture_gen_data;

	const char * file_path = (const char *)((u8**)data->folder_files_paths)[data->files_ids[id]];
	u8 * img_blob;
	V2u32 dims;
	{
		u64 s = data->rgbs[1][0] ? ((u64*)((u8*)(data->rgbs[1][0]) - 8))[0] : 0;
		lpr_assert( s <= game_mem->assets_mem_pool.size );
	}
	game_mem->mem_stack_for_asset_loading.first_free = 0;
	load_png( sys_api, file_path,
	          &game_mem->mem_stack_tmp,
	          &game_mem->mem_stack_for_asset_loading,
	          &img_blob, &dims.w, &dims.h );

	lpr_log_dbg( "loaded image '%s' %u x %u", file_path, dims.w, dims.h );

	{
		u64 s = data->rgbs[1][0] ? ((u64*)((u8*)(data->rgbs[1][0]) - 8))[0] : 0;
		lpr_assert( s <= game_mem->assets_mem_pool.size );
	}

	Lpr_Texture * image = data->images + id;
	lpr_create_texture( img_blob, dims, LPR_RGBA8, true, true, true, image, LPR_MIPMAP_TRILINEAR, LPR_NEAREST, LPR_CLAMP_TO_EDGE, LPR_CLAMP_TO_EDGE, LPR_SCALE_LINEAR, {1,1}, 0 );

	lpr_assert( !data->canary );
	lpr_assert( !game_mem->assets_mem_pool_canary );
	{
		u64 s = data->rgbs[1][0] ? ((u64*)((u8*)(data->rgbs[1][0]) - 8))[0] : 0;
		lpr_assert( s <= game_mem->assets_mem_pool.size );
	}

	Mem_Pool * mp = &game_mem->assets_mem_pool;

	static kiss_fft_cpx * rgbc[3] = {};
	for ( s32 c = 0; c!= 3; ++c )
	{
		if ( rgbc[c] )
		{
			mp->deallocate( rgbc[c] );
		}
		lpr_log_dbg( "mp occupied = %lu/%lu (%04.01f%%)", mp->occupied, mp->size, (f32)mp->occupied / (f32)mp->size );
		rgbc[c] = (kiss_fft_cpx*)mp->allocate( dims.w * dims.h * sizeof( kiss_fft_cpx ) );
		lpr_log_dbg( "mp occupied = %lu/%lu (%04.01f%%)", mp->occupied, mp->size, (f32)mp->occupied / (f32)mp->size );
		lpr_assert( rgbc[c] );
	}
	separate_image_channels_cpx( img_blob, dims.w, dims.h, rgbc );

	lpr_assert( !data->canary );
	lpr_assert( !game_mem->assets_mem_pool_canary );

	{
		//u8 * rgbu8[3];
		u8 ** rgbu8= data->separate_rgb_u8[id];
		Mem_Stack * ms = &game_mem->mem_stack_tmp;
		u64 pop = ms->first_free;
		for ( s32 c = 0; c != 3; ++c )
		{
			//rgbu8[c] = ms->push( dims.w * dims.h );
			if ( rgbu8[c] )
			{
				game_mem->assets_mem_pool.deallocate( rgbu8[c] );
			}
			rgbu8[c] = (u8*)game_mem->assets_mem_pool.allocate( dims.w * dims.h );
		}
		separate_image_channels_u8( img_blob, dims.w, dims.h, rgbu8 );
		for ( s32 c = 0; c != 3; ++c )
		{
			lpr_create_texture( rgbu8[c], dims, LPR_GRAYSCALE8, false, true, false, data->images_channels[id] + c, LPR_MIPMAP_TRILINEAR, LPR_NEAREST, LPR_CLAMP_TO_EDGE, LPR_CLAMP_TO_EDGE, LPR_SCALE_LINEAR, {1,1}, 0 );
		}
		//ms->first_free = pop;
		s32 area = dims.w * dims.h;
		for ( s32 c = 0; c != 3; ++c )
		{
			for ( s32 j = 0; j != area; ++j )
			{
				//rgbi[i][c][j].r <<= 24;
				rgbc[c][j].r *= U32_MAX / U8_MAX;
			}
		}
	}

	lpr_assert( !data->canary );
	lpr_assert( !game_mem->assets_mem_pool_canary );

	V2u32 freqsn = V2u32{dims.x, dims.y};
	for ( s32 c = 0; c!= 3; ++c )
	{
		Mem_Stack * ms = &game_mem->mem_stack_tmp;
		u64 pop = ms->first_free;
		size_t lenmem = 0;
		void * mem = 0;
		kiss_fftnd_alloc( make_V2s32(dims).arr, 2, 0, mem, &lenmem );
		mem = ms->push( lenmem );
		memset( mem, 0, lenmem );
		kiss_fftnd_cfg cfg = kiss_fftnd_alloc( make_V2s32(dims).arr, 2, 0, mem, &lenmem );

		lpr_assert( !data->canary );
		lpr_assert( !game_mem->assets_mem_pool_canary );

		kiss_fft_cpx ** freqs = data->freqs + id;
		if ( *freqs )
		{
			mp->deallocate( *freqs );
		}
		//lpr_log_dbg( "cfg: %d, %d", cfg->dimReal, cfg->dimOther );
		lpr_log_dbg( "mp occupied = %lu/%lu (%04.01f%%)", mp->occupied, mp->size, (f32)mp->occupied / (f32)mp->size );
		*freqs = (kiss_fft_cpx*) mp->allocate( freqsn.w * freqsn.h * sizeof( kiss_fft_cpx ) );
		lpr_log_dbg( "mp occupied = %lu/%lu (%04.01f%%)", mp->occupied, mp->size, (f32)mp->occupied / (f32)mp->size );
		lpr_assert( *freqs );

		lpr_assert( !data->canary );
		lpr_assert( !game_mem->assets_mem_pool_canary );
		{
			u64 s = data->rgbs[1][0] ? ((u64*)((u8*)(data->rgbs[1][0]) - 8))[0] : 0;
			lpr_assert( s <= game_mem->assets_mem_pool.size );
		}

		kiss_fftnd( cfg, rgbc[c], *freqs);

		lpr_assert( !data->canary );
		lpr_assert( !game_mem->assets_mem_pool_canary );
		{
			u64 s = data->rgbs[1][0] ? ((u64*)((u8*)(data->rgbs[1][0]) - 8))[0] : 0;
			lpr_assert( s <= game_mem->assets_mem_pool.size );
		}

		u32 fntot = freqsn.w * freqsn.h;
		Lpr_Rgb_u8 * power_spectrum = (Lpr_Rgb_u8*)ms->push( sizeof( power_spectrum[0] ) * fntot );
		u64 * power_spectrum_u64 = (u64*)ms->push( sizeof( power_spectrum_u64[0] ) * fntot );
		for ( u32 f = 0; f != fntot; ++f )
		{
			freqs[0][f].r /= (f32)fntot;
			freqs[0][f].i /= (f32)fntot;
			u64 ps = squares64((s64)freqs[0][f].r) + squares64( (s64)freqs[0][f].i );
			power_spectrum_u64[f] = ps;
		}

		u64 psmin = U64_MAX, psmax = 0;
		for ( u32 f = 0; f != fntot; ++f )
		{
			psmin = min( power_spectrum_u64[f], psmin );
			psmax = max( power_spectrum_u64[f], psmax );
		}
		u64 psspan = psmax - psmin;
		f32 max_v = 0.0, min_v = F64_MAX;
		for ( u32 f = 0; f != fntot; ++f )
		{
			f32 v = log10f( 1.0f + 9.0f * log10f( 1.0f + 9.0f * (f32)((f64)(power_spectrum_u64[f] - psmin) / (f64)psspan) ) );
			min_v = min( v, min_v );
			max_v = max( v, max_v );
			power_spectrum[f] =
			   lpr_Rgb_u8_from_f32(
			      lpr_Rgb_f32_sRGB_from_linear(
			         rgb_from_hsv( {0.333333f, 1.0f, v } ) ) );
					 //rgb_from_hsv( {mod1( 10.0f * log10( 1.0 + (f64)(power_spectrum_u64[f] - psmin) / (f64)psspan ) ), 1.0f, 1.0f } ) ) );
		}
		lpr_log_dbg( "min_v = %f max_v = %f", min_v, max_v );

		fftshift( power_spectrum, freqsn.w, freqsn.h );

		lpr_assert( !data->canary );
		lpr_assert( !game_mem->assets_mem_pool_canary );
		{
			u64 s = data->rgbs[1][0] ? ((u64*)((u8*)(data->rgbs[1][0]) - 8))[0] : 0;
			lpr_assert( s <= game_mem->assets_mem_pool.size );
		}

		{
			u8 ** power_spectrums_u8 = &data->power_spectrums_u8[id][c];
			if ( *power_spectrums_u8 )
			{
				game_mem->assets_mem_pool.deallocate( *power_spectrums_u8 );
			}
			*power_spectrums_u8 = (u8*)game_mem->assets_mem_pool.allocate( dims.w * dims.h );
			separate_image_channel_u8_red( (u8*)power_spectrum, freqsn.w, freqsn.h, *power_spectrums_u8 );
		}

		lpr_create_texture( power_spectrum, freqsn, LPR_RGB8, false, true, false, data->power_spectrum_images[id] + c, LPR_MIPMAP_TRILINEAR, LPR_NEAREST, LPR_CLAMP_TO_EDGE, LPR_CLAMP_TO_EDGE, LPR_SCALE_LINEAR, {1,1}, 0 );

		lpr_assert( !data->canary );
		lpr_assert( !game_mem->assets_mem_pool_canary );
		{
			u64 s = data->rgbs[1][0] ? ((u64*)((u8*)(data->rgbs[1][0]) - 8))[0] : 0;
			lpr_assert( s <= game_mem->assets_mem_pool.size );
		}

		ms->first_free = pop;
	}
}

void
compute_similarity_measures( Game_Memory::Texture_Gen_Data * data )
{
	for ( s32 c = 0; c != 3; ++c )
	{
		Image_Gray image_top    = { .pixels = data->separate_rgb_u8[0][c],
		                            .size   = make_V2u32( data->images_channels[0][c].size ) };
		Image_Gray image_bottom = { .pixels = data->separate_rgb_u8[1][c],
		                            .size   = make_V2u32( data->images_channels[1][c].size ) };
		data->mssim_rgb[c] = MSSIM( image_top, image_bottom );
		data->mstsim_rgb[c] = MSTSIM( image_top, image_bottom );
	}
	for ( s32 c = 0; c != 3; ++c )
	{
		Image_Gray image_top    = { .pixels = data->power_spectrums_u8[0][c],
		                            .size   = make_V2u32( data->power_spectrum_images[0][c].size ) };
		Image_Gray image_bottom = { .pixels = data->power_spectrums_u8[1][c],
		                            .size   = make_V2u32( data->power_spectrum_images[1][c].size ) };
		data->mssim_fft_rgb[c] = MSSIM( image_top, image_bottom );
		data->mstsim_fft_rgb[c] = MSTSIM( image_top, image_bottom );
	}
}

inline
void
texture_gen_render( Game_Memory * game_mem, System_API * sys_api )
{
	//Lpr_Draw_Data * draw_data = &game_mem->main_window.lpr_draw_data;
	u32 textures_batch_id = 1;
	Game_Memory::Texture_Gen_Data * data = &game_mem->texture_gen_data;
	Lpr_Draw_Data * draw_data = &game_mem->main_window.lpr_draw_data;
	V2 ws = make_V2( make_V2u32( draw_data->window_size ) );
	//V2s32 win = make_V2s32( make_V2u32( draw_data->window_size ) );
#if 0
	for ( s32 i = 0; i != 2; ++i )
	{
		Lpr_Texture * image = data->images + i;
		V2s32 img_size = make_V2s32( make_V2u32( image->size ) );
		V2 i_pos = hadamard( (V2)( img_size / 2 - V2s32{(s32)ws.w / 2, 0 } ),
		                     V2{1.0f,i?-1.0f:1.0f} );
		lpr_add_sprite_px_perfect( draw_data, textures_batch_id,
		                           image,
		                           i_pos,
		                           false, false, lpr_C_colors_u8.white );

		for ( s32 c = 0; c != 3; ++c )
		{
			Lpr_Texture * psp = data->power_spectrum_images[i] + c;
			V2s32 psp_size = make_V2s32( make_V2u32( psp->size ) );
			V2 psp_pos = hadamard( (V2)( psp_size / 2 + V2s32{img_size.w / 2 + psp_size.w * c,0} ) + V2{i_pos.x,0.0f},
			                       V2{1.0f,i?-1.0f:1.0f} );
			lpr_add_sprite_px_perfect( draw_data, textures_batch_id,
			                           psp,
			                           psp_pos,
			                           false, false, lpr_C_colors_u8.white );

			lpr_add_sprite_w_scale( draw_data, textures_batch_id,
			                        data->images_channels[i] + c,
			                        psp_pos + V2{0.0f,(i?-1.0f:1.0f) * (img_size.h / 2 + psp_size.h ) / 2 },
			                        0.5f, 0.0f, lpr_C_colors_u8.white );
		}
	}
#endif

	tgui_Context tgui_context = {};
	tgui_context.lepre.draw_data = draw_data;
	tgui_context.lepre.batch_id = textures_batch_id;
	tgui_context.lepre.multi_font = &game_mem->fonts.multi_font;
	tgui_context.lepre.font_id = 0;
	tgui_context.lepre.font_size_id = 1,
	tgui_context.lepre.bg_z = 1;
	tgui_Context_Init( &tgui_context );
	tgui_default_style( &tgui_context );
	lpr_stencil_clear_all( tgui_context.lepre.draw_data, tgui_context.lepre.batch_id, false );
	tgui_context.lepre.stencil_id = lpr_new_stencil( tgui_context.lepre.draw_data, tgui_context.lepre.batch_id );
	tgui_context.inputs = &game_mem->inputs;
	tgui_context.window_id = game_mem->main_window.window_id;

	auto * gui = &data->gui;

	Lpr_AA_Rect aa_rect = {};
	f32 bar_h = 32.0f;//tgui_container_get_current( &tgui_context )->contextual_data.button.min_size.h;
	V2 size = {ws.w, bar_h};
	aa_rect = lpr_AA_Rect_from_minmax( {0.0f, ws.h - bar_h}, {ws.w, ws.h} );
	tgui_container_begin( &tgui_context, aa_rect, 24.0f, false );
	tgui_line_start( &tgui_context, {}, aa_rect.size.w, bar_h );
	const char * txt = u8"open dir";
	if ( !gui->selecting_dir )
	{
		if ( tgui_button( &tgui_context, txt, str_bytes_to_null( txt ) ).clicked_left )
		{
			gui->selecting_dir = true;
		}

		tgui_button_unclickable( &tgui_context, data->folder_files_count < 2 );

		bool couple_changed = false;
		txt = u8"top <-";
		if ( tgui_button( &tgui_context, txt, str_bytes_to_null( txt ) ).clicked_left )
		{
			couple_changed = true;
			data->files_ids[TGEN_UP] =
			( data->files_ids[TGEN_UP] + data->folder_files_count - 1u ) % data->folder_files_count;
			texture_gen_load_image( game_mem, sys_api, TGEN_UP );
		}
		txt = u8"top ->";
		if ( tgui_button( &tgui_context, txt, str_bytes_to_null( txt ) ).clicked_left )
		{
			couple_changed = true;
			data->files_ids[TGEN_UP] =
			( data->files_ids[TGEN_UP] + 1u ) % data->folder_files_count;
			texture_gen_load_image( game_mem, sys_api, TGEN_UP );
		}
		txt = u8"bottom <-";
		if ( tgui_button( &tgui_context, txt, str_bytes_to_null( txt ) ).clicked_left )
		{
			couple_changed = true;
			data->files_ids[TGEN_DOWN] =
			( data->files_ids[TGEN_DOWN] + data->folder_files_count - 1u ) % data->folder_files_count;
			texture_gen_load_image( game_mem, sys_api, TGEN_DOWN );
		}
		txt = u8"bottom ->";
		if ( tgui_button( &tgui_context, txt, str_bytes_to_null( txt ) ).clicked_left )
		{
			couple_changed = true;
			data->files_ids[TGEN_DOWN] =
			( data->files_ids[TGEN_DOWN] + 1u ) % data->folder_files_count;
			texture_gen_load_image( game_mem, sys_api, TGEN_DOWN );
		}

		if ( couple_changed )
		{
			compute_similarity_measures( data );
		}
		char text[256] = {};
		u64 first_free = 0;
		first_free +=
		snprintf( text, sizeof( text ) - first_free,
		          "MSSIM: R = %f G = %f B = %f fftR = %f fftG = %f fftB = %f "
		          "MSTSIM: R = %f G = %f B = %f fftR = %f fftG = %f fftB = %f",
		          data->mssim_rgb[0],
		          data->mssim_rgb[1],
		          data->mssim_rgb[2],
		          data->mssim_fft_rgb[0],
		          data->mssim_fft_rgb[1],
		          data->mssim_fft_rgb[2],
		          data->mstsim_rgb[0],
		          data->mstsim_rgb[1],
		          data->mstsim_rgb[2],
		          data->mstsim_fft_rgb[0],
		          data->mstsim_fft_rgb[1],
		          data->mstsim_fft_rgb[2]
		        );
		//tgui_Container * container = tgui_container_get_current( &tgui_context );
		//V2 text_start_point = container->contextual_data.line.start;
		//text_start_point.x += container->contextual_data.line.curr_length;
		//tgui_text_start_point( &tgui_context, text_start_point );
		//tgui_line_carry( &tgui_context );
		tgui_text( &tgui_context, text, first_free );
	}
	tgui_container_end( &tgui_context, 0 );

	size = ws;
	size.h -= bar_h;
	aa_rect.size = size;
	aa_rect.pos = size * 0.5f;
	if ( data->gui.selecting_dir )
	{
		if ( !gui->dir_path )
		{
			gui->dir_path = (u8*)".";
		}
		tgui_container_begin( &tgui_context, aa_rect, 24.0f, false );
		tgui_File_Select_Data * file_select_data = &gui->file_select_data;
		tgui_file_select_dirs_only( file_select_data );
		tgui_file_select_start_dir( file_select_data, (const u8*)gui->dir_path );
		tgui_file_select( &tgui_context, file_select_data );
		tgui_container_end( &tgui_context, 0 );
		if ( ! tgui_file_select_is_active( file_select_data ) )
		{
			gui->selecting_dir = false;
			if ( tgui_file_select_aborted( file_select_data ) )
			{
			} else
			{
				gui->dir_path = (u8*)tgui_file_select_file_path( file_select_data );
				s64 res =
				sys_api->list_directory( (const char *)gui->dir_path,
				                         data->folder_files_paths,
				                         sizeof(data->folder_files_paths), SYS_LDIR_FULL_PATHS );
				if ( SYS_API_RET_IS_ERROR( res ) )
				{
					lpr_log_err( "Error listing dir '%s': %s\nDescription: %s",
					             gui->dir_path, sys_api->get_error_name( res ),
					             sys_api->get_error_description( res ) );
					gui->dir_path = 0;
				} else
				{
					lpr_log_dbg( "directory has %lu files", res );
					u8 ** files = (u8**)data->folder_files_paths;
					u8 ext[] = ".png";
					u8 * pattern = &ext[0];
					u32 pattern_bytes_to_null = sizeof( ext ) - 1;
					strings_compute_bytes_to_null( files, res, data->folder_files_paths_bytes_to_null );
					data->folder_files_count =
					strings_filter_ends( files, res, data->folder_files_paths_bytes_to_null,
					                     &pattern, 1, &pattern_bytes_to_null );
					lpr_log_dbg( "filtered files are %u", data->folder_files_count );
					for ( u32 i = 0; i != data->folder_files_count; ++i )
					{
						lpr_log_dbg( "files[%02d] %s", i, files[i] );
					}
					if ( data->folder_files_count >= 2 )
					{
						for ( u32 i = 0; i != 2; ++i )
						{
							data->files_ids[i] = i;
							texture_gen_load_image( game_mem, sys_api, i );
						}
						compute_similarity_measures( data );
					}
				}
			}
		}
	} else
	{
		tgui_Scrolling_Data * scrolling_data = &data->gui.scrolling_data;
		tgui_container_begin( &tgui_context, aa_rect, 24.0f, true );
		tgui_container_dynamic_scrolling_directions(
		   &tgui_context, TGUI_SCROLLING_H | TGUI_SCROLLING_V, scrolling_data );
		tgui_container_set_scrolling( &tgui_context, scrolling_data->scrolling );

		//tgui_Container * container = tgui_container_get_current( &tgui_context );
		//f32 button_h = container->contextual_data.button.max_size.h;
		f32 win_border = 10.0f;
		tgui_line_start( &tgui_context, {}, FLT_MAX, 512 );

		//tgui_pic_px_perfect( &tgui_context );
		//tgui_pic_size( &tgui_context, V2{256.0f,256.0f} );
		if ( data->folder_files_count >= 2 )
		{
			for ( s32 i = 0; i != 2; ++i )
			{
				Lpr_Texture * image = data->images + i;
				tgui_pic( &tgui_context, image );

				for ( s32 c = 0; c != 3; ++c )
				{
					Lpr_Texture * psp = data->power_spectrum_images[i] + c;
					tgui_pic( &tgui_context, psp );
					tgui_pic( &tgui_context, data->images_channels[i] + c );
				}
				tgui_line_carry( &tgui_context );
			}
		}

		tgui_line_stop( &tgui_context );
		tgui_container_end( &tgui_context, scrolling_data );
	}
}
