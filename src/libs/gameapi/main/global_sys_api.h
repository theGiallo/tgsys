#ifndef GLOBAL_SYS_API_H
#define GLOBAL_SYS_API_H 1

#include "generic_system.h"
extern
System_API * global_sys_api;

inline
u8
thread_sys_id()
{
	u8 ret;

	while( !global_sys_api ){}
	ret = global_sys_api->thread_id();

	return ret;
}

#endif /* ifndef GLOBAL_SYS_API_H */
