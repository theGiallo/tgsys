#ifndef INTRINSICS_H
#define INTRINSICS_H 1

#include "basic_types.h"

#include <time.h>

//#include <immintrin.h>
#include <x86intrin.h>
#include <cpuid.h>
#define RDTSC() _rdtsc()
#define RDTSC_SERIALIZING() _rdtsc_serializing()

inline
u64
ns_time()
{
	u64 ret = {};

	timespec t;
	s32 r = clock_gettime( CLOCK_REALTIME, &t );
	if ( r == -1 )
	{
		lpr_log_err( "error: clock_gettime\n" );
	} else
	{
		ret = t.tv_sec * (u64)1e9 + t.tv_nsec;
	}

	return ret;
}

inline
u64
_rdtsc_serializing()
{
	u32 tmp;
	__cpuid( 0, tmp, tmp, tmp, tmp );
	return _rdtsc();
}

// NOTE(theGiallo): intrinsics
#define COMPILER_BARRIER() asm volatile("" ::: "memory")
#define STORELOAD_BARRIER() asm volatile("mfence" ::: "memory")

#if defined(__GNUC__) || defined(__GNUG__) || defined(__clang__)
	#define PRAGMA_ALIGN_PRE(bits)
	#define PRAGMA_ALIGN_POST( bits ) __attribute__ ((aligned ( bits/8 )))
#elif defined(_WIN32)
	#define PRAGMA_ALIGN_PRE( bits ) __declspec(align( bits/8 ))
	#define PRAGMA_ALIGN_POST(bits)
#endif

#if defined(__clang__) || defined(__GNUC_) || defined(__GNUG__)
	#define interlocked_compare_exchange_64( dst, exchange, comparand ) \
	__sync_val_compare_and_swap( dst, comparand, exchange )
#elif defined(_WIN32) 
	#define interlocked_compare_exchange_64( dst, exchange, comparand ) \
	InterlockedCompareExchange64(dst, exchange, comparand )
#endif

#if defined(__clang__) || defined(__GNUC_) || defined(__GNUG__)
	#define interlocked_compare_exchange_32( dst, exchange, comparand ) \
	__sync_val_compare_and_swap( dst, comparand, exchange )
#elif defined(_WIN32) 
	#define interlocked_compare_exchange_32( dst, exchange, comparand ) \
	InterlockedCompareExchange(dst, exchange, comparand )
#endif

#endif /* ifndef INTRINSICS_H */
