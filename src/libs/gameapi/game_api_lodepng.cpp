#include "game_api_lodepng.h"
#undef internal
#include <lodepng.cpp>
#define internal static

bool
load_png( System_API * sys_api, const char * file_path,
          Mem_Stack * tmp_mem, Mem_Stack * mem,
          u8 ** out_blob, u32 * width, u32 * height )
{
	u32 w, h;
	u8 * lodepng_blob;
	u32 res;

	s64 file_size, read_size;

	file_size = sys_api->get_file_size( file_path );
	if ( file_size < 0 )
	{
		lpr_log_err( "error getting file size in %s, file '%s',\n"
		             "error name: %s\nerror description:\n%s",
		             __PRETTY_FUNCTION__, file_path,
		             sys_api->get_error_name( file_size ),
		             sys_api->get_error_description( file_size ) );
		return false;
	}

	// TODO(theGiallo, 2016-04-18): if this is too much use a tmp mem allocator
	//u8 file_mem[file_size];
	u64 old_tmp_stack_first = tmp_mem->first_free;
	u8 * file_mem = (u8*)tmp_mem->push( file_size );

	lpr_log_dbg( "file_size = %lu", file_size );

	read_size =
	sys_api->read_entire_file( file_path, file_mem, file_size );
	if ( read_size != file_size )
	{
		if ( read_size < 0 )
		{
			lpr_log_err( "error reading file in %s, file '%s',\n"
			             "error name: %s\nerro description: %s",
			             __PRETTY_FUNCTION__, file_path,
			             sys_api->get_error_name( file_size ),
			             sys_api->get_error_description( file_size ) );
		} else
		{
			lpr_log_err( "read_entire_file read less bytes(%ld) than "
			             "get_file_size returned (%ld) ",
			             read_size, file_size );
		}

		tmp_mem->first_free = old_tmp_stack_first;
		return false;
	}

	res =
#if 1
	lodepng_decode32( &lodepng_blob, &w, &h, file_mem, file_size );
#else
	lodepng_decode_memory( &lodepng_blob, &w, &h, file_mem, file_size,
	                       LodePNGColorType::LCT_RGBA, 8 );
#endif

	tmp_mem->first_free = old_tmp_stack_first;
	if ( res )
	{
		lpr_log_err( "lodepng error %u: %s", res,
		             lodepng_error_text( res ) );
		// TODO(theGiallo, 2016-05-28): can we free lodepng mem?
		lodepng_free( lodepng_blob );
		return false;
	}

	// NOTE(theGiallo): image loaded, now little fix ---------------------------

	const u32 bytes_per_pixel = 4;
	u64 raw_size = w * h * bytes_per_pixel;
	u8 * final_blob = mem->push( raw_size );
	if ( !final_blob )
	{
		lpr_log_err( "no more memory available into the assets mem stack!" );
		return false;
	}
	// TODO(theGiallo, 2016-05-28): make lodepng use custom mem functions
	memcpy( final_blob, lodepng_blob, raw_size );
	lodepng_free( lodepng_blob );

	flip_RGBA8_image_vertically_and_premultiply_alpha( final_blob, w, h, true );

	*out_blob = final_blob;
	*width  = w;
	*height = h;

	return true;
}

void
flip_RGBA8_image_vertically_and_premultiply_alpha( u8 * image,
                                                   s32 width, s32 height,
                                                   bool sRGB )
{
	const s32 hh = height / 2;
	const s32 w  = width;
	const s32 BYTES_PER_PIXEL = 4;
	const s32 BYTES_PER_ROW = w * BYTES_PER_PIXEL;
	u8 * bottom = image + BYTES_PER_ROW * ( height - 1 );
	u8 * top = image;
	for ( s32 y = 0; y != hh; ++y )
	{
		u8 row_buf[BYTES_PER_ROW];
		memcpy( row_buf, top,     BYTES_PER_ROW );
		memcpy( top,     bottom,  BYTES_PER_ROW );
		memcpy( bottom,  row_buf, BYTES_PER_ROW );
		for ( s32 x = 0; x != BYTES_PER_ROW; x += BYTES_PER_PIXEL )
		{
			f32 top_alpha    = (f32)top   [x+3] / 255.0f;
			f32 bottom_alpha = (f32)bottom[x+3] / 255.0f;
			if ( sRGB )
			{
				top_alpha    = lpr_pow( top_alpha,    1.0f/2.2f );
				bottom_alpha = lpr_pow( bottom_alpha, 1.0f/2.2f );
			}
			for ( s32 c = 0; c != 3; ++c )
			{
				top   [x+c] = ( top   [x+c] * top_alpha    ) + 0.5f;
				bottom[x+c] = ( bottom[x+c] * bottom_alpha ) + 0.5f;
			}
		}
		bottom -= BYTES_PER_ROW;
		top    += BYTES_PER_ROW;
	}

	if ( hh * 2 != height )
	// NOTE: center row
	{
		for ( s32 x = 0; x != BYTES_PER_ROW; x += BYTES_PER_PIXEL )
		{
			f32 top_alpha    = (f32)top   [x+3] / 255.0f;
			if ( sRGB )
			{
				top_alpha    = lpr_pow( top_alpha,    1.0f/2.2f );
			}
			for ( s32 c = 0; c != 3; ++c )
			{
				top   [x+c] = ( top   [x+c] * top_alpha    ) + 0.5f;
			}
		}
	}
}
