#define GAME_API_CPP 1

#if !WE_LOAD_OUR_GL
#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/gl.h>
#else
//#define OUR_GL_EXTERN_POINTERS 1
#define OUR_GL_EXPORT_POINTERS 1
#include "our_gl.h"
#endif

#define GAME_API_FFT_EMIT_IMPLEMENTATION 1

#include "game_api_lodepng.h"
#include "game_api_lodepng.cpp"

#define STBTT_STATIC
#define STB_TRUETYPE_IMPLEMENTATION
#include "stb_truetype.h"

#define STB_VORBIS_NO_STDIO
//#define STB_VORBIS_NO_CRT
#define STB_VORBIS_NO_PUSHDATA_API

#define STB_VORBIS_HEADER_ONLY
#include "stb_vorbis.c"

#include "game_api_lepre.h"

#include "game_api.h"

#include "global_game_mem.h"
PRAGMA_ALIGN_PRE(64)
Game_Memory * global_game_mem PRAGMA_ALIGN_POST(64);

#include "global_sys_api.h"
PRAGMA_ALIGN_PRE(64)
System_API * global_sys_api PRAGMA_ALIGN_POST(64);

#define MH_ILLEGAL_PATH LPR_ILLEGAL_PATH
#include "game_api_mem_hotels.cpp"

#include "intrinsics.h"

#include "tgprof.h"

#include <float.h>
#include <unistd.h> // usleep

#include "game_api_render.h"

#include "game_api_opengl_tools.h"
#include "game_api_utility.h"

#define USE_F64 0
#if USE_F64
#define SPEED_OF_SOUND 340.0
#else
#define SPEED_OF_SOUND 340.0f
#endif
#define SOUND_MAX_DELAY_TIME 1.0f
#define SOUND_DIST_MAX (SPEED_OF_SOUND*SOUND_MAX_TIME_DELAY)
#define SOUND_MAX_DELAY_SAMPLES (SOUND_MAX_DELAY_TIME*AUDIO_SAMPLING_FREQUENCY)
#define SOUND_SAMPLE_DELAY_FROM_DISTANCE(d) \
           ((d) * (AUDIO_SAMPLING_FREQUENCY/SPEED_OF_SOUND))

#define DEBUG_TO_FILE 0
#if DEBUG_TO_FILE
static u64 samples_played;
static FILE * right_ear;
static FILE * right_ear_sql;
static FILE * right_ear_dist;
static FILE * right_ear0;
static FILE * right_ear_sql_sampled;
static FILE * right_ear_dist_sampled;
#endif

const char * prof_top_sorts_name_table[] =
   { PROFTOP_SORTS( PROFTOP_EXPAND_COLUMN_AS_STRING ) };

////////////////////////////////////////////////////////////////////////////////

internal
const Font_LL *
pack_single_font( System_API * sys_api,
                  const char * font_path,
                  Fonts * fonts, f32 pixel_height,
                  const u8 * utf8_glyphs, u32 oversample_v, u32 oversample_h );

internal
const Font_LL *
pack_single_font( System_API * sys_api,
                  const char * font_path,
                  Fonts * fonts, f32 pixel_height,
                  const u8 * utf8_glyphs, u32 oversample_v, u32 oversample_h )
{
	const Font_LL * ret = NULL;
	u8 atlas_blob[FONT_ATLAS_W * FONT_ATLAS_H];
	u8 font_file_blob[MAX_FONT_FILE_SIZE];

	s32 i_res =
	sys_api->read_entire_file( font_path, font_file_blob,
	                           MAX_FONT_FILE_SIZE );
	if ( SYS_API_RET_IS_ERROR( i_res ) )
	{
		lpr_log_err( "failed to load font file '%s' [%d]. %s:%s",
		             font_path, i_res, sys_api->get_error_name( i_res ),
		             sys_api->get_error_description( i_res ) );
		return ret;
	}

	const s32 glyphs_count =
	   lpr_utf8_char_count( utf8_glyphs, UINT_MAX, true );
	s32 ucs4_codepoints[glyphs_count];
	const u8 * curr_char =  utf8_glyphs;
	u32 bytes_size = 0;
	while ( *(curr_char++) ){ ++bytes_size; }
	curr_char =  utf8_glyphs;
	for ( s32 i = 0; i < glyphs_count; ++i )
	{
		Lpr_UTF8_Fullchar fullchar = lpr_utf8_full_char( curr_char );
		lpr_assert( fullchar.all );
		ucs4_codepoints[i] = lpr_utf8_fullchar_to_ucs4( fullchar );
		curr_char =
		lpr_utf8_next_char_ptr( curr_char, utf8_glyphs, bytes_size );
	}
	{
		u32 tmp[glyphs_count];
		lpr_radix_sort_mc_32_keys_only( (u32*)ucs4_codepoints, tmp,
		                                glyphs_count );
	}
	s32 unique_glyphs_count = 0;
	for ( s32 i = 0; i < glyphs_count; ++i )
	{
		s32 j;
		for ( j = 0; j < unique_glyphs_count; ++j )
		{
			if ( ucs4_codepoints[j] == ucs4_codepoints[i] )
			{
				break;
			}
		}
		if ( j == unique_glyphs_count )
		{
			ucs4_codepoints[unique_glyphs_count++] = ucs4_codepoints[i];
		}
	}
	stbtt_packedchar char_data[unique_glyphs_count];
	stbtt_pack_range font_range;
	font_range.num_chars = unique_glyphs_count;
	font_range.array_of_unicode_codepoints = ucs4_codepoints;
	font_range.chardata_for_range = char_data;
	font_range.font_size = pixel_height;
	font_range.v_oversample = oversample_v;
	font_range.h_oversample = oversample_h;

	const s32 padding = 1;

	stbtt_pack_context pack_context{};
	i_res =
	stbtt_PackBegin( &pack_context, atlas_blob,
	                 FONT_ATLAS_W, FONT_ATLAS_H, 0, padding, NULL );
	if ( !i_res )
	{
		lpr_log_err( "stbtt_PackBegin failed!" );
		return ret;
	}

	stbtt_PackSetOversampling( &pack_context, oversample_v, oversample_h );
	oversample_v = pack_context.v_oversample;
	oversample_h = pack_context.h_oversample;

	//pack_context.v_oversample = oversample_v;
	//pack_context.h_oversample = oversample_h;
	//font_range.v_oversample = oversample_v;
	//font_range.h_oversample = oversample_h;

	i_res =
	stbtt_PackFontRanges( &pack_context, font_file_blob, 0, &font_range, 1 );
	if ( !i_res )
	{
		lpr_log_err( "stbtt_PackFontRanges failed! (font='%s' pixel height %f)", font_path, pixel_height );
		return ret;
	}

	stbtt_PackEnd( &pack_context );

	stbtt_fontinfo font_info;
	i_res =
	stbtt_InitFont( &font_info, font_file_blob,
	                stbtt_GetFontOffsetForIndex( font_file_blob, 0 ) );
	if ( !i_res )
	{
		lpr_log_err( "stbtt_InitFont failed!" );
		return ret;
	}

	// TODO(theGiallo, 2016-03-15): check if the font is already there, maybe
	// hash the filename concatenated with the TTF font id; also check for size
	// to be already there
	const Font_LL * f =
	fonts->find_or_record( font_path, pixel_height );
	if ( !f )
	{
		lpr_log_err( "cannot store another font!" );
		LPR_ILLEGAL_PATH();
	}

	u32 font_id  = f->font_id;
	u32 atlas_id = f->atlas_id;
	u32 size_id  = f->size_id;

	Lpr_Multi_Font * multi_font = &fonts->multi_font;
	Lpr_Font * font = multi_font->fonts + font_id;
	font->scales[size_id] =
	   stbtt_ScaleForPixelHeight( &font_info, pixel_height );
	font->px_heights[size_id] = pixel_height;
	Lpr_Font_Of_Size * of_size = font->of_size + size_id;
	of_size->oversampling_scale.x = 1.0f / oversample_h;
	of_size->oversampling_scale.y = 1.0f / oversample_v;

	stbtt_GetFontVMetrics( &font_info,
	                       &font->metrics.ascent,
	                       &font->metrics.descent,
	                       &font->metrics.line_gap );
	for ( s32 i = 0; i < unique_glyphs_count; ++i )
	{
		s32 codepoint = ucs4_codepoints[i];
		Lpr_UTF8_Fullchar fullchar =
		   lpr_utf8_fullchar_from_ucs4( (u32)codepoint );
		s32 h =  lpr_hash_fullchar( fullchar );
		s32 glyph_id =
		stbtt_FindGlyphIndex( &font_info, codepoint );

		if ( of_size->glyphs_indexes_ht[h] )
		{
			lpr_log_err( "codepoint -> glyph hash map collision! (codepoint = %d, fullchar = %.4s, h=%d)", codepoint, fullchar.subchars, h );
			LPR_ILLEGAL_PATH();
		}
		of_size->glyphs_indexes_ht[h] = glyph_id;

		Lpr_Glyph_Metrics * cm = font->glyphs_metrics + glyph_id;
		stbtt_GetGlyphHMetrics( &font_info, glyph_id,
		                        &cm->advance_width,
		                        &cm->left_side_bearing );
	}

	for ( s32 i = 0; i < unique_glyphs_count; ++i )
	{
		s32 codepoint_i = ucs4_codepoints[i];
		Lpr_UTF8_Fullchar fullchar_i =
		   lpr_utf8_fullchar_from_ucs4( (u32)codepoint_i );
		s32 glyph_id_i = lpr_glyph_id_from_fullchar( of_size, fullchar_i );

		for ( s32 j = 0; j < unique_glyphs_count; ++j )
		{
			s32 codepoint_j = ucs4_codepoints[j];
			Lpr_UTF8_Fullchar fullchar_j =
			   lpr_utf8_fullchar_from_ucs4( (u32)codepoint_j );
			s32 glyph_id_j = lpr_glyph_id_from_fullchar( of_size, fullchar_j );

			font->kerning_table[glyph_id_i][glyph_id_j] =
			stbtt_GetGlyphKernAdvance( &font_info, glyph_id_i, glyph_id_j );
		}
	}
	for ( s32 i = 0; i < unique_glyphs_count; ++i )
	{
		s32 codepoint = ucs4_codepoints[i];
		Lpr_UTF8_Fullchar fullchar =
		   lpr_utf8_fullchar_from_ucs4( (u32)codepoint );
		s32 glyph_id = lpr_glyph_id_from_fullchar( of_size, fullchar );
		of_size->atlases_of_glyphs[glyph_id] = atlas_id;
		Lpr_Glyph_In_Atlas * gia = of_size->glyphs_in_atlas + glyph_id;

		char_data[i].x0 -= padding;
		char_data[i].y0 -= padding;
		char_data[i].x1 += padding;
		char_data[i].y1 += padding;

		gia->px_size =
		   {(u32)(char_data[i].x1 - char_data[i].x0),
		    (u32)(char_data[i].y1 - char_data[i].y0)};

		gia->pos_offset_px =
		   lpr_V2_mult_f32(
		      lpr_V2_add_V2( lpr_V2( char_data[i].xoff,
		                            -char_data[i].yoff),
		                     lpr_V2( char_data[i].xoff2,
		                            -char_data[i].yoff2) ),
		      0.5f );

		gia->bl_uv.x = char_data[i].x0 / (f32)FONT_ATLAS_W;
		gia->bl_uv.y = 1.0f - char_data[i].y1 / (f32)FONT_ATLAS_H;
		gia->uv_size.x =
		   ( char_data[i].x1 - char_data[i].x0 ) /
		   (f32)FONT_ATLAS_W;
		gia->uv_size.y =
		   ( char_data[i].y1 - char_data[i].y0 ) /
		   (f32)FONT_ATLAS_H;

		gia->x_advance_px = char_data[i].xadvance;
	}

	bool b_ret =
	lpr_create_texture( atlas_blob,
	                    {FONT_ATLAS_W, FONT_ATLAS_H},
	                    LPR_GRAYSCALE8_ALPHA,
	                    false,
	                    false,
	                    false,
	                    multi_font->atlases + atlas_id,
	                    Lpr_Texture_Filter::LPR_BILINEAR,
	                    Lpr_Texture_Filter::LPR_BILINEAR,
	                    LPR_CLAMP_TO_EDGE,
	                    LPR_CLAMP_TO_EDGE,
	                    Lpr_Scale_Filter::LPR_SCALE_LINEAR,
	                    {1,1},
	                    NULL );
	if ( !b_ret )
	{
		lpr_log_err( "failed to create texture for font atlas!" );
	}

	ret = f;

	return ret;
}

////////////////////////////////////////////////////////////////////////////////

internal
bool
load_vorbis( System_API * sys_api, const char * file_path, Mem_Stack * mem, Sound * out_sound );

////////////////////////////////////////////////////////////////////////////////

#include "game_api_utility.cpp"
#include "tgmath.cpp"
#include "game_api_texture_gen.cpp"
#include "game_api_opengl_tools.cpp"
#define TGUI_IMPLEMENTATION 1
#include "tgui.h"

////////////////////////////////////////////////////////////////////////////////

internal
inline
V2
make_25d_from_3d( Game_Memory * game_mem, V3 * pos, f32 * scale_in_out )
{
	(void) game_mem;
	(void) scale_in_out;

	V2 ret = pos->xy;
	ret.y += pos->z;

	return ret;
}

inline
internal
f32
sort_value( V3 * pos )
{
	// TODO(theGiallo, 2016-06-03): this works, but is plausible that won't
	// in cases with big coordinates. The real solution would be to sort
	// a 64bit key like this: |-Y|Z|
	f32 ret = (1e2 + pos->z) * 1e0f + ( 1e2 - pos->y ) * 1e4f;

	return ret;
}
enum
Ortho_Orientation : u8
{
	OO_HORIZONTAL,
	OO_VERTICAL,
};

#if 0
internal
bool
add_25d_sprite( Game_Memory * game_mem, const Lpr_Texture * lpr_texture,
                V3 * pos, V2 center, V2 size, f32 rot_deg, Ortho_Orientation oo,
                Lpr_Rgba_u8 color )
{
	f32 scale = 1.0f;
	V2 pos_25d = make_25d_from_3d( game_mem, pos, &scale );
	size *= scale;
	pos_25d -= hadamard( center - V2{0.5f,0.5f}, size );


	s32 id =
	lpr_add_sprite_w_size( &game_mem->main_window.lpr_draw_data, WORLD_BATCH_ID,
	                       lpr_texture, pos_25d, size, rot_deg, color );
	if ( id < 0 )
	{
		return false;
	}
	V3 p = *pos;
	p.z += size.h * ( 1.0f - center.y );
	if ( oo == OO_HORIZONTAL )
	{
		p.y += size.h * ( 1.0f - center.y );
	}
	game_mem->y_sorting_values[id] = sort_value( &p );

	return true;
}

internal
bool
load_resource_image( System_API * sys_api, Game_Memory * game_mem,
                     const char * file_path, Resource_Image ** res_image_out )
{
	bool ret = false;
	bool b_res;
	Image image = {};

	b_res =
	load_png( sys_api, file_path,
	          &game_mem->mem_stack_tmp,
	          &game_mem->mem_stack_for_asset_loading,
	          &image.raw,
	          &image.size.w,
	          &image.size.h );
	if ( !b_res )
	{
		lpr_log_err( "failed to load test image!" );
		return ret;
	}

	u32 guid = game_mem->resources.guids.find_or_record( file_path );
	bool already_there = false;
	Resource_Image * res_image =
	   game_mem->resources.images.allocate( guid, &already_there);
	if ( !res_image )
	{
		lpr_log_err( "no more free Resource_Image" );
		*res_image_out = NULL;
		return ret;
	}
	if ( !already_there )
	{
		Lpr_Texture * lpr_texture =
		   allocate_clean( &game_mem->lpr_textures_mh );
		if ( !lpr_texture )
		{
			lpr_log_err( "no more free lpr_textures" );
			game_mem->resources.images.deallocate( res_image );
			*res_image_out = NULL;
			return ret;
		}

		res_image->image = image;

		b_res =
		lpr_create_texture( res_image->image.raw,
		                    res_image->image.size,
		                    Lpr_Img_Fmt::LPR_RGBA8,
		                    true, true, true,
		                    lpr_texture,
		                    Lpr_Texture_Filter::LPR_MIPMAP_TRILINEAR,
		                    Lpr_Texture_Filter::LPR_BILINEAR,
		                    Lpr_Wrap::LPR_CLAMP_TO_EDGE,
		                    Lpr_Wrap::LPR_CLAMP_TO_EDGE,
		                    Lpr_Scale_Filter::LPR_SCALE_LINEAR,
		                    V2{1,1},
		                    NULL );
		if ( !b_res )
		{
			lpr_log_err( "failed to create metal man texture!" );
			game_mem->resources.images.deallocate( res_image   );
			deallocate( &game_mem->lpr_textures_mh, lpr_texture );
			*res_image_out = NULL;
			return ret;
		}

		res_image->lpr_texture = lpr_texture;
	}

	*res_image_out = res_image;

	ret = true;
	return ret;
}
#endif

Resource_Image *
Resource_Image_Storage::
allocate( u32 guid, bool * already_there )
{
	PROFILE_SECTION();

	Resource_Image * ret = NULL;

	if ( is_full( &this->storage ) )
	{
		return ret;
	}

	u32 h = guid % MH_ROOMS;
	u32_u32_LL ** ln;
	for ( ln = this->guids_hm + h; (*ln); ln = &(*ln)->next )
	{
		if ( (*ln)->key == guid )
		{
			ret = this->storage.mem + (*ln)->value;
			*already_there = true;
			return ret;
		}
	}

	ret = allocate_clean( &this->storage );
	if ( ret )
	{
		s32 i = ( ret - this->storage.mem ) / sizeof( Resource_Image );
		*ln = ::allocate( &this->list_nodes_hm );
		if ( *ln )
		{
			(*ln)->next  = NULL;
			(*ln)->key   = guid;
			(*ln)->value = i;
			ret->guid = guid;
		} else
		{
			lpr_log_err( "no more hash table list nodes!" );
			::deallocate( &this->storage, ret );
			ret = NULL;
			return ret;
		}
	}

	return ret;
}

void
Resource_Image_Storage::
deallocate( Resource_Image * ri )
{
	PROFILE_SECTION();

	lpr_assert( ri >= this->storage.mem );
	lpr_assert( ri <  this->storage.mem + MH_ROOMS );

	u32 guid = ri->guid;
	u32 h = guid % MH_ROOMS;
	u32_u32_LL ** ln;
	for ( ln = this->guids_hm + h; (*ln); ln = &(*ln)->next )
	{
		if ( (*ln)->key == guid )
		{
			u32_u32_LL * to_del_node = *ln;
			*ln = (*ln)->next;
			::deallocate( &this->list_nodes_hm, to_del_node );
		}
	}

	::deallocate( &this->storage, ri );
}

u32
Resources_GUIDs_Table::
record( const char * path )
{
	PROFILE_SECTION();

	u32 ret = this->compute_guid( path );

	if ( !this->_record( ret, path ) )
	{
		ret = 0;
	}

	return ret;
}

bool
Resources_GUIDs_Table::
_record( u32 guid, const char * path )
{
	PROFILE_SECTION();

	bool ret = true;

	if ( is_full( &this->paths ) )
	{
		ret = false;
		return ret;
	}

	u32 h = guid % MH_ROOMS;
	u32_u32_LL ** ln_old = this->guids_hm + h,
	            * ln;

	char ** str = allocate_clean( &this->paths );
	if ( str )
	{
		u64 strl = strlen( path ) + 1;
		char * sm = (char*)this->paths_pool.allocate( strl );
		memcpy( sm, path, strl );
		*str = sm;
		s32 i = ( str - this->paths.mem ) / sizeof( this->paths.mem[0] );
		ln = allocate( &this->list_nodes_hm );
		if ( ln )
		{
			ln->next  = *ln_old;
			ln->key   = guid;
			ln->value = i;
			*ln_old = ln;
		} else
		{
			lpr_log_err( "no more hash table list nodes!" );
			deallocate( &this->paths, str );
			this->paths_pool.deallocate( *str );
			ret = false;
			return ret;
		}
	}

	return ret;
}

u32
Resources_GUIDs_Table::
find_or_record( const char * path )
{
	PROFILE_SECTION();

	u32 ret = 0;

	if ( !this->find( path, &ret ) )
	{
		if ( !this->_record( ret, path ) )
		{
			ret = 0;
		}
	}

	return ret;
}

void
Resources_GUIDs_Table::
remove( u32 guid )
{
	PROFILE_SECTION();

	u32 h = guid % MH_ROOMS;
	u32_u32_LL ** ln;
	char ** to_del_path = 0;
	for ( ln = this->guids_hm + h; (*ln); ln = &(*ln)->next )
	{
		if ( (*ln)->key == guid )
		{
			u32_u32_LL * to_del_node = *ln;
			to_del_path = this->paths.mem + to_del_node->value;
			*ln = (*ln)->next;
			deallocate( &this->list_nodes_hm, to_del_node );
			this->paths_pool.deallocate( *to_del_path );
			deallocate( &this->paths, to_del_path );
		}
	}

}

const char *
Resources_GUIDs_Table::
find( u32 guid, u32_u32_LL *** guid_node )
{
	PROFILE_SECTION();

	const char * ret = NULL;

	u32 h = guid % MH_ROOMS;
	u32_u32_LL ** ln;
	for ( ln = this->guids_hm + h; (*ln); ln = &(*ln)->next )
	{
		if ( (*ln)->key == guid )
		{
			u32_u32_LL * node = *ln;
			ret = *( this->paths.mem + node->value );
			if ( guid_node )
			{
				*guid_node = ln;
			}
			break;
		}
	}

	return ret;
}

bool
Resources_GUIDs_Table::
find( const char * path, u32 * guid_out )
{
	PROFILE_SECTION();

	bool ret = false;

	u32 guid = this->compute_guid( path );
	*guid_out = guid;

	u32 h = guid % MH_ROOMS;
	u32_u32_LL ** ln;
	for ( ln = this->guids_hm + h; (*ln); ln = &(*ln)->next )
	{
		if ( (*ln)->key == guid )
		{
			ret = true;
			break;
		}
	}

	return ret;
}

u32
Resources_GUIDs_Table::
compute_guid( const char * path )
{
	PROFILE_SECTION();

	u32 ret = u32_FNV1a( path );
	return ret;
}

const Font_LL *
Fonts::
find_or_record( const char * path, f32 pixel_height )
{
	PROFILE_SECTION();

	const Font_LL * ret = NULL;

	u32 font_id;
	bool font_id_found = false;
	u32 key = u32_FNV1a( path );
	u32 mask = ( 1 << 10 ) - 1;
	u32 h = ( ( key >>  0 ) & mask ) ^
	        ( ( key >> 10 ) & mask ) ^
	        ( ( key >> 20 ) & mask );
	Font_LL ** n = this->fonts_ht + h;
	for ( ;
	      *n;
	      *n = (*n)->next )
	{
		if ( (*n)->key )
		{
			font_id_found = true;
			font_id = (*n)->font_id;
		}
		if ( (*n)->key       == key &&
		     (*n)->px_height == pixel_height )
		{
			ret = (*n);
			return ret;
		}
	}

	if ( ( !font_id_found && this->multi_font.fonts_count == MAX_FONTS_COUNT ) ||
	     ( font_id_found &&
	       this->fonts[font_id].sizes_count == MAX_SIZES_PER_FONT ) )
	{
		return ret;
	}

	*n = allocate( &this->list_nodes_hm );
	ret = *n;
	if ( *n )
	{
		if ( !font_id_found )
		{
			font_id = this->multi_font.fonts_count++;
		}
		(*n)->font_id   = font_id;
		(*n)->atlas_id  = this->multi_font.atlases_count++;
		(*n)->size_id   = this->fonts[(*n)->font_id].sizes_count++;
		(*n)->px_height = pixel_height;
		(*n)->key       = key;
	}

	return ret;
}

void
Fonts::
init()
{
	Lpr_Multi_Font * mf = &this->multi_font;
	mf->atlases = this->font_atlases;
	mf->fonts   = this->fonts;
	Lpr_Font * f = mf->fonts;
	for ( u32 i = 0; i < MAX_FONTS_COUNT; ++i, ++f )
	{
		f->of_size    = this->font_of_sizes  [i];
		f->scales     = this->font_scales    [i];
		f->px_heights = this->font_px_heights[i];
	}
}

////////////////////////////////////////////////////////////////////////////////

#if 1
extern "C"
GAME_API_CYCLE_BEGINNING( game_api_cycle_beginning )
{
	(void)sys_api;
	(void)game_mem;
	global_sys_api = sys_api;
	global_game_mem = game_mem;

	{
		PROF_STOP_START_FRAME_AT_END();
	}

	Inputs * inputs = &game_mem->inputs;
	inputs->init();
}
#endif

#if 1
extern "C"
GAME_API_INPUT( game_api_input )
{
	(void)sys_api;
	(void)game_mem;
	(void)e;
	global_sys_api = sys_api;
	global_game_mem = game_mem;
	PROFILE_SECTION();

	Inputs * inputs = &game_mem->inputs;
	if ( ! inputs->handle_event( e ) )
	{
		switch ( e->type )
		{
			case EVENT_WINDOW:
				if ( e->window.window_id == game_mem->main_window.window_id )
				{
					switch ( e->window.type )
					{
						case EVENT_WINDOW_CLOSE:
							#if DEBUG_TO_FILE
							fclose( right_ear );
							fclose( right_ear_sql );
							fclose( right_ear_dist );
							fclose( right_ear0 );
							fclose( right_ear_sql_sampled );
							fclose( right_ear_dist_sampled );
							#endif
							sys_api->exit();
							break;
						case EVENT_WINDOW_SIZE_CHANGED:
							game_mem->main_window.resized = true;
							game_mem->main_window.new_size =
							   {e->window.w, e->window.h};
							break;
						default:
							break;
					}
				}
			#if DEBUG
				else
				if ( e->window.window_id == game_mem->DEBUG_window.window_id )
				{
					switch ( e->window.type )
					{
						case EVENT_WINDOW_SIZE_CHANGED:
							game_mem->DEBUG_window.resized = true;
							game_mem->DEBUG_window.new_size =
							   {e->window.w, e->window.h};
							break;
						case EVENT_WINDOW_CLOSE:
							sys_api->close_window( game_mem->DEBUG_window.window_id );
							game_mem->DEBUG_window.window_id = -1;
							break;
						default:
							break;
					}
				}
			#endif
				break;
			default:
				break;
		}
	} else
	{
		switch ( e->type )
		{
			case EVENT_KEYBOARD:
				switch ( e->keyboard.type )
				{
					case EVENT_KEYBOARD_KEY_PRESSED:
						switch ( e->keyboard.virt_key )
						{
						#if DEBUG
							case VK_F1:
								if ( game_mem->DEBUG_window.window_id < 0 )
								{
									s32 w_id =
									sys_api->create_window( "DEBUG WINDOW",
									                        {3,3,false},
									                        true, false,
									                        SYS_SCREEN_MODE_WINDOWED,
									                        false, 256, 640 );
									if ( SYS_API_RET_IS_ERROR( w_id ) )
									{
										lpr_log_err( "%s", sys_api->get_error_name( w_id ) );
										break;
									}
									game_mem->DEBUG_window.window_id = w_id;
									game_mem->DEBUG_window.lpr_draw_data.window_size = { 256, 640};
									lpr_init_draw_data( &game_mem->DEBUG_window.lpr_draw_data, 2 );
									game_mem->DEBUG_window.lpr_draw_data.viewport_origin = {256/2, 640/2};
									game_mem->DEBUG_camera.pos = {};
									game_mem->DEBUG_camera.vanishing_point = {0.0f, 0.0f};
									game_mem->DEBUG_camera.world_span = {256, 640};
									game_mem->DEBUG_camera.angle = 0.0f;
									//lpr_set_draw_target_default_full( &game_mem->DEBUG_window.lpr_draw_data );
								}
								break;
							case VK_F2:
								{
									f32 vol = game_mem->audio_player.volume;
										vol = lpr_max( 0.0f, vol - 0.01f );
									game_mem->audio_player.volume = vol;
								}
								break;
							case VK_F3:
								{
									f32 vol = game_mem->audio_player.volume;
										vol = lpr_max( 0.0f, vol + 0.01f );
									game_mem->audio_player.volume = vol;
								}
								break;
							case VK_m:
								{
									game_mem->audio_player.mute =
									   !game_mem->audio_player.mute;
								}
								break;
							case VK_LEFT:
								if ( e->keyboard.window_id ==
								     game_mem->DEBUG_window.window_id )
								{
									if ( game_mem->prof_drawing_data.prof_graph_mode ==
									     PROF_GRAPH_MODE_TIME_FLOW )
									{
										u32 increment = e->keyboard.mods & KEY_MOD_SHIFT ? 50 : 10;
										u32 * scrolling = &game_mem->prof_drawing_data.time_flow_scrolling;
										if ( *scrolling + increment <= PROF_GRAPH_TIME_FLOW_WIDTH_PX )
										{
											*scrolling += increment;

										} else
										{
											*scrolling = PROF_GRAPH_TIME_FLOW_WIDTH_PX;
										}
									}
								}
								break;
							case VK_RIGHT:
								if ( e->keyboard.window_id ==
								     game_mem->DEBUG_window.window_id )
								{
									if ( game_mem->prof_drawing_data.prof_graph_mode ==
									     PROF_GRAPH_MODE_TIME_FLOW )
									{
										u32 decrement = e->keyboard.mods & KEY_MOD_SHIFT ? 50 : 10;
										u32 * scrolling = &game_mem->prof_drawing_data.time_flow_scrolling;
										if ( *scrolling >= decrement )
										{
											*scrolling -= decrement;
										} else
										{
											*scrolling = 0;
										}
									}
								}
								break;
							case VK_HOME:
								if ( e->keyboard.window_id ==
								     game_mem->DEBUG_window.window_id )
								{
									if ( game_mem->prof_drawing_data.prof_graph_mode ==
									     PROF_GRAPH_MODE_TIME_FLOW )
									{
										game_mem->prof_drawing_data.time_flow_scrolling = 0;
									}
								}
								break;
							case VK_END:
								if ( e->keyboard.window_id ==
								     game_mem->DEBUG_window.window_id )
								{
									if ( game_mem->prof_drawing_data.prof_graph_mode ==
									     PROF_GRAPH_MODE_TIME_FLOW )
									{
										game_mem->prof_drawing_data.time_flow_scrolling =
										   PROF_GRAPH_TIME_FLOW_WIDTH_PX
										   + PROF_GRAPH_TIME_FLOW_RIGHT_BORDER_PX
										   - game_mem->DEBUG_window.lpr_draw_data.window_size.w;
									}
								}
								break;
						#endif // DEBUG
							case VK_KP_PLUS:
								sys_api->set_window_size( game_mem->main_window.window_id,
								                          game_mem->main_window.lpr_draw_data.window_size.x+1,
								                          game_mem->main_window.lpr_draw_data.window_size.y );
								break;
							case VK_KP_MINUS:
								sys_api->set_window_size( game_mem->main_window.window_id,
								                          game_mem->main_window.lpr_draw_data.window_size.x-1,
								                          game_mem->main_window.lpr_draw_data.window_size.y );
								break;
							case VK_KP_4:
								game_mem->txt_pos.x -= 0.001f;
								break;
							case VK_KP_6:
								game_mem->txt_pos.x += 0.001f;
								break;
							case VK_KP_8:
								game_mem->txt_pos.y += 0.001f;
								break;
							case VK_KP_2:
								game_mem->txt_pos.y -= 0.001f;
								break;
							case VK_SPACE:
								#if 0
								if ( !e->keyboard.repeat )
								{
									Playable_Sound test_p_sound = {};
									test_p_sound.sound = game_mem->test_vorbis_sound;
									test_p_sound.loops = 0;
									enqueue_sound( &game_mem->audio_player.new_sounds_queue, &test_p_sound );
								}
								#endif
								break;
							case VK_PAGEDOWN:
								game_mem->camera.world_span.w /= 1.1f;
								game_mem->camera.world_span.h /= 1.1f;
								break;
							case VK_PAGEUP:
								game_mem->camera.world_span.w *= 1.1f;
								game_mem->camera.world_span.h *= 1.1f;
								break;
							#if 0
							case VK_w:
							{
								V3 p = game_mem->player_pos;
								V3 dp = {};
								dp.xy +=
								   rotate_deg( V2{0.0f,0.1f},
								               game_mem->player_rot );
								game_mem->player_pos = p + dp;
							}
								break;
							case VK_s:
								// NOTE(theGiallo): test sounds
								if ( !e->keyboard.repeat &&
								     ( e->keyboard.mods & KEY_MOD_CTRL ) )
								{
									lpr_log_info( "pressed s" );
									Playable_Sound test_p_sound = {};
									test_p_sound.sound = game_mem->test_sound;
									lpr_assert( test_p_sound.sound.channels_count == 1 );
									test_p_sound.loops = 1;
									enqueue_sound( &game_mem->audio_player.new_sounds_queue, &test_p_sound );
									break;
								}
							{
								V3 p = game_mem->player_pos;
								V3 dp = {};
								dp.xy +=
								rotate_deg( V2{0.0f,-0.1f},
								            game_mem->player_rot );
								game_mem->player_pos = p + dp;
							}
								break;
							case VK_a:
							{
								V3 p = game_mem->player_pos;
								V3 dp = {};
								dp.xy +=
								   rotate_deg( V2{-0.1f,0.0f},
								               game_mem->player_rot );
								game_mem->player_pos = p + dp;
							}
								break;
							case VK_d:
							{
								V3 p = game_mem->player_pos;
								V3 dp = {};
								dp.xy +=
								   rotate_deg( V2{0.1f,0.0f},
								               game_mem->player_rot );
								game_mem->player_pos = p + dp;
							}
								break;
							case VK_q:
							{
								f32 d = 1.0f;
								if ( e->keyboard.mods & KEY_MOD_SHIFT )
								{
									d *= 10.0f;
								}
								game_mem->player_rot += d;
							}
								break;
							case VK_e:
							{
								f32 d = 1.0f;
								if ( e->keyboard.mods & KEY_MOD_SHIFT )
								{
									d *= 10.0f;
								}
								game_mem->player_rot -= d;
							}
							#endif
								break;
							default:
								break;
						}
						break;
					case EVENT_KEYBOARD_KEY_RELEASED:
						break;
					default:
						break;
				}
				break;
			case EVENT_MOUSE:
				switch ( e->mouse.type )
				{
					case EVENT_MOUSE_MOTION:
						break;
					case EVENT_MOUSE_WHEEL:
						#if 0
						game_mem->sound_source_dist *=
						   lpr_pow( 1.1f, e->mouse.wheel.scrolled_y );
						#endif
						break;
					case EVENT_MOUSE_BUTTON_PRESSED:
					case EVENT_MOUSE_BUTTON_RELEASED:
					default:
						break;
				}
				break;
			case EVENT_GAME_SYSTEM:
			default:
				break;
		}
	}

	// lpr_log_info( "event of type %u with time %lf", e->type, e->common.time_fired );
}
#endif

internal
void
kalman_1d( Mx<f32,2,2> * P, Mx<f32,2,1> * x,  f32 sample, f32 dt, f32 Q, f32 R )
{
	Mx<f32,2,2> F = { 1.0f, dt, 0.0f, 1.0f },
	            Ft = ~F;
	Mx<f32,1,2> H = {1.0f, 0.0f}; // TODO(theGiallo, 2016-05-20): assign some value
	Mx<f32,2,1> Ht = ~H;
	Mx<f32,2,1> xk = F * (*x);
	Mx<f32,2,2> Pk = F * (*P) * Ft + (Q * Mx_identity<f32,2,2>());
	Mx<f32,2,1> PkHt  = Pk * Ht;
	Mx<f32,1,1> HPkHt = H * PkHt;
	Mx<f32,1,1> HPkHtR = HPkHt + R;
	Mx<f32,2,1> K_ = PkHt / HPkHtR[0][0];
	Mx<f32,1,1> z  = {sample};
	*x = xk + K_ * ( z - H * xk );
	*P = Pk - K_ * H * Pk;
}
internal
void
kalman_on_rot( Kalman_Rot * kalman, f32 sampled_rot, f32 dt, f32 Q, f32 R )
{
	kalman_1d( &kalman->P, &kalman->x, sampled_rot, dt, Q, R );
}
internal
void
kalman_on_pos_separate( Kalman_Pos * kalman, V2 sampled_pos, f32 dt, f32 Q, f32 R )
{
	for ( s32 d = 0; d < 2; ++d )
	{
		kalman_1d( kalman->P+d, kalman->x+d, (&sampled_pos.x)[d], dt, Q, R );
	}
}

#if 0
internal
// NOTE(theGiallo): test sound pushing
void
update_test_sound_position( Game_Memory * game_mem, Time game_time )
{
	PRAGMA_ALIGN_PRE(64)
	V2 source_pos PRAGMA_ALIGN_POST(64);
	#if USE_F64
	f64 r = game_mem->sound_source_dist;
	f64 a = game_time * ((f64)LPR_PI) * 2.0 / (4.0 * 64.0 + 8.0 * r);
	f64 rot_a = a * 8.0;//LPR_PI * 0.5f;
	f64 d = 2.5 * r;
	f64 r_a = -((f64)LPR_PI)*0.5/11.0;
	#else
	f32 r = game_mem->sound_source_dist;
	f32 a = game_time * LPR_PI * 2.0f / (4.0f * 64.0f + 8.0f * r);
	f32 rot_a = a * 8.0f;//LPR_PI * 0.5f;
	//f32 d = 2.5f * r;
	//f32 r_a = -LPR_PI*0.5f/11.0f;
	#endif
	//r += d * lpr_sin( 11.0f * r_a );
	#if 0
	V2 c = { lpr_sin( rot_a / TAU_F ),
	         lpr_cos( rot_a / 2.0f ) };
	#else
	V2 c = {};
	#endif
	//r += d * ( 0.5f + 0.5*lpr_sin( 11.0f * r_a ) );
	#if USE_F64
	source_pos.x = c.x + 10.0 * r * cos( rot_a );
	source_pos.y = c.y + r * sin( rot_a );
	#else
	source_pos.x = c.x + 10.0f * r * lpr_cos( rot_a );
	source_pos.y = c.y + r * lpr_sin( rot_a );
	#endif
	*(u64*)&game_mem->sound_source_pos = *(u64*)&source_pos;
}
#endif

////////////////////////////////////////////////////////////////////////////////
// NOTE(theGiallo): @draw

#include "game_api_render.cpp"

#if 1
extern "C"
GAME_API_RENDER( game_api_render )
{
	(void)sys_api;
	(void)game_mem;
	global_sys_api = sys_api;
	global_game_mem = game_mem;
	// NOTE(theGiallo): IMPORTANT this has to be the first,
	// so it will run as last
	//PROF_STOP_START_FRAME_AT_END();
	DEFER_GATHER_ONE_FRAME();
	PROFILE_SECTION();

	if ( !game_mem->initialized )
	{
		return;
	}

	Inputs * inputs = &game_mem->inputs;

	s32 i_ret;
#if DEBUG
	i_ret =
	sys_api->make_window_current( game_mem->main_window.window_id );
	if ( SYS_API_RET_IS_ERROR( i_ret ) )
	{
		lpr_log_err( "Error %s: %s", sys_api->get_error_name( i_ret ),
		             sys_api->get_error_description( i_ret ) );
		return;
	} else
	{
#endif
	if ( game_mem->main_window.resized )
	{
		V2u32 new_size = game_mem->main_window.new_size;
		game_mem->main_window.resized = false;
		lpr_resize( &game_mem->main_window.lpr_draw_data,
		             new_size );
		game_mem->camera.world_span.h =
		   ( game_mem->camera.world_span.w *
		       (f32)game_mem->main_window.new_size.h ) /
		   (f32)game_mem->main_window.new_size.w;
	}

	Lpr_Draw_Data * draw_data = &game_mem->main_window.lpr_draw_data;
	//V2u32 win_size = make_V2u32( game_mem->main_window.lpr_draw_data.window_size );
	f32 w_w = (f32)game_mem->main_window.lpr_draw_data.window_size.w;
	f32 w_h = (f32)game_mem->main_window.lpr_draw_data.window_size.h;
	s32 glyph_id;
	Lpr_AA_Rect bb;
	V2 end_pt;

	{
		PROFILE_SECTION_NAMED( "reset all batches" );
		lpr_reset_all_batches( draw_data );
	}
	{
		PROFILE_SECTION_NAMED( "set batch camera 0" );
		#if 0
		lpr_set_batch_camera( draw_data, 0,
		                      game_mem->camera.pos, game_mem->camera.angle,
		                      game_mem->camera.vanishing_point,
		                      game_mem->camera.world_span );
		#else
		lpr_set_batch_camera( draw_data, 0,
		                      V2{0.0f,0.0f}, 0.0f,
		                      V2{0.5f,0.5f},
		                      lpr_V2_from_V2u32( game_mem->main_window.lpr_draw_data.window_size ) );
		#endif
	}

	{
		PROFILE_SECTION_NAMED( "set batch camera 1" );
		lpr_set_batch_camera( draw_data, 1,
		                      V2{0.0f,0.0f}, 0.0f,
		                      V2{0.0f,0.0f},
		                      lpr_V2_from_V2u32( game_mem->main_window.lpr_draw_data.window_size ) );
	}

#if 0
	id =
	lpr_add_colored_rect_full_window( draw_data, 0 );
	p = {0.0f, 0.0f, -FLT_MAX };
	game_mem->y_sorting_values[id] = sort_value( &p );
#endif

	char string_buf[1024];
	s32 written;
	{
		PROFILE_SECTION_NAMED( "snprintf" );

		written =
		snprintf( &string_buf[0], sizeof(string_buf),
		          " ms: %6.2f\n"
		          "FPS: %6.2f\n"
		          "volume: %4.2f\n"
		          "game time: %4.2fs\n",
		          1000.0f * game_mem->delta_time,
		          1.0f / game_mem->delta_time,
		          game_mem->audio_player.volume,
		          game_mem->game_time
	               );
	}
	f32 scale = 1.0f;
	u16 z = 10;

	{
		PROFILE_SECTION_NAMED( "add multiline txt" );
		lpr_add_multiline_text( &game_mem->main_window.lpr_draw_data, 1,
		                        &game_mem->fonts.multi_font, 0, 0,
		                        {w_w - ( 300 + 4 ), w_h - 24},
		                        &string_buf[0], written,
		                        lpr_C_colors_u8.gray_50,
		                        true, z, scale,
		                        false, true,
		                        LPR_STENCIL_NONE,
		                        &glyph_id, &bb, (Lpr_V2*)&end_pt );
	}

	// --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // ---
	// NOTE(theGiallo): render here to texture and add it to lpr_draw_data with z = 0

	texture_gen_render( game_mem, sys_api );

	glBindFramebuffer( GL_FRAMEBUFFER, 0 );

	// NOTE(theGiallo): rendering end
	// --- // --- // --- // --- // --- // --- // --- // --- // --- // --- // ---

	lpr_set_draw_target_default_full( &game_mem->main_window.lpr_draw_data );

	{
		PROFILE_SECTION_NAMED( "clear fb 0" );
		lpr_clear_framebuffer_zero( draw_data,
		                            &lpr_C_colors_f32.orange );
	}
	{
		PROFILE_SECTION_NAMED( "set dt def full" );
		lpr_set_draw_target_default_full( draw_data );
	}
	{
		PROFILE_SECTION_NAMED( "prep batch drawing" );
		lpr_prep_batch_drawing( draw_data );
	}
	//lpr_sort_batch( draw_data, 0 );
	lpr_sort_batch( draw_data, 1 );
	//lpr_draw_all_batches_sorted( draw_data );
#if 0
	sort_lpr_batch_f32( draw_data, 0, game_mem->y_sorting_values, true );
#endif
	{
		PROFILE_SECTION_NAMED( "lpr_draw_all_batches" );
		glEnable( GL_DITHER );
		glEnable( GL_MULTISAMPLE );
		lpr_draw_all_batches( draw_data );
	}
	{
		PROFILE_SECTION_NAMED( "lpr_finalize_draw" );
		lpr_finalize_draw( draw_data );
	}


	{
		PROFILE_SECTION_NAMED( "swap main window" );
		sys_api->swap_window( game_mem->main_window.window_id );
	}

#if DEBUG
	}
////////////////////////////////////////////////////////////////////////////////
	if ( game_mem->DEBUG_window.window_id < 0 )
	{
		return;
	}
	PROFILE_SECTION_NAMED( "render debug window" );
	i_ret =
	sys_api->make_window_current( game_mem->DEBUG_window.window_id );
	if ( SYS_API_RET_IS_ERROR( i_ret ) )
	{
		lpr_log_err( "Error %s: %s", sys_api->get_error_name( i_ret ),
		                             sys_api->get_error_description( i_ret ) );
		return;
	}
	if ( game_mem->DEBUG_window.resized )
	{
		game_mem->DEBUG_window.resized = false;
		lpr_resize( &game_mem->DEBUG_window.lpr_draw_data,
		             game_mem->DEBUG_window.new_size );
		game_mem->DEBUG_camera.world_span.w = game_mem->DEBUG_window.new_size.w;
		game_mem->DEBUG_camera.world_span.h = game_mem->DEBUG_window.new_size.h;
		//game_mem->DEBUG_camera.world_span.h =
		//   ( game_mem->DEBUG_camera.world_span.w * (f32)game_mem->DEBUG_window.new_size.h ) / (f32)game_mem->DEBUG_window.new_size.w;
	}

	lpr_reset_all_batches( &game_mem->DEBUG_window.lpr_draw_data );

	lpr_set_batch_camera( &game_mem->DEBUG_window.lpr_draw_data, 1,
	                      game_mem->DEBUG_camera.pos, 0,
	                      game_mem->DEBUG_camera.vanishing_point,
	                      game_mem->DEBUG_camera.world_span );
	lpr_set_batch_camera( &game_mem->DEBUG_window.lpr_draw_data, 0,
	                      game_mem->DEBUG_camera.pos, 0,
	                      game_mem->DEBUG_camera.vanishing_point,
	                      game_mem->DEBUG_camera.world_span );
#if 0
	lpr_add_colored_rect( &game_mem->DEBUG_window.lpr_draw_data, 0,
	                      {0,0}, {1,1}, lpr_C_colors_u8.metallic_gold );
#endif

	f32 w_w = (f32)game_mem->DEBUG_window.lpr_draw_data.window_size.w;
	f32 w_h = (f32)game_mem->DEBUG_window.lpr_draw_data.window_size.h;

	Lpr_Draw_Data  * draw_data  = &game_mem->DEBUG_window.lpr_draw_data;
	Lpr_Multi_Font * multi_font = &game_mem->fonts.multi_font;
	u32 dbg_font_id      = game_mem->fonts.debug_font->font_id;
	u32 dbg_font_size_id = game_mem->fonts.debug_font->size_id;

	Lpr_AA_Rect bb, aar;
	V2 end_pt;
	s32 glyph_id;
	f32 scale = 1.1f;
	const u32 buf_size = 1024;
	char string_buf[buf_size] = {};
	s32 written =
	snprintf( &string_buf[0], buf_size,
	          "camera_world_span: %fx%f\n"
	          "camera_pos: %f, %f\n"
	          "window_screen: %ux%u\n",
	          game_mem->camera.world_span.w, game_mem->camera.world_span.h,
	          game_mem->camera.pos.x, game_mem->camera.pos.y,
	          game_mem->main_window.lpr_draw_data.window_size.x,
	          game_mem->main_window.lpr_draw_data.window_size.y
	          );
	lpr_add_multiline_text( draw_data, 1,
	                        multi_font, dbg_font_id, dbg_font_size_id,
	                        {50, w_h - 150},
	                        &string_buf[0], written,
	                        lpr_C_colors_u8.white,
	                        true, 10, scale,
	                        false, true,
	                        LPR_STENCIL_NONE,
	                        &glyph_id, &bb, (Lpr_V2*)&end_pt );

	i_ret =
	lpr_add_colored_rect( &game_mem->DEBUG_window.lpr_draw_data, 0,
	                      bb.pos, bb.size, lpr_C_colors_u8.gray_75 );
	lpr_set_instance_z( &game_mem->DEBUG_window.lpr_draw_data, 0, i_ret, 1 );


#if TEST_FONT_SPRITE
// lpr_s32 lpr_add_sprite_px_perfect( Lpr_Draw_Data *draw_data, lpr_u32 batch_id, const Lpr_Texture *texture, Lpr_V2 pos, lpr_bool screen_aligned, lpr_bool viewport_pxs, Lpr_Rgba_u8 color )
#if 0
	lpr_add_sprite_px_perfect( &game_mem->DEBUG_window.lpr_draw_data, 0,
	                           &game_mem->multi_font.atlases[0],
	                           {10 + FONT_ATLAS_W/2,
	                           bb.bl.y - 10 - FONT_ATLAS_H/2},
	                           true, true, lpr_C_colors_u8.white );
#else
//lpr_add_sprite( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
//                Lpr_Texture const * texture, Lpr_V2 pos,
//                float scale = 1.0f, float rot_deg = 0.0f,
//                Lpr_Rgba_u8 color = lpr_C_colors_u8.white );
	f32 atlas_scale = 0.5f;
	lpr_add_sprite_w_scale( draw_data, 0,
	                        multi_font->atlases + 0,
	                        {10 + atlas_scale*FONT_ATLAS_W/2,
	                        bb.pos.y - bb.size.w - 10 - atlas_scale*FONT_ATLAS_H/2},
	                        atlas_scale, 0, lpr_C_colors_u8.white );
	lpr_add_sprite_w_scale( draw_data, 0,
	                        multi_font->atlases + 1,
	                        {10 + atlas_scale*FONT_ATLAS_W,
	                        bb.pos.y - bb.size.w - 10 - atlas_scale*FONT_ATLAS_H},
	                        atlas_scale, 0, lpr_C_colors_u8.white );
#endif
#endif // TEST_FONT_SPRITE

#if 0
	{
		Lpr_Rgba_u8 c = lpr_C_colors_u8.white;
		//c.a = 128;
		Lpr_Texture lt = *game_mem->metal_man_ri->lpr_texture;

		s32 id =
		lpr_add_sprite_w_scale( draw_data, 0,
		                        &lt,
		                        {w_w/2,w_h/2},
		                        1.0f, 0.0f, c );
		lpr_set_instance_z( draw_data, 0, id, 1 );
	}
#endif

	written =
	snprintf( &string_buf[0], buf_size,
	          " ms: %6.2f\n"
	          "FPS: %6.2f\n"
	          "clock_period_ns: %f (%fGHz)\n"
			  //"\nHI THERE!\n\n"
	          "thread id: %u\n"
	          "mouse pos: %f, %f\n"
	          "mouse window id: %d\n"
	          "top scrolling: x=%f, y=%f\n"
	          "sections_mh: capacity=%u, occupancy=%u"
	          ,
	          1000.0f * game_mem->delta_time,
	          1.0f / game_mem->delta_time,
	          game_mem->prof.clock_period_ns,
	          1.0 / game_mem->prof.clock_period_ns,
	          thread_sys_id(),
	          inputs->mouse_pos.x,
	          inputs->mouse_pos.y,
	          inputs->mouse_window_id,
	          game_mem->prof_drawing_data.top.scrolling.x,
	          game_mem->prof_drawing_data.top.scrolling.y,
	          game_mem->prof.frames_data.sections_mh.capacity,
	          game_mem->prof.frames_data.sections_mh.total_occupancy
	          );

#if TGPROF_COPY_STRINGS
	for ( u32 tid = 0; tid < GAME_API_MAX_THREADS_NUM; ++tid )
	{
		written +=
		snprintf( string_buf + written, buf_size - written,
		          "prof strings mem pool: %lu/%lu%s",
		          game_mem->prof.strings_mem_pools[tid].occupied,
		          game_mem->prof.strings_mem_pools[tid].size,
		          tid != GAME_API_MAX_THREADS_NUM - 1 ? "\n" : ""
		        );
	}
#endif
	scale = 1.0f;
	u16 z = 10;
	lpr_add_multiline_text( draw_data, 1,
	                        multi_font, dbg_font_id, dbg_font_size_id,
	                        {4, w_h - 16},
	                        &string_buf[0], written,
	                        lpr_C_colors_u8.white,
	                        true, z, scale,
	                        false, true,
	                        LPR_STENCIL_NONE,
	                        &glyph_id, &bb, (Lpr_V2*)&end_pt );

	char str_m[2*1024] = {};
	u64 tot_written = 0;
	char * str = str_m;
#if 0
	written =
	snprintf( str, sizeof( str_m ), "- Resources GUIDs Table [%5u] -\n",
	          game_mem->resources.guids.list_nodes_hm.total_occupancy );
	str += written;
	tot_written += written;
	u32_u32_LL_MH_Iterator guid_it = {};
	char * s = {};
	u32_u32_LL * gn;
	while ( ( gn = iterate( &game_mem->resources.guids.list_nodes_hm, &guid_it ) ) )
	{
		u32 guid = gn->key;
		s = game_mem->resources.guids.paths.mem[gn->value];
		written = snprintf( str, sizeof( str_m ) - tot_written,
		                     "%10u %s\n", guid, s );
		str += written;
		tot_written += written;
	}
	written = snprintf( str, sizeof( str_m ) - tot_written,
	                    "---------------------------------\n" );
#endif
	str += written;
	tot_written += written;
	V2 size = {w_w-8, bb.pos.y-bb.size.h*0.5f};
	aar.size = size;
	aar.pos = V2{4,0} + size*0.5f;
	bool
	b_ret =
	lpr_add_multiline_text_in_rect( draw_data, 1, multi_font,
	   dbg_font_id, dbg_font_size_id,
	   aar, str_m, sizeof( str_m ), lpr_C_colors_u8.white,
	   true, z, 1.0f, false, true, LPR_STENCIL_NONE, &glyph_id, &bb, (Lpr_V2 *)&end_pt );
	if ( !b_ret )
	{
		lpr_log_err( "lpr_add_multiline_text_in_rect failed" );
	}


	// NOTE(theGiallo): test render for profile
#if 0
	{
		Prof_Section_Thread_Frame * this_frame =
		   game_mem->prof.frames_data.frames[0] + game_mem->prof.frames_data.last_frame;
		if ( this_frame->section )
		{
			u64 last_duration = this_frame->section->end_time - this_frame->section->start_time;
			lpr_log_info( "last_duration = %lu delta_time = %f clocks/time = %f", last_duration, game_mem->delta_time, last_duration / game_mem->delta_time );
		}
	}
#endif

	written =
	snprintf( &string_buf[0], buf_size,
	          "max depth: %u",
	          game_mem->prof_drawing_data.max_depth
	          );
	bool clicked_left = false;
	bool clicked_right = false;
	f32 margin = 2;
	f32 button_height = 24;
	V2 button_row_start = {DEBUG_GUI_START_X_PX,DEBUG_GUI_START_Y_PX};
	V2 min_p = button_row_start;

	clicked_left = clicked_right = false;
	bb =
	draw_dbg_button( game_mem, game_mem->prof.is_paused?"unpause":"pause", written,
	                 min_p, min_p + V2{70, button_height}, 0,
	                 false,
	                 &clicked_left, &clicked_right,
	                 margin );
	if ( clicked_left )
	{
		game_mem->prof.is_paused = !game_mem->prof.is_paused;
	}

	min_p = next_dbg_button_min_in_row( bb, margin );
	clicked_left = clicked_right = false;
	bb =
	draw_dbg_button( game_mem, "color shift", written,
	                 min_p, min_p + V2{110, button_height}, 0,
	                 false,
	                 &clicked_left, &clicked_right,
	                 margin );
	if ( clicked_left )
	{
		++game_mem->prof_drawing_data.color_shift;
	}
	if ( clicked_right )
	{
		--game_mem->prof_drawing_data.color_shift;
	}

	min_p = next_dbg_button_min_in_row( bb, margin );
	clicked_left = clicked_right = false;
	bb =
	draw_dbg_button( game_mem, string_buf, written,
	                 min_p, min_p + V2{110, button_height}, 0,
	                 false,
	                 &clicked_left, &clicked_right,
	                 margin );
	if ( clicked_left )
	{
		++game_mem->prof_drawing_data.max_depth;
	}
	if ( clicked_right && game_mem->prof_drawing_data.max_depth )
	{
		--game_mem->prof_drawing_data.max_depth;
	}
	clicked_left = clicked_right = false;
	min_p = next_dbg_button_min_in_row( bb, margin );
	bb =
	draw_dbg_button( game_mem, "view:", 6,
	                 min_p,
	                 min_p + V2{50, 24}, 0, false, NULL, NULL, 2 );
	min_p = next_dbg_button_min_in_row( bb, margin );
	const char * msg = "frame piles";
	bool selected = game_mem->prof_drawing_data.prof_graph_mode ==
	                PROF_GRAPH_MODE_FRAME_PILES;
	bb =
	draw_dbg_button( game_mem, msg, strlen(msg),
	                 min_p, min_p + V2{110, button_height}, 0,
	                 selected,
	                 selected?NULL:&clicked_left,
	                 selected?NULL:&clicked_right,
	                 margin );
	if ( clicked_left )
	{
		game_mem->prof_drawing_data.prof_graph_mode =
		   PROF_GRAPH_MODE_FRAME_PILES;
	}
	clicked_left = clicked_right = false;

	selected = game_mem->prof_drawing_data.prof_graph_mode ==
	           PROF_GRAPH_MODE_TIME_FLOW;
	msg = "time flow";
	min_p = next_dbg_button_min_in_row( bb, margin );
	bb =
	draw_dbg_button( game_mem, msg, strlen(msg),
	                 min_p, min_p + V2{110, button_height}, 0,
	                 selected,
	                 selected?NULL:&clicked_left,
	                 selected?NULL:&clicked_right,
	                 margin );
	if ( clicked_left )
	{
		game_mem->prof_drawing_data.prof_graph_mode =
		   PROF_GRAPH_MODE_TIME_FLOW;
	}
	clicked_left = clicked_right = false;

	selected = game_mem->prof_drawing_data.prof_graph_mode ==
	           PROF_GRAPH_MODE_TOP;
	msg = "top";
	min_p = next_dbg_button_min_in_row( bb, margin );
	bb =
	draw_dbg_button( game_mem, msg, strlen(msg),
	                 min_p, min_p + V2{110, button_height}, 0,
	                 selected,
	                 selected?NULL:&clicked_left,
	                 selected?NULL:&clicked_right,
	                 margin );
	if ( clicked_left )
	{
		game_mem->prof_drawing_data.prof_graph_mode =
		   PROF_GRAPH_MODE_TOP;
	}
	clicked_left = clicked_right = false;


	u32 max_depth = game_mem->prof_drawing_data.max_depth;
	f32 x_px = button_row_start.x;
	f32 starting_y_px = button_row_start.y + button_height + margin;

	if ( game_mem->prof_drawing_data.prof_graph_mode ==
	     PROF_GRAPH_MODE_FRAME_PILES )
	{
		PROFILE_SECTION_NAMED( "prof graph frame piles" );
		for ( u32 i = 0; i < MAX_PROF_FRAMES; ++i, ++x_px )
		{
			//for ( u32 tid = 0; tid < GAME_API_MAX_THREADS_NUM; ++tid )
			for ( u32 tid = 0; tid < 2; ++tid )
			{
				f32 thread_height_px = 400;
				f32 base_y = starting_y_px + tid * thread_height_px;
				Prof_Section_Thread_Frame * t_frame =
				   game_mem->prof.frames_data.frames[tid] + i;

				Prof_Section_Child_List_Node stub_child;
				stub_child.section = t_frame->section;
				stub_child.next = t_frame->next;
				V2 start = {x_px, base_y};
				for ( Prof_Section_Child_List_Node * fcn = &stub_child;
				      fcn;
				      fcn = fcn->next )
				{
					if ( fcn->section )
					{
						bool mouse_on;
						// TODO(theGiallo, 2016-06-12): displace according to
						// start time, like the children in draw_prof_section
						start.y =
						draw_prof_section( game_mem,
						                   game_mem->DEBUG_window.window_id,
						                   draw_data,
						                   fcn->section, start, 0, max_depth,
						                   thread_height_px,
						                   &mouse_on );
					}

					continue;
				}
			}
		}
	} else
	if ( game_mem->prof_drawing_data.prof_graph_mode ==
	     PROF_GRAPH_MODE_TIME_FLOW )
	{
		PROFILE_SECTION_NAMED( "prof graph time flow" );

		u32 threads_count = 2;
		f32 thread_height_px = 128;
		f32 thread_padding = 2.0f;
		V2 size = {50.0f, thread_height_px - thread_padding * 2.0f};
		V2 pos = {x_px + size.w * 0.5f,
		          starting_y_px + 0.5f * thread_height_px};
		u64 zero_ticks = UINT64_MAX;
		u64 max_ticks = 0;
		for ( u32 tid = 0; tid < 1; ++tid )
		{
			Prof_Section_Thread_Frame * t_frame;
			u64 oldest_frame_id = game_mem->prof.frames_data.last_frame;
			t_frame =
			   game_mem->prof.frames_data.frames[tid] + oldest_frame_id;
			if ( !t_frame->section )
			{
				continue;
			}
			u64 start = t_frame->section->start_time;
			if ( start < zero_ticks )
			{
				zero_ticks = start;
			}
			u64 newest_frame_id =
			   game_mem->prof.frames_data.last_frame == 0 ?
			   MAX_PROF_FRAMES - 1 :
			   game_mem->prof.frames_data.last_frame - 1;
			t_frame =
			   game_mem->prof.frames_data.frames[tid] + newest_frame_id;
			if ( !t_frame->section )
			{
				continue;
			}
			Prof_Section * section = t_frame->section;
			if ( t_frame->last )
			{
				section = t_frame->last->section;
			}
			u64 end = section->end_time;
			if ( end && end > max_ticks )
			{
				max_ticks = end;
			}
		}
		u32 graph_width = PROF_GRAPH_TIME_FLOW_WIDTH_PX;
		u64 delta_ticks = max_ticks - zero_ticks;
		f32 ticks_to_px = graph_width / (f32)delta_ticks;
		u32 scrolling   = game_mem->prof_drawing_data.time_flow_scrolling;
		V2  origin_px   = {x_px, starting_y_px};

		min_p = next_dbg_button_min_in_row( bb, margin );
		char string_buf[1024];
		snprintf( string_buf, sizeof( string_buf ),
		          "%u/%u px", scrolling, graph_width );
		bool selected = game_mem->prof_drawing_data.prof_graph_mode ==
		                PROF_GRAPH_MODE_FRAME_PILES;
		bb =
		draw_dbg_button( game_mem, string_buf, sizeof( string_buf ),
		                 min_p, min_p + V2{130, button_height}, 0,
		                 false,
		                 NULL,
		                 NULL,
		                 margin );

#if 0
		lpr_log_dbg( "ticks_to_px = %e", ticks_to_px );
		lpr_log_dbg( "zero_ticks  = %lu", zero_ticks );
		lpr_log_dbg( "max_ticks   = %lu", max_ticks );
		lpr_log_dbg( "delta_ticks = %lu %ens", delta_ticks, delta_ticks * game_mem->prof.clock_period_ns );
#endif
		for ( u32 tid = 0; tid < threads_count; ++tid )
		{
			//Lpr_Rgba_u8 color = palette_pastel[tid];
			for ( u32 i = 0; i < MAX_PROF_FRAMES; ++i )
			{
				Prof_Section_Thread_Frame * t_frame =
				   game_mem->prof.frames_data.frames[tid] + i;

				Prof_Section_Child_List_Node stub_child;
				stub_child.section = t_frame->section;
				stub_child.next = t_frame->next;
				for ( Prof_Section_Child_List_Node * fcn = &stub_child;
				      fcn;
				      fcn = fcn->next )
				{
					if ( fcn->section )
					{
						draw_prof_section_time_flow(
						   game_mem,
						   game_mem->DEBUG_window.window_id,
						   draw_data,
						   1,
						   fcn->section,
						   0,
						   max_depth,
						   ticks_to_px,
						   delta_ticks,
						   zero_ticks,
						   scrolling,
						   origin_px );
					}
				}
			}

			origin_px.y += thread_height_px;
		}
	} else
	if ( game_mem->prof_drawing_data.prof_graph_mode ==
	     PROF_GRAPH_MODE_TOP )
	{
		u64 newest_frame_id =
		   game_mem->prof.frames_data.last_frame == 0 ?
		   MAX_PROF_FRAMES - 1 :
		   game_mem->prof.frames_data.last_frame - 1;

		draw_prof_top( game_mem,
		               game_mem->DEBUG_window.window_id,
		               draw_data, 1,
		               newest_frame_id, 1 );
	}
	// NOTE(theGiallo): test render for profile - end


	lpr_clear_framebuffer_zero( &game_mem->DEBUG_window.lpr_draw_data,
	                            &lpr_C_colors_f32.metallic_gold );
	lpr_set_draw_target_default_full( &game_mem->DEBUG_window.lpr_draw_data );
	lpr_prep_batch_drawing( &game_mem->DEBUG_window.lpr_draw_data );

	{
		PROFILE_SECTION_NAMED( "sort and draw debug 0" );
		lpr_sort_batch( draw_data, 0 );
		lpr_draw_batch( draw_data, 0 );
	}

	{
		PROFILE_SECTION_NAMED( "draw debug lines" );
		lpr_draw_debug_lines( draw_data, 0 );
	}

	{
		PROFILE_SECTION_NAMED( "sort and draw debug 1" );
		lpr_sort_batch( draw_data, 1 );
		lpr_draw_batch( draw_data, 1 );
	}

	{
		PROFILE_SECTION_NAMED( "finalize debug draw" );
		lpr_finalize_draw( &game_mem->DEBUG_window.lpr_draw_data );
	}

	{
		PROFILE_SECTION_NAMED( "swap debug window" );
		sys_api->swap_window( game_mem->DEBUG_window.window_id );
	}

#endif
}
#endif

#if 1
extern "C"
GAME_API_UPDATE( game_api_update )
{
	(void)sys_api;
	(void)game_mem;
	(void)game_time;
	(void)delta_time;

#if TGPROF_COPY_STRINGS
	if ( !game_mem->initialized )
	{
		for ( u32 i = 0; i < GAME_API_MAX_THREADS_NUM; ++i )
		{
			game_mem->prof.strings_mem_pools[i].init(
			   game_mem->prof.strings_mem_buffs[i],
			   sizeof( game_mem->prof.strings_mem_buffs[i] ) );
		}
	}
#endif
	PROFILE_SECTION();

	// TODO(theGiallo, 2016-09-04): remove this usleep!!!!!
	//usleep(2000);

	Inputs * inputs = &game_mem->inputs;

	if ( !game_mem->initialized )
	{
#if DEBUG_TO_FILE
		right_ear      = fopen( "./right_ear.txt",     "w+" );
		right_ear_sql  = fopen( "./right_ear_sql.txt", "w+" );
		right_ear_dist = fopen( "./right_ear_dist.txt", "w+" );
		right_ear0     = fopen( "./right_ear0.txt", "w+" );
		right_ear_sql_sampled = fopen( "./right_ear_sql_sampled.txt", "w+" );
		right_ear_dist_sampled = fopen( "./right_ear_dist_sampled.txt", "w+" );
#endif

#if 0
		game_mem->resources.guids.paths_pool.init( game_mem->mem_for_paths_pool,
		                           sizeof( game_mem->mem_for_paths_pool ) );
#endif

		game_mem->mem_stack_for_asset_loading.mem_buf = game_mem->mem_for_asset_loading;
		game_mem->mem_stack_for_asset_loading.total_size = sizeof( game_mem->mem_for_asset_loading );

		game_mem->mem_stack_tmp.mem_buf = game_mem->mem_for_tmp;
		game_mem->mem_stack_tmp.total_size = sizeof( game_mem->mem_for_tmp );

		game_mem->assets_mem_pool.init( game_mem->mem_for_assets_mem_pool,
		                                sizeof( game_mem->mem_for_assets_mem_pool ) );

		s32 window_id =
		sys_api->create_window( "weirdsuff",
		                        {3,3,false}, false, false,
		                        SYS_SCREEN_MODE_WINDOWED, false,
		                        1024, 576 );
		if ( SYS_API_RET_IS_ERROR( window_id ) )
		{
			lpr_log_err( "%s:\n%s", sys_api->get_error_name( window_id ),
			                        sys_api->get_error_description( window_id ) );
			LPR_ILLEGAL_PATH();
			return;
		}
		game_mem->main_window.window_id = window_id;

		lpr_init();
		u32 w, h;
		sys_api->get_window_size( game_mem->main_window.window_id, &w, &h );
		game_mem->main_window.lpr_draw_data.window_size.w = w;
		game_mem->main_window.lpr_draw_data.window_size.h = h;
		// TODO(theGiallo, 2016-03-05): custom allocation
		lpr_init_draw_data( &game_mem->main_window.lpr_draw_data, 2 );
		game_mem->main_window.lpr_draw_data.viewport_origin = {w/2, h/2};
		game_mem->main_window.lpr_draw_data.batches[1].stencils_enabled = true;
		game_mem->main_window.lpr_draw_data.batches[1].manage_stencils_rt = true;
		game_mem->main_window.lpr_draw_data.batches[0].stencils_enabled = true;
		game_mem->main_window.lpr_draw_data.batches[0].manage_stencils_rt = true;
		lpr_create_stencils_fbs_for_batches( &game_mem->main_window.lpr_draw_data );
		V2 camera_pos{0,0},
		   vanishing_point{0.5f,0.5f},
		   camera_world_span{50,(50*h)/(f32)w};
		f32 camera_angle = 0;
		game_mem->camera.pos = camera_pos;
		game_mem->camera.vanishing_point = vanishing_point;
		game_mem->camera.world_span = camera_world_span;
		game_mem->camera.angle = camera_angle;
		lpr_set_draw_target_default_full( &game_mem->main_window.lpr_draw_data );
		game_mem->txt_pos = {-23,5};

		lpr_assert( lpr_utf8_fullchar_from_ucs4( lpr_utf8_fullchar_to_ucs4( lpr_utf8_full_char( (u8*)"ñ" ) ) ).all == lpr_utf8_full_char( (u8*)"ñ" ).all );

		const char * dosis_font_path =
		   "resources/fonts/dosis/Dosis-Book.ttf";
		const char * hack_DEV_font_path =
		   "resources/fonts/hack/Hack-Regular-DEV.ttf";
		const char * hack_font_path =
		   "resources/fonts/hack/Hack-Regular.ttf";
		const char * hack_resaved_font_path =
		   "resources/fonts/hack/Hack-Regular_resaved.ttf";
		const char * dejavu_mono_font_path =
		   "resources/fonts/dejavu-sans-mono/DejaVuSansMono.ttf";
		const char * dejavu_mono_2_35_resaved_font_path =
		   "resources/fonts/dejavu-fonts-ttf-2.35/ttf/DejaVuSansMono_resaved.ttf";
		const char * dejavu_mono_2_35_font_path =
		   "resources/fonts/dejavu-fonts-ttf-2.35/ttf/DejaVuSansMono.ttf";
		const char * dejavu_serif_2_35_font_path =
		   "resources/fonts/dejavu-fonts-ttf-2.35/ttf/DejaVuSerif.ttf";
		const char * dejavu_sans_2_35_font_path =
		   "resources/fonts/dejavu-fonts-ttf-2.35/ttf/DejaVuSans.ttf";
		const char * dejavu_sanscondensed_2_35_font_path =
		   "resources/fonts/dejavu-fonts-ttf-2.35/ttf/DejaVuSansCondensed.ttf";

		(void)dosis_font_path;
		(void)hack_DEV_font_path;
		(void)hack_font_path;
		(void)hack_resaved_font_path;
		(void)dejavu_mono_font_path;
		(void)dejavu_mono_2_35_resaved_font_path;
		(void)dejavu_mono_2_35_font_path;
		(void)dejavu_serif_2_35_font_path;
		(void)dejavu_sans_2_35_font_path;
		(void)dejavu_sanscondensed_2_35_font_path;

		const char * font_path = hack_resaved_font_path;

		const u8 * glyphs_to_load = (u8*)u8" !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~ñredundant△▽◁▷◀▶▲▼✓✗❌↩○◌↻‖∷≔≡≣▤▦▬▭▮▯";
		//const u8 * dbg_glyphs_to_load = (u8*)u8" !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~ñredundant△▽";


		game_mem->fonts.init();

		const Font_LL *
		f =
		pack_single_font( sys_api, font_path, &game_mem->fonts,
		                  30.0f, glyphs_to_load, 2, 2 );
		if ( !f )
		{
			lpr_log_err( "pack_single_font failed!" );
		}
		game_mem->fonts.game_font = f;

		// NOTE(theGiallo): oversampling was 5 but I've added 2 characters
		// and now they don't fit in a single texture.
		// TODO(theGiallo, 2016-09-02): manage automatic splitting
		f =
		pack_single_font( sys_api, font_path, &game_mem->fonts,
		                  16.0f, glyphs_to_load, 4, 4 );
		if ( !f )
		{
			lpr_log_err( "pack_single_font failed!" );
		}
		game_mem->fonts.debug_font = f;

		DEBUG_ONLY(
			game_mem->DEBUG_window.window_id = -1;
			for ( u32 i = 0; i < PROF_TOP_SORTS_COUNT; ++i )
			{
				game_mem->prof_drawing_data.top.sort_whats[i]  = (Prof_Top_Sort)i;
				switch ( i )
				{
					case PROF_TOP_SORT_THREAD_ID:
					case PROF_TOP_SORT_NAME:
					case PROF_TOP_SORT_FILE:
					case PROF_TOP_SORT_FUNCTION:
					case PROF_TOP_SORT_LINE:
						game_mem->prof_drawing_data.top.sort_orders[i] =
						   SORT_ASCENDANTLY;
					break;
					default:
						game_mem->prof_drawing_data.top.sort_orders[i] =
						   SORT_DESCENDANTLY;
					break;
				}
			}
		)

		bool b_res;
		game_mem->audio_player.volume = 1.0f;
#if 0
		game_mem->audio_player.head.position = (V2*)&game_mem->player_pos;
		game_mem->audio_player.head.rotation = &game_mem->player_rot;
		game_mem->audio_player.head.kalman_rot.x[0][0] = game_mem->player_rot;
		game_mem->audio_player.head.kalman_pos.x[0][0][0] =
		   game_mem->player_pos.x;
		game_mem->audio_player.head.kalman_pos.x[1][0][0] =
		   game_mem->player_pos.y;
		f32 s = 0.99f;
		game_mem->audio_player.head.kalman_rot.P =
		game_mem->audio_player.head.kalman_pos.P[0] =
		game_mem->audio_player.head.kalman_pos.P[1] = { s*s, 0.0f, 0.0f, s*s};

		game_mem->test_sound.samples = game_mem->test_sound_samples;
		f32 freq = TEST_SOUND_FREQ;
		game_mem->test_sound.tot_samples_count =
		   ARRAY_COUNT( game_mem->test_sound_samples );
		game_mem->test_sound.channels_count = 1;

		FILE * of = fopen( "./sound_test.txt", "w" );
		for ( u32 i = 0; i < game_mem->test_sound.tot_samples_count; ++i )
		{
			f32 v;
			v = lpr_sin( ( i * TAU_F * freq) / (f32)AUDIO_SAMPLING_FREQUENCY );
			f32 fade = pow( 1.5f, -20.0f * (f32)i / (f32)AUDIO_SAMPLING_FREQUENCY );
			fade = exp( -4.0*tan( LPR_PI/2.0f * i / (f32)game_mem->test_sound.tot_samples_count ) );
			#if 0
			v *= fade;
			#else
			//v *= 0.5;
			#endif
			f32 volume = 0.25f;
			v *= volume;
			#if 0
				if ( v >= 1.0f )
				{
					lpr_log_info( "v >= 1 %e", v );
					v = 1.0f;
				}
				if ( v <= -1.0f )
				{
					lpr_log_info( "v <= -1 %e", v );
					v = -1.0f;
				}
			#endif
			fprintf( of, "%d, %e\n", i, v );
			game_mem->test_sound_samples[i] = v;
		}
		fclose ( of );

		if ( load_vorbis( sys_api, "resources/sounds/shot.ogg", &game_mem->mem_stack_for_asset_loading, &game_mem->test_vorbis_sound ) )
		{
			lpr_log_info( "test sound correctly loaded" );
		} else
		{
			lpr_log_err( "failed to load test sound" );
		}
		b_res =
		load_vorbis( sys_api, "resources/sounds/campfire.ogg",
		             &game_mem->mem_stack_for_asset_loading,
		             &game_mem->sound_campfire );
		if ( !b_res )
		{
			lpr_log_err( "failed to load campfire sound!" );
		}
		#if 0
		{
			game_mem->campfire_pos = {2.0f, 2.0f};

			Playable_Sound p_sound = {};
			p_sound.sound = game_mem->sound_campfire;
			lpr_assert( p_sound.sound.channels_count == 1 );
			p_sound.loops = -1;
			p_sound.source_pos = &game_mem->campfire_pos;
			enqueue_sound( &game_mem->audio_player.new_sounds_queue,
			               &p_sound );
		}
		#endif

		#if 0
		{
			Playable_Sound test_p_sound = {};
			test_p_sound.sound = game_mem->test_sound;
			lpr_assert( test_p_sound.sound.channels_count == 1 );
			test_p_sound.loops = -1;
			test_p_sound.source_pos = &game_mem->sound_source_pos;
			game_mem->sound_source_dist = 2.0f;
			update_test_sound_position( game_mem, game_time );
			if ( !enqueue_sound( &game_mem->audio_player.new_sounds_queue,
			                     &test_p_sound ) )
			{
				lpr_log_info( "enqueue_sound failed" );
			}
		}
		#endif
#endif

		// NOTE(theGiallo): test memory hotel
		#if 0
		{
			lpr_log_info( "going to test memory hotel" );
			Sound_MH smh = {};

			Sound * s = smh.allocate_clean();

			lpr_log_info( "capacity = %u", smh.capacity );
			lpr_log_info( "total_occupancy = %u", smh.total_occupancy );
			lpr_assert( smh.total_occupancy == 1 );
			smh.deallocate( s );
			lpr_log_info( "capacity = %u", smh.capacity );
			lpr_log_info( "total_occupancy = %u", smh.total_occupancy );
			lpr_assert( smh.total_occupancy == 0 );

			for ( u32 i = 0; i < smh.capacity; ++i )
			{
				s = smh.allocate_clean();
				lpr_log_info( "capacity = %u", smh.capacity );
				lpr_log_info( "total_occupancy = %u", smh.total_occupancy );
			}
			if ( smh.is_full() )
			{
				lpr_log_info( "smh is full, correct" );
			} else
			{
				lpr_log_err( "smh should be full now" );
				LPR_ILLEGAL_PATH();
			}

			// NOTE(theGiallo): this have to fail
			s = smh.allocate_clean();
			lpr_assert( !s );

			Sound_MH_10 smh_10 = {};

			s = smh_10.allocate_clean();

			lpr_log_info( "capacity = %u", smh_10.capacity );
			lpr_log_info( "total_occupancy = %u", smh_10.total_occupancy );
			lpr_assert( smh_10.total_occupancy == 1 );
			smh_10.deallocate( s );
			lpr_log_info( "capacity = %u", smh_10.capacity );
			lpr_log_info( "total_occupancy = %u", smh_10.total_occupancy );
			lpr_assert( smh_10.total_occupancy == 0 );

			for ( u32 i = 0; i < smh_10.capacity; ++i )
			{
				s = smh_10.allocate_clean();
				lpr_log_info( "capacity = %u", smh_10.capacity );
				lpr_log_info( "total_occupancy = %u", smh_10.total_occupancy );
			}
			if ( smh_10.is_full() )
			{
				lpr_log_info( "smh_10 is full, correct" );
			} else
			{
				lpr_log_err( "smh_10 should be full now" );
				LPR_ILLEGAL_PATH();
			}

			// NOTE(theGiallo): this have to fail
			s = smh_10.allocate_clean();
			lpr_assert( !s );

			Sound_MH_1024 smh_1024 = {};

			s = smh_1024.allocate_clean();

			lpr_log_info( "capacity = %u", smh_1024.capacity );
			lpr_log_info( "total_occupancy = %u", smh_1024.total_occupancy );
			lpr_assert( smh_1024.total_occupancy == 1 );
			smh_1024.deallocate( s );
			lpr_log_info( "capacity = %u", smh_1024.capacity );
			lpr_log_info( "total_occupancy = %u", smh_1024.total_occupancy );
			lpr_assert( smh_1024.total_occupancy == 0 );

			for ( u32 i = 0; i < smh_1024.capacity; ++i )
			{
				s = smh_1024.allocate_clean();
				lpr_log_info( "capacity = %u", smh_1024.capacity );
				lpr_log_info( "total_occupancy = %u", smh_1024.total_occupancy );
			}
			if ( smh_1024.is_full() )
			{
				lpr_log_info( "smh_1024 is full, correct" );
			} else
			{
				lpr_log_err( "smh_1024 should be full now" );
				LPR_ILLEGAL_PATH();
			}

			// NOTE(theGiallo): this have to fail
			s = smh_1024.allocate_clean();
			lpr_assert( !s );
		}
		#endif

#if 0
		b_res =
		load_png( sys_api, "./resources/imgs/metal_man.png",
				  &game_mem->mem_stack_tmp,
				  &game_mem->mem_stack_for_asset_loading,
				  &game_mem->test_image.raw,
				  &game_mem->test_image.size.w,
				  &game_mem->test_image.size.h );
		if ( !b_res )
		{
			lpr_log_err( "failed to load test image!" );
			LPR_ILLEGAL_PATH();
			return;
		}
		b_res =
		lpr_create_texture( game_mem->test_image.raw,
							game_mem->test_image.size,
							Lpr_Img_Fmt::LPR_RGBA8,
							true, true, true,
							&game_mem->metal_man_texture,
							Lpr_Texture_Filter::LPR_MIPMAP_TRILINEAR,
							Lpr_Texture_Filter::LPR_BILINEAR,
							Lpr_Wrap::LPR_CLAMP_TO_EDGE,
							Lpr_Wrap::LPR_CLAMP_TO_EDGE,
							Lpr_Scale_Filter::LPR_SCALE_LINEAR,
							V2{1,1},
							NULL );
		if ( !b_res )
		{
			lpr_log_err( "failed to create metal man texture!" );
			LPR_ILLEGAL_PATH();
			return;
		}

		b_res =
		load_resource_image( sys_api, game_mem,
		                     "./resources/imgs/metal_man.png",
		                     &game_mem->metal_man_ri );
		if ( !b_res )
		{
		    lpr_log_err( "failed to load test image resource!" );
		    LPR_ILLEGAL_PATH();
		    return;
		}

		game_mem->player_entity = allocate_clean( &game_mem->entities_mh );
		game_mem->player_entity->image = game_mem->metal_man_ri;
		game_mem->player_entity->sprite_size = {1.9f,1.9f};
		game_mem->player_entity->color = lpr_C_colors_u8.white;
		game_mem->player_entity->center = {0.5f, 0.0f};

		u64 seeds[2] = {27162348972345687, 1234};
		for ( s32 i = 0; i < 100; ++i )
		{
			Test_2D_Entity * e = allocate_clean( &game_mem->entities_mh );
			e->image = game_mem->metal_man_ri;
			e->sprite_size = {1.9f,1.9f};
			e->center = {0.5f, 0.1f};
			e->pos = {randd_between( seeds, -10.0f, 10.0f ),
			          randd_between( seeds, -10.0f, 10.0f ), 0.0f };
			e->color = lpr_C_colors_u8.red;
		}
#endif

		// NOTE(theGiallo): init stuff here
		{
			//texture_gen_load_images( game_mem, sys_api );
		}

		game_mem->initialized = true;
	}

////////////////////////////////////////////////////////////////////////////////

	inputs->mouse_motion_total_in_frame =
	   inputs->mouse_pos - inputs->mouse_motion_total_in_frame;


	inputs->keyboard_state =
	   sys_api->get_keyboard_state( &inputs->keyboard_state_byte_size );

	game_mem->game_time = game_time;
	game_mem->delta_time = delta_time;

	u64 mstmp_pop = game_mem->mem_stack_tmp.first_free;

	// NOTE(theGiallo): put here the update code!

	if ( inputs->keyboard_pressions[PK_F5] )
	{
		//texture_gen_load_images( game_mem, sys_api );
	}

	game_mem->mem_stack_tmp.first_free = mstmp_pop;
}
#endif

////////////////////////////////////////////////////////////////////////////////
// NOTE(theGiallo): @audio

internal
f32
spatial_sound_value( Playable_Sound * p_sound, V2 source_pos, V2 ear_pos,
                     s32 sample_id, bool left )
{
	f32 ret = 0.0f;

	V2 d_pos = source_pos - ear_pos;
	//f32 angle = lpr_atan2( d_pos.y, d_pos.x );
#if !USE_F64
	f32 sqlength = lpr_max( 0.01f, sq_length( d_pos ) );
	f32 v = 1.0f / sqlength;
#else
	f64 dx = d_pos.x;
	f64 dy = d_pos.y;
	f64 sqlength = dx*dx+dy*dy;
	if ( sqlength < 0.01 )
	{
		sqlength = 0.01;
	}
	f64 v = 1.0 / sqlength;
#endif

	lpr_assert( v >= 0.0f );
#if DEBUG_TO_FILE
	if ( ! left )
	{
		fprintf( right_ear_sql, "%lu %30.18f\n", samples_played, sqlength );
	}
#endif

#if !USE_F64
	f32 dist = lpr_sqrt( sqlength );
#else
	f64 dist = sqrt( sqlength );
#endif
#if DEBUG_TO_FILE
	if ( ! left )
	{
		fprintf( right_ear_dist, "%lu %30.18f\n", samples_played, dist );
	}
#endif


	s32 id = sample_id - SOUND_SAMPLE_DELAY_FROM_DISTANCE( dist );

#if !USE_F64
	f32 sv = 0.0f;
	f32 sc = 0.0f;
#else
	f64 sv = 0.0;
	f64 sc = 0.0;
#endif

	s32 prev_id;
	if ( p_sound->sound.ever_played )
	{
		if ( left )
		{
			prev_id = p_sound->last_played_sample_left;
		} else
		{
			prev_id = p_sound->last_played_sample_right;
		}
	} else
	{
		prev_id = id-1;
	}

	s32 start_id = prev_id+1;
	start_id +=
	    ( 1 + lpr_abs( start_id/(s64)p_sound->sound.tot_samples_count ) ) *
	    (s64)p_sound->sound.tot_samples_count;
	start_id %= (s64)p_sound->sound.tot_samples_count;
	if ( id > (s64)p_sound->sound.tot_samples_count &&
	     p_sound->loops )
	{
		for ( s32 s = start_id;
		      s < (s64)p_sound->sound.tot_samples_count;
		      ++s )
		{
			// NOTE(theGiallo): this could be assumed true
			if ( s >= 0 )
			{
				sv += p_sound->sound.samples[s];
				++sc;
			} else
			{
				lpr_log_err( "WTF IS GOING ON?!" );
			}
		}
		start_id = 0;
	}
	//lpr_log_info( "id is %d before %%", id );
	id += ( 1 + lpr_abs( id/(s64)p_sound->sound.tot_samples_count ) ) *
	      (s64)p_sound->sound.tot_samples_count;
	id %= (s64)p_sound->sound.tot_samples_count;
	//lpr_log_info( "id is %d after %%", id );
	s32 end_id = id;
	if ( start_id > end_id )
	{
		s32 tmp = start_id;
		start_id = end_id;
		end_id = tmp;
	}
	for ( s32 s = start_id;
	      s <= end_id;
	      ++s )
	{
		if ( s >=0 &&
		     s < (s64)p_sound->sound.tot_samples_count )
		{
			sv += p_sound->sound.samples[s];
			++sc;
		} else
		{
			lpr_log_err( "... WTF... s = %d/%u prev_id = %d",
			   s, p_sound->sound.tot_samples_count, prev_id );
		}
	}
	if ( left )
	{
		p_sound->last_played_sample_left = id;
	} else
	{
		p_sound->last_played_sample_right = id;
	}
	if ( sc )
	{
		ret = v * ( sv / sc );
	} else
	{
		lpr_log_dbg( "wtf no sound samples on %c! start_id=%d id=%d", left?'l':'r', start_id, id );
	}
#if DEBUG_TO_FILE
	if ( ! left ) fprintf( right_ear, "%lu %30.18f\n", samples_played, ret );
#endif
	return ret;
}

extern "C"
GAME_API_AUDIO_CALLBACK( game_api_audio_callback )
{
	(void)sys_api;
	PROFILE_SECTION();

	Audio_Player * ap = &game_mem->audio_player;
	if ( len > AUDIO_SAMPLES_IN_BUF )
	{
		lpr_log_err( "len = %u != %u", len, AUDIO_SAMPLES_IN_BUF );
		return;
	}
	lpr_assert( len <= AUDIO_SAMPLES_IN_BUF );
	f32 acc_buf[AUDIO_SAMPLES_IN_BUF] = {};

	// NOTE(theGiallo): accumulation
	s32 sounds_count = (s32)ap->sounds_count;
	for ( s32 i = 0; i < sounds_count; ++i )
	{
		Playable_Sound * p_sound = ap->sounds + i;
		Sound sound = p_sound->sound;
		s32 played_samples = p_sound->played_samples;
		u32 s = 0;
		if ( sound.channels_count == 2 )
		{
			for ( s = 0; s < len; ++s )
			{
				s32 id = played_samples+s;
				if ( id >= (s32)sound.tot_samples_count )
				{
					break;
				}
				if ( id < 0 )
				{
					continue;
				}
				acc_buf[s] += sound.samples[id];
			}
			p_sound->played_samples += s;
		} else
		if ( sound.channels_count == 1 )
		{
			V2 last_source_pos = p_sound->last_source_pos;
			V2 new_source_pos,
			#if DEBUG_TO_FILE
			   last_sampled_source_pos = p_sound->last_sampled_source_pos,
			#endif
			   new_sampled_source_pos;
			if ( p_sound->source_pos )
			{
				*(u64*)&new_source_pos = *(u64*)p_sound->source_pos;
				new_sampled_source_pos = new_source_pos;

				#define KALMAN_EACH_SAMPLE 1
				#if !KALMAN_EACH_SAMPLE
					// NOTE(theGiallo): Kalman
					f32 dt = 1.0f / AUDIO_SAMPLING_FREQUENCY;
					f32         sigma_a = 0.04,
					            Q = sigma_a * sigma_a,
					            R = 0.8;
					kalman_on_pos_separate( &p_sound->kalman, source_pos, dt, Q, R );
					#if 0
					lpr_log_dbg( "----------------" );
					lpr_log_dbg( "source_pos = %f, %f", source_pos.x, source_pos.y );
					#endif
					new_source_pos.x = p_sound->kalman.x[0][0][0];
					new_source_pos.y = p_sound->kalman.x[1][0][0];
					#if 0
					lpr_log_dbg( "source_pos = %f, %f", source_pos.x, source_pos.y );
					lpr_log_dbg( "----------------" );
					#endif
					//////////////////////////
				#endif
			}
			for ( s = 0; s < len; ++s )
			{
				s32 id = (s32)(played_samples+(s>>1));
				f32 v_l = 1.0f, v_r = 1.0f;
				if ( p_sound->source_pos )
				{
					if ( id >= (s32)p_sound->sound.tot_samples_count )
					{
						if ( p_sound->loops )
						{
							--p_sound->loops;
							p_sound->played_samples = 0;
						} else
						{
							if ( id >= p_sound->sound.tot_samples_count +
							     SOUND_MAX_DELAY_SAMPLES )
							{
								break;
							}
						}
					}
					#define INTERPOLATE_SOUND_POS 1
					#if !INTERPOLATE_SOUND_POS
					V2 source_pos;
					*(u64*)&source_pos = *(u64*)p_sound->source_pos;
					#else
					// NOTE(theGiallo): next starts at this new pos,
					// this ends before
					#if !USE_F64
					f32 alpha_p = s / (f32)len;
					#if 0
					#if 1
					alpha_p = lpr_smootherstep(0.0f, 1.0f, alpha_p);
					#else
					alpha_p = lpr_smoothstep(0.0f, 1.0f, alpha_p);
					#endif
					#endif
					#else
					f64 alpha_p = s / (f64)len;
					#if 0
					f64 v0 = 0.0, v1 = 1.0;
					f64 a = lpr_clamp( (alpha_p - v0) / (v1 - v0), 0.0, 1.0 );
					alpha_p =  a*a*a*(a*(a*6.0 - 15.0) + 10.0);
					#endif
					#endif

					#if USE_F64
						V2 source_pos;
						source_pos.x =
						   last_source_pos.x * (1.0 - (f64)alpha_p) +
						   new_source_pos.x * (f64)alpha_p;
						source_pos.y =
						   last_source_pos.y * (1.0 - (f64)alpha_p) +
						   new_source_pos.y * (f64)alpha_p;
					#else
						V2 source_pos = last_source_pos * ( 1.0f - alpha_p ) +
						                new_source_pos * alpha_p;
					#endif
					#endif

					#if KALMAN_EACH_SAMPLE
						// NOTE(theGiallo): Kalman
						f32 dt = 1.0f / AUDIO_SAMPLING_FREQUENCY;
						f32         sigma_a = 0.0001f,
						            Q = sigma_a * sigma_a,
						            R = 0.9f;
						kalman_on_pos_separate( &p_sound->kalman, source_pos, dt, Q, R );
						#if 0
						lpr_log_dbg( "----------------" );
						lpr_log_dbg( "source_pos = %f, %f", source_pos.x, source_pos.y );
						#endif
						source_pos.x = p_sound->kalman.x[0][0][0];
						source_pos.y = p_sound->kalman.x[1][0][0];
						#if 0
						lpr_log_dbg( "source_pos = %f, %f", source_pos.x, source_pos.y );
						lpr_log_dbg( "----------------" );
						#endif
						//////////////////////////
					#endif

					V2 left_ear_dp  = {-0.1f,0.0f};
					V2 right_ear_dp = { 0.1f,0.0f};
					// NOTE(theGiallo): probably we need another Kalman here
					V2 listener_pos;
					*(u64*)&listener_pos = *(u64*)ap->head.position;
					f32 listener_rot = *ap->head.rotation;
					kalman_on_pos_separate( &ap->head.kalman_pos, listener_pos, dt, Q, R );
					kalman_on_rot( &ap->head.kalman_rot, listener_rot, dt, Q, R );
					listener_pos.x = ap->head.kalman_pos.x[0][0][0];
					listener_pos.y = ap->head.kalman_pos.x[1][0][0];
					listener_rot   = ap->head.kalman_rot.x[0][0];

					V2 ear_pos =
					   rotate_deg( left_ear_dp, listener_rot );
					ear_pos = listener_pos + ear_pos,
					acc_buf[s] += spatial_sound_value( p_sound,
					                                   source_pos,
					                                   ear_pos,
					                                   id,
					                                   true );
					++s;
					ear_pos =
					   rotate_deg( right_ear_dp, listener_rot );
					ear_pos = listener_pos + ear_pos,
					acc_buf[s] += spatial_sound_value( p_sound,
					                                   source_pos,
					                                   ear_pos,
					                                   id,
					                                   false );

					#if DEBUG_TO_FILE
					if ( right_ear_sql_sampled )
					{
						V2 s_p = last_sampled_source_pos * ( 1.0f - alpha_p ) +
						         new_sampled_source_pos * alpha_p;
						f32 sq_l = sq_length( s_p - listener_pos );
						fprintf( right_ear_sql_sampled, "%lu %30.18f\n",
						         samples_played, sq_l );
						fprintf( right_ear_dist_sampled, "%lu %30.18f\n",
						         samples_played, lpr_sqrt( sq_l ) );
					}

					if ( s == 1 )
					{
						fprintf( right_ear0, "%lu %30.18f\n", samples_played,
						         1.0f );//acc_buf[s] );
					}
					++samples_played;
					#endif
				} else
				{
					if ( id >= (s32)sound.tot_samples_count )
					{
						break;
					}
					if ( id < 0 )
					{
						continue;
					}

					acc_buf[s] += v_l * sound.samples[id];
					++s;
					acc_buf[s] += v_r * sound.samples[id];
				}
				p_sound->sound.ever_played = 1;
			}
			p_sound->played_samples += s>>1;
			if ( p_sound->source_pos )
			{
				p_sound->last_source_pos = new_source_pos;
				p_sound->last_sampled_source_pos = new_sampled_source_pos;
			}
		} else
		{
			LPR_ILLEGAL_PATH();
		}

		if ( p_sound->played_samples == (s32)sound.tot_samples_count )
		{
			if ( p_sound->loops )
			{
				--p_sound->loops;
				p_sound->played_samples = 0;
				lpr_log_info( "%d", p_sound->loops );
			} else
			{
				if ( !p_sound->source_pos ||
				     p_sound->played_samples >= sound.tot_samples_count +
				     SOUND_MAX_DELAY_SAMPLES )
				{
					// NOTE(theGiallo): sound is done, throw it away
					--sounds_count;
					LPR_COPY_STRUCT( ap->sounds + i, ap->sounds + sounds_count );
					// NOTE(theGiallo): step back to remain in place
					--i;
				}
			}
		}
	}
	ap->sounds_count = sounds_count;

	// NOTE(theGiallo): take new sounds
	while ( sounds_count < MAX_MIXABLE_SOUNDS &&
	        dequeue_sound( &ap->new_sounds_queue, ap->sounds + sounds_count ) )
	{
		++sounds_count;
	}
	ap->sounds_count = sounds_count;
	// TODO(theGiallo, 2016-04-16): play the new sounds immediately (maybe dequeue and play, 1by1 )

		#if 0
			#define SIN_OF_FREQ( freq ) ( lpr_sin( ( adv * TAU_F * (freq) ) / AUDIO_SAMPLING_FREQUENCY ) )
			f32 v = ( SIN_OF_FREQ( 440 ) +
			          SIN_OF_FREQ( 277 ) +
			          SIN_OF_FREQ( 330 )   ) / 3.0;
			f32 volume = 0.125;
			v *= volume;
			#undef SIN_OF_FREQ
		#endif

	#if BLA
		1.0f / ( max_v - tv ) = v
		1.0f = v ( max_v - tv )
		-1.0f / v + max_v = tv
	#endif

	for ( u32 i = 0; i < len; ++i )
	{
		#if 0
			const f32 max_v = 1.0f;
			if ( acc_buf[i] > max_v )
			{
				acc_buf[i] = max_v;
			} else
			if ( acc_buf[i] < - max_v )
			{
				acc_buf[i] = - max_v;
			}
		#else
		#if 0
			const f32 max_v = 1.0f;
			f32 s;
			const f32 b = 32.0f * 32.0f;
			if ( acc_buf[i] >= 0 )
			{
				s = acc_buf[i];
				acc_buf[i] = max_v * (1.0f - 1.0f/lpr_pow( b, s ) );
			} else
			{
				s = -acc_buf[i];
				acc_buf[i] = -max_v * (1.0f - 1.0f/lpr_pow( b, s ) );
			}
		#else
		f32 sv = acc_buf[i] * ap->volume;
		if ( ap->mute )
		{
			sv = 0.0f;
		}
		f32 s = lpr_abs( sv );
		f32 sign = sv >=0 ? 1.0f : -1.0f;
		if ( s > 0.7 )
		{
			#if 0
			f32 start = 0.7;
			const f32 h_pi = 0.5f * LPR_PI;
			s = start + (max_v-s) * sin( h_pi - h_pi/lpr_pow(2.0f, s - start) );
			#else
			s = 1.0f - 1.0f / ( lpr_pow( 2.0f,
			                             lpr_pow( s + 0.50207179f, 3.0f )
			                             )
			                   );
			#endif
			sv = sign * s;
		}
		acc_buf[i] = sv;
		#endif
		#endif

		//buf[i] = acc_buf[i];
	}
	memcpy(buf, acc_buf, len * sizeof(f32) );
	#if 0
		for ( u32 i = 1; i < len; i+=2 )
		{
			fprintf( right_ear, "%f\n", acc_buf[i] );
		}
	#endif

	u32 adv = len >> 1;
	ap->played_samples += adv;
	ap->audio_time += adv / AUDIO_SAMPLING_FREQUENCY;
}

////////////////////////////////////////////////////////////////////////////////

internal
bool
enqueue_sound( Sounds_Queue * sq, Playable_Sound * in_p_sound )
{
	u64 f = sq->first;
	PRAGMA_ALIGN_PRE(64) u64 next PRAGMA_ALIGN_POST(64) = sq->last+1;
	if ( next == f || ( f == 0 && next == MAX_ENQUEUABLE_SOUNDS ) )
	{
		return false;
	}
	// NOTE(theGiallo): remember this is single producer!
	lpr_assert( next <= MAX_ENQUEUABLE_SOUNDS );
	if ( next == MAX_ENQUEUABLE_SOUNDS )
	{
		next = 0;
	}

	LPR_COPY_STRUCT( sq->cb_sounds + next, in_p_sound );

	STORELOAD_BARRIER();

	// NOTE(theGiallo): this is atomic on x86 because it's 64 bit aligned
	// 64-ia-32-architectures-software-developer-manual-325462.pdf 8.1.1 pag2209
	sq->last = next;

	lpr_assert_m( sq->last < MAX_ENQUEUABLE_SOUNDS, "sq->last=%lu", sq->last );
	return true;
}

internal
bool
dequeue_sound( Sounds_Queue * sq, Playable_Sound * out_p_sound )
{
	u64 f = sq->first;
	PRAGMA_ALIGN_PRE(64) u64 l PRAGMA_ALIGN_POST(64) = sq->last;
	if ( f == l )
	{
		return false;
	}
	++f;
	if ( f == MAX_ENQUEUABLE_SOUNDS )
	{
		f = 0;
	}

	#if 1
		lpr_assert_m( ((u64)&sq->first & 0xFu) % 8 == 0, "struct is not aligned properly!!!" );
		lpr_assert_m( ((u64)&sq->last & 0xFu) % 8 == 0, "struct is not aligned properly!!!" );
		lpr_assert_m( ((u64)&f & 0xFu) % 8 == 0, "struct is not aligned properly!!!" );
		lpr_assert_m( ((u64)&l & 0xFu) % 8 == 0, "struct is not aligned properly!!!" );
		lpr_assert_m( l < MAX_ENQUEUABLE_SOUNDS, "l=%lu", l );
	#endif

	LPR_COPY_STRUCT( out_p_sound,  sq->cb_sounds + f );

	STORELOAD_BARRIER();
	sq->first = f;

	if ( out_p_sound->source_pos )
	{
		out_p_sound->kalman = {};
		PRAGMA_ALIGN_PRE(64)
		V2 source_pos PRAGMA_ALIGN_POST(64);
		source_pos = *out_p_sound->source_pos;
		out_p_sound->kalman.x[0][0][0] = source_pos.x;
		out_p_sound->kalman.x[1][0][0] = source_pos.y;
		f32 s = 0.99f;
		out_p_sound->kalman.P[0] =
		out_p_sound->kalman.P[1] = { s*s, 0.0f, 0.0f, s*s};
	}

	return true;
}

#if THESE_ARE_THE_STBVORBIS_THINGS_WE_NEED
	stb_vorbis_info stb_vorbis_get_info(stb_vorbis *f);

	typedef struct
	{
	   unsigned int sample_rate;
	   int channels;

	   unsigned int setup_memory_required;
	   unsigned int setup_temp_memory_required;
	   unsigned int temp_memory_required;

	   int max_frame_size;
	} stb_vorbis_info;

	int stb_vorbis_get_samples_float(stb_vorbis *f, int channels, float **buffer, int num_samples);
	// gets num_samples samples, not necessarily on a frame boundary--this requires
	// buffering so you have to supply the buffers. DOES NOT APPLY THE COERCION RULES.
	// Returns the number of samples stored per channel; it may be less than requested
	// at the end of the file. If there are no more samples in the file, returns 0.

	stb_vorbis * stb_vorbis_open_memory(const unsigned char *data, int len,
	                                  int *error, const stb_vorbis_alloc *alloc_buffer);
	// create an ogg vorbis decoder from an ogg vorbis stream in memory (note
	// this must be the entire stream!). on failure, returns NULL and sets *error

	int stb_vorbis_decode_memory(const unsigned char *mem, int len, int *channels, int *sample_rate, short **output);
	// decode an entire file and output the data interleaved into a malloc()ed
	// buffer stored in *output. The return value is the number of samples
	// decoded, or -1 if the file could not be opened or was not an ogg vorbis file.
	// When you're done with it, just free() the pointer returned in *output.
#endif

internal
void
log_stb_vorbis_err( s32 error )
{
	switch ( error )
	{
		case VORBIS__no_error:
			lpr_log_err( "error during " "%s" ": " "VORBIS__no_error", __PRETTY_FUNCTION__ );
			break;

		// not a real error
		case VORBIS_need_more_data:
			lpr_log_err( "error during " "%s" ": " "VORBIS_need_more_data", __PRETTY_FUNCTION__ );
			break;

		// can't mix API modes
		case VORBIS_invalid_api_mixing:
			lpr_log_err( "error during " "%s" ": " "VORBIS_invalid_api_mixing", __PRETTY_FUNCTION__ );
			break;
		// not enough memory
		case VORBIS_outofmem:
			lpr_log_err( "error during " "%s" ": " "VORBIS_outofmem", __PRETTY_FUNCTION__ );
			break;
		// uses floor 0
		case VORBIS_feature_not_supported:
			lpr_log_err( "error during " "%s" ": " "VORBIS_feature_not_supported", __PRETTY_FUNCTION__ );
			break;
		// STB_VORBIS_MAX_CHANNELS is too small
		case VORBIS_too_many_channels:
			lpr_log_err( "error during " "%s" ": " "VORBIS_too_many_channels", __PRETTY_FUNCTION__ );
			break;
		// fopen() failed
		case VORBIS_file_open_failure:
			lpr_log_err( "error during " "%s" ": " "VORBIS_file_open_failure", __PRETTY_FUNCTION__ );
			break;
		// can't seek in unknown-length file
		case VORBIS_seek_without_length:
			lpr_log_err( "error during " "%s" ": " "VORBIS_seek_without_length", __PRETTY_FUNCTION__ );
			break;

		// file is truncated?
		case VORBIS_unexpected_eof:
			lpr_log_err( "error during " "%s" ": " "VORBIS_unexpected_eof", __PRETTY_FUNCTION__ );
			break;
		// seek past EOF
		case VORBIS_seek_invalid:
			lpr_log_err( "error during " "%s" ": " "VORBIS_seek_invalid", __PRETTY_FUNCTION__ );
			break;

		// decoding errors (corrupt/invalid stream) -- you probably
		// don't care about the exact details of these

		// vorbis errors:
		case VORBIS_invalid_setup:
			lpr_log_err( "error during " "%s" ": " "VORBIS_invalid_setup", __PRETTY_FUNCTION__ );
			break;
		case VORBIS_invalid_stream:
			lpr_log_err( "error during " "%s" ": " "VORBIS_invalid_stream", __PRETTY_FUNCTION__ );
			break;

		// ogg errors:
		case VORBIS_missing_capture_pattern:
			lpr_log_err( "error during " "%s" ": " "VORBIS_missing_capture_pattern", __PRETTY_FUNCTION__ );
			break;
		case VORBIS_invalid_stream_structure_version:
			lpr_log_err( "error during " "%s" ": " "VORBIS_invalid_stream_structure_version", __PRETTY_FUNCTION__ );
			break;
		case VORBIS_continued_packet_flag_invalid:
			lpr_log_err( "error during " "%s" ": " "VORBIS_continued_packet_flag_invalid", __PRETTY_FUNCTION__ );
			break;
		case VORBIS_incorrect_stream_serial_number:
			lpr_log_err( "error during " "%s" ": " "VORBIS_incorrect_stream_serial_number", __PRETTY_FUNCTION__ );
			break;
		case VORBIS_invalid_first_page:
			lpr_log_err( "error during " "%s" ": " "VORBIS_invalid_first_page", __PRETTY_FUNCTION__ );
			break;
		case VORBIS_bad_packet_type:
			lpr_log_err( "error during " "%s" ": " "VORBIS_bad_packet_type", __PRETTY_FUNCTION__ );
			break;
		case VORBIS_cant_find_last_page:
			lpr_log_err( "error during " "%s" ": " "VORBIS_cant_find_last_page", __PRETTY_FUNCTION__ );
			break;
		case VORBIS_seek_failed:
			lpr_log_err( "error during " "%s" ": " "VORBIS_seek_failed", __PRETTY_FUNCTION__ );
			break;
		default:
			break;
	}
}

internal
bool
load_vorbis( System_API * sys_api, const char * file_path, Mem_Stack * mem, Sound * out_sound )
{
	s64 file_size, read_size;

	file_size = sys_api->get_file_size( file_path );
	if ( file_size < 0 )
	{
		lpr_log_err( "error getting file size in %s, file '%s',\nerror name: %s\nerror description:\n%s",
		             __PRETTY_FUNCTION__, file_path, sys_api->get_error_name( file_size ),
		             sys_api->get_error_description( file_size ) );
		return false;
	}

	// TODO(theGiallo, 2016-04-18): if this is too much use a tmp mem allocator
	u8 file_mem[file_size];

	read_size =
	sys_api->read_entire_file( file_path, file_mem, file_size );
	if ( read_size != file_size )
	{
		if ( read_size < 0 )
		{
			lpr_log_err( "error reading file in %s, file '%s',\nerror name: %s\nerro description: %s",
			             __PRETTY_FUNCTION__, file_path, sys_api->get_error_name( file_size ),
			             sys_api->get_error_description( file_size ) );
		} else
		{
			lpr_log_err( "read_entire_file read less bytes(%ld) than get_file_size returned (%ld) ",
			             read_size, file_size );
		}
		return false;
	}

	stb_vorbis * decoder;
	u32 some_mem_size = 1024*1024;
	// NOTE(theGiallo): use alloca you say? pfff: goto instead!
	alloca_some_mem:
	u8 some_mem[some_mem_size];
	for (;;)
	{
		stb_vorbis_alloc alloc_buffer;
		alloc_buffer.alloc_buffer = (char*)some_mem;
		alloc_buffer.alloc_buffer_length_in_bytes = (s32) sizeof( some_mem );

		s32 error = 0;

		decoder =
		stb_vorbis_open_memory( file_mem, file_size, &error, &alloc_buffer );
		if ( !decoder )
		{
			log_stb_vorbis_err( error );
			if ( error != VORBIS_need_more_data )
			{
				return false;
			}
			some_mem_size *= 2;
			goto alloca_some_mem;
			//continue;
		}
		break;
	}

	stb_vorbis_info info = stb_vorbis_get_info( decoder );

	s32 res;
	// NOTE(theGiallo): 4 minutes
	u64 original_first_free = mem->first_free;
	s32 samples_count = info.channels * info.sample_rate * 60 * 4;
	samples_count = lpr_min( (u64)samples_count, (mem->total_size - mem->first_free)/sizeof(f32) );
	s32 new_samples_count = samples_count;

	float * sound_buffer = (float*)(mem->mem_buf + mem->first_free);
	mem->first_free += samples_count * sizeof( f32 );
	float * buf_clean = sound_buffer;

	for (;;)
	{
		if ( !new_samples_count )
		{
			mem->first_free = original_first_free;
			return false;
		}
		res =
		stb_vorbis_get_samples_float_interleaved( decoder, info.channels, buf_clean, new_samples_count );
		if ( res == new_samples_count )
		{
			buf_clean = (f32*)( mem->mem_buf + mem->first_free );
			new_samples_count = lpr_min( (u64)new_samples_count, (mem->total_size - mem->first_free)/sizeof(f32) );
			mem->first_free += new_samples_count;
			samples_count += new_samples_count;
			continue;
		}
		if ( res < new_samples_count/info.channels )
		{
			u32 not_read = new_samples_count - res * info.channels;
			samples_count -= not_read;
			mem->first_free -= not_read * sizeof( f32 );
		}
		break;
	}

	// TODO(theGiallo, 2016-04-18): resample to AUDIO_SAMPLING_FREQUENCY
	lpr_log_info( "loaded sound '%s' has %d samples rate, %d channels, %d samples",
	              file_path, info.sample_rate, info.channels, samples_count );
	out_sound->samples = sound_buffer;
	out_sound->channels_count = info.channels;
	out_sound->tot_samples_count = samples_count;

	// NOTE(theGiallo): this should do nothing if we pass alloc_buffer
	// stb_vorbis_close( decoder );

	return true;
}
// NOTE(theGiallo): this is included at the end because has some dumb macros, like "R" :|
#define STBVORBIS_NO_SQUARE_FUNCTION 1
#undef STB_VORBIS_HEADER_ONLY
#include "stb_vorbis.c"
