#ifndef GAME_API_RENDER_H
#define GAME_API_RENDER_H 1
#include "basic_types.h"
#include "game_api_lepre.h"

#define PROF_GRAPH_TIME_FLOW_WIDTH_PX ( 0x1<<16 )
#define PROF_GRAPH_TIME_FLOW_RIGHT_BORDER_PX 20
#define DEBUG_GUI_START_X_PX 10
#define DEBUG_GUI_START_Y_PX DEBUG_GUI_START_X_PX

#define PALETTE_STRING_TABLE( pname, r,g,b,a, name, num ) LPR_TOSTRING(name),
#define PALETTE_ENUM( pname, r,g,b,a, name, num ) pname##name = num,
#define PALETTE_RGBAU8( pname, r,g,b,a, name, num ) {r,g,b,a},
#define PALETTE_PASTEL( ENTRY ) \
        ENTRY( palette_pastel_, 0xe2, 0x91, 0x91, 0xff, petite_orchid,     0  ) \
        ENTRY( palette_pastel_, 0x99, 0xdd, 0x92, 0xff, granny_smith,      1  ) \
        ENTRY( palette_pastel_, 0x93, 0xd8, 0xb9, 0xff, pancho,            2  ) \
        ENTRY( palette_pastel_, 0x94, 0xc4, 0xd3, 0xff, regent_st_blue,    3  ) \
        ENTRY( palette_pastel_, 0x94, 0x9a, 0xce, 0xff, blue_bell,         4  ) \
        ENTRY( palette_pastel_, 0xb3, 0x94, 0xcc, 0xff, medium_orchid,     5  ) \
        ENTRY( palette_pastel_, 0xcc, 0x96, 0xb1, 0xff, lily,              6  ) \
        ENTRY( palette_pastel_, 0xcc, 0xa4, 0x99, 0xff, eunry,             7  ) \
        ENTRY( palette_pastel_, 0xdf, 0xe5, 0x92, 0xff, primrose,          8  ) \
        ENTRY( palette_pastel_, 0xff, 0xa5, 0x60, 0xff, rajah,             9  ) \
        ENTRY( palette_pastel_, 0x6b, 0xff, 0x63, 0xff, screamin_green,    10 ) \
        ENTRY( palette_pastel_, 0x65, 0xff, 0xcc, 0xff, acquamarine,       11 ) \
        ENTRY( palette_pastel_, 0x65, 0xc4, 0xff, 0xff, maya_blue,         12 ) \
        ENTRY( palette_pastel_, 0x65, 0x6b, 0xff, 0xff, medium_slate_blue, 13 ) \
        ENTRY( palette_pastel_, 0xad, 0x65, 0xff, 0xff, medium_purple,     14 ) \
        ENTRY( palette_pastel_, 0xff, 0x65, 0xf4, 0xff, pink_flamingo,     15 ) \
        ENTRY( palette_pastel_, 0xff, 0x65, 0x84, 0xff, brink_pink,        16 ) \
        ENTRY( palette_pastel_, 0xff, 0x65, 0x65, 0xff, bitter_sweet,      17 ) \

extern
Lpr_Rgba_u8
palette_pastel[];

enum
Palette_Pastel_Enum : u8
{
	PALETTE_PASTEL( PALETTE_ENUM )
	// ---
	palette_pastel_count,
};

extern
const char *
pastel_palette_names[];

#endif /* ifndef GAME_API_RENDER_H */
