#!/bin/bash

if [ -z ${SRC_PATH+x} ]
then
	SRC_PATH="."
fi

echo "// NOTE(theGiallo): auto-generated file" > "$SRC_PATH/gl_funcs_load.inc"

grep '_t \* gl' "$SRC_PATH/our_gl.h" | \
grep 'OPTIONAL' | \
sed -r 's/^gl[0-9a-zA-Z]*_t \* (gl[0-9a-zA-Z]*); *OPTIONAL$/\1/g' | \
sed -r 's/(.*)/#define \1_IS_OPTIONAL 1/g' >> "$SRC_PATH/gl_funcs_load.inc"

grep '_t \* gl' "$SRC_PATH/our_gl.h" | \
grep -v 'OPTIONAL' | \
sed -r 's/^gl[0-9a-zA-Z]*_t \* (gl[0-9a-zA-Z]*);$/\1/g' | \
sed -r 's/(.*)/#define \1_IS_OPTIONAL 0/g' >> "$SRC_PATH/gl_funcs_load.inc"

grep '_t \* gl' "$SRC_PATH/our_gl.h" | \
sed -r 's/^gl[0-9a-zA-Z]*_t \* (gl[0-9a-zA-Z]*);( *OPTIONAL)?$/\1/g' | \
sed -r 's/(.*)/LOAD_GL_FUNCTION( \1 )/g' >> "$SRC_PATH/gl_funcs_load.inc"
