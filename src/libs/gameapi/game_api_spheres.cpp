#include "tgmath.h"

#include "game_api.h"
#include "global_game_mem.h"

#include "game_api_utility.h"
#include "game_api_opengl_tools.h"


struct
Sphere_Arc
{
	V3 middle_point;
	u16 start_id, end_id;
};
struct
Sphere_Arc_ID
{
	u32 id : 31;
	u32 inverted : 1;
};
struct
Sphere_Voronoi_Polygon
{
	Sphere_Arc_ID * arcs;
	u32 arcs_count;
};
struct
Sphere_Voronoi
{
	V3 * points;
	V3 * intersections;
	Sphere_Arc * arcs;
	Sphere_Voronoi_Polygon * polygons;
	V3 center;
	f32 radius;
	u32 points_count;
	u32 intersections_count;
	u32 arcs_count;
	u32 polygons_count;
};

void
sphere_voronoi_compute( Sphere_Voronoi * voronoi, Mem_Stack * mem_stack )
{
	if ( voronoi->points_count == 2 )
	{
		voronoi->intersections_count = 2;
		voronoi->intersections = (V3*)mem_stack->push( sizeof( V3 ) * (voronoi->intersections_count) );
		voronoi->arcs_count = 2;
		voronoi->arcs = (Sphere_Arc*)mem_stack->push( sizeof( Sphere_Arc ) * (voronoi->arcs_count) );

		V3 right = ( voronoi->points[1] - voronoi->center ) ^ ( voronoi->points[0] - voronoi->center );
		V3 avg = ( voronoi->points[0] + voronoi->points[1] ) * 0.5f;
		V3 avg_on_sphere_c_rel = normalize_or_0( avg - voronoi->center ) * voronoi->radius;
		if ( avg_on_sphere_c_rel == V3{} )
		{
			V3 plane_normal = normalize( voronoi->points[0] - voronoi->center );
			f32 d = -sum( hadamard( plane_normal, voronoi->center ) );
			V3 p = voronoi->points[0];
			if ( plane_normal.x != 0.0f )
			{
				p.yz += V2{1.0f,1.0};
				p.x = - ( sum( hadamard( p.yz, plane_normal.yz ) ) + d ) / plane_normal.x;
			} else
			if ( plane_normal.y != 0.0f )
			{
				p.x += 1.0f;
				p.z += 1.0f;
				p.y = - ( sum( hadamard( V2{p.x,p.z}, V2{plane_normal.x,plane_normal.z} ) ) + d ) / plane_normal.y;
			} else
			if ( plane_normal.z != 0.0f )
			{
				p.xy += V2{1.0f,1.0};
				p.z = - ( sum( hadamard( p.xy, plane_normal.xy ) ) + d ) / plane_normal.z;
			}

			avg_on_sphere_c_rel = normalize( p - voronoi->center ) * voronoi->radius;
			lpr_assert( isfinite( avg_on_sphere_c_rel.x ) );
			lpr_assert( isfinite( avg_on_sphere_c_rel.y ) );
			lpr_assert( isfinite( avg_on_sphere_c_rel.z ) );
			right = avg_on_sphere_c_rel ^ ( voronoi->points[0] - voronoi->center );
			lpr_assert( isfinite( right.x ) );
			lpr_assert( isfinite( right.y ) );
			lpr_assert( isfinite( right.z ) );
		}
		voronoi->intersections[0] = voronoi->center + avg_on_sphere_c_rel;
		voronoi->intersections[1] = voronoi->center - avg_on_sphere_c_rel;

		V3 right_on_sphere_c_rel = normalize( right ) * voronoi->radius;
		lpr_assert( isfinite( right_on_sphere_c_rel.x ) );
		lpr_assert( isfinite( right_on_sphere_c_rel.y ) );
		lpr_assert( isfinite( right_on_sphere_c_rel.z ) );
		voronoi->arcs[0].start_id = 0;
		voronoi->arcs[0].  end_id = 1;
		voronoi->arcs[0].middle_point = voronoi->center + right_on_sphere_c_rel;
		voronoi->arcs[1].start_id = 0;
		voronoi->arcs[1].  end_id = 1;
		voronoi->arcs[1].middle_point = voronoi->center - right_on_sphere_c_rel;

		voronoi->polygons_count = 2;
		voronoi->polygons = (Sphere_Voronoi_Polygon*) mem_stack->push( sizeof( Sphere_Voronoi_Polygon ) * voronoi->polygons_count );
		voronoi->polygons[0].arcs_count = 2;
		voronoi->polygons[0].arcs = (Sphere_Arc_ID*) mem_stack->push( sizeof( Sphere_Arc_ID ) * voronoi->polygons[0].arcs_count );
		voronoi->polygons[0].arcs[0].id = 1;
		voronoi->polygons[0].arcs[0].inverted = 0;
		voronoi->polygons[0].arcs[1].id = 0;
		voronoi->polygons[0].arcs[1].inverted = 1;
		voronoi->polygons[1].arcs_count = 2;
		voronoi->polygons[1].arcs = (Sphere_Arc_ID*) mem_stack->push( sizeof( Sphere_Arc_ID ) * voronoi->polygons[1].arcs_count );
		voronoi->polygons[1].arcs[0].id = 0;
		voronoi->polygons[1].arcs[0].inverted = 1;
		voronoi->polygons[1].arcs[1].id = 1;
		voronoi->polygons[1].arcs[1].inverted = 0;

		global_game_mem->sim_test_data.ogl.spheres_voronoi.mid_points[0] = voronoi->center + avg_on_sphere_c_rel;
	} else
	if ( voronoi->points_count == 3 )
	{
		voronoi->intersections_count = 2;
		voronoi->intersections = (V3*)mem_stack->push( sizeof( V3 ) * (voronoi->intersections_count) );
		voronoi->arcs_count = 3;
		voronoi->arcs = (Sphere_Arc*)mem_stack->push( sizeof( Sphere_Arc ) * (voronoi->arcs_count) );

#if 0
		V3 normals[3] = {};
		V3 v_mids_c_rel[3] = {};
		for ( s32 i = 0; i != 3; ++i )
		{
			s32 next_i = i == 2 ? 0 : i + 1;
			s32 prev_i = i == 0 ? 2 : i - 1;
			V3 p_i_c_rel = voronoi->points[i] - voronoi->center;
			V3 p_prev_i_c_rel = voronoi->points[prev_i] - voronoi->center;
			if ( ( voronoi->points[i] + voronoi->points[next_i] ) == voronoi->center )
			{
				V3 up = p_i_c_rel ^ p_prev_i_c_rel;
				v_mids_c_rel[i] = normalize( p_i_c_rel ^ up ) * voronoi->radius;
			} else
			{
				V3 p_mid = ( voronoi->points[i] + voronoi->points[next_i] ) * 0.5f;
				v_mids_c_rel[i] = normalize( p_mid - voronoi->center ) * voronoi->radius;
			}
			normals[i] = ( p_i_c_rel ^ v_mids_c_rel[i] ) ^ p_i_c_rel;
		}

#if CALCULUS

		Ax * n0.x + Ay * n0.y + Az * n0.z = 0
		Ax * n1.x + Ay * n1.y + Az * n1.z = 0
		Ax * n2.x + Ay * n2.y + Az * n2.z = 0
		Bx * n0.x + By * n0.y + Bz * n0.z = 0
		Bx * n1.x + By * n1.y + Bz * n1.z = 0
		Bx * n2.x + By * n2.y + Bz * n2.z = 0
		Ax^2 + Ay^2 + Az^2 = r^2
		Bx^2 + By^2 + Bz^2 = r^2

		( - ( Ay * n1.y + Az * n1.z ) / n1.x ) * n0.x + Ay * n0.y + Az * n0.z = 0
		Ay * ( n0.y - n1.y * n0.x / n1.x ) + Az * ( n0.z - n1.z * n0.x / n1.x ) = 0

		( - ( Ay * n1.y + Az * n1.z ) / n1.x ) * n2.x + Ay * n2.y + Az * n2.z = 0
		Ay * ( n2.y - n1.y * n2.x / n1.x ) + Az * ( n2.z - n1.z * n2.x / n1.x ) = 0

		Ay = - Az * ( n2.z - n1.z * n2.x / n1.x ) / ( n2.y - n1.y * n2.x / n1.x )
#endif
#endif

		// TODO(theGiallo, 2017-04-13): reorganize stupid ifs so it's a bit more optimized

		V3 mid_points[3] = {};
		for ( s32 i = 0; i != 3; ++i )
		{
			s32 next_i = i == 2 ? 0 : i + 1;
			mid_points[i] = ( voronoi->points[i] + voronoi->points[next_i] ) * 0.5f;
		}

		s32 mp_diametrical_id = -1;
		V3 mid_points_c_rel[3] = {};
		V3 mid_points_sph_c_rel[3] = {};
		for ( s32 i = 0; i != 3; ++i )
		{
			mid_points_c_rel[i] = mid_points[i] - voronoi->center;
			mid_points_sph_c_rel[i] = normalize_or_0( mid_points_c_rel[i] ) * voronoi->radius;
			if ( mid_points_sph_c_rel[i] == V3{} )
			{
				mp_diametrical_id = i;
			}
		}

		f32 change_of_sign = 1.0f; // NOTE(theGiallo): should be -1 if normal of triangle points into sphere, but at the end it's not used

		V3 tr_normal;
		{
			V3 A = voronoi->points[0] - voronoi->points[2];
			V3 B = voronoi->points[1] - voronoi->points[2];
			tr_normal = ( A ^ B );
			// NOTE(theGiallo): this is terrible, I know, but we have to avoid ~0s
			tr_normal = normalize( tr_normal ); // TODO(theGiallo, 2017-04-13): could be unnecessary
			tr_normal.x = abs( tr_normal.x ) < 1e-6f ? 0.0f : tr_normal.x;
			tr_normal.y = abs( tr_normal.y ) < 1e-6f ? 0.0f : tr_normal.y;
			tr_normal.z = abs( tr_normal.z ) < 1e-6f ? 0.0f : tr_normal.z;
			tr_normal = normalize( tr_normal ); // TODO(theGiallo, 2017-04-13): could be unnecessary
			lpr_assert( tr_normal != V3{} );
			if ( mp_diametrical_id == -1 ) // NOTE(theGiallo): this is to avoid explosions with diametrical points
			{
				f32 new_change_of_sign = sign( tr_normal * ( voronoi->points[0] - voronoi->center ) );
				change_of_sign = new_change_of_sign != 0.0f ? new_change_of_sign : change_of_sign;
				tr_normal *= change_of_sign;
			}
			lpr_assert( tr_normal != V3{} );
		}
		//lpr_log_dbg( "change_of_sign = %f", change_of_sign );

		V3 height_vs[3] = {};
		for ( s32 i = 0; i != 3; ++i )
		{
			s32 next_i = i == 2 ? 0 : i + 1;
			//s32 prev_i = i == 0 ? 2 : i - 1;
			V3 A = voronoi->points[next_i] - voronoi->points[i]; //mid_points_sph_c_rel[i] - ( voronoi->points[next_i] - voronoi->center );
			//height_vs[i] = change_of_sign * normalize( mid_points_sph_c_rel[i] ^ A );
			height_vs[i] = change_of_sign * normalize( tr_normal ^ A );
			lpr_assert( isfinite( height_vs[i].x ) );
			lpr_assert( isfinite( height_vs[i].y ) );
			lpr_assert( isfinite( height_vs[i].z ) );
		}

#if CALCULUS

		mid_points_sph_c_rel[0].x + height_vs[0].x * t[0] = mid_points_sph_c_rel[1].x + height_vs[1].x * t[1]       = mid_points_sph_c_rel[2].x + height_vs[2].x * t[2]
		mid_points_sph_c_rel[0].y + height_vs[0].y * t[0] = mid_points_sph_c_rel[1].y + height_vs[1].y * t[1]       = mid_points_sph_c_rel[2].y + height_vs[2].y * t[2]
		mid_points_sph_c_rel[0].z + height_vs[0].z * t[0] = mid_points_sph_c_rel[1].z + height_vs[1].z * t[1]       = mid_points_sph_c_rel[2].z + height_vs[2].z * t[2]

		mid_points_sph_c_rel[0].x + height_vs[0].x * t[0] = mid_points_sph_c_rel[1].x + height_vs[1].x * t[1]
		mid_points_sph_c_rel[0].y + height_vs[0].y * t[0] = mid_points_sph_c_rel[1].y + height_vs[1].y * t[1]
		mid_points_sph_c_rel[0].z + height_vs[0].z * t[0] = mid_points_sph_c_rel[1].z + height_vs[1].z * t[1]

		( mid_points_sph_c_rel[0].x + height_vs[0].x * t[0] - mid_points_sph_c_rel[1].x ) / height_vs[1].x =
		( mid_points_sph_c_rel[0].y + height_vs[0].y * t[0] - mid_points_sph_c_rel[1].y ) / height_vs[1].y

		height_vs[0].x / height_vs[1].x * t[0] + ( mid_points_sph_c_rel[0].x - mid_points_sph_c_rel[1].x ) / height_vs[1].x =
		height_vs[0].y / height_vs[1].y * t[0] + ( mid_points_sph_c_rel[0].y - mid_points_sph_c_rel[1].y ) / height_vs[1].y


		( height_vs[0].x / height_vs[1].x - height_vs[0].y / height_vs[1].y ) * t[0] =
		( mid_points_sph_c_rel[0].y - mid_points_sph_c_rel[1].y ) / height_vs[1].y - ( mid_points_sph_c_rel[0].x - mid_points_sph_c_rel[1].x ) / height_vs[1].x

		t[0] = ( ( mid_points_sph_c_rel[0].y - mid_points_sph_c_rel[1].y ) / height_vs[1].y - ( mid_points_sph_c_rel[0].x - mid_points_sph_c_rel[1].x ) / height_vs[1].x  ) / ( height_vs[0].x / height_vs[1].x - height_vs[0].y / height_vs[1].y )

#endif

		V3 * m = mid_points_c_rel,
		   * h = height_vs;
		V3 circocenter = {};
		// TODO(theGiallo, 2017-04-10): choose the values to use to minimize error and not just avoid nans or infs
		//if ( h[1].x != 0.0f && h[1].y !=0.0f )
		//{
		//    f32 t0 = ( ( m[0].y - m[1].y ) / h[1].y - ( m[0].x - m[1].x ) / h[1].x )
		//             /
		//             ( h[0].x / h[1].x - h[0].y / h[1].y );
		//    circocenter = m[0] + t0 * height_vs[0];
		//} else

		s32 i,j,ii,jj;
		bool found = false;
		for ( i = 0; i != 3; ++i )
		{
			for ( j = 0; j != 3; ++j )
			{
				for ( ii = 0; ii != 3; ++ii )
				{
					for ( jj = 0; jj != 3; ++jj )
					{
						if ( h[j].arr[ii] != 0.0f &&
						    h[j].arr[jj] != 0.0f &&
						    abs( h[i].arr[ii] / h[j].arr[ii] - h[i].arr[jj] / h[j].arr[jj] ) > 1e-6f )
						{
							found = true;
							goto found_ijiijj;
						}
					}
				}
			}
		}
		found_ijiijj:
		lpr_assert( found );
#if 0
		// NOTE(theGiallo): IMPORTANT WARNING this generates some 0 / 0 or f / 0
		if ( ( h[1].x != 0.0f && h[1].y !=0.0f ) ||
		     ( h[1].z != 0.0f && h[1].y !=0.0f ) ||
		     ( h[1].x != 0.0f && h[1].z !=0.0f ) )
		{
			i = 0;
			j = 1;
		}
		if ( ( h[2].x != 0.0f && h[2].y !=0.0f ) ||
		     ( h[2].z != 0.0f && h[2].y !=0.0f ) ||
		     ( h[2].x != 0.0f && h[2].z !=0.0f ) )
		{
			i = 1;
			j = 2;
		} else
		if ( ( h[0].x != 0.0f && h[0].y !=0.0f ) ||
		     ( h[0].z != 0.0f && h[0].y !=0.0f ) ||
		     ( h[0].x != 0.0f && h[0].z !=0.0f ) )
		{
			i = 1;
			j = 0;
		} else
		{
			lpr_log_dbg( "wtf man!" );
		}

		if ( h[j].y != 0.0f )
		{
			jj = 1;
		} else
		{
			jj = 2;
		}
		if ( h[j].x != 0.0f )
		{
			ii = 0;
		} else
		{
			ii = 2;
		}
#endif

		//lpr_log_dbg( "i = %d, j = %d   ii = %d, jj = %d", i, j, ii, jj );

		f32 ti = ( ( m[i].arr[jj] - m[j].arr[jj] ) / h[j].arr[jj] - ( m[i].arr[ii] - m[j].arr[ii] ) / h[j].arr[ii] )
		         /
		         ( h[i].arr[ii] / h[j].arr[ii] - h[i].arr[jj] / h[j].arr[jj] );
		f32 num = ( m[i].arr[jj] - m[j].arr[jj] ) / h[j].arr[jj] - ( m[i].arr[ii] - m[j].arr[ii] ) / h[j].arr[ii];
		f32 den = h[i].arr[ii] / h[j].arr[ii] - h[i].arr[jj] / h[j].arr[jj];
		circocenter = m[i] + ti * height_vs[i];
		V3 circocenter_on_sph_tr_n = normalize( circocenter + tr_normal ) * voronoi->radius;
		lpr_assert( isfinite( circocenter_on_sph_tr_n.x ) );
		lpr_assert( isfinite( circocenter_on_sph_tr_n.y ) );
		lpr_assert( isfinite( circocenter_on_sph_tr_n.z ) );
		V3 circocenter_on_sph =
		   mp_diametrical_id == -1
		   ?
		   circocenter_on_sph_tr_n
		   :
		   normalize(
		      ( ( voronoi->points[mp_diametrical_id==0?2:mp_diametrical_id-1] - voronoi->center ) ^
		        ( voronoi->points[mp_diametrical_id==2?0:mp_diametrical_id+1] - voronoi->center ) ) ^
		      ( voronoi->points[mp_diametrical_id==2?0:mp_diametrical_id+1] - voronoi->center ) ) *
		   voronoi->radius;
		lpr_assert( isfinite( circocenter_on_sph.x ) );
		lpr_assert( isfinite( circocenter_on_sph.y ) );
		lpr_assert( isfinite( circocenter_on_sph.z ) );

		global_game_mem->sim_test_data.ogl.spheres_voronoi.circocenter   = circocenter_on_sph;//normalize( circocenter ) * voronoi->radius;
		//lpr_log_dbg( "circocenter_on_sph = %10.3e %10.3e %10.3e", circocenter_on_sph.x, circocenter_on_sph.y, circocenter_on_sph.z );
		//lpr_log_dbg( "circocenter        = %10.3e %10.3e %10.3e", circocenter.x, circocenter.y, circocenter.z );
		//lpr_log_dbg( "tr_normal          = %10.3e %10.3e %10.3e", tr_normal.x, tr_normal.y, tr_normal.z );
		//lpr_log_dbg( "h[j]               = %10.3e %10.3e %10.3e  ii = %d   jj = %d", h[j].x, h[j].y, h[j].z, ii, jj );
		//lpr_log_dbg( "h[i]               = %10.3e %10.3e %10.3e", h[i].x, h[i].y, h[i].z );
		//lpr_log_dbg( "mp_diametrical_id = %d", mp_diametrical_id );
		//lpr_log_dbg( "ti = %10.3e", ti );
		//lpr_log_dbg( "num = %10.3e", num );
		//lpr_log_dbg( "den = %10.3e", den );
		global_game_mem->sim_test_data.ogl.spheres_voronoi.mid_points[0] = mid_points_sph_c_rel[0];
		global_game_mem->sim_test_data.ogl.spheres_voronoi.mid_points[1] = mid_points_sph_c_rel[1];
		global_game_mem->sim_test_data.ogl.spheres_voronoi.mid_points[2] = mid_points_sph_c_rel[2];

		for ( s32 i = 0; i != 3; ++i )
		{
			s32 next_i = i == 2 ? 0 : i + 1;
			//lpr_log_dbg( "length( circocenter + voronoi->center - voronoi->points[i     ] = %f", length( circocenter + voronoi->center - voronoi->points[i] ) );
			//lpr_log_dbg( "length( circocenter + voronoi->center - voronoi->points[next_i] = %f", length( circocenter + voronoi->center - voronoi->points[next_i] ) );
			//lpr_assert( length( circocenter + voronoi->center - voronoi->points[i] ) ==
			//            length( circocenter + voronoi->center - voronoi->points[next_i] ) );
		}

		voronoi->intersections[0] = voronoi->center + circocenter_on_sph_tr_n; //normalize( circocenter ) * voronoi->radius;
		voronoi->intersections[1] = voronoi->center - circocenter_on_sph_tr_n; //normalize( circocenter ) * voronoi->radius;



		for ( s32 i = 0; i != 3; ++i )
		{
			voronoi->arcs[i].start_id = 0;
			voronoi->arcs[i].  end_id = 1;
			s32 next_i = i == 2 ? 0 : i + 1;
			s32 prev_i = i == 0 ? 2 : i - 1;
			V3 p_mid_c_rel = mid_points_sph_c_rel[i];
			V3 v_mid_c_rel;
			if ( mp_diametrical_id != -1 || sq_length( circocenter ) < 1.7e-10f )
			{
				v_mid_c_rel = normalize( m[i] - height_vs[i] * voronoi->radius ) * voronoi->radius;
				lpr_assert( isfinite( v_mid_c_rel.x ) );
				lpr_assert( isfinite( v_mid_c_rel.y ) );
				lpr_assert( isfinite( v_mid_c_rel.z ) );
			} else
			#if 0
			if ( p_mid_c_rel == V3{} )
			{
				V3 up = ( voronoi->points[i] - voronoi->center ) ^ ( voronoi->points[prev_i] - voronoi->center );
				v_mid_c_rel = circocenter_on_sph;//normalize( ( voronoi->points[i] - voronoi->center ) ^ up ) * voronoi->radius;
				lpr_log_dbg( "\n\nthat's it\n\n" );
			} else
			#endif
			{
				f32 obtuse =
				   sign( normalize( voronoi->points[i] - voronoi->points[prev_i] ) *
				         normalize( voronoi->points[next_i] - voronoi->points[prev_i] ) );
				lpr_assert( isfinite( obtuse ) );
				//lpr_log_dbg( "obtuse = %f", obtuse );
				#if 1
				// NOTE(theGiallo): works
				V3 left = obtuse * ( circocenter ^ m[i] );
				left = normalize( left );
				v_mid_c_rel = normalize( left ^ ( circocenter ) ) * voronoi->radius;
				lpr_assert( isfinite( v_mid_c_rel.x ) );
				lpr_assert( isfinite( v_mid_c_rel.y ) );
				lpr_assert( isfinite( v_mid_c_rel.z ) );
				#endif
			}
			voronoi->arcs[i].middle_point = v_mid_c_rel + voronoi->center;
		}

		voronoi->polygons_count = 3;
		voronoi->polygons = (Sphere_Voronoi_Polygon*) mem_stack->push( sizeof( Sphere_Voronoi_Polygon ) * voronoi->polygons_count );
		voronoi->polygons[0].arcs_count = 2;
		voronoi->polygons[0].arcs = (Sphere_Arc_ID*) mem_stack->push( sizeof( Sphere_Arc_ID ) * voronoi->polygons[0].arcs_count );
		voronoi->polygons[0].arcs[0].id = 2;
		voronoi->polygons[0].arcs[0].inverted = change_of_sign == 1.0f ? 0 : 1;
		voronoi->polygons[0].arcs[1].id = 0;
		voronoi->polygons[0].arcs[1].inverted = change_of_sign == 1.0f ? 1 : 0;
		voronoi->polygons[1].arcs_count = 2;
		voronoi->polygons[1].arcs = (Sphere_Arc_ID*) mem_stack->push( sizeof( Sphere_Arc_ID ) * voronoi->polygons[1].arcs_count );
		voronoi->polygons[1].arcs[0].id = 0;
		voronoi->polygons[1].arcs[0].inverted = change_of_sign == 1.0f ? 0 : 1;
		voronoi->polygons[1].arcs[1].id = 1;
		voronoi->polygons[1].arcs[1].inverted = change_of_sign == 1.0f ? 1 : 0;
		voronoi->polygons[2].arcs_count = 2;
		voronoi->polygons[2].arcs = (Sphere_Arc_ID*) mem_stack->push( sizeof( Sphere_Arc_ID ) * voronoi->polygons[2].arcs_count );
		voronoi->polygons[2].arcs[0].id = 1;
		voronoi->polygons[2].arcs[0].inverted = change_of_sign == 1.0f ? 0 : 1;
		voronoi->polygons[2].arcs[1].id = 2;
		voronoi->polygons[2].arcs[1].inverted = change_of_sign == 1.0f ? 1 : 0;
	} else
	if ( voronoi->points_count == 1 )
	{
		voronoi->polygons_count = 1;
	} else
	{
		LPR_ILLEGAL_PATH();
	}
}

void
sphere_voronoi_fill_ogl_vbuffer( Sphere_Voronoi * voronoi,
                                 Colored_Vertex * colored_vertex_buffer, u32 * vb_frees )
{
	for ( u32 ai = 0; ai != voronoi->arcs_count; ++ai )
	{
		if ( *vb_frees < 6 )
		{
			lpr_log_err( "filling ogl buffer for sphere-voronoi needed more vertices space into the buffer" );
			return;
		}

		lpr_assert( voronoi->arcs[ai].start_id < voronoi->points_count );
		lpr_assert( voronoi->arcs[ai].end_id   < voronoi->points_count );

		f32 perc = 1.5f;

		colored_vertex_buffer[0].vertex =
		   ( voronoi->intersections[voronoi->arcs[ai].start_id] - voronoi->center ) * perc + voronoi->center;
		colored_vertex_buffer[1].vertex = voronoi->center;
		colored_vertex_buffer[2].vertex =
		   ( voronoi->arcs[ai].middle_point - voronoi->center ) * perc + voronoi->center;

		colored_vertex_buffer[3].vertex =
		   ( voronoi->arcs[ai].middle_point - voronoi->center ) * perc + voronoi->center;
		colored_vertex_buffer[4].vertex = voronoi->center;
		colored_vertex_buffer[5].vertex =
		   ( voronoi->intersections[voronoi->arcs[ai].end_id] - voronoi->center ) * perc + voronoi->center;

		for ( s32 i = 0; i != 6; ++i )
		{
			colored_vertex_buffer[i].color = V3{1.0f,1.0f,1.0f};
		}

		colored_vertex_buffer += 6;
		*vb_frees -= 6;
	}
}

void
sphere_voronoi_areas( Sphere_Voronoi * voronoi, f32 * polygons_areas )
{
	f32 squared_radius = square( voronoi->radius );
	f32 sphere_area = 4.0f * PI_F * squared_radius;
	f32 total_area = 0.0f;

	if ( voronoi->polygons_count == 1 )
	{
		polygons_areas[0] = sphere_area;
		return;
	}

	for ( u32 pi = 0; pi != voronoi->polygons_count; ++pi )
	{
		f32 total_angles_rad = 0.0f;
		Sphere_Voronoi_Polygon * p = voronoi->polygons + pi;
		Sphere_Arc_ID * prev_arc_id = p->arcs + ( p->arcs_count - 1 );
		Sphere_Arc_ID *      arc_id = p->arcs + ( 0 );
		Sphere_Arc_ID * next_arc_id = p->arcs + ( 1 );
		u16 v_id =
		   arc_id->inverted
		   ?
		   voronoi->arcs[arc_id->id].start_id
		   :
		   voronoi->arcs[arc_id->id].end_id;
		V3 v_prev = voronoi->arcs[prev_arc_id->id].middle_point,
		   v      = voronoi->intersections[v_id],
		   v_next = voronoi->arcs[next_arc_id->id].middle_point;
		bool inverted_polygon = ( v - voronoi->center ) * ( ( v_next - v ) ^ ( v_prev - v ) ) < 0.0f;
		for ( u32 vi = 0; vi != p->arcs_count; ++vi )
		{
			u16 next_vi = vi == p->arcs_count-1 ? 0 : vi + 1;
			next_arc_id = p->arcs + next_vi;
			arc_id = p->arcs + vi;
			Sphere_Arc * arc = voronoi->arcs + arc_id ->id;
			#if 0
			u16 v_id      = arc->start_id * ( 1 - a_id->inverted ) + arc->end_id * a_id->inverted;
			#else
			v_id      = arc_id->inverted == 0 ? arc->start_id : arc->end_id;
			#endif
			v_prev = voronoi->arcs[prev_arc_id->id].middle_point,
			v      = voronoi->intersections[v_id];
			v_next = voronoi->arcs[arc_id->id].middle_point;

			V3 v_c_rel = v - voronoi->center;
			V3 tangent_next = v_c_rel ^ ( ( v_next - v ) ^ v_c_rel );
			V3 tangent_prev = v_c_rel ^ ( ( v_prev - v ) ^ v_c_rel );

			total_angles_rad += angle_rad_between( tangent_next, tangent_prev );
			lpr_assert( isfinite( total_angles_rad ) );

			prev_arc_id = arc_id;
		}
		f32 area = squared_radius * ( total_angles_rad - ( p->arcs_count - 2 ) * PI_F );
		polygons_areas[pi] = inverted_polygon ? sphere_area - area : area;
		total_area += polygons_areas[pi];
	}

	lpr_assert( eps_cmp( total_area, sphere_area, total_area * 0.01f ) );
}
