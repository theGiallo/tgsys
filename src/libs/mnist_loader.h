#ifndef _MNIST_LOADER_H_
#define _MNIST_LOADER_H_ 1

#include <cstdio>
#include "basic_types.h"
#include "macro_tools.h"
#include "tg_io.h"
#include "stack_generic_vm.h"

namespace tg
{
struct
Grayscale_Images_u8
{
	s32 rows_count;
	s32 cols_count;
	Stack_Generic_VM images;
};

struct
MNIST
{
	// NOTE(theGiallo): http://yann.lecun.com/exdb/mnist/

	inline
	static
	bool
	load_images( const char * path, Grayscale_Images_u8 * out_images )
	{
		bool ret = false;
		FILE * f = fopen( path, "r" );
		if ( f == 0 )
		{
			return ret;
		}
		const s32 expected_magic_number = 2051;
		s32 magic_number;
		s32 images_count;
		s32 rows_count;
		s32 cols_count;

		ret = read_s32( f, & magic_number ); if ( ! ret ) return ret;
		if ( magic_number != expected_magic_number )
		{
			ret = false;
			log_err( "magic number different from expected ( %d )", magic_number );
			return ret;
		}
		ret = read_s32( f, & images_count ); if ( ! ret ) return ret;
		ret = read_s32( f, & rows_count   ); if ( ! ret ) return ret;
		ret = read_s32( f, & cols_count   ); if ( ! ret ) return ret;

		log_dbg( "images_count %d, rows_count %d, cols_count %d",
		          images_count, rows_count, cols_count );

		out_images->cols_count = cols_count;
		out_images->rows_count = rows_count;

		s32 image_area = rows_count * cols_count;
		ret = out_images->images.init( (u64)image_area, (u64)images_count, (u64)images_count  );
		//ret = out_images->images.reserve_free( (u64)image_area * (u64)images_count );
		if ( ! ret )
		{
			log_err( "failed to initialize mem for images pixels %ld", (u64)image_area * (u64)images_count );
			return ret;
		}
		for_0M( i, images_count )
		{
			u8 * ptr;
			ret = out_images->images.push_space( (void **)&ptr ); if ( ! ret ) return ret;
			ret = tg_io_read_n( f, ptr, image_area ); if ( ! ret ) return ret;
		}

		fclose( f );
		return ret;
	}

	inline
	static
	bool
	load_labels( const char * path, Stack_Generic_VM * labels )
	{
		bool ret = false;
		FILE * f = fopen( path, "r" );
		if ( f == 0 )
		{
			return ret;
		}
		const s32 expected_magic_number = 2049;
		s32 magic_number;
		s32 labels_count;

		ret = read_s32( f, & magic_number ); if ( ! ret ) return ret;
		if ( magic_number != expected_magic_number )
		{
			ret = false;
			log_err( "magic number different from expected ( %d )", magic_number );
			return ret;
		}
		ret = read_s32( f, & labels_count ); if ( ! ret ) return ret;

		ret = labels->init( 1, labels_count, labels_count );
		if ( ! ret )
		{
			log_err( "failed to initialize mem for labels %d", labels_count );
			return ret;
		}

		u8 * ptr = NULL;
		ret = labels->push_space( (void **)&ptr, labels_count ); if ( ! ret ) return ret;
		ret = tg_io_read_n( f, ptr, labels_count ); if ( ! ret ) return ret;

		fclose( f );
		return ret;
	}

	inline
	static
	bool
	read_s32( FILE * f, s32 * s32_ptr )
	{
		bool ret = tg_io_read_one( f, s32_ptr );
		if ( ret )
		{
			tg::IO::invert_endianess( s32_ptr );
		}
		return ret;
	}
};
};

#endif /* ifndef _MNIST_LOADER_H_ */
