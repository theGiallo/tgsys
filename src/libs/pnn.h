#ifndef __PNN_H__
#define __PNN_H__ 1

//#define PNN_SGD PNN_SGD_ADAM
//#define PNN_SGD_ADAM    0x2
//#define PNN_SGD_VANILLA 0x3

//#define PNN_L2 1

#define PNN_DEFAULT_L2_LAMBDA 1e-5f
#define PNN_DEFAULT_DROPOUT_P 0.5f
#define PNN_DEFAULT_ADAM_B1   0.9f
#define PNN_DEFAULT_ADAM_B2   0.999f
#define PNN_DEFAULT_ADAM_EPS  1e-8f
#define PNN_DEFAULT_MOMENTUM_MU 0.75f

#include <initializer_list>
#include "basic_types.h"
#include "macro_tools.h"
#include "tgmath.h"
#include "system.h"
#include "utility.h"
#include "stack_generic_vm.h"
#include "tg_io.h"

namespace pnn
{
	//#pragma omp declare target
	inline
	f32
	sigmoid( f32 p )
	{
		f32 ret = 1.f / ( 1.f + pow( E_F, -p ) );
		return ret;
	}
	//#pragma omp end declare target

	enum class
	Flags : u32
	{
		NONE = 0,
		L2         = 1 << 0,
		ADAM       = 1 << 1,
		DROPOUT    = 1 << 2,
		SIGMOID    = 1 << 3,
		RELU       = 1 << 4,
		LEAKY_RELU = 1 << 5,
		PRELU      = 1 << 6,
		LINEAR     = 1 << 7,
		SOFTMAX    = 1 << 8,
		MOMENTUM   = 1 << 9,
	};

	inline
	Flags
	constexpr operator~( Flags a )
	{
		return static_cast<Flags>( ~static_cast<u32>(a) );
	}
	inline
	Flags
	constexpr operator&( Flags a, Flags b )
	{
		return static_cast<Flags>( static_cast<u32>(a) & static_cast<u32>(b) );
	}
	inline
	Flags
	constexpr operator|( Flags a, Flags b )
	{
		return static_cast<Flags>( static_cast<u32>(a) | static_cast<u32>(b) );
	}

	inline
	bool
	has_flags( Flags this_flags, Flags to_find )
	{
		bool ret = ( this_flags & to_find ) == to_find;
		return ret;
	}

	struct
	PNN
	{
		struct
		Layer_Parameters
		{
			s32 cardinality;
			Flags flags;

			f32 l2;
			f32 adam_beta1;
			f32 adam_beta2;
			f32 adam_eps;
			f32 dropout_p;
			f32 momentum_mu;
			f32 relu_k;
		};

		struct
		Layer
		{
			Flags flags;
			s32 cardinality;
			s32 prev_layer_cardinality;
			Layer * prev_layer;
			Layer * next_layer;
			f32 * activations;
			f32 * weights;
			f32 * biases;
			f32 * prev_epoch_weights;
			//f32 * wx_b;
			//f32 * a_wx_b;
			f32 * dadz;
			f32 * dCdb;
			f32 * dCdb_running_avg;
			f32 * dCda;
			//f32 * dCda_running_avg;
			f32 * dCdw;
			f32 * dCdw_running_avg;

			f32 iterations_counter;
			f32 minibatch_counter;

			f32 dropout_p;

			f32 momentum_mu;
			f32 * momentum_v;

			f32 relu_k;

			f32 l2;

			f32 * adam_m;
			f32 * adam_v;
			f32 * adam_mt;
			f32 * adam_vt;

			f32 adam_beta1;
			f32 adam_beta2;
			f32 adam_eps;

			bool backpropagated;

			u64 seeds[2];

			bool
			init( Layer * prev_layer, Layer_Parameters parameters )
			{
				bool ret = false;
				this->cardinality            = parameters.cardinality;
				this->prev_layer_cardinality = prev_layer ? prev_layer->cardinality : 0;
				this->prev_layer             = prev_layer;
				this->flags       = parameters.flags;
				this->l2          = parameters.l2          ? parameters.l2          : PNN_DEFAULT_L2_LAMBDA;
				this->dropout_p   = parameters.dropout_p   ? parameters.dropout_p   : PNN_DEFAULT_DROPOUT_P;
				this->momentum_mu = parameters.momentum_mu ? parameters.momentum_mu : PNN_DEFAULT_MOMENTUM_MU;
				this->adam_beta1  = parameters.adam_beta1  ? parameters.adam_beta1  : PNN_DEFAULT_ADAM_B1;
				this->adam_beta2  = parameters.adam_beta2  ? parameters.adam_beta2  : PNN_DEFAULT_ADAM_B2;
				this->adam_eps    = parameters.adam_eps    ? parameters.adam_eps    : PNN_DEFAULT_ADAM_EPS;
				this->relu_k      = parameters.relu_k;
				if ( has_flags( flags, Flags::RELU ) )
				{
					this->relu_k = 0.f;
				} else
				if ( has_flags( flags, Flags::LEAKY_RELU ) )
				{
					this->relu_k = 0.01f;
					flags = ( flags & (~Flags::LEAKY_RELU) ) | Flags::RELU;
				} else
				if ( has_flags( flags, Flags::PRELU ) )
				{
					if ( relu_k <= 0 )
					{
						log_err( "PReLU with relu_k = %f", relu_k );
						ILLEGAL_PATH();
						return ret;
					}
					flags = ( flags & (~Flags::PRELU) ) | Flags::RELU;
				}
				log_dbg( "relu_k      : %e", relu_k     );
				log_dbg( "l2          : %e", l2         );
				log_dbg( "dropout_p   : %e", dropout_p  );
				log_dbg( "momentum_mu : %e", momentum_mu  );
				log_dbg( "adam_beta1  : %e", adam_beta1 );
				log_dbg( "adam_beta2  : %e", adam_beta2 );
				log_dbg( "adam_eps    : %e", adam_eps   );

				bool succ;

				#define PNN_LAYER_ALLOCATE(name,type) \
				{ \
					s32 GLUE(name, _bytes_size_) = GLUE(name, _bytes_size)(); \
					if ( GLUE(name, _bytes_size_) ) \
					{ \
						name = (type*)sys_mem_reserve( GLUE(name, _bytes_size_) ); \
						if ( ! name || name == MAP_FAILED ) \
						{ \
							log_err( TOSTRING(name)"_bytes_size_ = %d B", GLUE(name, _bytes_size_) ); \
							perror( "mem reserve for layer has failed" ); \
							destroy(); \
							return ret; \
						} \
						succ = sys_mem_commit( name, GLUE(name, _bytes_size_) ); \
						if ( ! succ ) \
						{ \
							log_err( TOSTRING(name)"_bytes_size_ = %d B", GLUE(name, _bytes_size_) ); \
							perror( "mem commit for layer has failed" ); \
							destroy(); \
							return ret; \
						} \
						memset( name, 0x0, GLUE(name, _bytes_size_) ); \
					} \
				}


				PNN_LAYER_ALLOCATE(activations,f32)
				PNN_LAYER_ALLOCATE(weights,f32)
				PNN_LAYER_ALLOCATE(momentum_v,f32)
				PNN_LAYER_ALLOCATE(prev_epoch_weights,f32)
				PNN_LAYER_ALLOCATE(biases,f32)
				//PNN_LAYER_ALLOCATE(wx_b,f32)
				//PNN_LAYER_ALLOCATE(a_wx_b,f32)
				PNN_LAYER_ALLOCATE(dadz,f32)
				PNN_LAYER_ALLOCATE(dCdb,f32)
				PNN_LAYER_ALLOCATE(dCdb_running_avg,f32)
				PNN_LAYER_ALLOCATE(dCda,f32)
				//PNN_LAYER_ALLOCATE(dCda_running_avg,f32)
				PNN_LAYER_ALLOCATE(dCdw,f32)
				PNN_LAYER_ALLOCATE(dCdw_running_avg,f32)
				PNN_LAYER_ALLOCATE(adam_m,f32)
				PNN_LAYER_ALLOCATE(adam_v,f32)
				PNN_LAYER_ALLOCATE(adam_mt,f32)
				PNN_LAYER_ALLOCATE(adam_vt,f32)

				seeds[0] = (u64)rand() | ( (u64)rand() << 32 );
				seeds[1] = (u64)rand() | ( (u64)rand() << 32 );

				random_init_weights_and_biases();

				iterations_counter = 0;
				minibatch_counter = 0;

				ret = true;
				return ret;
			}

			inline
			void
			random_init_weights_and_biases()
			{
				if ( ! prev_layer_cardinality )
				{
					return;
				}

				Marsaglia_Gauss_Data g = {};
				g.seeds[0] = seeds[0];
				g.seeds[1] = seeds[1];

				for_0M( j, cardinality )
				{
					f32 sq = sqrtf( prev_layer_cardinality );
					f32 oosq = 1.f / sq;
					for_0M( k, prev_layer_cardinality )
					{
						//weight_jk( j, k ) = randf_between( seeds, -1.f, 1.f );
						weight_jk( j, k ) = (f32)randd_gauss_m0std1( &g ) * oosq;
						if ( abs( weight_jk( j, k ) ) > 10.f )
						{
							log_info( "w [%d][%d] %e", j, k, weight_jk( j, k ) );
						}
						//log_info( "w [%d][%d] %e", j, k, weight_jk( j, k ) );
					}
					//biases[j] = randf_between( seeds, -1.f, 1.f ) * 0.1f;
					biases[j] = 0;//(f32)randd_gauss_01( &g ) * oosq;
					//log_info( "bias [%d] %e", j, biases[j] );
				}
			}

			#define NN_OPENMP 0
			template<Flags t_flags = Flags::SIGMOID, bool learning = false>
			void
			run_()
			{
				if ( ! prev_layer )
				{
					return;
				}

				if ( backpropagated )
				{
					clean_gradients_buffers();
					backpropagated = false;
				}

				//#pragma omp declare target
				f64 sum = 0;
				f32 * prev_layer_acts    = prev_layer->activations;
				//#pragma omp end declare target

				#if NN_OPENMP
				//#pragma omp declare target
				f32 * activations = this->activations;
				f32 * dadz        = this->dadz;
				f32 * weights     = this->weights;
				f32 * biases      = this->biases;
				const u64 j_count = cardinality;
				//#pragma omp end declare target

				#define jk_index( j, k ) ((j) * prev_layer_cardinality + (k))
				#define weight_jk( j, k ) weights[jk_index( j, k )]
				#define sigmoid(p) (1.f / ( 1.f + pow( E_F, -p ) ))

				//#pragma omp target data map(from:activations[:cardinality], dadz[:cardinality]), map(to:weights[:cardinality*prev_layer_cardinality],prev_layer_acts[:prev_layer_cardinality],biases[:cardinality])
				//#pragma omp target teams distribute parallel for schedule(static,1) reduction(+:sum)
				#pragma omp parallel for schedule(static,1) reduction(+:sum)
				for ( u64 j = 0; j < j_count; ++j )
				#else
				for_0M ( j, cardinality )
				#endif
				{
					f32 z = biases[j];
					for_0M ( k, prev_layer_cardinality )
					{
						f32 prev_a;
						prev_a = prev_layer_acts[k];
						z += weight_jk( j, k ) * prev_a;
						#if ! NN_OPENMP
						if ( isnan( z ) )
						{
							log_err( "z is nan weight_jk( %d, %d ) = %e prev_a = %e", s32(j), k, weight_jk( j, k ), prev_a );
							ILLEGAL_PATH();
						}
						#endif
					}
					//wx_b[j] = z;
					f32 a = 0;
					if constexpr ( ( t_flags & Flags::LINEAR ) != Flags::NONE )
					{
						a = z;
						dadz[j] = 1;
					} else
					if constexpr ( u32( t_flags & Flags::SIGMOID ) != 0 )
					{
						a = sigmoid( z );
						dadz[j] = a * ( 1.f - a ) * z;
					} else
					if constexpr ( u32( t_flags & Flags::RELU ) != 0 )
					{
						f32 k = z >= 0 ? 1 : relu_k;
						a = z * k;
						dadz[j] = k;
					} else
					if constexpr ( u32( t_flags & Flags::SOFTMAX ) != 0 )
					{
						a = exp( z );
						#if ! NN_OPENMP
						if ( isnan( a ) )
						{
							log_err( "a [%d] is nan", s32(j) );
							ILLEGAL_PATH();
						}
						#endif
						sum += a;
						//log_info( "sum = %e | a = %e", sum, a );
						dadz[j] = 1;
					} else
					{
						#if ! NN_OPENMP
						//static_assert( false );
						log_err( "no activation selected for layer" );
						ILLEGAL_PATH();
						#endif
					}

					#if ! NN_OPENMP
					if constexpr ( u32( t_flags & Flags::DROPOUT ) != 0 )
					{
						if constexpr( learning )
						{
							f32 r = randf_01( seeds );
							f32 d = r > dropout_p ? 0 : 1.f / dropout_p;
							a *= d;
						}
					}
					#endif

					activations[j] = a;
				}
				#if NN_OPENMP
				#undef jk_index
				#undef weight_jk
				#undef sigmoid
				#endif
				if constexpr ( u32( t_flags & Flags::SOFTMAX ) != 0 )
				{
					#if ! NN_OPENMP
					if ( isnan( sum ) )
					{
						log_err( "activations sum is nan" );
						ILLEGAL_PATH();
					}
					#endif
					//f32 oosum = 1.f / sum;
					if ( sum == 0 )
					{
						//sum = 1e-8;
						//log_err( "sum == 0 " );
						//ILLEGAL_PATH();
					} else
					{
						if ( sum == 0 || isinf( sum ) )
						{
							for_0M ( j, cardinality )
							{
								activations[j] = 1.0f / f32(cardinality);
							}
						} else
						for_0M ( j, cardinality )
						{
							activations[j] /= sum;
							//log_dbg( "a[%d] = %e", j, activations[j] );
							#if ! NN_OPENMP
							if ( isnan( activations[j] ) )
							{
								log_err( "activations[%d] is nan, sum = %e", s32(j), sum );
								ILLEGAL_PATH();
							}
							#endif
						}
					}
				}
			}
			inline
			void
			run( bool learning = false )
			{
				#define FLAG_COMBO(the_flags) \
					case u32(the_flags): \
						if ( learning ) run_<the_flags,true>(); else run_<the_flags,false>(); break;

				switch ( (u32)flags )
				{
					FLAG_COMBO( Flags::SIGMOID           )
					FLAG_COMBO( Flags::RELU              )
					FLAG_COMBO( Flags::LINEAR            )
					FLAG_COMBO( Flags::SOFTMAX           )
					FLAG_COMBO( Flags::SIGMOID | Flags::ADAM    )
					FLAG_COMBO( Flags::RELU    | Flags::ADAM    )
					FLAG_COMBO( Flags::LINEAR  | Flags::ADAM    )
					FLAG_COMBO( Flags::SOFTMAX | Flags::ADAM    )
					FLAG_COMBO( Flags::SIGMOID | Flags::L2      )
					FLAG_COMBO( Flags::RELU    | Flags::L2      )
					FLAG_COMBO( Flags::LINEAR  | Flags::L2      )
					FLAG_COMBO( Flags::SOFTMAX | Flags::L2      )
					FLAG_COMBO( Flags::SIGMOID | Flags::DROPOUT )
					FLAG_COMBO( Flags::RELU    | Flags::DROPOUT )
					FLAG_COMBO( Flags::LINEAR  | Flags::DROPOUT )
					FLAG_COMBO( Flags::SOFTMAX | Flags::DROPOUT )
					FLAG_COMBO( Flags::SIGMOID | Flags::ADAM | Flags::L2     )
					FLAG_COMBO( Flags::RELU    | Flags::ADAM | Flags::L2     )
					FLAG_COMBO( Flags::LINEAR  | Flags::ADAM | Flags::L2     )
					FLAG_COMBO( Flags::SOFTMAX | Flags::ADAM | Flags::L2     )
					FLAG_COMBO( Flags::SIGMOID | Flags::ADAM | Flags::DROPOUT)
					FLAG_COMBO( Flags::RELU    | Flags::ADAM | Flags::DROPOUT)
					FLAG_COMBO( Flags::LINEAR  | Flags::ADAM | Flags::DROPOUT)
					FLAG_COMBO( Flags::SOFTMAX | Flags::ADAM | Flags::DROPOUT)
					FLAG_COMBO( Flags::SIGMOID | Flags::ADAM | Flags::L2      | Flags::DROPOUT )
					FLAG_COMBO( Flags::RELU    | Flags::ADAM | Flags::L2      | Flags::DROPOUT )
					FLAG_COMBO( Flags::LINEAR  | Flags::ADAM | Flags::L2      | Flags::DROPOUT )
					FLAG_COMBO( Flags::SOFTMAX | Flags::ADAM | Flags::L2      | Flags::DROPOUT )
					FLAG_COMBO( Flags::SIGMOID | Flags::L2   | Flags::DROPOUT )
					FLAG_COMBO( Flags::RELU    | Flags::L2   | Flags::DROPOUT )
					FLAG_COMBO( Flags::LINEAR  | Flags::L2   | Flags::DROPOUT )
					FLAG_COMBO( Flags::SOFTMAX | Flags::L2   | Flags::DROPOUT )
					FLAG_COMBO( Flags::SIGMOID | Flags::MOMENTUM    )
					FLAG_COMBO( Flags::RELU    | Flags::MOMENTUM    )
					FLAG_COMBO( Flags::LINEAR  | Flags::MOMENTUM    )
					FLAG_COMBO( Flags::SOFTMAX | Flags::MOMENTUM    )
					FLAG_COMBO( Flags::SIGMOID | Flags::MOMENTUM | Flags::L2     )
					FLAG_COMBO( Flags::RELU    | Flags::MOMENTUM | Flags::L2     )
					FLAG_COMBO( Flags::LINEAR  | Flags::MOMENTUM | Flags::L2     )
					FLAG_COMBO( Flags::SOFTMAX | Flags::MOMENTUM | Flags::L2     )
					FLAG_COMBO( Flags::SIGMOID | Flags::MOMENTUM | Flags::DROPOUT)
					FLAG_COMBO( Flags::RELU    | Flags::MOMENTUM | Flags::DROPOUT)
					FLAG_COMBO( Flags::LINEAR  | Flags::MOMENTUM | Flags::DROPOUT)
					FLAG_COMBO( Flags::SOFTMAX | Flags::MOMENTUM | Flags::DROPOUT)
					FLAG_COMBO( Flags::SIGMOID | Flags::MOMENTUM | Flags::L2      | Flags::DROPOUT )
					FLAG_COMBO( Flags::RELU    | Flags::MOMENTUM | Flags::L2      | Flags::DROPOUT )
					FLAG_COMBO( Flags::LINEAR  | Flags::MOMENTUM | Flags::L2      | Flags::DROPOUT )
					FLAG_COMBO( Flags::SOFTMAX | Flags::MOMENTUM | Flags::L2      | Flags::DROPOUT )
					default:
					{
						log_err( "something is wrong with flags %x", (u32)flags );
						ILLEGAL_PATH();
					}
				}
				#undef FLAG_COMBO
			}

			#if 0
			inline
			void
			run()
			{
				if ( ! prev_layer )
				{
					return;
				}

				#if PNN_BV
				u8  * prev_layer_acts_bv = prev_layer->activations_bv;
				#else
				f32 * prev_layer_acts    = prev_layer->activations;
				#endif
				for ( s32 j = 0; j != cardinality; ++j )
				{
					f32 z = biases[j];
					for ( s32 k = 0; k != prev_layer_cardinality; ++k )
					{
						f32 prev_a;
						#if PNN_BV
						u32 activation_bit = BITTEST1( prev_layer_acts_bv, k );
						prev_a = activation_bit;
						#else
						prev_a = prev_layer_acts[k];
						#endif
						z += weight_jk( j, k ) * prev_a;
					}
					//wx_b[j] = z;
					#if PNN_LAST_LAYER_AF_LINEAR
					f32 a = next_layer ? activation_function( z ) : z;
					#else
					f32 a = activation_function( z );
					#endif
					activations[j] = a;
					//a_wx_b[j] = a;
					#if PNN_LAST_LAYER_AF_LINEAR
					dadz[j] = next_layer ? a * ( 1.f - a ) * z : 1;
					#else
					dadz[j] = a * ( 1.f - a ) * z;
					#endif
					#if PNN_BV
					bool activated = activation( a );
					if ( activated )
					{
						BITSET( activations_bv, j );
					} else
					{
						BITCLEAR( activations_bv, j );
					}
					#endif
				}
			}
			#endif

			inline
			void
			running_avg( f32 * sum, f32 * new_v, s32 count )
			{
				f32 new_ic = iterations_counter + 1;
				f32 oonew = 1.f / new_ic;
				f32 di = iterations_counter * oonew;
				for ( s32 j = 0; j != count; ++j )
				{
					f32 v = new_v[j];
					f32 avg = sum[j];
					sum[j] = avg * di + v * oonew;
					//log_dbg( "avg = %e di = %e v = %e oonew = %e sum[j] = %e", avg, di, v, oonew, sum[j] );
				}
			}
			//inline
			//void
			//sum_dCda()
			//{
			//    running_avg( dCda_running_avg, dCda, cardinality );
			//}
			inline
			void
			sum_dCdw()
			{
				running_avg( dCdw_running_avg, dCdw, prev_layer_cardinality * cardinality );
			}
			inline
			void
			sum_dCdb()
			{
				running_avg( dCdb_running_avg, dCdb, cardinality );
			}

			inline
			void
			compute_error( f32 * truth )
			{
				//log_dbg( "compute_error" );
				for ( s32 j = 0; j != cardinality; ++j )
				{
					f32 t = truth[j];
					f32 o = activations[j];
					//dCda[j] = abs( t - o );
					dCda[j] = o - t;
					//log_dbg( "output[%d] = %e", j, o );
					//log_dbg( "truth[%d] = %e", j, t );
					//log_dbg( "dCda[%d] = %e", j, dCda[j] );
				}
			}

			inline
			void
			compute_error( u8 * truth )
			{
				for ( s32 j = 0; j != cardinality; ++j )
				{
					f32 t = f32(BITTEST1( truth, j )) ? 1 : -1;
					f32 o = activations[j];
					//dCda[j] = abs( t - o );
					dCda[j] = o - t;
				}
			}
			inline
			void
			compute_dCdw_dCdb()
			{
				//log_dbg( "compute_dCdw_dCdb" );
				for ( s32 j = 0; j != cardinality; ++j )
				{
					dCdb[j] = dadz[j] * dCda[j];
					//log_dbg( "b[%d] = %e", j, biases[j] );
					//log_dbg( "dadz[%d] = %e", j, dadz[j] );
					//log_dbg( "dCda[%d] = %e", j, dCda[j] );
					//log_dbg( "dCdb[%d] = %e", j, dCdb[j] );
					for ( s32 k = 0; k != prev_layer_cardinality; ++k )
					{
						f32 ak;
						ak = prev_layer->activations[k];
						//log_dbg( "weight_jk[%d][%d] = %e", j, k, weight_jk( j, k ) );
						//log_dbg( "ak [%d] = %e", k, ak );
						dCdw_jk( j, k ) = ak * dCdb[j];
						//log_dbg( "dCdw_jk[%d][%d] = %e", j, k, dCdw_jk(j,k) );
					}
				}
			}
			inline
			void
			compute_dCda( f32 * truth )
			{
				if ( ! next_layer )
				{
					compute_error( truth );
				} else
				{
					compute_dCda_internal_layer();
				}
			}
			inline
			void
			compute_dCda( u8 * truth )
			{
				if ( ! next_layer )
				{
					compute_error( truth );
				} else
				{
					compute_dCda_internal_layer();
				}
			}
			inline
			void
			compute_dCda_internal_layer()
			{
				//log_dbg( "compute_dCda_internal_layer" );
				for_0M( j, cardinality )
				{
					f32 dCda_j = 0;
					for_0M( jj, next_layer->cardinality )
					{
						dCda_j += next_layer->weight_jk( jj, j ) * next_layer->dadz[jj] * next_layer->dCda[jj];
					}
					dCda[j] = dCda_j;
					//log_dbg( "dCda[%d] = %e", j, dCda_j );
				}
			}

			inline
			void
			compute_gradient( f32 * truth )
			{
				compute_dCda( truth );
				compute_dCdw_dCdb();

				//sum_dCda();
				sum_dCdw();
				sum_dCdb();

				++iterations_counter;
			}

			inline
			void
			compute_gradient( u8 * truth )
			{
				compute_dCda( truth );
				compute_dCdw_dCdb();

				//sum_dCda();
				sum_dCdw();
				sum_dCdb();

				++iterations_counter;
			}

			template<Flags t_flags = Flags::SIGMOID>
			void
			apply_backprop_( f32 rate )
			{
				//log_dbg( "prev_layer = %p prev_layer_cardinality = %d", prev_layer, prev_layer_cardinality );
				if ( ! prev_layer )
				{
					return;
				}

				for ( s32 j = 0; j != cardinality; ++j )
				{
					for ( s32 k = 0; k != prev_layer_cardinality; ++k )
					{
						f32 dCdw_jk_ = dCdw_avg_jk( j, k );

						f32 starting_w;
						if constexpr ( u32( t_flags & Flags::L2 ) != 0 )
						{
							starting_w = weight_jk( j, k );
						}

						//log_dbg( "[%d][%d] dCdw_avg_jk_ %e dCdw_jk %e", j, k, dCdw_jk_, dCdw_jk(j,k) );

						if constexpr ( u32( t_flags & Flags::ADAM ) != 0 )
						{
							s32 adam_t = minibatch_counter + 1;
							adam_m_jk( j, k ) = adam_beta1 * adam_m_jk( j, k ) + ( 1 - adam_beta1 ) * dCdw_jk_;
							adam_mt_jk( j, k ) = adam_m_jk( j, k ) / ( 1 - powf( adam_beta1, adam_t ) );
							adam_v_jk( j, k ) = adam_beta2 * adam_v_jk( j, k ) + ( 1 - adam_beta2 ) * ( dCdw_jk_ * dCdw_jk_ );
							adam_vt_jk( j, k ) = adam_v_jk( j, k ) / ( 1 - powf( adam_beta2, adam_t ) );
							//log_dbg( "[%2d][%2d] weight_jk %e adam_mt_jk %e adam_vt_jk %e", j, k,
							//         weight_jk( j, k ),
							//         adam_mt_jk( j, k ),
							//         adam_vt_jk( j, k ) );
							f32 adam_factor = adam_mt_jk( j, k ) / ( sqrtf( adam_vt_jk( j, k ) ) + adam_eps );
							//log_dbg( "adam_factor = %6e", adam_factor );
							weight_jk( j, k ) += - rate * adam_factor;
						} else
						if constexpr ( u32( t_flags & Flags::MOMENTUM ) != 0 )
						{
							momentum_v_jk( j, k ) = momentum_mu * momentum_v_jk( j, k ) - rate * dCdw_jk_;
							weight_jk( j, k ) += momentum_v_jk( j, k );
						} else
						{
							//log_dbg( "weight_jk( %d, %d ) = %e", j, k, weight_jk( j, k ) );
							weight_jk( j, k ) += - dCdw_jk_ * rate;
						}

						if constexpr ( u32( t_flags & Flags::L2 ) != 0 )
						{
							weight_jk( j, k ) += - l2 * starting_w;
						}

					}
					if constexpr ( u32( t_flags & Flags::L2 ) != 0 )
					{
						biases[j] += - dCdb_running_avg[j] * rate - l2 * biases[j];
					} else
					{
						biases[j] += - dCdb_running_avg[j] * rate;
					}
				}

				#if 0
				//memset( wx_b            , 0x0, wx_b_bytes_size()             );
				//memset( a_wx_b          , 0x0, a_wx_b_bytes_size()           );
				memset( dadz            , 0x0, dadz_bytes_size()             );
				memset( dCdb            , 0x0, dCdb_bytes_size()             );
				memset( dCdb_running_avg, 0x0, dCdb_running_avg_bytes_size() );
				memset( dCda            , 0x0, dCda_bytes_size()             );
				//memset( dCda_running_avg, 0x0, dCda_running_avg_bytes_size() );
				memset( dCdw            , 0x0, dCdw_bytes_size()             );
				memset( dCdw_running_avg, 0x0, dCdw_running_avg_bytes_size() );
				//memset( adam_m          , 0x0, adam_m_bytes_size() );
				//memset( adam_v          , 0x0, adam_v_bytes_size() );
				//memset( adam_mt         , 0x0, adam_mt_bytes_size() );
				//memset( adam_vt         , 0x0, adam_vt_bytes_size() );
				#else
				backpropagated = true;
				#endif

				iterations_counter = 0;
				++minibatch_counter;
			}
			inline
			void
			apply_backprop( f32 rate )
			{
				#define FLAG_COMBO(the_flags) case u32(the_flags): apply_backprop_<the_flags>( rate ); break;

				switch ( (u32)flags )
				{
					FLAG_COMBO( Flags::SIGMOID           )
					FLAG_COMBO( Flags::RELU              )
					FLAG_COMBO( Flags::LINEAR            )
					FLAG_COMBO( Flags::SOFTMAX           )
					FLAG_COMBO( Flags::SIGMOID | Flags::ADAM    )
					FLAG_COMBO( Flags::RELU    | Flags::ADAM    )
					FLAG_COMBO( Flags::LINEAR  | Flags::ADAM    )
					FLAG_COMBO( Flags::SOFTMAX | Flags::ADAM    )
					FLAG_COMBO( Flags::SIGMOID | Flags::L2      )
					FLAG_COMBO( Flags::RELU    | Flags::L2      )
					FLAG_COMBO( Flags::LINEAR  | Flags::L2      )
					FLAG_COMBO( Flags::SOFTMAX | Flags::L2      )
					FLAG_COMBO( Flags::SIGMOID | Flags::DROPOUT )
					FLAG_COMBO( Flags::RELU    | Flags::DROPOUT )
					FLAG_COMBO( Flags::LINEAR  | Flags::DROPOUT )
					FLAG_COMBO( Flags::SOFTMAX | Flags::DROPOUT )
					FLAG_COMBO( Flags::SIGMOID | Flags::ADAM | Flags::L2     )
					FLAG_COMBO( Flags::RELU    | Flags::ADAM | Flags::L2     )
					FLAG_COMBO( Flags::LINEAR  | Flags::ADAM | Flags::L2     )
					FLAG_COMBO( Flags::SOFTMAX | Flags::ADAM | Flags::L2     )
					FLAG_COMBO( Flags::SIGMOID | Flags::ADAM | Flags::DROPOUT)
					FLAG_COMBO( Flags::RELU    | Flags::ADAM | Flags::DROPOUT)
					FLAG_COMBO( Flags::LINEAR  | Flags::ADAM | Flags::DROPOUT)
					FLAG_COMBO( Flags::SOFTMAX | Flags::ADAM | Flags::DROPOUT)
					FLAG_COMBO( Flags::SIGMOID | Flags::ADAM | Flags::L2      | Flags::DROPOUT )
					FLAG_COMBO( Flags::RELU    | Flags::ADAM | Flags::L2      | Flags::DROPOUT )
					FLAG_COMBO( Flags::LINEAR  | Flags::ADAM | Flags::L2      | Flags::DROPOUT )
					FLAG_COMBO( Flags::SOFTMAX | Flags::ADAM | Flags::L2      | Flags::DROPOUT )
					FLAG_COMBO( Flags::SIGMOID | Flags::L2   | Flags::DROPOUT )
					FLAG_COMBO( Flags::RELU    | Flags::L2   | Flags::DROPOUT )
					FLAG_COMBO( Flags::LINEAR  | Flags::L2   | Flags::DROPOUT )
					FLAG_COMBO( Flags::SOFTMAX | Flags::L2   | Flags::DROPOUT )
					FLAG_COMBO( Flags::SIGMOID | Flags::MOMENTUM    )
					FLAG_COMBO( Flags::RELU    | Flags::MOMENTUM    )
					FLAG_COMBO( Flags::LINEAR  | Flags::MOMENTUM    )
					FLAG_COMBO( Flags::SOFTMAX | Flags::MOMENTUM    )
					FLAG_COMBO( Flags::SIGMOID | Flags::MOMENTUM | Flags::L2     )
					FLAG_COMBO( Flags::RELU    | Flags::MOMENTUM | Flags::L2     )
					FLAG_COMBO( Flags::LINEAR  | Flags::MOMENTUM | Flags::L2     )
					FLAG_COMBO( Flags::SOFTMAX | Flags::MOMENTUM | Flags::L2     )
					FLAG_COMBO( Flags::SIGMOID | Flags::MOMENTUM | Flags::DROPOUT)
					FLAG_COMBO( Flags::RELU    | Flags::MOMENTUM | Flags::DROPOUT)
					FLAG_COMBO( Flags::LINEAR  | Flags::MOMENTUM | Flags::DROPOUT)
					FLAG_COMBO( Flags::SOFTMAX | Flags::MOMENTUM | Flags::DROPOUT)
					FLAG_COMBO( Flags::SIGMOID | Flags::MOMENTUM | Flags::L2      | Flags::DROPOUT )
					FLAG_COMBO( Flags::RELU    | Flags::MOMENTUM | Flags::L2      | Flags::DROPOUT )
					FLAG_COMBO( Flags::LINEAR  | Flags::MOMENTUM | Flags::L2      | Flags::DROPOUT )
					FLAG_COMBO( Flags::SOFTMAX | Flags::MOMENTUM | Flags::L2      | Flags::DROPOUT )
					default:
					{
						log_err( "something is wrong with flags %x", (u32)flags );
						ILLEGAL_PATH();
					}
				}
				#undef FLAG_COMBO
			}

			template<Flags t_flags = Flags::SIGMOID>
			void
			apply_backprop_to_input_( f32 rate )
			{
				//log_dbg( "prev_layer = %p prev_layer_cardinality = %d", prev_layer, prev_layer_cardinality );
				if ( ! next_layer )
				{
					log_err( "applying backprop to layer without next layer" );
					return;
				}
				if ( prev_layer )
				{
					log_err( "should apply backprop to input only in first layer" );
				}

				for_0M( j, cardinality )
				{
					f32 g = dCda[j];

					f32 starting_a;
					if constexpr ( u32( t_flags & Flags::L2 ) != 0 )
					{
						starting_a = activations[j];
					}

					//log_dbg( "[%d][%d] dCdw_avg_jk_ %e dCdw_jk %e", j, k, dCdw_jk_, dCdw_jk(j,k) );

					if constexpr ( u32( t_flags & Flags::ADAM ) != 0 )
					{
						s32 k = j;
						s32 adam_j = 0;
						s32 adam_t = minibatch_counter + 1;
						adam_m_jk( adam_j, k ) = adam_beta1 * adam_m_jk( adam_j, k ) + ( 1 - adam_beta1 ) * g;
						adam_mt_jk( adam_j, k ) = adam_m_jk( adam_j, k ) / ( 1 - powf( adam_beta1, adam_t ) );
						adam_v_jk( adam_j, k ) = adam_beta2 * adam_v_jk( adam_j, k ) + ( 1 - adam_beta2 ) * ( g * g );
						adam_vt_jk( adam_j, k ) = adam_v_jk( adam_j, k ) / ( 1 - powf( adam_beta2, adam_t ) );
						f32 adam_factor = adam_mt_jk( adam_j, k ) / ( sqrtf( adam_vt_jk( adam_j, k ) ) + adam_eps );
						activations[j] += - rate * adam_factor;
					} else
					if constexpr ( u32( t_flags & Flags::MOMENTUM ) != 0 )
					{
						s32 k = j;
						s32 m_j = 0;
						momentum_v_jk( m_j, k ) = momentum_mu * momentum_v_jk( m_j, k ) - rate * g;
						weight_jk( m_j, k ) += momentum_v_jk( j, k );
					} else
					{
						//log_dbg( "weight_jk( %d, %d ) = %e", j, k, weight_jk( j, k ) );
						activations[j] += - g * rate;
					}

					if constexpr ( u32( t_flags & Flags::L2 ) != 0 )
					{
						activations[j] += - l2 * starting_a;
					}
				}

				#if 0
				memset( dadz            , 0x0, dadz_bytes_size()             );
				memset( dCdb            , 0x0, dCdb_bytes_size()             );
				memset( dCdb_running_avg, 0x0, dCdb_running_avg_bytes_size() );
				memset( dCda            , 0x0, dCda_bytes_size()             );
				memset( dCdw            , 0x0, dCdw_bytes_size()             );
				memset( dCdw_running_avg, 0x0, dCdw_running_avg_bytes_size() );
				#else
				backpropagated = true;
				#endif

				iterations_counter = 0;
				++minibatch_counter;
			}
			inline
			void
			apply_backprop_to_input( f32 rate )
			{
				#define FLAG_COMBO(the_flags) case u32(the_flags): apply_backprop_to_input_<the_flags>( rate ); break;

				switch ( (u32)flags )
				{
					FLAG_COMBO( Flags::SIGMOID           )
					FLAG_COMBO( Flags::RELU              )
					FLAG_COMBO( Flags::LINEAR            )
					FLAG_COMBO( Flags::SOFTMAX           )
					FLAG_COMBO( Flags::SIGMOID | Flags::ADAM    )
					FLAG_COMBO( Flags::RELU    | Flags::ADAM    )
					FLAG_COMBO( Flags::LINEAR  | Flags::ADAM    )
					FLAG_COMBO( Flags::SOFTMAX | Flags::ADAM    )
					FLAG_COMBO( Flags::SIGMOID | Flags::L2      )
					FLAG_COMBO( Flags::RELU    | Flags::L2      )
					FLAG_COMBO( Flags::LINEAR  | Flags::L2      )
					FLAG_COMBO( Flags::SOFTMAX | Flags::L2      )
					FLAG_COMBO( Flags::SIGMOID | Flags::DROPOUT )
					FLAG_COMBO( Flags::RELU    | Flags::DROPOUT )
					FLAG_COMBO( Flags::LINEAR  | Flags::DROPOUT )
					FLAG_COMBO( Flags::SOFTMAX | Flags::DROPOUT )
					FLAG_COMBO( Flags::SIGMOID | Flags::ADAM | Flags::L2     )
					FLAG_COMBO( Flags::RELU    | Flags::ADAM | Flags::L2     )
					FLAG_COMBO( Flags::LINEAR  | Flags::ADAM | Flags::L2     )
					FLAG_COMBO( Flags::SOFTMAX | Flags::ADAM | Flags::L2     )
					FLAG_COMBO( Flags::SIGMOID | Flags::ADAM | Flags::DROPOUT)
					FLAG_COMBO( Flags::RELU    | Flags::ADAM | Flags::DROPOUT)
					FLAG_COMBO( Flags::LINEAR  | Flags::ADAM | Flags::DROPOUT)
					FLAG_COMBO( Flags::SOFTMAX | Flags::ADAM | Flags::DROPOUT)
					FLAG_COMBO( Flags::SIGMOID | Flags::ADAM | Flags::L2      | Flags::DROPOUT )
					FLAG_COMBO( Flags::RELU    | Flags::ADAM | Flags::L2      | Flags::DROPOUT )
					FLAG_COMBO( Flags::LINEAR  | Flags::ADAM | Flags::L2      | Flags::DROPOUT )
					FLAG_COMBO( Flags::SOFTMAX | Flags::ADAM | Flags::L2      | Flags::DROPOUT )
					FLAG_COMBO( Flags::SIGMOID | Flags::L2   | Flags::DROPOUT )
					FLAG_COMBO( Flags::RELU    | Flags::L2   | Flags::DROPOUT )
					FLAG_COMBO( Flags::LINEAR  | Flags::L2   | Flags::DROPOUT )
					FLAG_COMBO( Flags::SOFTMAX | Flags::L2   | Flags::DROPOUT )
					default:
					{
						log_err( "something is wrong with flags %x", (u32)flags );
						ILLEGAL_PATH();
					}
				}
				#undef FLAG_COMBO
			}

			inline
			void
			clean_gradients_buffers()
			{
				memset( dadz            , 0x0, dadz_bytes_size()             );
				memset( dCdb            , 0x0, dCdb_bytes_size()             );
				memset( dCdb_running_avg, 0x0, dCdb_running_avg_bytes_size() );
				memset( dCda            , 0x0, dCda_bytes_size()             );
				memset( dCdw            , 0x0, dCdw_bytes_size()             );
				memset( dCdw_running_avg, 0x0, dCdw_running_avg_bytes_size() );
			}

			inline
			void
			clip_weights( f32 clip_l )
			{
				f32 clip_ll = clip_l * clip_l;
				for_0M( j, cardinality )
				{
					f32 ll = 0;
					for_0M( k, prev_layer_cardinality )
					{
						f32 w = weight_jk( j, k );
						ll += w * w;
					}
					if ( ll > clip_ll )
					{
						f32 l = sqrt( ll );
						f32 clol = clip_l / l;
						for_M0( k, prev_layer_cardinality )
						{
							weight_jk( j, k ) *= clol;
						}
					}
				}
			}
			inline
			void
			rescale_weights( f32 target_l )
			{
				for_0M( j, cardinality )
				{
					f32 ll = 0;
					for_0M( k, prev_layer_cardinality )
					{
						f32 w = weight_jk( j, k );
						ll += w * w;
					}
					f32 l = sqrt( ll );
					f32 tlol = target_l / l;
					for_M0( k, prev_layer_cardinality )
					{
						weight_jk( j, k ) *= tlol;
					}
				}
			}

			inline
			f32
			dCdw_avg_frobenius_norm()
			{
				f32 ret = 0;
				for_0M( j, cardinality )
				{
					for_0M( k, prev_layer_cardinality )
					{
						f32 g = dCdw_avg_jk( j, k );
						ret += g * g;
					}
				}
				ret = sqrtf( ret );
				return ret;
			}
			inline
			f32
			dCdw_frobenius_norm()
			{
				f32 ret = 0;
				for_0M( j, cardinality )
				{
					for_0M( k, prev_layer_cardinality )
					{
						f32 g = dCdw_jk( j, k );
						ret += g * g;
					}
				}
				ret = sqrtf( ret );
				return ret;
			}
			inline
			f32
			weights_frobenius_norm()
			{
				f32 ret = 0;
				for_0M( j, cardinality )
				{
					for_0M( k, prev_layer_cardinality )
					{
						f32 w = weight_jk( j, k );
						ret += w * w;
					}
				}
				ret = sqrtf( ret );
				return ret;
			}
			inline
			f32
			weights_deltas_sum()
			{
				f32 ret = 0;
				for_0M( j, cardinality )
				{
					for_0M( k, prev_layer_cardinality )
					{
						f32 w = weight_jk( j, k );
						f32 pw = prev_epoch_weight_jk( j, k );
						ret += abs( w - pw );
					}
				}
				return ret;
			}
			inline
			void
			rec_weights_as_prev()
			{
				memcpy( prev_epoch_weights, weights, weights_bytes_size() );
			}

			inline
			bool
			write_weights( FILE * f )
			{
				bool ret = true;
				if ( ! prev_layer )
				{
					return ret;
				}
				s32 s32_minibatch_counter = minibatch_counter;
				ret = tg_io_write_one( f, &s32_minibatch_counter ); if ( ! ret ) return ret;
				s32 s32_cardinality = cardinality;
				ret = tg_io_write_one( f, &s32_cardinality ); if ( ! ret ) return ret;
				ret = tg_io_write_one( f, &prev_layer_cardinality ); if ( ! ret ) return ret;
				for_0M ( j, cardinality )
				{
					for_0M ( k, prev_layer_cardinality )
					{
						ret = tg_io_write_one( f, &weight_jk( j, k ) ); if ( ! ret ) return ret;
						ret = tg_io_write_one( f, &adam_m_jk( j, k ) ); if ( ! ret ) return ret;
						ret = tg_io_write_one( f, &adam_mt_jk( j, k ) ); if ( ! ret ) return ret;
						ret = tg_io_write_one( f, &adam_v_jk( j, k ) ); if ( ! ret ) return ret;
						ret = tg_io_write_one( f, &adam_vt_jk( j, k ) ); if ( ! ret ) return ret;
						ret = tg_io_write_one( f, &momentum_v_jk( j, k ) ); if ( ! ret ) return ret;
					}
					ret = tg_io_write_one( f, &biases[j] ); if ( ! ret ) return ret;
				}
				return ret;
			}
			inline
			bool
			read_weights( FILE * f )
			{
				bool ret = false;
				if ( ! prev_layer )
				{
					ret = true;
					return ret;
				}
				s32 s32_minibatch_counter = minibatch_counter;
				ret = tg_io_read_one( f, &s32_minibatch_counter ); if ( ! ret ) return ret;
				minibatch_counter = s32_minibatch_counter;
				s32 read_cardinality;
				s32 read_prev_layer_cardinality;
				ret = tg_io_read_one( f, &read_cardinality ); if ( ! ret ) return ret;
				ret = tg_io_read_one( f, &read_prev_layer_cardinality ); if ( ! ret ) return ret;
				if ( read_cardinality != cardinality
				  || read_prev_layer_cardinality != prev_layer_cardinality )
				{
					return ret;
				}
				for_0M ( j, cardinality )
				{
					for_0M ( k, prev_layer_cardinality )
					{
						ret = tg_io_read_one( f, &weight_jk( j, k ) ); if ( ! ret ) return ret;
						ret = tg_io_read_one( f, &adam_m_jk( j, k ) ); if ( ! ret ) return ret;
						ret = tg_io_read_one( f, &adam_mt_jk( j, k ) ); if ( ! ret ) return ret;
						ret = tg_io_read_one( f, &adam_v_jk( j, k ) ); if ( ! ret ) return ret;
						ret = tg_io_read_one( f, &adam_vt_jk( j, k ) ); if ( ! ret ) return ret;
						ret = tg_io_read_one( f, &momentum_v_jk( j, k ) ); if ( ! ret ) return ret;
					}
					ret = tg_io_read_one( f, &biases[j] ); if ( ! ret ) return ret;
				}
				ret = true;
				return ret;
			}
			inline
			void
			print_weights( FILE * f )
			{
				if ( ! prev_layer )
				{
					return;
				}
				fprintf( f, "minibatch_counter %d\n", (s32)minibatch_counter );
				for_0M ( j, cardinality )
				{
					for_0M ( k, prev_layer_cardinality )
					{
						f32 w = weight_jk( j, k );
						fprintf( f, "    %2d %2d %26.23e adam_m %26.23e adam_mt %26.23e adam_v %26.23e adam_vt %26.23e\n",
						            j, k, w, adam_m_jk( j, k ), adam_mt_jk( j, k ), adam_v_jk( j, k ), adam_vt_jk( j, k ) );
					}
					fprintf( f, "\n" );
					fprintf( f, "    %2d %26.23e\n\n", j, biases[j] );
				}
			}

			#if 0
			inline
			void
			incentivize( f32 rate, f32 lambda = PNN_DEFAULT_L2_LAMBDA )
			{
				if ( ! prev_layer )
				{
					return;
				}

				u8 * prev_layer_acts_bv = prev_layer->activations_bv;
				for ( s32 j = 0; j != cardinality; ++j )
				{
					f32 s = a_wx_b[j];//sigmoid( wx_b[j] );
					f32 ddot = s * ( 1 - s );

					f32 v = biases[j];
					for ( s32 k = 0; k != prev_layer_cardinality; ++k )
					{
						f32 dw;
						u32 activation = BITTEST1( prev_layer_acts_bv, k );
						#if 0
						f32 wx = weight_jk( j, k ) * activation;
						v += wx;
						//f32 d = dw( activation, j, k );
						//log_info( "wx = %f bias = %f", wx, biases[j] );
						f32 s = sigmoid( wx + biases[j] ) * activation;
						f32 d = s * ( 1.f - s );
						dw = d * rate;
						f32 w = weight_jk( j, k );
						//if ( w && dw ) log_info( "[%d][%d] w:%e dw:%e pc:%e", j, k, w, dw, w?dw/w:0 );
						#else
						dw = ddot * activation;
						#endif
						weight_jk( j, k ) += dw - lambda * weight_jk( j, k );
					}
					//f32 s = sigmoid ( v );
					//f32 d = s * ( 1.f - s );
					biases[j] += ddot * rate;
				}
			}
			#endif

			inline
			bool
			activation( f32 p )
			{
				f32 r = randf_01( seeds );
				bool ret = r && r <= p;
				return ret;
			}

			inline
			f32
			activation_function( f32 p )
			{
				f32 ret = sigmoid( p );
				return ret;
			}

			inline
			s32
			activations_bv_bytes_size()
			{
				s32 slots_count = BITNSLOTS(cardinality);
				s32 ret = slots_count * sizeof ( u8 );
				return ret;
			}
			inline
			s32
			activations_bytes_size()
			{
				s32 ret = cardinality * sizeof ( f32 );
				return ret;
			}
			inline
			s32
			weights_bytes_size()
			{
				s32 ret = prev_layer_cardinality * cardinality * sizeof ( f32 );
				return ret;
			}
			inline
			s32
			prev_epoch_weights_bytes_size()
			{
				s32 ret = weights_bytes_size();
				return ret;
			}
			inline
			s32
			biases_bytes_size()
			{
				s32 ret = cardinality * sizeof ( f32 );
				return ret;
			}
			inline
			s32
			wx_b_bytes_size()
			{
				s32 ret = cardinality * sizeof ( f32 );
				return ret;
			}
			inline
			s32
			a_wx_b_bytes_size()
			{
				s32 ret = wx_b_bytes_size();
				return ret;
			}
			inline
			s32
			dadz_bytes_size()
			{
				s32 ret = wx_b_bytes_size();
				return ret;
			}
			inline
			s32
			dCdb_bytes_size()
			{
				s32 ret = cardinality * sizeof ( f32 );
				return ret;
			}
			inline
			s32
			dCdb_running_avg_bytes_size()
			{
				s32 ret = cardinality * sizeof ( f32 );
				return ret;
			}
			inline
			s32
			dCda_bytes_size()
			{
				s32 ret = cardinality * sizeof ( f32 );
				return ret;
			}
			inline
			s32
			dCda_running_avg_bytes_size()
			{
				s32 ret = dCda_bytes_size();
				return ret;
			}
			inline
			s32
			dCdw_bytes_size()
			{
				s32 ret = weights_bytes_size();
				return ret;
			}
			inline
			s32
			dCdw_running_avg_bytes_size()
			{
				s32 ret = weights_bytes_size();
				return ret;
			}

			inline
			s32
			adam_m_bytes_size()
			{
				return weights_bytes_size();
			}
			inline
			s32
			adam_v_bytes_size()
			{
				return weights_bytes_size();
			}
			inline
			s32
			adam_mt_bytes_size()
			{
				return weights_bytes_size();
			}
			inline
			s32
			adam_vt_bytes_size()
			{
				return weights_bytes_size();
			}
			inline
			s32
			momentum_v_bytes_size()
			{
				s32 ret = prev_layer_cardinality * cardinality * sizeof ( f32 );
				return ret;
			}

			//#pragma omp declare target
			inline
			s32
			jk_index( s32 j, s32 k )
			{
				s32 ret = j * prev_layer_cardinality + k;
				return ret;
			}
			inline
			f32&
			weight_jk( s32 j, s32 k )
			{
				f32 & ret = weights[jk_index( j, k )];
				return ret;
			}
			//#pragma omp end declare target
			inline
			f32&
			prev_epoch_weight_jk( s32 j, s32 k )
			{
				f32 & ret = prev_epoch_weights[jk_index( j, k )];
				return ret;
			}

			inline
			f32&
			dCdw_jk( s32 j, s32 k )
			{
				f32 & ret = dCdw[jk_index( j, k )];
				return ret;
			}
			inline
			f32&
			dCdw_avg_jk( s32 j, s32 k )
			{
				f32 & ret = dCdw_running_avg[jk_index( j, k )];
				return ret;
			}

			inline
			f32&
			adam_m_jk( s32 j, s32 k )
			{
				f32 & ret = adam_m[jk_index( j, k )];
				return ret;
			}
			inline
			f32&
			adam_mt_jk( s32 j, s32 k )
			{
				f32 & ret = adam_mt[jk_index( j, k )];
				return ret;
			}
			inline
			f32&
			adam_v_jk( s32 j, s32 k )
			{
				f32 & ret = adam_v[jk_index( j, k )];
				return ret;
			}
			inline
			f32&
			adam_vt_jk( s32 j, s32 k )
			{
				f32 & ret = adam_vt[jk_index( j, k )];
				return ret;
			}
			inline
			f32&
			momentum_v_jk( s32 j, s32 k )
			{
				f32 & ret = momentum_v[jk_index( j, k )];
				return ret;
			}


			inline
			void
			destroy()
			{
				if ( weights )
				{
					sys_mem_free( weights, weights_bytes_size() );
				}
				if ( biases )
				{
					sys_mem_free( biases, biases_bytes_size() );
				}
				//if ( wx_b )
				//{
				//    sys_mem_free( wx_b, wx_b_bytes_size() );
				//}
				//if ( a_wx_b )
				//{
				//    sys_mem_free( a_wx_b, a_wx_b_bytes_size() );
				//}
				if ( dCda )
				{
					sys_mem_free( dCda, dCda_bytes_size() );
				}
				//if ( dCda_running_avg )
				//{
				//    sys_mem_free( dCda_running_avg, dCda_running_avg_bytes_size() );
				//}
				if ( dCdw )
				{
					sys_mem_free( dCdw, dCdw_bytes_size() );
				}
				if ( dCdw_running_avg )
				{
					sys_mem_free( dCdw_running_avg, dCdw_running_avg_bytes_size() );
				}
				if ( adam_m )
				{
					sys_mem_free( adam_m, adam_m_bytes_size() );
				}
				if ( adam_v )
				{
					sys_mem_free( adam_v, adam_v_bytes_size() );
				}
				if ( adam_mt )
				{
					sys_mem_free( adam_mt, adam_mt_bytes_size() );
				}
				if ( adam_vt )
				{
					sys_mem_free( adam_vt, adam_vt_bytes_size() );
				}
				if ( momentum_v )
				{
					sys_mem_free( momentum_v, momentum_v_bytes_size() );
				}
			}
		};

		Stack_Generic_VM layers;

		inline
		bool
		init( std::initializer_list<Layer_Parameters> layers_parameters )
		{
			bool ret = false;
			s32 count = layers_parameters.size();
			log_info( "%d layers", count );

			layers.init( sizeof( Layer ), count, count );

			//s32 prev_layer_cardinality = 0;
			Layer * prev_layer = 0;
			s32 li = 0;
			for ( Layer_Parameters lp : layers_parameters )
			{
				log_info( "[%3d] %3d", li, lp.cardinality );
				++li;
				Layer l = {};
				bool b_res =
				   l.init( prev_layer, lp );//layers_cardinality[i] );
				if ( ! b_res )
				{
					layers.destroy();
					return ret;
				}
				layers.push( &l );
				Layer * last_layer = (Layer*)layers.peek();
				if ( prev_layer )
				{
					prev_layer->next_layer = last_layer;
				}
				prev_layer = last_layer;
			}
			ret = true;

			return ret;
		}

		inline
		void
		run( u8 * input, s32 * out_bytes_size, f32 ** output_f32, bool learning = false )
		{
			Layer * l = layer( 0 );
			for ( s32 i = 0; i != l->cardinality; ++i )
			{
				l->activations[i] = BITTEST1(input,i) * 2 - 1;
			}

			for ( s32 i = 1; i < (s32)layers.occupancy; ++i )
			{
				layer( i )->run( learning );
			}

			l = ((Layer*)layers.peek());
			*out_bytes_size = l->activations_bv_bytes_size();
			*output_f32 = l->activations;
		}

		inline
		void
		setup_input( f32 * input )
		{
			Layer * l = layer( 0 );
			for ( s32 i = 0; i != l->cardinality; ++i )
			{
				l->activations[i] = input[i];
			}
		}
		inline
		void
		run( f32 ** output_f32, bool learning = false )
		{
			for_mM( i, 1, (s32)layers.occupancy )
			{
				layer( i )->run( learning );
			}

			Layer * l = ((Layer*)layers.peek());
			*output_f32 = l->activations;
		}
		inline
		void
		run( f32 * input, f32 ** output_f32, bool learning = false )
		{
			setup_input( input );

			run( output_f32, learning );
		}

		#if 0
		inline
		void
		incentivize( f32 rate )
		{
			for ( s32 i = 1; i < (s32)layers.occupancy; ++i )
			{
				layer( i )->incentivize( rate );
			}
		}
		#endif

		inline
		f32
		run_and_compute_gradient( u8 * input, u8 * expected_output, bool learning = false )
		{
			f32 ret;

			s32 output_bytes_size;
			f32 * output_f32 = NULL;
			run( input, &output_bytes_size, &output_f32, learning );
			Layer * l = ((Layer*)layers.peek());

			ret = err( expected_output, output_f32, l->cardinality, l->flags );

			for ( int i = layers.occupancy - 1; i != 0; --i )
			{
				layer( i )->compute_gradient( expected_output );
			}

			return ret;
		}

		inline
		f32
		run_and_compute_err( f32 * input, f32 * expected_output, bool learning = false )
		{
			f32 ret = 0;

			f32 * output_f32 = NULL;
			run( input, &output_f32, learning );
			Layer * l = ((Layer*)layers.peek());

			ret = err( expected_output, output_f32, l->cardinality, l->flags );

			return ret;
		}

		inline
		f32
		run_and_compute_gradient( f32 * input, f32 * expected_output, bool learning = false )
		{
			f32 ret = run_and_compute_err( input, expected_output, learning );

			//log_dbg( "compute gradients" );
			for ( int i = layers.occupancy - 1; i != 0; --i )
			{
				//log_dbg( "layer %d", i );
				layer( i )->compute_gradient( expected_output );
			}

			return ret;
		}

		inline
		f32
		run_and_compute_gradient_for_input_generation( f32 * expected_layer_activation, s32 target_layer )
		{
			for_mM( i, 1, (s32)layers.occupancy )
			{
				layer( i )->run( false );
			}

			Layer * l = layer( target_layer );

			f32 * output_f32 = l->activations;

			f32 ret = err( expected_layer_activation, output_f32, l->cardinality, l->flags );

			Layer * next_layer = l->next_layer;
			l->next_layer = NULL;

			for_M0 ( i, target_layer + 1 )
			{
				layer( i )->compute_dCda( expected_layer_activation );
			}

			l->next_layer = next_layer;

			return ret;
		}

		inline
		void
		apply_backprop_to_input( f32 rate )
		{
			Layer * l = layer( 0 );
			l->apply_backprop_to_input( rate );
		}

		inline
		void
		apply_backprop( f32 rate )
		{
			for_Mm( i, 1, layers.occupancy )
			{
				//log_dbg( "apply backprop at layer %d", i );
				layer( i )->apply_backprop( rate );
			}
		}

		inline
		void
		rescale_weights( f32 target_l )
		{
			for_M0( i, layers.occupancy - 1 )
			{
				layer( i )->rescale_weights( target_l );
			}
		}
		inline
		void
		clip_weights( f32 clip_l )
		{
			for_M0( i, layers.occupancy - 1 )
			{
				layer( i )->clip_weights( clip_l );
			}
		}

		#if 0
		inline
		f32
		run_and_incentivize( u8 * input, u8 * expected_output, f32 rate )
		{
			f32 ret;

			s32 output_bytes_size;
			f32 * output_f32;
			u8 * output = run( input, &output_bytes_size, &output_f32 );

			s32 res = memcmp( expected_output, output, output_bytes_size );
			ret = res == 0;
			Layer * l = ((Layer*)layers.peek());

			f32 diff_pc = diff_percentage_equality( output, expected_output, l->cardinality );

			f32 r = randf_01( layer( 0 )->seeds );
			r = 0.5f;
			bool positive = r <= diff_pc;
			if ( ! positive )
			{
				rate *= -1.f;
				rate *= ( 1.f - diff_pc * diff_pc );
			} else
			{
				rate *= diff_pc * diff_pc;
			}
			//if ( rate > 0 )
			incentivize( rate );

			ret = diff_pc;

			return ret;
		}
		#endif

		inline
		static
		f32
		diff_percentage_equality( u8 * a, u8 * b, s32 bits_count )
		{
			f32 ret = 0.f;
			s32 count = 0;
			for_0M( i, bits_count )
			{
				if ( BITTEST1(a,i) == BITTEST1(b,i) )
				{
					++count;
				}
			}
			ret = count / f32(bits_count);
			return ret;
		}
		inline
		f32
		err( f32 * expected_output, f32 * output_f32, s32 count, Flags flags  )
		{
			f32 ret = 0;

			f64 sum = 0;

			if ( ( flags & Flags::SOFTMAX ) != Flags::NONE )
			{
				for_0M( i, count )
				{
					f32 eo = expected_output[i];
					f32 o = output_f32[i];
					sum -= eo * logf( o );
				}
			} else
			{
				for_0M( i, count )
				{
					f32 eo = expected_output[i];
					f32 o = output_f32[i];
					f32 e = eo - o;
					sum += 0.5f * e * e;
					//log_dbg( "eo %e o %e e %e count %d ret %e", eo, o, e, count, ret );
				}
				sum /= f64(count);
			}

			ret = sum;

			return ret;
		}
		inline
		f32
		err( u8 * expected_output, f32 * output_f32, s32 count, Flags flags )
		{
			f32 ret = 0;
			if ( ( flags & Flags::SOFTMAX ) != Flags::NONE )
			{
				for_0M( i, count )
				{
					f32 eo = f32(BITTEST1(expected_output,i)) ? 1 : -1;
					f32 o = output_f32[i];
					ret -= eo * logf( o );
				}
			} else
			{
				for_0M( i, count )
				{
					f32 eo = f32(BITTEST1(expected_output,i)) ? 1 : -1;
					//tg_assert( eo == 1 || eo == 0 );
					f32 e = eo - output_f32[i];
					ret += 0.5f * e * e;
				}
				ret /= f32(count);
			}

			return ret;
		}

		inline
		Layer*
		layer( s32 index )
		{
			Layer * ret = ((Layer*)layers.at( index ));
			return ret;
		}

		inline
		void
		rec_weights_as_prev()
		{
			for_0M( i, layers.occupancy )
			{
				layer( i )->rec_weights_as_prev();
			}
		}

		inline
		void
		destroy()
		{
			layers.destroy();
		}

		inline
		bool
		write_weights( FILE * f )
		{
			bool ret = true;
			for_mM(i,1,(s32)layers.occupancy)
			{
				ret = layer( i )->write_weights( f );
				if ( ! ret ) break;
			}
			return ret;
		}
		inline
		bool
		read_weights( FILE * f )
		{
			bool ret = true;
			for_mM(i,1,(s32)layers.occupancy)
			{
				ret = layer( i )->read_weights( f );
				if ( ! ret ) break;
			}
			return ret;
		}
		inline
		void
		print_weights( FILE * f )
		{
			for_mM(i,1,(s32)layers.occupancy)// ( s32 i = 1; i != (s32)layers.occupancy; ++i )
			{
				fprintf( f, "\n\nlayer %2d\n", i );
				layer( i )->print_weights( f );
			}
		}
		inline
		bool
		parse_weights( FILE * f )
		{
			bool ret = false;
			for_mM ( i, 1, (s32)layers.occupancy )
			{
				s32 layer_index = -1;
				s32 read = fscanf( f, "\n\nlayer %2d\n", &layer_index );
				ret = read == 1; if ( ! ret ) return ret;
				ret = layer_index == i; if ( ! ret ) return ret;
				pnn::PNN::Layer * l = layer( i );
				pnn::PNN::Layer * pl = layer( i - 1 );

				log_dbg( "loading layer %d", layer_index );

				s32 s32_minibatch_counter;
				read = fscanf( f, "minibatch_counter %d\n", &s32_minibatch_counter );
				ret = read == 1; if ( ! ret ) return ret;
				l->minibatch_counter = s32_minibatch_counter;

				log_dbg( "minibatch counter %d", s32_minibatch_counter );

				for_0M(j,l->cardinality)
				{
					s32 read_j;
					for_0M(k,pl->cardinality)
					{
						s32 read_k;
						f32 read_w;
						f32 read_adam_m ;
						f32 read_adam_mt;
						f32 read_adam_v ;
						f32 read_adam_vt;
						read = fscanf( f, "%d %d %e adam_m %e adam_mt %e adam_v %e adam_vt %e\n",
						                  &read_j, &read_k, &read_w, &read_adam_m, &read_adam_mt, &read_adam_v, &read_adam_vt );
						ret = read == 7; if ( ! ret ) return ret;
						ret = read_j == j; if ( ! ret ) return ret;
						ret = read_k == k; if ( ! ret ) return ret;
						l->weight_jk ( j, k ) = read_w;
						l->adam_m_jk ( j, k ) = read_adam_m ;
						l->adam_mt_jk( j, k ) = read_adam_mt;
						l->adam_v_jk ( j, k ) = read_adam_v ;
						l->adam_vt_jk( j, k ) = read_adam_vt;
					}
					f32 read_b;
					read = fscanf( f, "%d %e", &read_j, &read_b );
					ret = read == 2; if ( ! ret ) return ret;
					ret = read_j == j; if ( ! ret ) return ret;
					l->biases[j] = read_b;
				}
			}
			return ret;
		}
	};
}

#endif // __PNN_H__
