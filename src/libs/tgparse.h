#ifndef _TGPARSE_H_
#define _TGPARSE_H_ 1

#include "basic_types.h"
#include "macro_tools.h"

namespace tg::parse
{
	inline
	bool
	consume( const char * & parsed, const char * str )
	{
		bool ret = true;
		s32 i;
		for ( i = 0; ( ret = ( parsed[i] == str[i] || ! str[i] ) ) && parsed[i] && str[i]; ++i )
		{
		}
		if ( ret )
		{
			parsed += i;
		}
		return ret;
	}
	inline
	bool
	consume_any( const char * & parsed, const char ** strings, s32 strings_count, s32 * out_consumed_index = 0 )
	{
		bool ret = false;
		for_0M( i, strings_count )
		{
			ret = consume( parsed, strings[i] );
			if ( ret )
			{
				if ( out_consumed_index )
				{
					*out_consumed_index = i;
				}
				break;
			}
		}
		return ret;
	}
	inline
	bool
	until( const char * & parsed, char c, bool allow_blanks = true )
	{
		bool ret = false;
		s32 i;
		for ( i = 0; ! ( ret = parsed[i] == c || ( !allow_blanks && parsed[i] <= ' ' ) ) && parsed[i]; ++i )
		{
		}
		if ( ret )
		{
			parsed += i;
		}
		return ret;
	}
	inline
	bool
	until_one( const char * & parsed, const char * chars, bool allow_blanks = true )
	{
		bool ret = false;
		s32 i = 0;
		auto is_in = [parsed, chars, &i]()
		{
			bool ret = false;
			char c = parsed[i];
			for ( s32 j = 0; chars[j] && ! ret; ++j )
			{
				ret = c == chars[j];
			}
			return ret;
		};
		for ( i = 0; ! ( ret = is_in() || ( !allow_blanks && parsed[i] <= ' ' ) ) && parsed[i]; ++i )
		{
		}
		if ( ret )
		{
			parsed += i;
		}
		return ret;
	}
	inline
	bool
	consume_c_name( const char * & parsed, const char ** out_name_start, s32 * out_name_length )
	{
		bool ret = false;
		s32 i = 0;
		const char * start_parsed = parsed;

		auto is_in = [parsed, &i]()
		{
			bool ret = false;
			char c = parsed[i];
			ret = ( c >= 'A' && c <='Z' )
			   || ( c >= 'a' && c <='z' )
			   || ( i != 0 && c >= 0 && c <= 9 )
			   || ( c == '_' )
			;
			return ret;
		};

		for ( i = 0; ( ret = is_in() ) && parsed[i]; ++i )
		{}

		bool eof = ret;

		ret = i != 0;

		if ( ret )
		{
			parsed += i;

			if ( out_name_start )
			{
				*out_name_start = start_parsed;
			}
			if ( out_name_length )
			{
				s32 l = parsed - start_parsed;
				if ( eof )
				{
					--l;
				}
				*out_name_length = l;
			}
		}

		return ret;
	}
	inline
	s32
	consume_blanks( const char * & parsed )
	{
		s32 ret;

		const char * start = parsed;

		while ( parsed[0] <= ' ' && parsed[0] )
		{
			++parsed;
		}

		const char * end = parsed;
		ret = end - start;

		return ret;
	}


}
#endif /* ifndef _TGPARSE_H_ */
