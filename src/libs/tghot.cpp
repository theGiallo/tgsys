#include "tghot.h"

namespace tg::hot
{
	using String = tg::String;
#if 0
#define tg_hot_log(...) log_dbg(__VA_ARGS__)
#else
#define tg_hot_log(...)
#endif

	#define TG_HOT_INTERNALS 1
	#if TG_HOT_INTERNALS
	////////////////////////////////////////////////////////////////////////////////
	// NOTE(theGiallo): Structs

	struct
	Callbacks
	{
		struct
		CallbackEntry
		{
			DL * dl;
			void ( * callback ) ( DL * dl, void * custom_data );
			void * custom_data;
			const char * requester_name;

			inline
			void
			invoke()
			{
				if ( callback ) callback( dl, custom_data );
			}
		};


		// NOTE(theGiallo): key: const char * dl_name, value: CallbackEntry *
		Hash_Table_Generic_Multi_VM callbacks_about_dl;

		// NOTE(theGiallo): key: const char * requester_name, value: CallbackEntry *
		Hash_Table_Generic_Multi_VM callbacks_of_requester;

		Pool_Allocator_Generic_VM entries_pool;

		static const s64 MAX_CALLBACKS = 1024;
		static const s64 CALLBACKS_ABOUT_DL_TABLE_SIZE = MAX_CALLBACKS;
		static const s64 CALLBACKS_OF_REQUESTER_TABLE_SIZE = MAX_CALLBACKS;

		static
		Callbacks
		Make();

		bool
		init();

		void
		register_callback( DL & dl, void ( * callback ) ( DL * dl, void * custom_data ), void * custom_data, const char * requester_name );

		void
		get_callbacks_of_requester( const char * requester_name, HTGMVM_Element_List_VM ** out_callbacks_entries_list );
		void
		get_callbacks_of_requester( const char * requester_name, tg::Array<CallbackEntry*> & out_callbacks );

		void
		get_callbacks_about_dl( const char * dl_name, HTGMVM_Element_List_VM ** out_callbacks_entries_list );

		void
		get_callbacks_about_dl( const char * dl_name, tg::Array<CallbackEntry*> & out_callbacks );

		void
		remove_callbacks_of_requester( const char * requester_name );

		CallbackEntry *
		allocate_callback_entry();

		void
		deallocate_callback_entry( CallbackEntry * e );
	};

	struct
	Lib
	{
		const char * name;
		void * dl_handle;
		Hot_Api_Lib_Data data;
		String loaded_version;

		// NOTE(theGiallo): key: const char * f_name, value: void * function
		Hash_Table_Generic_VM functions_table = {};

		void
		init();

		bool
		get_function( const char * fname, void ** out_fptr );

		bool
		get_function_ref( const char * fname, void *** out_fptr );

		void
		get_or_load_function( const char * fname, void ** out_fptr );

		void
		reload_function( const char * fname, void ** out_fptr );

		void
		reload_all_functions();

		void
		fill_dl( DL & dl );

		void
		reload_all_and_call_callbacks( Callbacks * callbacks );

		bool
		is_present( const char * fname );

		void *
		_load_function( const char * fname );

		bool
		_add_function( const char * fname, void * fptr );
	};

	struct
	Libraries
	{
		Hash_Table_Generic_VM libraries_table = {};
		Pool_Allocator_Generic_VM lib_pool = {};

		void
		init();

		static
		Libraries
		Make();

		Lib *
		allocate_lib();

		void
		deallocate_lib( Lib * lib );

		bool
		is_lib_present( const char * lib_name );

		bool
		add_lib( const char * lib_name );

		bool
		get_lib( const char * lib_name, Lib ** out_lib );

		bool
		get_or_add_lib( const char * lib_name, Lib ** out_lib, bool * is_new );

		void
		reload_all_and_call_callbacks( Callbacks * callbacks );

		Lib *
		_new_library( const char * lib_name );
	};


	// NOTE(theGiallo): Structs
	////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// NOTE(theGiallo): Statics
	tg_internal Libraries libraries = Libraries::Make();
	tg_internal Callbacks callbacks = Callbacks::Make();
	// NOTE(theGiallo): Statics
	////////////////////////////////////////////////////////////////////////////////
	#endif // TG_HOT_INTERNALS

	tg_internal
	Lib *
	load_dl( const char * lib_name, bool * just_loaded );

	tg_internal
	bool
	load_dl( DL & dl, bool * just_loaded );

	////////////////////////////////////////////////////////////////////////////////
	// NOTE(theGiallo): Structs

	#if 0
	bool
	Hot_Api::request_dl( DL dl, void ( * callback ) ( DL * dl ) )
	{
		return ::tg::hot::request_dl( dl, callback, lib_name );
	}

	void
	Hot_Api::set_lib_data( void * data, s64 size )
	{
		::tg::hot::set_lib_data( Hot_Api_Lib_Data{ data, size }, lib_name );
	}
	#endif

	Callbacks
	Callbacks::Make()
	{
		Callbacks ret = {};
		ret.init();
		return ret;
	}

	bool
	Callbacks::init()
	{
		bool ret;

		ret = entries_pool.init( sizeof( CallbackEntry ), MAX_CALLBACKS );

		ret = callbacks_about_dl    .init( -1, MAX_CALLBACKS, CALLBACKS_ABOUT_DL_TABLE_SIZE     ); if ( ! ret ) return ret;
		ret = callbacks_of_requester.init( -1, MAX_CALLBACKS, CALLBACKS_OF_REQUESTER_TABLE_SIZE ); if ( ! ret ) return ret;

		return ret;
	}

	void
	Callbacks::register_callback( DL & dl, void ( * callback ) ( DL * dl, void * custom_data ), void * custom_data, const char * requester_name )
	{
		tg_hot_log( "Callbacks::register_callback( DL & dl { name = %s %lx }, void ( * callback ) ( DL * dl ), const char * requester_name = %s )",
		            dl.name, (u64)dl.name, requester_name );

		CallbackEntry * callback_entry = allocate_callback_entry();
		if ( ! callback_entry )
		{
			log_err( "callback entry just allocated is NULL" );
		}
		callback_entry->dl             = &dl;
		callback_entry->callback       = callback;
		callback_entry->custom_data    = custom_data;
		callback_entry->requester_name = requester_name;
		bool b_res;
		b_res = callbacks_of_requester.insert( (void*)requester_name, (void*)callback_entry );
		if ( ! b_res ) log_err( "failed to insert into callbacks_of_requester" );
		b_res = callbacks_about_dl    .insert( (void*)dl.name,        (void*)callback_entry );
		if ( ! b_res ) log_err( "failed to insert into callbacks_about_dl" );
	}

	void
	Callbacks::get_callbacks_of_requester( const char * requester_name, HTGMVM_Element_List_VM ** out_callbacks_entries_list )
	{
		callbacks_of_requester.get_all( (void*)requester_name, out_callbacks_entries_list );
	}
	void
	Callbacks::get_callbacks_of_requester( const char * requester_name, tg::Array<CallbackEntry*> & out_callbacks )
	{
		HTGMVM_Element_List_VM * values_list = {};
		bool b_res = callbacks_of_requester.get_all( (void*)requester_name, &values_list );
		if ( ! b_res )
		{
			return;
		}

		b_res = out_callbacks.allocate( values_list->length );
		if ( ! b_res )
		{
			ILLEGAL_PATH_M( "failed to allocate memory for %ld callbacks", values_list->length );
		}

		s32 i = 0;
		for ( HTGMVM_Element_List_VM_Node * node = values_list->first;
		      node;
		      node = node->next )
		{
			out_callbacks.array[i] = (CallbackEntry*)node->element.value;
		}
	}

	void
	Callbacks::get_callbacks_about_dl( const char * dl_name, HTGMVM_Element_List_VM ** out_callbacks_entries_list )
	{
		callbacks_about_dl.get_all( (void*)dl_name, out_callbacks_entries_list );
	}

	void
	Callbacks::get_callbacks_about_dl( const char * dl_name, tg::Array<CallbackEntry*> & out_callbacks )
	{
		HTGMVM_Element_List_VM * values_list = {};
		bool b_res = callbacks_about_dl.get_all( (void*)dl_name, &values_list );
		if ( ! b_res )
		{
			return;
		}

		b_res = out_callbacks.allocate( values_list->length );
		if ( ! b_res )
		{
			ILLEGAL_PATH_M( "failed to allocate memory for %ld callbacks", values_list->length );
		}

		s32 i = 0;
		for ( HTGMVM_Element_List_VM_Node * node = values_list->first;
		      node;
		      node = node->next )
		{
			out_callbacks.array[i] = (CallbackEntry*)node->element.value;
		}
	}

	void
	Callbacks::remove_callbacks_of_requester( const char * requester_name )
	{
		HTGMVM_Element_List_VM list = {};
		bool b_res = callbacks_of_requester.remove_and_get_all( (void*)requester_name, & list );
		if ( ! b_res )
		{
			return;
		}


		// TODO(theGiallo, 2021-03-27): query only for the dl names that are in list
		for_0M( i, callbacks_about_dl.table_length )
		{
			for ( auto tnode = callbacks_about_dl.table[i].first; tnode; tnode = tnode->next )
			{
				auto * tlist = &tnode->element.list;
				for ( HTGMVM_Element_List_VM_Node * node = tlist->first, * prev = 0;
				      node;
				    )
				{
					CallbackEntry * ce = (CallbackEntry*)node->element.value;
					if ( string_equal( ce->requester_name, requester_name ) )
					{
						tg_hot_log( "removing callback about %s from %s", ce->dl->name, ce->requester_name );
						if ( prev )
						{
							list_vm_remove_next( tlist, prev );
							node = prev;
						}
						else
						{
							list_vm_remove_first( tlist );
							node = tlist->first;
							continue;
						}
					}
					prev = node;
					node = node->next;
				}
			}
		}


		for ( HTGMVM_Element_List_VM_Node * node = list.first;
		      node;
		      node = node->next )
		{
			CallbackEntry * ce = (CallbackEntry *) node->element.value;
			deallocate_callback_entry( ce );
		}

		list_vm_remove_all( &list );
	}

	Callbacks::CallbackEntry *
	Callbacks::allocate_callback_entry()
	{
		CallbackEntry * ret = (CallbackEntry *)entries_pool.allocate_clean();
		return ret;
	}

	void
	Callbacks::deallocate_callback_entry( CallbackEntry * e )
	{
		entries_pool.deallocate( (void*) e );
	}

	void
	Lib::init()
	{
		bool b_res;
		b_res = functions_table.init( -1 );
		if ( ! b_res )
		{
			ILLEGAL_PATH_M( "failed to initialize hash table" );
		}
	}

	void *
	Lib::_load_function( const char * fname )
	{
		dlerror();

		void * ret = dlsym( dl_handle, fname );

		const char * error = dlerror();
		if ( error )
		{
			log_err( "error loading function '%s' from library '%s': %s",
			         fname, name, error );
		} else
		if ( ! ret )
		{
			tg_hot_log( "%s | loading function %s as null", name, fname );
		} else
		{
			tg_hot_log( "%s | loading function %s as %lx", name, fname, (u64)ret );
		}

		return ret;
	}

	bool
	Lib::get_function( const char * fname, void ** out_fptr )
	{
		bool ret = functions_table.get_one( (void*)fname, out_fptr );
		return ret;
	}

	bool
	Lib::get_function_ref( const char * fname, void *** out_fptr )
	{
		bool ret = functions_table.get_one_ref( (void*)fname, out_fptr );
		return ret;
	}

	void
	Lib::get_or_load_function( const char * fname, void ** out_fptr )
	{
		bool b_res = get_function( fname, out_fptr );
		if ( ! b_res )
		{
			*out_fptr = _load_function( fname );
			_add_function( fname, *out_fptr );
		}
	}

	void
	Lib::reload_function( const char * fname, void ** out_fptr )
	{
		tg_hot_log( "%s | loading function %s", name, fname );

		void * new_function = _load_function( fname );
		void ** in_fptr;
		bool b_res = get_function_ref( fname, &in_fptr );
		if ( ! b_res )
		{
			bool b_res = _add_function( fname, new_function );

			if ( ! b_res )
			{
				log_err( "failed to add function to table:'%s' of library '%s'",
				         fname, name );
				*out_fptr = 0;

				return;
			} else
			{
				*out_fptr = new_function;
			}
		} else
		{
			*out_fptr = *in_fptr = new_function;
		}
	}

	void
	Lib::reload_all_functions()
	{
		for_0M( i, functions_table.table_length )
		{
			HTGVM_Element_List_VM list = functions_table.table[i];
			for ( HTGVM_Element_List_VM_Node * node = list.first; node; node = node->next )
			{
				const char * f_name = (const char *)node->element.key;
				void * func;
				reload_function( f_name, &func );
				node->element.value = func;
			}
		}
	}

	void
	Lib::fill_dl( DL & dl )
	{
		tg_assert( string_equal( dl.name, name ) );

		for_0M( fi, dl.functions_names.count )
		{
			const char * fname = dl.functions_names.array[fi];
			void * func;
			get_or_load_function( fname, &func );
			dl.functions.array[fi] = func;
		}
	}

	void
	Lib::reload_all_and_call_callbacks( Callbacks * callbacks )
	{
		bool just_loaded;
		Lib * lib = load_dl( this->name, &just_loaded );
		if ( ! just_loaded )
		{
			return;
		}
		(void)lib;

		reload_all_functions();

		HTGMVM_Element_List_VM * callbacks_entries_list;
		callbacks->get_callbacks_about_dl( name, &callbacks_entries_list );

		for ( HTGMVM_Element_List_VM_Node * node = callbacks_entries_list->first;
		      node;
		      node = node->next )
		{
			Callbacks::CallbackEntry * c = (Callbacks::CallbackEntry*)node->element.value;

			fill_dl( *c->dl );

			// TODO(theGiallo, 2021-03-03): what if in here they want to modify `callbacks`?
			c->invoke();
		}
	}

	bool
	Lib::is_present( const char * fname )
	{
		bool ret = functions_table.is_present( (void*)fname );
		return ret;
	}

	bool
	Lib::_add_function( const char * fname, void * fptr )
	{
		bool ret = functions_table.insert( (void*)fname, fptr );
		return ret;
	}

	void
	Libraries::init()
	{
		s64 max_libs = 128 * 1024;
		lib_pool.init( sizeof ( Lib ), max_libs );

		libraries_table.init( -1 );
	}

	Libraries
	Libraries::Make()
	{
		Libraries ret = {};
		ret.init();
		return ret;
	}

	Lib *
	Libraries::allocate_lib()
	{
		Lib * ret = (Lib*)lib_pool.allocate_clean();
		return ret;
	}
	void
	Libraries::deallocate_lib( Lib * lib )
	{
		lib_pool.deallocate( lib );
	}

	bool
	Libraries::is_lib_present( const char * lib_name )
	{
		bool ret = libraries_table.is_present( (void*)lib_name );
		return ret;
	}

	Lib *
	Libraries::_new_library( const char * lib_name )
	{
		Lib * ret = allocate_lib();
		ret->init();
		ret->name = lib_name;
		return ret;
	}

	bool
	Libraries::add_lib( const char * lib_name )
	{
		bool ret = false;
		Lib * lib = _new_library( lib_name );
		libraries_table.insert( (void*)lib_name, lib );
		return ret;
	}

	bool
	Libraries::get_lib( const char * lib_name, Lib ** out_lib )
	{
		bool ret = libraries_table.get_one( (void*)lib_name, (void**)out_lib );
		return ret;
	}

	bool
	Libraries::get_or_add_lib( const char * lib_name, Lib ** out_lib, bool * is_new )
	{
		Lib ** in_lib;
		bool ret = libraries_table.insert_unique_or_get_one_ref( (void*)lib_name, 0, (void***)&in_lib );
		if ( ret )
		{
			*is_new = ! *in_lib;
			if ( is_new )
			{
				*in_lib = _new_library( lib_name );
				{
					Lib * reget_lib;
					tg_assert( get_lib( lib_name, &reget_lib ) && reget_lib == *in_lib );
				}
			}
			ret = *in_lib;
		}
		*out_lib = *in_lib;
		return ret;
	}

	void
	Libraries::reload_all_and_call_callbacks( Callbacks * callbacks )
	{
		for_0M( ti, libraries_table.table_length )
		{
			HTGVM_Element_List_VM * list = libraries_table.table + ti;
			for ( HTGVM_Element_List_VM_Node * node = list->first;
			      node;
			      node = node->next )
			{
				((Lib*)node->element.value)->reload_all_and_call_callbacks( callbacks );
			}
		}
	}

	// NOTE(theGiallo): Structs
	////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// NOTE(theGiallo): Statics
	// tg_internal Libraries libraries = Libraries::Make();
	// tg_internal Callbacks callbacks = Callbacks::Make();
	// NOTE(theGiallo): Statics
	////////////////////////////////////////////////////////////////////////////////

	void
	register_callback( DL & dl, void ( * callback ) ( DL * dl, void * custom_data ), void * custom_data, const char * requester_name )
	{
		callbacks.register_callback( dl, callback, custom_data, requester_name );
	}

	void
	remove_callbacks_by( const char * requester_name )
	{
		callbacks.remove_callbacks_of_requester( requester_name );
	}

	bool
	dl_is_present( DL & dl )
	{
		bool ret = libraries.is_lib_present( dl.name );
		return ret;
	}

	bool
	build_lib_version_path( char * str, s32 buf_len, const char * dl_name )
	{
		bool ret = false;

		s32 i_res = snprintf( str, buf_len, TGHOT_DLIBS_DIR "./lib%s.version.txt", dl_name );
		ret = i_res >= 0;
		if ( ! ret )
		{
			log_err( "error building lib version file path for %s", dl_name );
		}

		return ret;
	}
	bool
	build_lib_path_versioned( char * str, s32 buf_len, const char * dl_name, const char * lib_version )
	{
		bool ret = false;

		s32 i_res = snprintf( str, buf_len, TGHOT_DLIBS_DIR "./lib%s.so.%s", dl_name, lib_version );
		ret = i_res >= 0;
		if ( ! ret )
		{
			log_err( "error building lib path versioned for %s", dl_name );
		}

		return ret;
	}

	tg_internal
	Lib *
	load_dl( const char * lib_name, bool * just_loaded )
	{
		Lib * ret = {};

		#define DLCLOSE_SO( so ) \
		if ( so ) \
		{ \
			i_res = dlclose( so ); \
			if ( i_res ) log_err( "dlclose error: %s", dlerror() ); \
		} \

		s32 i_res;

		char lib_version_path[256] = {};
		bool b_res = build_lib_version_path( &lib_version_path[0], (s32)sizeof ( lib_version_path ), lib_name );
		if ( ! b_res )
		{
			return ret;
		}

		char lib_version[512] = {};
		s64 lib_version_size = read_entire_file( lib_version_path, lib_version, sizeof(lib_version) );
		if ( lib_version_size > (s64)sizeof ( lib_version ) )
		{
			log_err( "failed to read file of lib version of '%s' ('%s') because it's too large. %lu > %lu", lib_name, lib_version_path, lib_version_size, sizeof ( lib_version ) );
			return ret;
		} else
		if ( lib_version_size < 0 )
		{
			log_err( "failed to read file of lib version of '%s' ('%s')", lib_name, lib_version_path );
			perror(0);
			return ret;
		} else
		if ( lib_version_size == 0 )
		{
			log_err( "file of lib version of '%s' is empty ('%s')", lib_name, lib_version_path );
			return ret;
		}

		// tg_hot_log( "lib version for '%s' is '%s'", lib_name, lib_version );

		// TODO(theGiallo): sanitize lib_version (refuse if it has slashes or invalid file name characters)
		for_0M( i, lib_version_size )
		{
			if ( lib_version[i] == '/'
			  || lib_version[i] == '\\' )
			{
				log_err( "library version for '%s' contains an invalid character", lib_name );
				return ret;
			}
		}


		Lib * old_lib;
		b_res = libraries.get_lib( lib_name, &old_lib );
		if ( b_res )
		{
			if ( string_equal( old_lib->loaded_version.str, lib_version ) )
			{
				*just_loaded = false;
				return ret;
			}
		}


		char lib_path_versioned[512] = {};
		b_res = build_lib_path_versioned( lib_path_versioned, sizeof ( lib_path_versioned ), lib_name, lib_version );
		if ( ! b_res )
		{
			return ret;
		}

		tg_hot_log( "versioned lib path for '%s' is '%s'", lib_name, lib_path_versioned );

		//if ( so ) tg_hot_log( "so previously loaded" );
		//DLCLOSE_SO();

		void * so = dlopen( lib_path_versioned, RTLD_NOW | RTLD_NOLOAD );

		if ( ! so )
		{
			tg_hot_log( "library not already loaded" );
			so = dlopen( lib_path_versioned, RTLD_NOW | RTLD_DEEPBIND | RTLD_GLOBAL );
		} else
		{
			tg_hot_log( "library already loaded" );
			DLCLOSE_SO( so );
			so = dlopen( lib_path_versioned, RTLD_NOW | RTLD_DEEPBIND | RTLD_GLOBAL );
		}

		if ( ! so )
		{
			log_err( "failed to load %s: %s", lib_path_versioned, dlerror() );
			return ret;
		}

		bool is_new;
		libraries.get_or_add_lib( lib_name, &ret, &is_new );

		if ( ! is_new )
		{
			void ( * hot_outdated ) ();
			ret->get_or_load_function( "hot_outdated", (void**)&hot_outdated );
			if ( hot_outdated )
			{
				hot_outdated();
			} else
			{
				log_err( "failed to get hot_outdated from lib %s", lib_name );
			}

			// TODO(theGiallo, 2021-03-09): we could make it optional to close old versions
			//void * old_so = lib->dl_handle;
			//DLCLOSE_SO( old_so );
		}

		ret->dl_handle = so;

		remove_callbacks_by( lib_name );

		// NOTE(theGiallo, 2021-02-19): if we need to keep string literals we can't close
		// DLCLOSE_SO( old_so );

		void ( * hot_boot )( Hot_Api * hot_api, Hot_Api_Lib_Data data );
		ret->reload_function( "hot_boot", (void**)&hot_boot );

		{
			void ( * hot_outdated )();
			ret->reload_function( "hot_outdated", (void**)&hot_outdated );
		}

		Hot_Api_Lib_Data data = ret->data;
		Hot_Api hot_api = Hot_Api{ .lib_name = lib_name };
		hot_boot( &hot_api, data );

		ret->loaded_version.clear();
		ret->loaded_version.add( "%s", lib_version );

		*just_loaded = true;

		return ret;
	}

	tg_internal
	bool
	load_dl( DL & dl )
	{
		bool ret;

		bool just_loaded;
		Lib * lib = load_dl( dl.name, &just_loaded );
		ret = lib != 0;

		if ( ret && just_loaded )
		{
			for_0M( i, dl.functions_names.count )
			{
				const char * fname = dl.functions_names.array[i];
				lib->reload_function( fname, dl.functions.array + i );
			}
		}

		return ret;
	}

	bool
	fill_functions( DL & dl )
	{
		Lib * lib;
		bool ret = libraries.get_lib( dl.name, &lib );
		if ( ! ret )
		{
			return ret;
		}

		lib->fill_dl( dl );

		return ret;
	}
	void
	reload_if_you_want()
	{
		libraries.reload_all_and_call_callbacks( & callbacks );
	}

	TG_DL_EXPORTED
	void
	set_lib_data( Hot_Api_Lib_Data data, const char * lib_name )
	{
		Lib * lib;
		bool b_res = libraries.get_lib( lib_name, &lib );
		tg_assert_m( b_res, "failed to get lib %s", lib_name );
		lib->data = data;
	}

	TG_DL_EXPORTED
	bool
	request_dl( DL & dl, void ( * callback ) ( DL * dl, void * custom_data ), const char * registerer_name, void * custom_data )
	{
		bool ret = false;

		register_callback( dl, callback, custom_data, registerer_name );

		if ( ! dl_is_present( dl ) )
		{
			bool b_res = load_dl( dl );
			if ( ! b_res )
			{
				log_err( "failed to load dl %s", dl.name );
				return ret;
			}
		}
		tg_assert( dl_is_present( dl ) );

		fill_functions( dl );
		if ( callback ) callback( &dl, custom_data );

		ret = true;
		return ret;
	}
}
