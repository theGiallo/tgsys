#include "pool_allocator_generic.h"
#include "macro_tools.h"
#include <string.h>

void
pool_allocator_generic_init( Pool_Allocator_Generic * pa, void * memory, s64 capacity, u64 element_size )
{
	tg_assert( pa );
	tg_assert( element_size >= sizeof(u64) );

	pa->memory = memory;
	pa->capacity = capacity;
	pa->element_size = element_size;
	pa->id_of_first_free = 0;
	pa->occupancy = 0;

	u8 * m = (u8*)memory;
	for ( s64 i = 0; i != capacity; ++i )
	{
		*(s64*)m = i == capacity - 1 ? -1 : i + 1;
		m += element_size;
	}
}

void *
pool_allocator_generic_allocate( Pool_Allocator_Generic * pa )
{
	void * ret = NULL;

	tg_assert( pa );

	if ( pool_allocator_generic_is_full( pa ) )
	{
		return ret;
	}

	u8 * m = (u8*)pa->memory;
	s64 * next = (s64*)( m + ( pa->element_size * pa->id_of_first_free ) );
	pa->id_of_first_free = *next;
	ret = next;
	++pa->occupancy;

	return ret;
}

void *
pool_allocator_generic_allocate_clean( Pool_Allocator_Generic * pa )
{
	void * ret = pool_allocator_generic_allocate( pa );

	tg_assert( pa );

	if ( ret )
	{
		memset( ret, 0x0, pa->element_size );
	}

	return ret;
}

void
pool_allocator_generic_deallocate( Pool_Allocator_Generic * pa, void * p )
{
	tg_assert( pa );
	tg_assert( p >= pa->memory );
	tg_assert( p < ( (u8*)pa->memory ) + pa->capacity );
	tg_assert( ( (s64)p - (s64)pa->memory ) % pa->element_size == 0 );

	*(s64*)p = pa->id_of_first_free;
	pa->id_of_first_free = ( (s64)p - (s64)pa->memory ) / pa->element_size;

	--pa->occupancy;
}

bool
pool_allocator_generic_is_full( Pool_Allocator_Generic * pa )
{
	bool ret = false;
	tg_assert( pa );
	tg_assert( pa->occupancy >= 0 );
	tg_assert( pa->capacity >= 0 );
	tg_assert( pa->occupancy <= pa->capacity );

	ret = pa->occupancy == pa->capacity;

	tg_assert( ( ret && pa->id_of_first_free < 0 ) || ( !ret && pa->id_of_first_free >= 0 ) );

	return ret;
}
