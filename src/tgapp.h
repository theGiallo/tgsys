#ifndef _TG_APP_H_
#define _TG_APP_H_ 1

#include "basic_types.h"
#include "macro_tools.h"
#include "tgsys.h"
#include "tghot.h"

namespace tg::app
{
	bool ( * update ) ( tg::sys::Impl * sys );

	tg::hot::DL dl;

	void _request_dl_callback( tg::hot::DL * dl, void * data )
	{
		(void) data;
		(*(void**)&update) = dl->functions.array[0];
	}

	bool request_dl( const char * app_name, const char * requester )
	{
		dl = tg::hot::DL::Make( app_name, {"tg_app_update"} );
		bool ret = tg::hot::request_dl( dl, _request_dl_callback, requester );
		return ret;
	}
};

#endif /* ifndef _TG_APP_H_ */
