#if 0
	echo $#
	echo $0 $1 $2

	#echo fullpath should be $(readlink -e $0)
	#export THIS_FILE_PATH=$0
	export THIS_FILE_PATH=$(readlink -e $0)
	#export THIS_FILE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
	export THIS_FILE_DIR=$(dirname $THIS_FILE_PATH)
	export THIS_FILE_NAME=${THIS_FILE_PATH##*/}
	export THIS_FILE_BASE_NAME=${THIS_FILE_NAME%%.*}

	exec /bin/bash -c 'exec "/bin/bash" <(tail -n +15 "$0") "$@"' "$0" "$@"
	exit $!

	# IFS= read -r -d '' BASH_TEXT <<BASH_SECTION
	# !/bin/bash
	echo -n "curr dir: "
	pwd

	APP_NAME='"tgsysSDL"'

	REL_ROOT_PROJ=./
	REL_DLIBS_DIR=../builds/hotlibs/
	DLIBS_DIR_FULL_PATH="$THIS_FILE_DIR/$REL_DLIBS_DIR"

	if [ ! -d $DLIBS_DIR_FULL_PATH ]
	then
		mkdir -p $DLIBS_DIR_FULL_PATH
	fi

	DLIB_NAME=$THIS_FILE_BASE_NAME
	DLIB_FILE_NAME=lib$DLIB_NAME.so
	if [ -z "${DLIB_VERSION}" ]
	then
		DLIB_VERSION=$(date +%Y.%m.%d.%H.%M.%S);
	fi
	DLIB_FILE_NAME_VERSIONED=$DLIB_FILE_NAME.$DLIB_VERSION

	DLIB_FULL_PATH="$DLIBS_DIR_FULL_PATH/$DLIB_FILE_NAME"
	DLIB_VERSIONED_FULL_PATH="$DLIBS_DIR_FULL_PATH/$DLIB_FILE_NAME_VERSIONED"

	echo "this file dir   = '$THIS_FILE_DIR'"
	echo "this file path  = '$THIS_FILE_PATH'"
	echo "this file name  = '$THIS_FILE_NAME'"
	echo "base file name  = '$THIS_FILE_BASE_NAME'"
	echo "executable name = '$EXECUTABLE_NAME'"
	echo "libs dir        = '$DLIBS_DIR_FULL_PATH'"
	echo "lib name        = '$DLIB_NAME'"
	echo "lib full path   = '$DLIB_FULL_PATH'"
	echo "lib v. fullpath = '$DLIB_VERSIONED_FULL_PATH'"
	echo "app name        = '$APP_NAME'"
	echo
	echo args count: $#
	i=0
	for arg in $*; do
		echo -e "\targ $i: $arg"
		((i++))
	done
	echo

	if [ -z "${CXX+x}" ]
	then
		#CXX=arm-linux-gnueabihf-g++
		CXX=clang++
		#CXX=g++
	fi

	if [[ "${CXX}" == @(g++*|gcc*) ]]
	then

	CXX_FLAGS="-std=c++20 \
	           -Wall \
	           -Wextra \
	           -Wno-missing-braces \
	           -Wno-unused-function \
	           -Wno-comment \
	           -Wl,-export-dynamic -rdynamic -fPIC -shared -fno-exceptions \
	           -feliminate-unused-debug-types \
	           -ffunction-sections -Wl,--gc-sections \
	           -fvisibility=hidden \
	           -O0 -ggdb3 \
	           -march=native \
	           -DAPP_NAME='$APP_NAME' \
	           -DLIB_NAME='\"$DLIB_NAME\"' \
	           -DLIB_VERSION=$DLIB_VERSION \
	           $CXX_FLAGS"

	elif [[ $CXX == clang* ]]
	then
	CXX_FLAGS="-std=c++20 \
	           -Wall \
	           -Wextra \
	           -Wno-missing-braces \
	           -Wno-unused-function \
	           -Wno-comment \
	           -Rpass-missed=loop-vectorize -Rpass-analysis=loop-vectorize \
	           -Wl,-export-dynamic -rdynamic -fdeclspec -fPIC -shared -fno-exceptions \
	           -feliminate-unused-debug-types \
	           -fvisibility=hidden \
	           -flto=thin \
	           -O0 -ggdb3 \
	           -march=native \
	           -DAPP_NAME='$APP_NAME' \
	           -DLIB_NAME='\"$DLIB_NAME\"' \
	           -DLIB_VERSION=$DLIB_VERSION \
	           $CXX_FLAGS"
	fi

	#          -fvisibility=hidden \ makes all functions internal to the compilation unit, and exported only if specified
	#          -fsave-optimization-record \ clang outputs text file for what it did
	#          -O3 \
	#          -O0 -ggdb3 \
	# -fwhole-program # ensures this CU represents the whole program, enabling more aggressive opts. Don t use if -flto is present
	# -flto=auto # link time optimization ( removes unused functions ) (g++ only)
	# -flto=thin # clang outputs small .so http://blog.llvm.org/2016/06/thinlto-scalable-and-incremental-lto.html
	# -ffunction-sections -Wl,--gc-sections # remove unused functions (this is also achieved with -flto), separating code from data
	# -fpermissive # downgrades some errors to warnings for non-conformant code
	#-fPIC # position independent code, useful for linked libraries


	# NOTE(theGiallo): relative to the .so
	LIBS_REL_PATH="../../src/libs"
	SDL_CONFIG="$THIS_FILE_DIR/libs/SDL2/linux/bin/sdl2-config --prefix=$THIS_FILE_DIR/libs/SDL2/linux --exec-prefix=$LIBS_REL_PATH/SDL2/linux"
	SDL_INCLUDE="$($SDL_CONFIG --cflags )"
	SDL_LIBS="$($SDL_CONFIG --libs)"
	CXX_FLAGS="$CXX_FLAGS $SDL_INCLUDE $SDL_LIBS"


	declare -a DEFINES
	DEFINES[${#DEFINES[@]}]=DEBUG=1
	DEFINES[${#DEFINES[@]}]=WE_LOAD_OUR_GL=1

	declare -a INCLUDE_DIRS
	INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]=$THIS_FILE_DIR
	INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]=$THIS_FILE_DIR/$REL_ROOT_PROJ"libs"
	INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]=$THIS_FILE_DIR/$REL_ROOT_PROJ"libs/SDL2/linux/include/"

	declare -a LIBS_DIRS
	#LIBS_DIRS[${#LIBS_DIRS[@]}]=$THIS_FILE_DIR/$REL_ROOT_PROJ"libraries/"
	LIBS_DIRS[${#LIBS_DIRS[@]}]=$THIS_FILE_DIR/$REL_ROOT_PROJ"libs/SDL2/linux/lib/"

	declare -a LIBS
	LIBS[${#LIBS[@]}]=pthread
	LIBS[${#LIBS[@]}]=dl

	# NOTE(theGiallo): sdl link parameters via config script
	#LIBS[${#LIBS[@]}]=SDL2

	#LIBS[${#LIBS[@]}]=rt

	function \
	defines()
	{
		for d in ${DEFINES[@]}
		do
			printf " -D$d "
		done
	}
	function \
	include_dirs()
	{
		for p in ${INCLUDE_DIRS[@]}
		do
			printf " -I$p "
		done
	}
	function \
	libs_dirs()
	{
		for p in ${LIBS_DIRS[@]}
		do
			printf " -L$p "
	#printf " -Wl,-rpath=$p "
		done
	}
	function \
	link_libs()
	{
		for l in ${LIBS[@]}
		do
			printf " -l$l "
		done
	}

	ret=3

	echo "calling compiler"

	CMD="$CXX $CXX_FLAGS `defines` `include_dirs` -Wl,-soname,lib$DLIB_NAME.so.$DLIB_VERSION -o $DLIB_FULL_PATH $THIS_FILE_PATH `libs_dirs` `link_libs`"
	echo CMD:
	echo $CMD
	RUNNABLE_CMD=$(echo $CMD | sed "s/'\"/\"/g" | sed "s/\"'/\"/g")
	echo RUNNABLE_CMD:
	echo $RUNNABLE_CMD

	#echo
	STRIPPED_CMD=$(echo $CMD | sed 's/"/\\"/g')
	#echo
	#echo $STRIPPED_CMD

	JSON="[\n{\n\t\"directory\":\"$THIS_FILE_DIR/$REL_ROOT_PROJ\",\n\t\"command\":\"$STRIPPED_CMD\",\n\t\"file\":\"$THIS_FILE_PATH\"\n},\n]";
	echo -e $JSON > compile_commands.json

	TIME="real %E | user %U | sys %S | max mem %M kB | avg mem %K kB" time $RUNNABLE_CMD

	ret=$?;

	if [ $ret -eq 0 ]
	then
		printf 'compilation succeeded\n'
		ln -s $DLIB_FULL_PATH $DLIB_VERSIONED_FULL_PATH
		ls -l $DLIB_FULL_PATH $DLIB_VERSIONED_FULL_PATH
		file $DLIB_FULL_PATH
		echo -n $DLIB_VERSION > "$DLIBS_DIR_FULL_PATH/lib$DLIB_NAME.version.txt"
		objdump -p $DLIB_FULL_PATH | grep SONAME

		#chmod +x ./$EXECUTABLE_NAME
	else
		printf 'compilation failed\n' >&2;
	fi

	echo "exiting $ret"
	exit $ret
	# BASH_SECTION

	# bash -c "exec <($BASH_TEXT)"
	# ret=$?
	# exit $ret
#endif

#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <locale.h>
#include <string.h>

#include "basic_types.h"
#include "macro_tools.h"
#include "tghot.h"
#include "tgsys.h"
#include "intrinsics.h"
#include "SDL2/SDL.h"

// TODO(theGiallo, 2021-04-11): probably this should be outside this library
#include "our_gl.h"

#define SDL_RET_IS_ERROR(r) ((r)!=0)

TG_DL_EXPORTED
TG_SYS_GET_GL_FUNC_LOADER( tg_sys_get_gl_func_loader );

namespace tg::sys::sdl
{

#define TG_SYS_SDL_ERRNO_TO_SYS_ERROR( e ) ( - ( e + (s64)0xFFFFFFFF ) )
#define TG_SYS_SDL_ERR_IS_ERRNO( e ) ( -(e) >= (s64)0xFFFFFFFF )
#define TG_SYS_SDL_ERRNO_FROM_ERR( e ) ( (-(e)) >> 32 )
#define TG_SYS_SDL_ERR_TO_TABLE_ID(e) (-(e)-1)

// NOTE(theGiallo): return codes
#define TG_SYS_SDL_ERR_EXPAND_AS_ENUM(a,b,c) a = b,
#define TG_SYS_SDL_ERR_EXPAND_AS_TABLE_ENTRY(a,b,c) { TOSTRING( a ), c },
#define TG_SYS_SDL_ERR_TABLE(ENTRY) \
        ENTRY( TOO_MANY_WINDOWS,        1, "" ) \
        ENTRY( CREATING_WINDOW ,        2, "" ) \
        ENTRY( CREATING_CONTEXT,        3, "" ) \
        ENTRY( NO_DISPLAYS,             4, "" ) \
        ENTRY( MAKING_WINDOW_CURRENT,   5, "" ) \
        ENTRY( LOADING_GL_FUNCTIONS,    6, "" ) \
        ENTRY( FILE_OPEN_FAILED,        7, "" ) \
        ENTRY( MEMORY_BUFFER_TOO_SMALL, 8, "" ) \
        ENTRY( INVALID_WINDOW_ID,       9, "" ) \

enum class
Err_Codes : u8
{
	TG_SYS_SDL_ERR_TABLE( TG_SYS_SDL_ERR_EXPAND_AS_ENUM )
	// ---
	__COUNT_PLUS_ONE
};

#define TG_SYS_SDL_SCREEN_MODE_EXPAND_AS_SWITCH_SDL_TO_SYS(a,b) case b: mode = a; break;
#define TG_SYS_SDL_SCREEN_MODE_EXPAND_AS_ENUM(a,b) a,
#define TG_SYS_SDL_SCREEN_MODE_EXPAND_AS_TABLE_ENTRY(a,b) { TOSTRING(a), (b) },
#define TG_SYS_SDL_SCREEN_MODE_TABLE(ENTRY) \
        ENTRY( WINDOWED,           0 ) \
        ENTRY( FULLSCREEN,         SDL_WINDOW_FULLSCREEN ) \
        ENTRY( FULLSCREEN_DESKTOP, SDL_WINDOW_FULLSCREEN_DESKTOP )
// NOTE(theGiallo): SDL_WINDOW_FULLSCREEN_DESKTOP ignores given res and uses
// native one, but it's a fake one, does not change videomode
#if 0
enum
Sys_Screen_Mode : u8
{
	SYS_SCREEN_MODE_TABLE( SYS_SCREEN_MODE_EXPAND_AS_ENUM )
	// ---
	SYS_SCREEN_MODE_COUNT
};
#endif

struct
Screen_Mode_Table_Entry
{
	const char * str;
	u32          sdl_flag;
};

struct
GL_Context
{
	SDL_GLContext sdl_context;
	GL_Version    version;
};

struct
Window
{
	GL_Context   gl_context;
	SDL_Window * sdl_window;
	const char * window_name;
	u32          sdl_window_id;
	bool         vsync;
	u8           id;
};

#define TG_SYS_SDL_INVALID_WINDOW_ID 255
#define TG_SYS_SDL_MAX_WINDOWS 255

struct
Controller
{
	SDL_GameController * sdl_controller;
	SDL_Joystick       * sdl_joystick;
	const char * name;
	s32 sdl_id;
	u8 id;
};
#define TG_SYS_SDL_INVALID_CONTROLLER_ID 255
#define TG_SYS_SDL_MAX_CONTROLLERS 64


struct
Err_Table_Entry
{
	const char * name,
	           * description;
};

struct
Tables
{
	Screen_Mode_Table_Entry screen_modes[(s32)tg::sys::Screen_Mode::__COUNT];
	Err_Table_Entry         errors      [(s32)Err_Codes::__COUNT_PLUS_ONE-1];
};
struct
SDL_Stuff
{
	struct
	Windows
	{
		Window windows[TG_SYS_SDL_MAX_WINDOWS];
		u8 windows_in_use[BITNSLOTS(TG_SYS_SDL_MAX_WINDOWS)];
		u32 windows_count;
	} windows;

	struct
	Input
	{
		struct
		Controllers
		{
			Controller controllers[TG_SYS_SDL_MAX_CONTROLLERS];
			u32 controllers_count;
			u8 controllers_mask[BITNSLOTS( TG_SYS_SDL_MAX_CONTROLLERS )];
			u8 sys_id_from_sdl_id_table[256];
		} controllers;
	} input;

	void * event_callback_data;
	Event_Callback * event_callback;

	struct
	OGL
	{
		bool fb_srgb_if_available;
		bool fb_is_srgb;
		bool vsync;
		bool vsync_is_supported;
#if WE_LOAD_OUR_GL
		bool were_gl_funcs_loaded;
#else
		bool was_glew_initialized;
#endif
		// NOTE(theGiallo): for now, using glew no different contexts
		// TODO(theGiallo, 2016-02-26): use glew mx or handmade code
		GL_Version gl_version;
	} ogl;

	Time perf_time_coefficient;

	struct
	Audio
	{
		SDL_mutex * mutex;
		SDL_AudioDeviceID sdl_audio_dev;
		void * callback_data;
		tg::sys::Audio_Callback * callback;
	} audio;
};

#define TG_SYS_SDL_MAX_THREADS_NUM 32
struct
Threads_Agenda
{
	u64 threads_ids[TG_SYS_SDL_MAX_THREADS_NUM];
};

struct
App_Data
{
	Cached_Time cached_time;
};

struct
Glob
{
	Tables         tables;
	SDL_Stuff      sdl_stuff;
	Threads_Agenda threads_agenda;
	App_Data       app;
};

Impl glob_impl;
Glob glob;

void
SDL_LockAudio_For_Real()
{
	SDL_LockMutex( glob.sdl_stuff.audio.mutex );
}

void
SDL_UnlockAudio_For_Real()
{
	SDL_UnlockMutex( glob.sdl_stuff.audio.mutex );
}


static
void
dumb_sdl_audio_callback( void *userdata, Uint8 *stream, int _len )
{
	(void)userdata;
	(void)stream;

	if ( glob.sdl_stuff.audio.callback )
	{
		f32 * buf = (f32*) stream;
		u32 len = _len / sizeof( f32 );

		// NOTE(theGiallo): this is because SDL_LockAudio does not really get
		// called before this callback, so we have to get redundant, until
		// those damn "FIXME" in SDL_audio.c:632 gets solved. :@
		SDL_LockAudio_For_Real();

		// TODO(theGiallo, 2021-04-11): these two 0s were sys_api and game_mem, do we need to pass something?
		glob.sdl_stuff.audio.callback( 0, 0, buf, len );

		SDL_UnlockAudio_For_Real();
		return;
	} else
	{
		memset( stream, 0, _len );
		//return;
	}

#if 0
	Time curr_time = get_time_in_sec();
	usleep( 0.5 * 42667 );
	static Time previous_call_time = curr_time;
	log_info( "%f", curr_time );
	//log_info( "getpid       %d",  getpid() );
	//log_info( "gettid       %lu", syscall( SYS_gettid ) );
	//log_info( "pthread_self %lu", pthread_self() );
	//log_info( "audio callback; threadid: %lu  len = %d  delta_time = %f", SDL_ThreadID(), _len, curr_time - previous_call_time );
	previous_call_time = curr_time;
	static s32 adv = 0;

	static const Time sound_duration = 0.125;
	static Time sound_played = 0.0;
	static const Time sound_delay = 1.0 - sound_duration;
	static Time next_time = curr_time;
	static u32 samples_played = 0;

	Time sym_time = curr_time;
	Time pi2 = 3.141592653589793238462643383279 * 2.0;
	f32 * f32_str = (f32*)stream;
	u32 f32_str_len = _len / sizeof( f32 );
	for ( u32 i = 0; i < f32_str_len; ++i )
	{
		if ( 0 == ( i % 2 ) )
		{
			++adv;
		} else
		{
		}
		#define SIN_OF_FREQ( freq ) ( sin( ( adv * pi2 * (freq) ) / AUDIO_SAMPLING_FREQUENCY ) )
		f32 v = ( SIN_OF_FREQ( 440 ) +
		          SIN_OF_FREQ( 277 ) +
		          SIN_OF_FREQ( 330 )   ) / 3.0;
		f32 volume = 0.0f;//0.000125f;
		f32_str[i] = 0.0f;//volume * v;
		#undef SIN_OF_FREQ

		#if 0
		if ( sym_time >= next_time )
		{
			f32 freq = 440;
			//f32 v = sin( sound_played * pi2 * freq );
			f32_str[i] = volume * v;
			if ( 1 == i % 2 )
			{
				++samples_played;
				sound_played += 1.0 / AUDIO_SAMPLING_FREQUENCY;
				sym_time = curr_time + (Time)(i>>1) / AUDIO_SAMPLING_FREQUENCY;
				if ( sound_played >= sound_duration )
				{
					sound_played = 0.0;
					next_time = sym_time + sound_delay;
					samples_played = 0;
				}
			}
		}
		#endif
	}
#endif
}


bool
init_SDL_stuff( SDL_Stuff * sdl_stuff )
{
	bool ret = true;

	log_dbg( "going to init SDL" );
	int i_res =
	SDL_Init( SDL_INIT_VIDEO
	        | SDL_INIT_EVENTS
	        | SDL_INIT_AUDIO
	        );
	if ( SDL_RET_IS_ERROR( i_res ) )
	{
		// TODO(theGiallo, 2016-02-24): do something! we are failing the init!!!
		ILLEGAL_PATH();
	}

	// NOTE(theGiallo): we init game controllers after events so device events are fired at startup
	i_res =
	SDL_InitSubSystem( SDL_INIT_GAMECONTROLLER );
	if ( SDL_RET_IS_ERROR( i_res ) )
	{
		// TODO(theGiallo, 2016-02-24): do something! we are failing the init!!!
		ILLEGAL_PATH();
	}

	u64 freq = SDL_GetPerformanceFrequency();
	sdl_stuff->perf_time_coefficient = 1.0 / (Time)freq;

	for ( u16 i = 0; i < ARRAY_SIZE( sdl_stuff->input.controllers.sys_id_from_sdl_id_table ); ++i )
	{
		sdl_stuff->input.controllers.sys_id_from_sdl_id_table[i] = TG_SYS_SDL_INVALID_CONTROLLER_ID;
	}

	for ( s32 i = 0; i < SDL_GetNumAudioDrivers(); ++i )
	{
		log_info( "Audio driver %d: %s", i, SDL_GetAudioDriver( i ) );
	}

	const char* driver_name = SDL_GetCurrentAudioDriver();
	if ( driver_name )
	{
		log_info( "Audio subsystem initialized; driver = %s.", driver_name );
	} else
	{
		log_info( "Audio subsystem not initialized." );
	}

	s32 count = SDL_GetNumAudioDevices( 0 );
	for ( s32 i = 0; i < count; ++i )
	{
		log_info( "Audio device %d: %s", i, SDL_GetAudioDeviceName( i, 0 ) );
	}

	// TODO(theGiallo, 2021-04-11): make these changeable at runtime
	const f32 AUDIO_SAMPLING_FREQUENCY = 44100.0;
	const s32 AUDIO_SAMPLES_IN_BUF     = (512*1);

	{
		SDL_AudioSpec want, have;

		SDL_memset( &want, 0, sizeof( want ) ); /* or SDL_zero(want) */
		want.freq = AUDIO_SAMPLING_FREQUENCY;
		want.format = AUDIO_F32;
		want.channels = 2;
		want.samples = AUDIO_SAMPLES_IN_BUF;
		want.callback = dumb_sdl_audio_callback; // you wrote this function elsewhere.

		sdl_stuff->audio.sdl_audio_dev =
		SDL_OpenAudioDevice( /*SDL_GetAudioDeviceName( 1, 0 )*/NULL, 0, &want, &have, 0 );
		if ( sdl_stuff->audio.sdl_audio_dev == 0 )
		{
			log_info( "Failed to open audio: %s", SDL_GetError() );
		} else
		{
			if ( have.format != want.format )
			{ // we let this one thing change.
				log_info( "We didn't get Float32 audio format." );
			}

			if ( have.freq != want.freq )
			{
				log_info( "We didn't get %d sampling frequency but %d", want.freq, have.freq );
			}

			if ( have.samples != want.samples )
			{
				log_info( "We didn't get %d samples but %d", want.samples, have.samples );
			}

			log_info( "\nformat = %u\nfreq = %d\nsamples = %d", have.format, have.freq, have.samples );

			SDL_PauseAudioDevice( sdl_stuff->audio.sdl_audio_dev, 0 ); // start audio playing.
		}
	}
	glob.sdl_stuff.audio.mutex = SDL_CreateMutex();

	#if WE_LOAD_OUR_GL
	i_res = SDL_GL_LoadLibrary( NULL );
	if ( SDL_RET_IS_ERROR( i_res ) )
	{
		log_err( "Error loading OpenGL library: %s ", SDL_GetError() );
		ret = false;
	} else
	{
		log_info( "OpenGL library loaded" );
	}
	#endif

	log_dbg( "SDL initialized" );
	return ret;
}

bool
load_gl_functions()
{
#if 0
	bool ret = true;

	#define M_GLUE2(a,b) a##b
	#define LOAD_GL_FUNCTION( fname ) \
	fname = ( M_GLUE2(fname,_t )* ) SDL_GL_GetProcAddress( TOSTRING( fname ) ); \
	if ( !fname /*|| !SDL_GL_ExtensionSupported( TOSTRING( fname ) )*/ )    \
	{                                                                           \
		log_dbg( "couldn't load " TOSTRING( fname ) );                          \
		if ( !M_GLUE2(fname,_IS_OPTIONAL) )                                     \
			ret = false;                                                        \
		fname = NULL;                                                           \
	}

	#include "gl_funcs_load.inc"

	return ret;

	#undef M_GLUE2
	#undef LOAD_GL_FUNCTION
#else
	return tg::ogl::load_gl_functions( tg_sys_get_gl_func_loader() );
#endif
}

s32
create_window( SDL_Stuff::Windows * windows,
               SDL_Stuff::OGL     * ogl,
               const char * win_name,
               GL_Version gl_version,
               bool share_context,
               bool vsync,
               Screen_Mode screen_mode,
               bool max_screen_size,
               u32 width, u32 height
             )
{
	s32 ret = TG_SYS_SDL_MAX_WINDOWS;

	if ( windows->windows_count >= TG_SYS_SDL_MAX_WINDOWS )
	{
		--windows->windows_count;
		log_err( "max number of windows (" TOSTRING( SYS_MAX_WINDOWS )\
		         ")reached! Can't create a new one!" );
		ret = -s32(Err_Codes::TOO_MANY_WINDOWS);
		return ret;
	}

	s32 w_id = TG_SYS_SDL_MAX_WINDOWS;
	for_0M( i, TG_SYS_SDL_MAX_WINDOWS )
	{
		if ( ! BITTEST( windows->windows_in_use, i ) )
		{
			w_id = i;
			BITSET( windows->windows_in_use, i );
			break;
		}
	}
	if ( w_id == TG_SYS_SDL_MAX_WINDOWS )
	{
		ret = -s32(Err_Codes::TOO_MANY_WINDOWS);
		return ret;
	}
	++windows->windows_count;

	GL_Context gl_context;
	SDL_GLContext current_sdl_gl_context = SDL_GL_GetCurrentContext();
	bool trying_srgb = false;

	s32 pos_x = SDL_WINDOWPOS_CENTERED, pos_y = SDL_WINDOWPOS_CENTERED;
	if ( max_screen_size )
	{
		s32 w_w, w_h;
		w_w = w_h = -1;
		s32 display_id{};
		s32 num_displays = SDL_GetNumVideoDisplays();
		if ( num_displays < 0 )
		{
			log_err( "%s", SDL_GetError() );
		} else
		{
			for ( s32 d_id = 0; d_id < num_displays; ++d_id )
			{
				SDL_DisplayMode native_mode = {};
				s32 i_res = SDL_GetDesktopDisplayMode( d_id, &native_mode );
				if ( SDL_RET_IS_ERROR( i_res ) )
				{
					log_err( "%s", SDL_GetError() );
					ret = -s32(Err_Codes::NO_DISPLAYS);
					goto return_err;
				} else
				{
					log_info( "native mode for display %d has res %dx%d",
					          d_id, native_mode.w, native_mode.h );
				}
				if ( native_mode.w > w_w )
				{
					w_w = native_mode.w;
					w_h = native_mode.h;
					display_id = d_id;
				}
			}
		}
		SDL_Rect bounds;
		SDL_GetDisplayBounds( display_id, &bounds );
		pos_x = bounds.x;
		pos_y = bounds.y;
		width = w_w;
		height = w_h;
	}

	SDL_GL_SetAttribute( SDL_GL_SHARE_WITH_CURRENT_CONTEXT, share_context );

	if ( ogl->fb_srgb_if_available )
	{
		SDL_GL_SetAttribute( SDL_GL_FRAMEBUFFER_SRGB_CAPABLE, 1 );
		trying_srgb = true;
	}

	SDL_Window * sdl_window;
	do
	{
		log_dbg( "Trying to create an OpenGL window..." );
		sdl_window = SDL_CreateWindow(
		   win_name,  // title
		   pos_x, // x pos, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_UNDEFINED
		   pos_y, // y pos, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_UNDEFINED
		   width,                // width, in pixels
		   height,               // height, in pixels
		   SDL_WINDOW_SHOWN      // u32 flags: window options
		   | SDL_WINDOW_OPENGL
		   | SDL_WINDOW_RESIZABLE
		   | glob.tables.screen_modes[(s32)screen_mode].sdl_flag
		);
		if( sdl_window == NULL )
		{
			const char *error = SDL_GetError();
			// if ( *error != '\0' ){}
			log_err( "Could not create window. SDL error: %s", error );
			if ( trying_srgb )
			{
				trying_srgb = false;
				SDL_GL_SetAttribute( SDL_GL_FRAMEBUFFER_SRGB_CAPABLE, 0 );
				continue;
			}
			ret = -s32(Err_Codes::CREATING_WINDOW);
			goto return_err;
		}
		break;
	} while ( true );

	log_dbg( "window created" );

	// NOTE(theGiallo): create OGL context
	if ( ! share_context || ! current_sdl_gl_context )
	{
		s32 context_flags = {};
		context_flags |= SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG;
		#if DEBUG_GL
		// NOTE(theGiallo): for the debug callback
		context_flags |= SDL_GL_CONTEXT_DEBUG_FLAG;
		#endif
		SDL_GL_SetAttribute( SDL_GL_CONTEXT_FLAGS,
		                     context_flags );

		if ( gl_version.es )
		{
			SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK,
			                     SDL_GL_CONTEXT_PROFILE_ES |
			                     SDL_GL_CONTEXT_PROFILE_CORE );
		} else
		{
			SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK,
			                     SDL_GL_CONTEXT_PROFILE_CORE );
		}

		if ( SDL_RET_IS_ERROR(
		     SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION,
		                          gl_version.major ) ) )
		{
			log_err( "Error on SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, %d): %s",
			         gl_version.major, SDL_GetError() );
		}
		if ( SDL_RET_IS_ERROR(
		     SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION,
		                          gl_version.minor ) ) )
		{
			log_err( "Error on SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, %d): %s",
			         gl_version.minor, SDL_GetError() );
		}

		int srgb_capability = 0,
		    sdl_err =
		SDL_GL_GetAttribute( SDL_GL_FRAMEBUFFER_SRGB_CAPABLE, &srgb_capability );
		if ( !sdl_err )
		{
			log_dbg( "OpenGL default framebuffer is%s sRGB", srgb_capability?"":" not");
		}
		ogl->fb_is_srgb = srgb_capability;

		SDL_GLContext
		sdl_glcontext = SDL_GL_CreateContext( sdl_window );
		if ( sdl_glcontext == NULL )
		{
			const char *error = SDL_GetError();
			if ( *error != '\0' )
			{
				log_err( "Could not create GL context: %s", error);
				--windows->windows_count;

				SDL_DestroyWindow( sdl_window );
				if ( ( error = SDL_GetError() ) )
				{
					log_err( "error destroying the failed window: %s", error );
				}

				ret = -s32(Err_Codes::CREATING_CONTEXT);
				goto return_err;
			}
		}
		log_dbg( "OpenGL context created." );
		gl_context.version = gl_version;
		gl_context.sdl_context = sdl_glcontext;

		s32 res =
		SDL_GL_MakeCurrent( sdl_window,
		                    sdl_glcontext );
		if (res<0)
		{
			log_err( "[res=%d]SDL_GL_MakeCurrent failed %s", res, SDL_GetError() );
		}

		#if WE_LOAD_OUR_GL
		if ( !ogl->were_gl_funcs_loaded )
		{
			bool b_res = load_gl_functions();
			if ( !b_res )
			{
				log_err( "couldn't load all the OpenGL functions!" );
				ret = -s32(Err_Codes::LOADING_GL_FUNCTIONS);
				goto return_err;
			} else
			{
				log_info( "OpenGL functions loaded" );
				ogl->were_gl_funcs_loaded = true;
			}
		}
		#else
		// NOTE(theGiallo): init GLEW
		if ( !ogl->was_glew_initialized )
		{
			/**
			 * NOTE:
			 * From https://www.opengl.org/wiki/OpenGL_Loading_Library
			 *
			 * GLEW has a problem with core contexts. It calls
			 * glGetString(GL_EXTENSIONS), which causes GL_INVALID_ENUM on GL 3.2+ core
			 * context as soon as glewInit() is called. It also doesn't fetch the
			 * function pointers. The solution is for GLEW to use glGetStringi​ instead.
			 * The current version of GLEW is 1.10.0 but they still haven't corrected it.
			 * The only fix is to use glewExperimental for now:
			 *
			 *	  glewExperimental=TRUE;
			 *	  GLenum err=glewInit();
			 *	  if(err!=GLEW_OK)
			 *	  {
			 *		//Problem: glewInit failed, something is seriously wrong.
			 *		cout<<"glewInit failed, aborting."<<endl;
			 *	  }
			 *
			 *	glewExperimental is a variable that is already defined by GLEW. You must
			 * set it to GL_TRUE before calling glewInit().
			 *	You might still get GL_INVALID_ENUM (depending on the version of GLEW you
			 * use), but at least GLEW ignores glGetString(GL_EXTENSIONS) and gets all
			 * function pointers. If you are creating a GL context the old way or if you
			 * are creating a backward compatible context for GL 3.2+, then you don't
			 * need glewExperimental.
			 *	As with most other loaders, you should not include gl.h, glext.h​, or any
			 * other gl related header file before glew.h, otherwise you'll get an error
			 * message that you have included gl.h before glew.h. You shouldn't be
			 * including gl.h at all; glew.h replaces it.
			 *	GLEW also provides wglew.h which provides Windows specific GL functions
			 * (wgl functions). If you include wglext.h before wglew.h, GLEW will
			 * complain. GLEW also provides glxew.h for X windows systems. If you include
			 * glxext.h before glxew.h, GLEW will complain.
			 **/
			glewExperimental = GL_TRUE;
			GLenum err = glewInit(); // NOTE(theGiallo): read the huge comment above!
			if ( GLEW_OK != err )
			{
				log_err( "Error initializing GLEW: %s", glewGetErrorString( err ) );
			} else
			{
				ogl->was_glew_initialized = true;
			}
			if ( GLEW_VERSION_1_3 )
			{
				log_dbg( "GLEW 1.3" );
			}

			log_dbg( "GLEW version: %d.%d.%d",
			         GLEW_VERSION_MAJOR, GLEW_VERSION_MINOR, GLEW_VERSION_MICRO );
		}
		#endif

		if ( ogl->fb_is_srgb )
		{
			glEnable( GL_FRAMEBUFFER_SRGB );
		} else
		{
			glDisable( GL_FRAMEBUFFER_SRGB );
		}

		#if DEBUG_GL // NOTE(theGiallo): register OpenGL debug callback function
			if ( glDebugMessageCallback != NULL )
			{
				log_info( "glDebugMessageCallback available" );
				// Enable synchronous callback. This ensures that your callback function
				// is called right after an error has occurred. This capability is not
				// defined in the AMD version.
				glEnable( GL_DEBUG_OUTPUT_SYNCHRONOUS );
				glDebugMessageCallback( opengl_callback_function, NULL );
				GLuint unused_ids = 0;
				// NOTE: GL_DONT_CARE because we want all of them
				glDebugMessageControl( GL_DONT_CARE, // source
									GL_DONT_CARE, // type
									GL_DONT_CARE, // severity
									0,
									&unused_ids,
									GL_TRUE );
			} else
			if ( glDebugMessageCallbackARB != NULL )
			{
				log_info( "glDebugMessageCallbackARB available" );
				// Enable synchronous callback. This ensures that your callback function
				// is called right after an error has occurred. This capability is not
				// defined in the AMD version.
				glEnable( GL_DEBUG_OUTPUT_SYNCHRONOUS );
				glDebugMessageCallbackARB( opengl_callback_function, NULL );
			} else
			if ( glDebugMessageCallbackAMD != NULL )
			{
				log_info( "glDebugMessageCallbackAMD available but not implemented" );
				// Enable synchronous callback. This ensures that your callback function
				// is called right after an error has occurred. This capability is not
				// defined in the AMD version.
				// glEnable( GL_DEBUG_OUTPUT_SYNCHRONOUS );
				// TODO(theGiallo, 2015/09/29): maybe specialise for AMD?
				// glDebugMessageCallbackAMD( opengl_callback_function, NULL );
			} else
			{
				log_info( "glDebugMessageCallback not available" );
			}
		#endif
	} else
	{
		int major, minor, es;
		SDL_GL_GetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, &major );
		SDL_GL_GetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, &minor );
		SDL_GL_GetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, &es );
		if ( !current_sdl_gl_context )
		{
			const char * error = SDL_GetError();
			log_err( "current context is NULL and we are trying to create a window sharing it! %s", error );
			SDL_DestroyWindow( sdl_window );
			if ( ( error = SDL_GetError() ) )
			{
				log_err( "error destroying the failed window: %s", error );
			}
		}
		gl_context.version.major = major;
		gl_context.version.minor = minor;
		gl_context.version.es = (bool)( es & SDL_GL_CONTEXT_PROFILE_ES );
		gl_context.sdl_context = current_sdl_gl_context;

		s32 res =
		SDL_GL_MakeCurrent( sdl_window,
		                    gl_context.sdl_context );
		if ( res < 0 )
		{
			log_err( "[res=%d]SDL_GL_MakeCurrent failed %s", res, SDL_GetError() );
		}
	}

	#if DEBUG_GL // NOTE(theGiallo): check default framebuffer encoding
	{
		GLint color_encoding = ZERO_STRUCT;
		glBindFramebuffer( GL_FRAMEBUFFER, 0 );
		glGetFramebufferAttachmentParameteriv( GL_FRAMEBUFFER, GL_BACK,
		   GL_FRAMEBUFFER_ATTACHMENT_COLOR_ENCODING, &color_encoding );
		switch ( color_encoding )
		{
			case GL_SRGB:
				log_info( "BACK of default framebuffer is encoded in SRGB" );
				break;
			case GL_LINEAR:
				log_info( "BACK of default framebuffer is encoded linearly" );
				break;
			default:
				log_err( "Unknonwn encoding value! %d", color_encoding );
				break;
		}
		glGetFramebufferAttachmentParameteriv( GL_FRAMEBUFFER, GL_FRONT,
		           GL_FRAMEBUFFER_ATTACHMENT_COLOR_ENCODING, &color_encoding );
		switch ( color_encoding )
		{
			case GL_SRGB:
				log_info( "FRONT of default framebuffer is encoded in SRGB" );
				break;
			case GL_LINEAR:
				log_info( "FRONT of default framebuffer is encoded linearly" );
				break;
			default:
				log_err( "Unknonwn encoding value! %d", color_encoding );
				break;
		}
	}
	#endif

	if( SDL_RET_IS_ERROR( SDL_GL_SetSwapInterval( true ) ) )
	{
		ogl->vsync_is_supported = false;
		vsync = false;
		log_err( "Swap interval not supported" );
	} else
	{
		ogl->vsync_is_supported = true;
		if ( vsync )
		{
			log_info( "V-Sync enabled" );
		} else
		{
			if ( SDL_RET_IS_ERROR( SDL_GL_SetSwapInterval( false ) ) )
			{
				const char * error = SDL_GetError();
				log_err( "Error turning vsync off: %s\n", error );
				vsync = true;
			} else
			{
				log_info( "V-Sync disabled" );
			}
		}
		s32 swap_interval = SDL_GL_GetSwapInterval();
		switch ( swap_interval )
		{
			case  0:
				log_dbg( "swap_interval: disabled" );
				break;
			case  1:
				log_dbg( "swap_interval: enabled" );
				break;
			case -1:
				log_dbg( "swap_interval: late swap tearing" );
				break;
			default:
				ILLEGAL_PATH();
				break;
		}
		if( vsync != ( 1 == swap_interval ) )
		{
			log_err( "swap interval is not as requested" );
		}
	}

	// NOTE(theGiallo): save window into array
	windows->windows[w_id].sdl_window = sdl_window;
	windows->windows[w_id].sdl_window_id = SDL_GetWindowID( sdl_window );
	windows->windows[w_id].window_name = win_name;
	windows->windows[w_id].gl_context = gl_context;
	windows->windows[w_id].vsync = vsync;
	windows->windows[w_id].id = w_id;

	return w_id;

	return_err:

	BITCLEAR( windows->windows_in_use, w_id );
	--windows->windows_count;
	return ret;
}

u8
window_id_from_sdl_window_id( u32 sdl_window_id )
{
	u8 ret = TG_SYS_SDL_INVALID_WINDOW_ID;
	for ( u8 i = 0; i < glob.sdl_stuff.windows.windows_count; ++i )
	{
		if ( sdl_window_id == glob.sdl_stuff.windows.windows[i].sdl_window_id )
		{
			ret = i;
		}
	}
	return ret;
}

inline
Time
time_from_sdl_ticks( u32 ticks )
{
	Time ret = 0.001 * (Time)ticks;

	return ret;
}

Time
get_sys_secs()
{
	Time ret = s_time();
	return ret;
}
Time
get_perf_secs()
{
	u64 pc = SDL_GetPerformanceCounter();

	Time ret = (Time)pc * glob.sdl_stuff.perf_time_coefficient;
	return ret;
}
Time
get_perf_run_secs()
{
	Time ret = tg::sys::sdl::get_perf_secs() - tg::sys::sdl::glob.app.cached_time.base_perf_time;
	return ret;
}
Time
get_sys_run_secs()
{
	Time ret = get_sys_secs() - tg::sys::sdl::glob.app.cached_time.base_sys_time;
	return ret;
}


}; // namespace tg::sys::sdl

using namespace tg::sys::sdl;
using namespace tg::sys;

TG_DL_EXPORTED
TG_SYS_GET_ERROR_NAME( tg_sys_get_error_name )
{
	const char * ret = "UNKNOWN ERROR";

	if ( error >= 0 )
	{
		ret = "NO ERROR";
	} else
	if ( TG_SYS_SDL_ERR_IS_ERRNO( error ) )
	{
		ret = "ERRNO ERROR";
	} else
	{
		s64 tid = TG_SYS_SDL_ERR_TO_TABLE_ID( error );
		if ( tid < (s64)Err_Codes::__COUNT_PLUS_ONE )
		{
			ret = glob.tables.errors[tid].name;
		}
	}

	return ret;
}
TG_DL_EXPORTED
TG_SYS_GET_ERROR_DESCRIPTION( tg_sys_get_error_description )
{
	const char * ret = "UNKNOWN ERROR";

	if ( error >= 0 )
	{
		ret = "NO ERROR";
	} else
	if ( TG_SYS_SDL_ERR_IS_ERRNO( error ) )
	{
		int the_errno = TG_SYS_SDL_ERRNO_FROM_ERR( error );
		ret = strerror_l( the_errno, uselocale( (locale_t)0 ) );
	} else
	{
		s64 tid = TG_SYS_SDL_ERR_TO_TABLE_ID( error );
		if ( tid < (s64)Err_Codes::__COUNT_PLUS_ONE )
		{
			ret = glob.tables.errors[tid].description;
		}
	}

	return ret;
}
TG_DL_EXPORTED
TG_SYS_SET_VSYNC( tg_sys_set_vsync )
{
	bool ret = true;

	for ( u32 i = 0; i < glob.sdl_stuff.windows.windows_count; ++i )
	{
		SDL_GL_MakeCurrent( glob.sdl_stuff.windows.windows[i].sdl_window, glob.sdl_stuff.windows.windows[i].gl_context.sdl_context );
		if ( SDL_RET_IS_ERROR( SDL_GL_SetSwapInterval( vsync ) ))
		{
			log_err( "Error setting vsync %s for window '%s'", vsync?"on":"off", glob.sdl_stuff.windows.windows[i].window_name );
			ret = false;
		} else
		{
			glob.sdl_stuff.windows.windows[i].vsync = vsync;
		}
	}

	return ret;
}
TG_DL_EXPORTED
TG_SYS_SET_SCREEN_MODE( tg_sys_set_screen_mode )
{
	tg_assert( window_id < TG_SYS_SDL_MAX_WINDOWS );

	bool ret = true;

	Window * w = glob.sdl_stuff.windows.windows + window_id;
	u32 sdl_flag = glob.tables.screen_modes[(s32)mode].sdl_flag;
	s32 display_id = SDL_GetWindowDisplayIndex( w->sdl_window );
	SDL_DisplayMode display_mode;

	s32 i_res = SDL_GetDesktopDisplayMode( display_id, &display_mode );

	if ( SDL_RET_IS_ERROR( i_res ) )
	{
		log_err( "%s", SDL_GetError() );
	} else
	{
		SDL_SetWindowSize( w->sdl_window, display_mode.w, display_mode.h );
	}

	if ( SDL_RET_IS_ERROR( SDL_SetWindowFullscreen( w->sdl_window, sdl_flag ) ) )
	{
		const char * error = SDL_GetError();
		log_err( "error setting window fullscreen to %s: %s", glob.tables.screen_modes[(s32)mode].str, error );
		ret = false;
	}

	return ret;
}
TG_DL_EXPORTED
TG_SYS_GET_SCREEN_MODE( tg_sys_get_screen_mode )
{
	Screen_Mode mode = {};

	tg_assert( window_id < TG_SYS_SDL_MAX_WINDOWS );
	SDL_Window * w = glob.sdl_stuff.windows.windows[window_id].sdl_window;

	u32 flags = SDL_GetWindowFlags( w );

	if ( flags & SDL_WINDOW_FULLSCREEN )
	{
		mode = Screen_Mode::FULLSCREEN;
	} else
	if ( flags & SDL_WINDOW_FULLSCREEN_DESKTOP )
	{
		mode = Screen_Mode::FULLSCREEN_DESKTOP;
	} else
	{
		mode = Screen_Mode::WINDOWED;
	}

	return mode;
}
TG_DL_EXPORTED
TG_SYS_GET_WINDOW_SIZE( tg_sys_get_window_size )
{
	tg_assert( window_id >= 0 );

	if ( window_id == TG_SYS_SDL_INVALID_WINDOW_ID )
	{
		*width = *height = 0;
		return;
	}

	tg_assert( (u32)window_id < glob.sdl_stuff.windows.windows_count );
	tg_assert( glob.sdl_stuff.windows.windows[window_id].sdl_window );


	s32 w,h;
	SDL_GetWindowSize( glob.sdl_stuff.windows.windows[window_id].sdl_window, &w, &h );
	*width  = (u32)w;
	*height = (u32)h;
}
TG_DL_EXPORTED
TG_SYS_SET_WINDOW_SIZE( tg_sys_set_window_size )
{
	tg_assert( window_id >= 0 );
	tg_assert( (u32)window_id < glob.sdl_stuff.windows.windows_count );
	tg_assert( glob.sdl_stuff.windows.windows[window_id].sdl_window );

	SDL_SetWindowSize( glob.sdl_stuff.windows.windows[window_id].sdl_window, width, height );
}
TG_DL_EXPORTED
TG_SYS_GET_WINDOW_NAME( tg_sys_get_window_name )
{
	if ( window_id < 0
	  || window_id == TG_SYS_SDL_INVALID_WINDOW_ID
	  || (u32)window_id >= glob.sdl_stuff.windows.windows_count )
	{
		return -s32(Err_Codes::INVALID_WINDOW_ID);
	}

	tg_assert( glob.sdl_stuff.windows.windows[window_id].sdl_window );

	const char * title = SDL_GetWindowTitle( glob.sdl_stuff.windows.windows[window_id].sdl_window );
	for_0M( i, TG_SYS_MAX_WINDOW_NAME_CHARS )
	{
		out_window_name_256[i] = title[i];
		if ( ! title[i] )
		{
			break;
		}
	}

	return 0;
}
TG_DL_EXPORTED
TG_SYS_SET_WINDOW_NAME( tg_sys_set_window_name )
{
	if ( window_id < 0
	  || window_id == TG_SYS_SDL_INVALID_WINDOW_ID
	  || (u32)window_id >= glob.sdl_stuff.windows.windows_count )
	{
		return -s32(Err_Codes::INVALID_WINDOW_ID);
	}

	tg_assert( glob.sdl_stuff.windows.windows[window_id].sdl_window );

	//u64 start = ns_time();
	SDL_SetWindowTitle( glob.sdl_stuff.windows.windows[window_id].sdl_window, window_name_256 );
	//u64 end = ns_time();
	//u64 delta_ns = end - start;
	//log_dbg( "SDL_SetWindowTitle took %lu ns ( %f ms )", delta_ns, f64( delta_ns ) * 1e-4 );

	return 0;
}
TG_DL_EXPORTED
TG_SYS_CREATE_WINDOW( tg_sys_create_window )
{
	s32 ret;

	ret = create_window( &glob.sdl_stuff.windows,
	                     &glob.sdl_stuff.ogl,
	                     win_name, gl_version,
	                     share_context, vsync, screen_mode, max_screen_size,
	                     width, height );

	return ret;
}
TG_DL_EXPORTED
TG_SYS_MAKE_WINDOW_CURRENT( tg_sys_make_window_current )
{
	tg_assert( window_id >= 0 );
	tg_assert( window_id < (s32)glob.sdl_stuff.windows.windows_count );
	tg_assert( glob.sdl_stuff.windows.windows_count <= TG_SYS_SDL_MAX_WINDOWS );

	SDL_GLContext * sdl_gl_context = &glob.sdl_stuff.windows.windows[window_id].gl_context.sdl_context;
	SDL_Window    * sdl_window     =  glob.sdl_stuff.windows.windows[window_id].sdl_window;

	tg_assert( *sdl_gl_context );
	tg_assert( sdl_window );

	s32 i_res =
	SDL_GL_MakeCurrent( sdl_window, *sdl_gl_context );
	if ( SDL_RET_IS_ERROR( i_res ) )
	{
		log_err( "[res=%d w_id=%d]SDL_GL_MakeCurrent failed %s", i_res, window_id, SDL_GetError() );
		return -s32(Err_Codes::MAKING_WINDOW_CURRENT);
	}

	return 0;
}
TG_DL_EXPORTED
TG_SYS_SWAP_WINDOW( tg_sys_swap_window )
{
	tg_assert( window_id >= 0 );
	tg_assert( window_id < (s32)glob.sdl_stuff.windows.windows_count );
	tg_assert( glob.sdl_stuff.windows.windows_count <= TG_SYS_SDL_MAX_WINDOWS );

	SDL_Window * sdl_window = glob.sdl_stuff.windows.windows[window_id].sdl_window;

	tg_assert( sdl_window );

	SDL_GL_SwapWindow( sdl_window );
}
TG_DL_EXPORTED
TG_SYS_CLOSE_WINDOW( tg_sys_close_window )
{
	tg_assert( window_id >= 0 );
	tg_assert( window_id < (s32)glob.sdl_stuff.windows.windows_count );
	tg_assert( glob.sdl_stuff.windows.windows_count <= TG_SYS_SDL_MAX_WINDOWS );

	SDL_Window * sdl_window = glob.sdl_stuff.windows.windows[window_id].sdl_window;
	BITCLEAR( glob.sdl_stuff.windows.windows_in_use, window_id );
	--glob.sdl_stuff.windows.windows_count;

	tg_assert( sdl_window );

	SDL_DestroyWindow( sdl_window );
}
TG_DL_EXPORTED
TG_SYS_EXIT( tg_sys_exit )
{
	// TODO(theGiallo, 2021-04-12): IMPLEMENT
	// was: glob.running = false;
}
TG_DL_EXPORTED
TG_SYS_GET_FILE_MODIFIED_TIME( tg_sys_get_file_modified_time )
{
	s64 ret;
	time_t sys_time = {};

	struct stat file_stat;
	if ( stat( file_path, &file_stat ) < 0 )
	{
		ret = TG_SYS_SDL_ERRNO_TO_SYS_ERROR( errno );
		return ret;
	} else
	{
		sys_time = file_stat.st_mtime;
	}

	*modified_time = (Time)sys_time - glob.app.cached_time.base_sys_time;

	ret = 0;
	return ret;
}
TG_DL_EXPORTED
TG_SYS_GET_FILE_SIZE( tg_sys_get_file_size )
{
#if 0
	s64 ret = 0;

	FILE * fp = fopen( file_path, "rb" );
	if ( !fp )
	{
		return -SYS_ERR_FILE_OPEN_FAILED;
	}

	fseek( fp, 0, SEEK_END ); // NOTE(theGiallo): SEEK_END is non std, so ~non-portable
	ret = ftell ( fp );

	fclose( fp );

	return ret;
#else

	s64 ret = 0;

	struct stat buf;

	if ( stat( file_path, &buf ) < 0 )
	{
		ret = TG_SYS_SDL_ERRNO_TO_SYS_ERROR( errno );
		return ret;
	}

	ret = buf.st_size;

	return ret;
#endif
}
TG_DL_EXPORTED
TG_SYS_READ_ENTIRE_FILE( tg_sys_read_entire_file )
{
	s64 ret = 0;

	FILE * fp = fopen( file_path, "rb" );
	if ( !fp )
	{
		return -s32(Err_Codes::FILE_OPEN_FAILED);
	}

	fseek( fp, 0, SEEK_END ); // NOTE(theGiallo): SEEK_END is non std, so ~non-portable
	s64 size  = ftell ( fp );
	rewind( fp );
	ret = fread( mem, 1, MIN( size, mem_size ), fp );
	if ( size > mem_size )
	{
		// TODO(theGiallo, 2016-03-08): sth
		log_info( "warning: reading file '%s' of size %ld into a smaller buffer of %ld", file_path, size, mem_size );
	}
	if ( ret!=MIN( size, mem_size ) )
	{
		// TODO(theGiallo, 2016-03-08): sth
	}

	fclose ( fp );

	return ret;
}
TG_DL_EXPORTED
TG_SYS_LIST_DIRECTORY_FAST( tg_sys_list_directory_fast )
{
	s64 ret = 0;

#define CHECK_MEM_SIZE(s) \
	if ( available_mem_size < s )\
	{\
		ret = -s32(Err_Codes::MEMORY_BUFFER_TOO_SMALL);\
		return ret;\
	}

	DIR * dir;
	int fd;
	struct dirent * ent;

	fd = open( dir_path, O_RDONLY | O_RDONLY );
	if ( fd < 0 )
	{
		ret = TG_SYS_SDL_ERRNO_TO_SYS_ERROR( errno );
		return ret;
	}

	dir = fdopendir( fd );
	if ( !dir )
	{
		ret = TG_SYS_SDL_ERRNO_TO_SYS_ERROR( errno );
		close( fd );
		return ret;
	}

	u32 path_bytes_count = 0;
	u32 full_path_bytes_count = 0;
	while ( dir_path[path_bytes_count++] );
	u8 full_path[path_bytes_count+1];
	memcpy( full_path, dir_path, path_bytes_count );
	full_path_bytes_count = path_bytes_count;
	if ( full_path[path_bytes_count-2] != '/' )
	{
		full_path[path_bytes_count-1] = '/';
		full_path[path_bytes_count] = 0;
		++full_path_bytes_count;
	}

	u8 * available_mem = mem;
	s64 available_mem_size = mem_size;
	u32 * files_count = (u32*)available_mem;
	available_mem_size -= sizeof(*files_count);
	available_mem      += sizeof(*files_count);
	CHECK_MEM_SIZE(0);
	u8 ** curr_file = 0;
	u8 *** next_file = 0;


	for ( ent = readdir( dir );
	      ent;
	      ent = readdir( dir ) )
	{
		u32 name_bytes_count = 0;
		while ( ent->d_name[name_bytes_count++] );

		if ( next_file )
		{
			*next_file = (u8**)available_mem;
		}
		curr_file = (u8**)available_mem;
		available_mem_size -= sizeof(*curr_file);
		available_mem      += sizeof(*curr_file);
		next_file = (u8***)available_mem;
		available_mem_size -= sizeof(*next_file);
		available_mem      += sizeof(*next_file);
		CHECK_MEM_SIZE(0);
		//tg_assert( (u64)next_file < (u64)(mem + mem_size) );
		*next_file = 0;

		//tg_assert( (u64)curr_file < (u64)(mem + mem_size) );
		*curr_file = available_mem;
		u8 * curr_file_target = *curr_file;
		CHECK_MEM_SIZE(1);
		//tg_assert( (u64)*curr_file < (u64)(mem + mem_size) );
		curr_file_target[0] = 0;
		if ( options_flags & (u32)tg::sys::List_Directory_Option::FULL_PATHS )
		{
			CHECK_MEM_SIZE(full_path_bytes_count);
			available_mem_size -= full_path_bytes_count-1;
			available_mem      += full_path_bytes_count-1;
			curr_file_target   += full_path_bytes_count-1;
			memcpy( curr_file_target, full_path, full_path_bytes_count );
		}
		CHECK_MEM_SIZE(name_bytes_count);
		available_mem_size -= name_bytes_count;
		available_mem      += name_bytes_count;
		memcpy( curr_file_target, ent->d_name, name_bytes_count );
		++*files_count;
	}

	ret = *files_count;

	closedir( dir );
	close( fd );

	return ret;
#undef CHECK_MEM_SIZE
}
TG_DL_EXPORTED
TG_SYS_LIST_DIRECTORY( tg_sys_list_directory )
{
	s64 ret = 0;

#define CHECK_MEM_SIZE(s) \
	if ( available_mem_size < s )\
	{\
		ret = -s32(Err_Codes::MEMORY_BUFFER_TOO_SMALL);\
		return ret;\
	}

	DIR * dir;
	int fd;
	struct dirent * ent;

	fd = open( dir_path, O_RDONLY | O_RDONLY );
	if ( fd < 0 )
	{
		ret = TG_SYS_SDL_ERRNO_TO_SYS_ERROR( errno );
		return ret;
	}

	dir = fdopendir( fd );
	if ( !dir )
	{
		ret = TG_SYS_SDL_ERRNO_TO_SYS_ERROR( errno );
		close( fd );
		return ret;
	}

	s64 files_count = 0;
	for ( ent = readdir( dir );
	      ent;
	      ent = readdir( dir ) )
	{
		++files_count;
	}
	rewinddir( dir );

	u32 path_bytes_count = 0;
	u32 full_path_bytes_count = 0;
	while ( dir_path[path_bytes_count++] );
	u8 full_path[path_bytes_count+1];
	memcpy( full_path, dir_path, path_bytes_count );
	full_path_bytes_count = path_bytes_count;
	if ( full_path[path_bytes_count-2] != '/' )
	{
		full_path[path_bytes_count-1] = '/';
		full_path[path_bytes_count] = 0;
		++full_path_bytes_count;
	}

	u8 * available_mem = mem;
	s64 available_mem_size = mem_size;
	u8 ** files = (u8**)available_mem;
	available_mem_size -= sizeof(u8*)*(files_count+1);
	available_mem      += sizeof(u8*)*(files_count+1);

	CHECK_MEM_SIZE(0);

	s64 file_id = 0;
	for ( ent = readdir( dir );
	      ent;
	      ent = readdir( dir ) )
	{
		// NOTE(theGiallo): is dir static once opened?
		tg_assert( file_id != files_count );

		u32 name_bytes_count = 0;
		while ( ent->d_name[name_bytes_count++] );

		files[file_id] = available_mem;
		u8 * curr_file_target = files[file_id];
		curr_file_target[0] = 0;
		if ( options_flags & (u32)tg::sys::List_Directory_Option::FULL_PATHS )
		{
			CHECK_MEM_SIZE(full_path_bytes_count);
			available_mem_size -= full_path_bytes_count-1;
			available_mem      += full_path_bytes_count-1;
			memcpy( curr_file_target, full_path, full_path_bytes_count );
			curr_file_target   += full_path_bytes_count-1;
		}
		CHECK_MEM_SIZE(name_bytes_count);
		available_mem_size -= name_bytes_count;
		available_mem      += name_bytes_count;
		memcpy( curr_file_target, ent->d_name, name_bytes_count );
		++file_id;
	}
	files[file_id] = available_mem;

	ret = files_count;

	closedir( dir );
	close( fd );

	return ret;
#undef CHECK_MEM_SIZE
}
TG_DL_EXPORTED
TG_SYS_FILE_IS_DIRECTORY( tg_sys_file_is_directory )
{
	s64 ret = 0;

	struct stat buf;

	if ( stat( file_path, &buf ) < 0 )
	{
		ret = TG_SYS_SDL_ERRNO_TO_SYS_ERROR( errno );
		return ret;
	}

	if ( S_ISDIR( buf.st_mode ) )
	{
		ret = 1;
	}

	return ret;
}
TG_DL_EXPORTED
TG_SYS_THREAD_CREATE( tg_sys_thread_create )
{
	SDL_Thread * ret;
	ret = SDL_CreateThread( fun, name, data );
	return (tg::sys::Thread*)ret;
}
TG_DL_EXPORTED
TG_SYS_THREAD_WAIT( tg_sys_thread_wait )
{
	if ( status )
	{
		s32 r_status;
		SDL_WaitThread( (SDL_Thread*)thread, &r_status );

		// TODO(theGiallo, 2016-03-31): convert SDL status to Sys status
	} else
	{
		SDL_WaitThread( (SDL_Thread*)thread, NULL );
	}
}
TG_DL_EXPORTED
TG_SYS_THREAD_DETACH( tg_sys_thread_detach )
{
	SDL_DetachThread( (SDL_Thread*)thread );
}
// NOTE(theGiallo): registers the thread in the threads agenda and/or returns
// its internal id
TG_DL_EXPORTED
TG_SYS_THREAD_ID( tg_sys_thread_id )
{
	u8 ret = TG_SYS_UNKNOWN_THREAD_ID;
	u64 tid = SDL_ThreadID();
	Threads_Agenda * ta = &glob.threads_agenda;
	for ( u8 i = 0; i < TG_SYS_SDL_MAX_THREADS_NUM; ++i )
	{
		u64 * tids = ta->threads_ids + i;
		if ( 0 == interlocked_compare_exchange_64( tids, tid, 0 )  ||
		     *tids == tid )
		{
			ret = i;
			break;
		}
	}
	return ret;
}
TG_DL_EXPORTED
TG_SYS_WARP_MOUSE_IN_WINDOW( tg_sys_warp_mouse_in_window )
{
	tg_assert( window_id >= 0 );
	tg_assert( window_id < (s32)glob.sdl_stuff.windows.windows_count );

	// NOTE(theGiallo): this is to fix mouse warping under debian,
	// I'm not sure this problem shows itself on other platforms
	s32 w,h;
	SDL_GetWindowSize( glob.sdl_stuff.windows.windows[window_id].sdl_window, &w, &h );
#if 0
	if ( ( h % 2 ) == 1 )
	{
		px_pos_y += 1;
	}
#endif
	SDL_WarpMouseInWindow( glob.sdl_stuff.windows.windows[window_id].sdl_window, px_pos_x, h-px_pos_y );
}
TG_DL_EXPORTED
TG_SYS_GET_KEYBOARD_STATE( tg_sys_get_keyboard_state )
{
	const u8 * ret = (const u8*) SDL_GetKeyboardState( bytes_size );
	return ret;
}
TG_DL_EXPORTED
TG_SYS_SET_AUDIO_CALLBACK( tg_sys_set_audio_callback )
{
	glob.sdl_stuff.audio.callback      = callback;
	glob.sdl_stuff.audio.callback_data = callback_data;
}
TG_DL_EXPORTED
TG_SYS_SET_EVENT_CALLBACK( tg_sys_set_event_callback )
{
	glob.sdl_stuff.event_callback      = callback;
	glob.sdl_stuff.event_callback_data = callback_data;
}


namespace tg::sys::sdl
{
u8
u8_hash_32( u32 x )
{
	u32 h = u32_FNV1a( &x, sizeof ( x ) );
	u8 ret = u8( (   h         & 0xff )
	           ^ ( ( h >>  8 ) & 0xff )
	           ^ ( ( h >> 16 ) & 0xff )
	           ^ ( ( h >> 24 ) & 0xff ) );
	return ret;
}

Event
event_from_sdl_event( SDL_Event * sdl_e )
{
	Event e = {};

	e.common.time_fired = time_from_sdl_ticks( sdl_e->common.timestamp );

	switch ( sdl_e->type )
	{
		case SDL_WINDOWEVENT:
			e.type = Event_Type::WINDOW;
			break;
		case SDL_KEYUP:
		case SDL_KEYDOWN:
			e.type = Event_Type::KEYBOARD;
			break;
		case SDL_MOUSEMOTION:
		case SDL_MOUSEBUTTONUP:
		case SDL_MOUSEBUTTONDOWN:
		case SDL_MOUSEWHEEL:
			e.type = Event_Type::MOUSE;
			break;
		case SDL_CONTROLLERAXISMOTION:
		case SDL_CONTROLLERBUTTONDOWN:
		case SDL_CONTROLLERBUTTONUP:
		case SDL_CONTROLLERDEVICEADDED:
		case SDL_CONTROLLERDEVICEREMOVED:
		case SDL_CONTROLLERDEVICEREMAPPED:
			e.type = Event_Type::PAD;
			break;
		default:
			// log_dbg( "sdl_event %u is not supported", sdl_e->type );
			e.type = Event_Type::__COUNT;
			return e;
			break;
	}
	switch ( e.type )
	{
		case Event_Type::WINDOW:
			e.window.window_id =
				window_id_from_sdl_window_id( sdl_e->window.windowID );
			switch ( sdl_e->window.event )
			{
				case SDL_WINDOWEVENT_SHOWN:
					e.window.type = Event_Window_Type::SHOWN;
					break;
				case SDL_WINDOWEVENT_HIDDEN:
					e.window.type = Event_Window_Type::HIDDEN;
					break;
				case SDL_WINDOWEVENT_EXPOSED:
					e.window.type = Event_Window_Type::EXPOSED;
					break;
				case SDL_WINDOWEVENT_MOVED:
					e.window.type = Event_Window_Type::MOVED;
					break;
				case SDL_WINDOWEVENT_RESIZED:
					e.window.type = Event_Window_Type::RESIZED;
					break;
				case SDL_WINDOWEVENT_SIZE_CHANGED:
					e.window.type = Event_Window_Type::SIZE_CHANGED;
					break;
				case SDL_WINDOWEVENT_MINIMIZED:
					e.window.type = Event_Window_Type::MINIMIZED;
					break;
				case SDL_WINDOWEVENT_MAXIMIZED:
					e.window.type = Event_Window_Type::MAXIMIZED;
					break;
				case SDL_WINDOWEVENT_RESTORED:
					e.window.type = Event_Window_Type::RESTORED;
					break;
				case SDL_WINDOWEVENT_ENTER:
					e.window.type = Event_Window_Type::ENTER;
					break;
				case SDL_WINDOWEVENT_LEAVE:
					e.window.type = Event_Window_Type::LEAVE;
					break;
				case SDL_WINDOWEVENT_FOCUS_GAINED:
					e.window.type = Event_Window_Type::FOCUS_GAINED;
					break;
				case SDL_WINDOWEVENT_FOCUS_LOST:
					e.window.type = Event_Window_Type::FOCUS_LOST;
					break;
				case SDL_WINDOWEVENT_CLOSE:
					e.window.type = Event_Window_Type::CLOSE;
					break;
				default:
					log_err( "EVENT_WINDOW %u", u32(sdl_e->window.event) );
					//SDL_WindowEventID s;
					//ILLEGAL_PATH();
					break;
			}
			break;
		case Event_Type::KEYBOARD:
			e.keyboard.window_id =
				window_id_from_sdl_window_id( sdl_e->key.windowID );
			switch ( sdl_e->key.type )
			{
				case SDL_KEYUP:
					e.keyboard.type = Event_Keyboard_Type::KEY_RELEASED;
					break;
				case SDL_KEYDOWN:
					e.keyboard.type = Event_Keyboard_Type::KEY_PRESSED;
					break;
				default:
					ILLEGAL_PATH();
					break;
			}
			break;
		case Event_Type::MOUSE:
			e.mouse.windows_id =
				window_id_from_sdl_window_id( sdl_e->key.windowID );
			switch ( sdl_e->type )
			{
				case SDL_MOUSEMOTION:
					e.mouse.type = Event_Mouse_Type::MOTION;
					break;
				case SDL_MOUSEBUTTONUP:
					e.mouse.type = Event_Mouse_Type::BUTTON_RELEASED;
					break;
				case SDL_MOUSEBUTTONDOWN:
					e.mouse.type = Event_Mouse_Type::BUTTON_PRESSED;
					break;
				case SDL_MOUSEWHEEL:
					e.mouse.type = Event_Mouse_Type::WHEEL;
					break;
				default:
					ILLEGAL_PATH();
					break;
			}
			break;
		case Event_Type::PAD:
			switch ( sdl_e->type )
			{
				case SDL_CONTROLLERAXISMOTION:
					e.controller.type = Event_Controller_Type::AXIS;
					break;
				case SDL_CONTROLLERBUTTONDOWN:
					e.controller.type = Event_Controller_Type::BUTTON_PRESSED;
					break;
				case SDL_CONTROLLERBUTTONUP:
					e.controller.type = Event_Controller_Type::BUTTON_RELEASED;
					break;
				case SDL_CONTROLLERDEVICEADDED:
					e.controller.type = Event_Controller_Type::DEVICE_ADDED;
					break;
				case SDL_CONTROLLERDEVICEREMOVED:
					e.controller.type = Event_Controller_Type::DEVICE_REMOVED;
					break;
				case SDL_CONTROLLERDEVICEREMAPPED:
					e.controller.type = Event_Controller_Type::DEVICE_REMAPPED;
					break;
				default:
					ILLEGAL_PATH();
					break;
			}
			break;
		default:
			break;
	}
	switch ( e.type )
	{
		case Event_Type::WINDOW:
			switch ( e.window.type )
			{
				// case Event_Window_Type::SHOWN:
				// case Event_Window_Type::HIDDEN:
				// case Event_Window_Type::EXPOSED:
				// 	break;
				case Event_Window_Type::MOVED:
					e.window.x = sdl_e->window.data1;
					e.window.y = sdl_e->window.data2;
					break;
				case Event_Window_Type::RESIZED:
					e.window.w = sdl_e->window.data1;
					e.window.h = sdl_e->window.data2;
					break;
				case Event_Window_Type::SIZE_CHANGED:
					e.window.w = sdl_e->window.data1;
					e.window.h = sdl_e->window.data2;
					break;
				// case Event_Window_Type::MINIMIZED:
				// case Event_Window_Type::MAXIMIZED:
				// case Event_Window_Type::RESTORED:
				// case Event_Window_Type::ENTER:
				// case Event_Window_Type::LEAVE:
				// case Event_Window_Type::FOCUS_GAINED:
				// case Event_Window_Type::FOCUS_LOST:
				// case Event_Window_Type::CLOSE:
				default:
					break;
			}
			break;
		case Event_Type::KEYBOARD:
			// NOTE(theGiallo): this is true until SDL changes, but it's unprobable
			e.keyboard.virt_key = (Virt_Key)sdl_e->key.keysym.sym;
			e.keyboard.phys_key = (Phys_Key)sdl_e->key.keysym.scancode;
			e.keyboard.repeat = sdl_e->key.repeat;
			e.keyboard.mods = sdl_e->key.keysym.mod;
			break;
		case Event_Type::MOUSE:
			switch ( e.mouse.type )
			{
				case Event_Mouse_Type::MOTION:
				{
					u32 ww, wh;
					tg_sys_get_window_size( e.mouse.windows_id, &ww, &wh );
					e.mouse.motion.pos_x = sdl_e->motion.x;
					e.mouse.motion.pos_y = wh - sdl_e->motion.y;
					e.mouse.motion.motion_x = sdl_e->motion.xrel;
					e.mouse.motion.motion_y = -sdl_e->motion.yrel;
					break;
				}
				case Event_Mouse_Type::BUTTON_RELEASED:
				case Event_Mouse_Type::BUTTON_PRESSED:
				{
					u32 ww, wh;
					tg_sys_get_window_size( e.mouse.windows_id, &ww, &wh );
					// TODO(theGiallo, 2016-03-03): check why is this missing
					// e.mouse.button.clicks_count = sdl_e->button.clicks;
					e.mouse.button.pos_x = sdl_e->button.x;
					e.mouse.motion.pos_y = wh - sdl_e->button.y;
					e.mouse.button.button =
					   (Mouse_Button)(sdl_e->button.button - 1);
					break;
				}
				case Event_Mouse_Type::WHEEL:
					/*if ( sdl_e->wheel.direction )
					{
					// TODO(theGiallo, 2016-03-03): check why is this missing
					}*/
					e.mouse.wheel.scrolled_x = sdl_e->wheel.x;
					e.mouse.wheel.scrolled_y = sdl_e->wheel.y;
					break;
				default:
					ILLEGAL_PATH();
					break;
			}
			break;
		case Event_Type::PAD:
			e.controller.controller_id = glob.sdl_stuff.input.controllers.sys_id_from_sdl_id_table[u8_hash_32( sdl_e->caxis.which )];
			tg_assert( e.controller.controller_id != TG_SYS_SDL_INVALID_CONTROLLER_ID );
			switch ( e.controller.type )
			{
				case Event_Controller_Type::AXIS:
					e.controller.axis = (Controller_Axis)sdl_e->caxis.axis;
					e.controller.motion = sdl_e->caxis.value;
						break;
				case Event_Controller_Type::BUTTON_PRESSED:
				case Event_Controller_Type::BUTTON_RELEASED:
					e.controller.button = (Controller_Button)sdl_e->cbutton.button;
					break;
				case Event_Controller_Type::DEVICE_ADDED:
					break;
				case Event_Controller_Type::DEVICE_REMOVED:
				case Event_Controller_Type::DEVICE_REMAPPED:
					break;
				default:
					ILLEGAL_PATH();
					break;
			}
			break;
		default:
			break;
	}
	return e;
}


inline
bool
poll_event( Event * out_event )
{
	bool ret = false;
	SDL_Event e = {};
	if ( SDL_PollEvent( &e ) )
	{
		switch ( e.type )
		{
		#if 0
			case SDL_WINDOWEVENT:
				switch ( e.window.event )
				{
					case SDL_WINDOWEVENT_CLOSE:
						// NOTE(theGiallo): demanded to the game layer
						break;
					default:
						break;
				}
				break;
			case SDL_KEYDOWN:
				switch( e.key.keysym.sym )
				{
					case SDLK_F5:
					{
						if ( !( e.key.keysym.mod & KMOD_ALT ) )
						{
							if ( glob.events_record.is_recording )
							{
								stop_input_recording( e.common.timestamp );
							}
							if ( !glob.events_record.events_count )
							{
								break;
							}
							start_input_replaying();
						}
						if ( ( e.key.keysym.mod & ( KMOD_SHIFT | KMOD_ALT ) )
						     &&
						     glob.recorded_mem )
						{
							restore_game_mem();
						}
						break;
					}
					case SDLK_F6:
					{
						if ( glob.events_replay.is_replaying )
						{
							stop_input_replaying();
						}
						if ( glob.events_record.is_recording )
						{
							stop_input_recording( e.common.timestamp );
						}
						break;
					}
					case SDLK_F7:
					{
						if ( e.key.keysym.mod & ( KMOD_SHIFT | KMOD_ALT ) )
						{
							store_game_mem();
						}
						if ( !( e.key.keysym.mod & KMOD_ALT ) )
						{
							start_input_recording( e.common.timestamp );
						}
						break;
					}
					case SDLK_F8:
					{
						if ( e.key.keysym.mod & ( KMOD_SHIFT | KMOD_ALT ) )
						{
							glob.events_replay.loop_mem =
								!glob.events_replay.loop_mem;
						}
						if ( !( e.key.keysym.mod & KMOD_ALT ) )
						{
							glob.events_replay.loop =
								!glob.events_replay.loop;
						}
					}
						break;
					case SDLK_F11:
					{
						Sys_Screen_Mode mode;
						switch ( get_screen_mode( 0 ) )
						{
							case SYS_SCREEN_MODE_WINDOWED:
								mode = SYS_SCREEN_MODE_FULLSCREEN;
								break;
							case SYS_SCREEN_MODE_FULLSCREEN:
							case SYS_SCREEN_MODE_FULLSCREEN_DESKTOP:
								mode = SYS_SCREEN_MODE_WINDOWED;
								break;
							default:
								ILLEGAL_PATH();
								mode = {};
								break;
						}
						set_screen_mode( 0, mode );
						break;
					}
					default:
						break;
				}
				break;
			case SDL_KEYUP:
				break;
		#endif
			case SDL_CONTROLLERDEVICEADDED:
			{
				s32 id = e.cdevice.which;

				log_dbg( "controller attached id:%u", id );

				if ( !SDL_IsGameController( e.cdevice.which ) )
				{
					log_err( "Unknown joystick! Mapping unknown." );
					break;
				}

				log_dbg( "   it's a game controller" );

				if ( glob.sdl_stuff.input.controllers.controllers_count == TG_SYS_SDL_MAX_CONTROLLERS )
				{
					log_err( "only " TOSTRING(SYS_MAX_CONTROLLERS) " controllers are supported!" );
					break;
				}

				SDL_GameController * sdl_controller = NULL;
				sdl_controller = SDL_GameControllerOpen( id );
				if ( !sdl_controller )
				{
					log_err( "couldn't open a game controller (id=%u)", id );
					break;
				}

				log_dbg( "   controller opened" );

				const char * name = SDL_GameControllerName( sdl_controller );
				if ( !name )
				{
					name = "unknown name";
				}
				log_dbg( "   name: '%s'", name );

				SDL_Joystick * sdl_joystick = SDL_GameControllerGetJoystick( sdl_controller );
				if ( ! sdl_joystick )
				{
					log_err( "couldn't get joystick of controller (id=%u)", id );
					break;
				}

				log_dbg( "   joystick retrieved" );

				Controller controller = {};
				controller.sdl_controller = sdl_controller;
				controller.sdl_joystick = sdl_joystick;
				controller.name = name;
				controller.id = TG_SYS_SDL_INVALID_CONTROLLER_ID;

				for ( u8 i = 0; i < TG_SYS_SDL_MAX_CONTROLLERS; ++i )
				{
					if ( ! BITTEST( glob.sdl_stuff.input.controllers.controllers_mask, i ) )
					{
						BITSET( glob.sdl_stuff.input.controllers.controllers_mask, i );
						controller.id = i;

						log_dbg( "controller got internal id %u", i );

						memcpy( glob.sdl_stuff.input.controllers.controllers + i,
						        &controller,
						        sizeof ( controller ) );

						++glob.sdl_stuff.input.controllers.controllers_count;
						break;
					}
				}

				u8 h = u8_hash_32( id );
				if ( glob.sdl_stuff.input.controllers.sys_id_from_sdl_id_table[h] !=
				     TG_SYS_SDL_INVALID_CONTROLLER_ID )
				{
					log_err( "hash collision!!! D: D: D: id=%u, table[hash(id)]=%u",
					             id, glob.sdl_stuff.input.controllers.sys_id_from_sdl_id_table[h] );
				} else
				{
					glob.sdl_stuff.input.controllers.sys_id_from_sdl_id_table[h] = id;
					tg_assert( controller.id != TG_SYS_SDL_INVALID_CONTROLLER_ID );
				}
			}
				break;
			case SDL_CONTROLLERDEVICEREMOVED:
			{
				s32 id = e.cdevice.which;

				log_dbg( "controller removed id:%u", id );

				if ( !SDL_IsGameController( e.cdevice.which ) )
				{
					break;
				}

				log_dbg( "   it's a game controller" );

				u8 h = u8_hash_32( id );
				if ( glob.sdl_stuff.input.controllers.sys_id_from_sdl_id_table[h] == TG_SYS_SDL_INVALID_CONTROLLER_ID )
				{
					log_err( "   the controller was not in hash map" );
				}

				u8 sys_id = glob.sdl_stuff.input.controllers.sys_id_from_sdl_id_table[h];
				BITCLEAR( glob.sdl_stuff.input.controllers.controllers_mask, sys_id );
				glob.sdl_stuff.input.controllers.sys_id_from_sdl_id_table[h] = TG_SYS_SDL_INVALID_CONTROLLER_ID;

				SDL_GameController * sdl_controller = NULL;
				sdl_controller = SDL_GameControllerFromInstanceID( id );

				if ( sdl_controller )
				{
					tg_assert( glob.sdl_stuff.input.controllers.controllers[sys_id].sdl_controller == sdl_controller );

					SDL_GameControllerClose( sdl_controller );

					log_dbg( "   controller closed (%s)", glob.sdl_stuff.input.controllers.controllers[sys_id].name );
				} else
				{
					const char * name = SDL_GameControllerNameForIndex( id );
					if ( !name )
					{
						name = "unknown name";
					}
					log_dbg( "   couldn't get controller from joystick (probably hadn't been opened)(%s)", name );
					SDL_GameControllerClose( sdl_controller );
					log_dbg( "   controller closed (%s)", glob.sdl_stuff.input.controllers.controllers[sys_id].name );
				}
			}
				break;
			case SDL_CONTROLLERDEVICEREMAPPED:
			{
				// TODO(theGiallo, 2016-03-04): what should we do?
				// Shouldn't 'remapped' happen only if we remap it?
				s32 id = e.cdevice.which;

				log_dbg( "controller remapped id:%u", id );

				if ( SDL_IsGameController( e.cdevice.which ) )
				{
					log_dbg( "   it's a game controller" );
					SDL_GameController * controller = NULL;
					controller = SDL_GameControllerOpen( id );

					if ( controller )
					{
						log_dbg( "   controller opened" );
					} else
					{
						log_err( "couldn't open a game controller (id=%u)", id );
					}
				}
			}
				break;
			default:
				break;
		}

		Event event = event_from_sdl_event( &e );
		if ( event.type == Event_Type::__COUNT )
		{
			return ret;
		}
		*out_event = event;
		ret = true;

	}
	return ret;
}
}; // namespace tg::sys::sdl

TG_DL_EXPORTED
TG_SYS_POLL_ALL_EVENTS_TO_CALLBACK( tg_sys_poll_all_events_to_callback )
{
	u32 ret = 0;
	for ( Event e = {}; poll_event( &e );  )
	{
		tg::sys::sdl::glob.sdl_stuff.event_callback( & tg::sys::sdl::glob_impl, tg::sys::sdl::glob.sdl_stuff.event_callback_data, &e );
	}
	return ret;
}

TG_DL_EXPORTED
TG_SYS_POLL_EVENT( tg_sys_poll_event )
{
	return poll_event( out_event );
}

TG_DL_EXPORTED
TG_SYS_GET_CACHED_TIME( tg_sys_get_cached_time )
{
	Cached_Time ret = glob.app.cached_time;
	return ret;
}
TG_DL_EXPORTED
TG_SYS_GET_PERF_SECS( tg_sys_get_perf_secs )
{
	Time ret = tg::sys::sdl::get_perf_secs();
	return ret;
}
TG_DL_EXPORTED
TG_SYS_GET_SYS_SECS( tg_sys_get_sys_secs )
{
	Time ret = tg::sys::sdl::get_sys_secs();
	return ret;
}
TG_DL_EXPORTED
TG_SYS_GET_PERF_RUN_SECS( tg_sys_get_perf_run_secs )
{
	Time ret = tg::sys::sdl::get_perf_run_secs();
	return ret;
}
TG_DL_EXPORTED
TG_SYS_GET_SYS_RUN_SECS( tg_sys_get_sys_run_secs )
{
	Time ret = tg::sys::sdl::get_sys_run_secs();
	return ret;
}
TG_DL_EXPORTED
TG_SYS_UPDATE_TIME( tg_sys_update_time )
{
	Cached_Time & ct = tg::sys::sdl::glob.app.cached_time;

	Time new_run_perf_time = get_perf_run_secs();
	Time new_run_sys_time  = get_sys_run_secs();

	ct.delta_perf_time = new_run_perf_time - ct.run_perf_time;
	ct.delta_sys_time  = new_run_sys_time  - ct.run_sys_time;
	ct.run_perf_time   = new_run_perf_time;
	ct.run_sys_time    = new_run_sys_time;

	++ct.time_updates_counter;
	ct.frame_updates_counter += delta_frames;

	if ( delta_frames )
	{
		ct.delta_frames           = delta_frames;
		ct.frames_delta_perf_time = new_run_perf_time - ct.frames_perf_time;
		ct.frames_delta_sys_time  = new_run_sys_time  - ct.frames_sys_time ;
		ct.frames_perf_time       = new_run_perf_time;
		ct.frames_sys_time        = new_run_sys_time;
	}
}
TG_DL_EXPORTED
TG_SYS_GET_GL_FUNC_LOADER( tg_sys_get_gl_func_loader )
{
	struct _
	{
		static void * loader( void * data, const char * f_name )
		{
			(void)data;
			return SDL_GL_GetProcAddress( f_name );
		}
	};
	tg::sys::GL_Func_Loader ret = {
		._data = 0,
		._loader = _::loader,
	};
	return ret;
}


TG_DL_EXPORTED
void
hot_boot( tg::hot::Hot_Api * hot_api, tg::hot::Hot_Api_Lib_Data data )
{
	(void)hot_api;
	(void)data;

	void * new_data = (void*)((u8*)&tg::sys::sdl::glob + sizeof ( tg::sys::sdl::Tables ) );
	s64 new_data_size = sizeof tg::sys::sdl::glob - sizeof ( tg::sys::sdl::Tables );

	if ( ! data.data )
	{
		bool b_res = init_SDL_stuff( &tg::sys::sdl::glob.sdl_stuff );
		if ( ! b_res )
		{
			log_err( "failed to init tg::sys::sdl" );
		} else
		{
			u32 sdl_ticks = SDL_GetTicks();
			Time perf_time = tg::sys::sdl::get_perf_secs();
			Time sys_time  = s_time();
			Time sdl_s = tg::sys::sdl::time_from_sdl_ticks( sdl_ticks );

			tg::sys::sdl::glob.app.cached_time.base_sys_time  = sys_time  - sdl_s;
			tg::sys::sdl::glob.app.cached_time.base_perf_time = perf_time - sdl_s;
			tg::sys::sdl::glob.app.cached_time.run_sys_time   = sdl_s;
			tg::sys::sdl::glob.app.cached_time.run_perf_time  = sdl_s;
		}
	} else
	if ( data.size == new_data_size )
	{
		memcpy( new_data, data.data, (size_t)data.size );
	}

	hot_api->set_lib_data( new_data, new_data_size );
}

TG_DL_EXPORTED
void
hot_outdated()
{
}


void
__attribute ((constructor))
init_function()
{
	//log_dbg( "constructor lib %s version %s", TOSTRING(LIB_NAME), TOSTRING(LIB_VERSION) );

	tg::sys::sdl::glob.tables =
	{
		{ TG_SYS_SDL_SCREEN_MODE_TABLE( TG_SYS_SDL_SCREEN_MODE_EXPAND_AS_TABLE_ENTRY ) },
		{ TG_SYS_SDL_ERR_TABLE( TG_SYS_SDL_ERR_EXPAND_AS_TABLE_ENTRY ) }
	};
	{
		using namespace tg::sys::sdl;
		tg::sys::sdl::glob_impl = {
			TG_SYS_API(TG_SYS_API_AS_IMPL_SELF_INIT)
		};
	}
}

void
__attribute ((destructor))
fini_function()
{
	//log_dbg( "destructor lib %s version %s", TOSTRING(LIB_NAME), TOSTRING(LIB_VERSION) );
}



#include "utility.cpp"
#include "tgmath.cpp"
