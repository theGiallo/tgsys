#version 330 core
layout(row_major) uniform;
#define STENCIL_ENABLED 1

in  vec2 vertex;
in  vec2 uv;
//struct Quad_Data
//{
in  vec2  quad_data_pos;
in  vec2  quad_data_size;
in  float quad_data_rot;
in  uint  quad_data_color;
in  uint  quad_data_texture_id;
#if STENCIL_ENABLED
in  uint  quad_data_stencil_id;
#endif
// NOTE(theGiallo): these 2 are to set the quad UV coordinates from the [0,1]
// NOTE(theGiallo): the resulting UV coordinates are in the limit space, not
// in the entire texture space
in  vec2  quad_data_uv_offset;
in  vec2  quad_data_uv_scale;
// NOTE(theGiallo): these 2 sets the rectangle in which to limit the sampling
// and the overflow behavior. This defines the uv space: this rectangle is
// considered [0,1]
in  vec2  quad_data_uv_origin;
in  vec2  quad_data_uv_limits;
in  uint  quad_data_texture_overflow_x;
in  uint  quad_data_texture_overflow_y;
in  uint  quad_data_texture_min_filter;
in  uint  quad_data_texture_mag_filter;
//};

//in  Quad_Data quad_data;

struct
Fragment_Tex_Data
{
	// NOTE(theGiallo): these 2 sets the rectangle in which to limit the sampling
	// and the overflow behavior. This defines the uv space: this rectangle is
	// considered [0,1]
	vec2 uv_origin;
	vec2 uv_limits;
	ivec2 overflow_policies;
	uint min_filter;
	uint mag_filter;
};

flat out vec4 color;
flat out uint texture_id;
#if STENCIL_ENABLED
flat out uint stencil_id;
#endif
flat out Fragment_Tex_Data tex_data;

out vec2 _uv;

uniform mat3 VP;

vec2
rotate( vec2 v, float rad )
{
	mat2 rotation = mat2(
	     vec2( cos( rad ),  -sin( rad ) ),
	     vec2( sin( rad ),   cos( rad ) )
	 );

	return v * rotation;
}

void main( void )
{
	// vec3 p3 = vec3( vertex * quad_data_size + quad_data_pos, 1.0) * VP;
	vec3 p3 = vec3( rotate( vertex * quad_data_size, radians( quad_data_rot ) ) + quad_data_pos, 1.0) * VP;
	// _pos = p3.xy / p3.z;
	// _pos = p3.xy;

	gl_Position = vec4( p3.xy, 0.0, 1.0 );

	_uv = quad_data_uv_offset + uv * quad_data_uv_scale;
	// _uv = uv;

	texture_id = quad_data_texture_id;
#if STENCIL_ENABLED
	stencil_id = quad_data_stencil_id;
#endif

	color.r = float( ( quad_data_color >> 0  ) & uint(0xff) ) / 255.0f;
	color.g = float( ( quad_data_color >> 8  ) & uint(0xff) ) / 255.0f;
	color.b = float( ( quad_data_color >> 16 ) & uint(0xff) ) / 255.0f;
	color.a = float( ( quad_data_color >> 24 ) & uint(0xff) ) / 255.0f;

	color.rgb = pow( color.rgb, vec3(2.2,2.2,2.2) ) * color.a; // NOTE: premultiply alpha

	tex_data.uv_origin = quad_data_uv_origin;
	tex_data.uv_limits = quad_data_uv_limits;
	tex_data.overflow_policies =
	ivec2( quad_data_texture_overflow_x, quad_data_texture_overflow_y );
	tex_data.min_filter = quad_data_texture_min_filter;
	tex_data.mag_filter = quad_data_texture_mag_filter;
}
