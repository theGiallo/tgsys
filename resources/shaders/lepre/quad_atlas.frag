#version 410 core
#define GLSL_VERSION 410
layout(row_major) uniform;
#define STENCIL_ENABLED 0

struct
Fragment_Tex_Data
{
	// NOTE(theGiallo): these 2 sets the rectangle in which to limit the sampling
	// and the overflow behavior. This defines the uv space: this rectangle is
	// considered [0,1]
	vec2 uv_origin;
	vec2 uv_limits;
	ivec2 overflow_policies;
	uint min_filter;
	uint mag_filter;
};

flat in vec4 color;
flat in uint texture_id;
#if STENCIL_ENABLED
flat in uint stencil_id;
#endif
flat in Fragment_Tex_Data tex_data;

in vec2 _uv;

#define NUM_TEXTURES 30
uniform mat3 VP;
uniform sampler2D textures[NUM_TEXTURES];
#if STENCIL_ENABLED
#define NUM_STENCILS 2
uniform usampler2D stencils[NUM_STENCILS];
#endif

out vec4 out_color;

// NOTE(theGiallo): texture overflow
#define LEPRE_CLAMP_TO_EDGE   0x0u
#define LEPRE_REPEAT          0x1u
#define LEPRE_MIRRORED_REPEAT 0x2u

// NOTE(theGiallo): texture filtering
#define LEPRE_NEAREST             0x0u
#define LEPRE_BILINEAR            0x1u
#define LEPRE_MIPMAP_NEAREST      0x2u
#define LEPRE_MIPMAP_BILINEAR     0x3u
#define LEPRE_MIPMAP_LINEAR       0x4u
#define LEPRE_MIPMAP_TRILINEAR    0x5u

// NOTE(theGiallo): retrieved empirically
#define MIPMAP_EPSYLON 0.005
float
mip_map_level( sampler2D sampler, vec2 full_uv )
{
	vec2 texture_coordinate = full_uv * textureSize( sampler, 0 );
	vec2 dx_vtc = dFdx( texture_coordinate );
	vec2 dy_vtc = dFdy( texture_coordinate );
	float delta_max_sqr = max( dot( dx_vtc, dx_vtc ), dot( dy_vtc, dy_vtc ) );
	float mml = 0.5 * log2( delta_max_sqr );
	if ( abs( mml ) < MIPMAP_EPSYLON )
	{
		mml = 0;
	}
	return mml;
}

#if GLSL_VERSION >= 410
#define sample_stencil_id( sampler_arr, in_texture_id, sampler_f ) sampler_f( sampler_arr[ in_texture_id ] )
#define sample_texture_id( sampler_arr, in_texture_id, sampler_f ) sampler_f( sampler_arr[ in_texture_id ] )
#else
#define sample_texture_id( sampler_arr, in_texture_id, sampler_f ) ( in_texture_id ==  0u ? sampler_f( sampler_arr[ 0] ) : \
                                                                   ( in_texture_id ==  1u ? sampler_f( sampler_arr[ 1] ) : \
                                                                   ( in_texture_id ==  2u ? sampler_f( sampler_arr[ 2] ) : \
                                                                   ( in_texture_id ==  3u ? sampler_f( sampler_arr[ 3] ) : \
                                                                   ( in_texture_id ==  4u ? sampler_f( sampler_arr[ 4] ) : \
                                                                   ( in_texture_id ==  5u ? sampler_f( sampler_arr[ 5] ) : \
                                                                   ( in_texture_id ==  6u ? sampler_f( sampler_arr[ 6] ) : \
                                                                   ( in_texture_id ==  7u ? sampler_f( sampler_arr[ 7] ) : \
                                                                   ( in_texture_id ==  8u ? sampler_f( sampler_arr[ 8] ) : \
                                                                   ( in_texture_id ==  9u ? sampler_f( sampler_arr[ 9] ) : \
                                                                   ( in_texture_id == 10u ? sampler_f( sampler_arr[10] ) : \
                                                                   ( in_texture_id == 11u ? sampler_f( sampler_arr[11] ) : \
                                                                   ( in_texture_id == 12u ? sampler_f( sampler_arr[12] ) : \
                                                                   ( in_texture_id == 13u ? sampler_f( sampler_arr[13] ) : \
                                                                   ( in_texture_id == 14u ? sampler_f( sampler_arr[14] ) : \
                                                                   ( in_texture_id == 15u ? sampler_f( sampler_arr[15] ) : \
                                                                   ( in_texture_id == 16u ? sampler_f( sampler_arr[16] ) : \
                                                                   ( in_texture_id == 17u ? sampler_f( sampler_arr[17] ) : \
                                                                   ( in_texture_id == 18u ? sampler_f( sampler_arr[18] ) : \
                                                                   ( in_texture_id == 19u ? sampler_f( sampler_arr[19] ) : \
                                                                   ( in_texture_id == 20u ? sampler_f( sampler_arr[20] ) : \
                                                                   ( in_texture_id == 21u ? sampler_f( sampler_arr[21] ) : \
                                                                   ( in_texture_id == 22u ? sampler_f( sampler_arr[22] ) : \
                                                                   ( in_texture_id == 23u ? sampler_f( sampler_arr[23] ) : \
                                                                   ( in_texture_id == 24u ? sampler_f( sampler_arr[24] ) : \
                                                                   ( in_texture_id == 25u ? sampler_f( sampler_arr[25] ) : \
                                                                   ( in_texture_id == 26u ? sampler_f( sampler_arr[26] ) : \
                                                                   ( in_texture_id == 27u ? sampler_f( sampler_arr[27] ) : \
                                                                   ( in_texture_id == 28u ? sampler_f( sampler_arr[28] ) : \
                                                                   ( in_texture_id == 29u ? sampler_f( sampler_arr[29] ) : \
                                                                                            sampler_f( sampler_arr[00] ) \
                                                                   ))))))))))))))))))))))))))))))


#define sample_stencil_id( sampler_arr, in_texture_id, sampler_f ) ( in_texture_id ==  0u ? sampler_f( sampler_arr[ 0] ) : \
                                                                   ( in_texture_id ==  1u ? sampler_f( sampler_arr[ 1] ) : \
                                                                                            sampler_f( sampler_arr[00] ) \
                                                                   ))
#endif
// NOTE(theGiallo): start and end are inclusive
int
overflow_repeat( int v, int start, int end )
{
	return start + int( mod( v - start, end - start + 1 ) );
}

// NOTE(theGiallo): start and end are inclusive
int
overflow_repeat_mirrored( int v, int start, int end )
{
	int span = end - start + 1;
	int val = v - start;
	int f = int( floor( float( val ) / float( span ) ) );

	// NOTE(theGiallo): it's mod( val, span )
	int m = val - span * f;

	if ( f % 2 == 0 )
	{
		return start + m;
	} else
	{
		return start + span - m - 1;
	}
}

// NOTE(theGiallo): min_v and max_v are inclusive
int
manage_overflow( int v, uint policy, int min_v, int max_v )
{
	if ( policy == LEPRE_CLAMP_TO_EDGE )
	{
		v = clamp( v, min_v, max_v );
	} else
	if ( policy == LEPRE_REPEAT )
	{
		v = overflow_repeat( v, min_v, max_v );
	} else
	if ( policy == LEPRE_MIRRORED_REPEAT )
	{
		v = overflow_repeat_mirrored( v, min_v, max_v );
	}
	#if 1
	else
	{
		v = 0;
	}
	#endif

	return v;
}

ivec2
manage_overflow( ivec2 v, ivec2 overflow_policies, ivec2 min_v, ivec2 max_v )
{
	v.x = manage_overflow( v.x, uint( overflow_policies.x ), min_v.x, max_v.x );
	v.y = manage_overflow( v.y, uint( overflow_policies.y ), min_v.y, max_v.y );
	return v;
}

ivec2
texel_coords( ivec2 tex_size, vec2 full_uv,
              vec2 uv_origin, vec2 uv_span, ivec2 overflow_policies )
{
	ivec2 bl_limit = ivec2( tex_size * uv_origin );
	// NOTE(theGiallo): start and end are inclusive, so max gets a -1
	ivec2 tr_limit = ivec2( tex_size * ( uv_origin + uv_span ) ) - 1;
	// NOTE(theGiallo): it was 'floor' originally
	// ivec2 texel_coords = ivec2( floor( full_uv * tex_size ) );
	ivec2 texel_coords = ivec2( floor( full_uv * tex_size ) );
	texel_coords = manage_overflow( texel_coords, overflow_policies,
	                                bl_limit, tr_limit );
	return texel_coords;
}

vec4
texture_bilinear( sampler2D sampler, int lod, vec2 full_uv )
{
	ivec2 tex_size = textureSize( sampler, lod );

	vec2 tx_size_in_uv = 1.0 / vec2(tex_size);
	vec2 h_tx_size_in_uv = tx_size_in_uv * 0.5;
	vec2 intra_f_uv = mod( full_uv - h_tx_size_in_uv, tx_size_in_uv );

	vec2 f_uv = full_uv - h_tx_size_in_uv;
	ivec2 tex_coords =
	texel_coords( tex_size,
	              f_uv, tex_data.uv_origin,
	              tex_data.uv_limits, tex_data.overflow_policies );
	vec4 tc_bl = texelFetch( sampler, tex_coords, lod );

	f_uv = full_uv + h_tx_size_in_uv;
	tex_coords =
	texel_coords( tex_size,
	              f_uv, tex_data.uv_origin,
	              tex_data.uv_limits, tex_data.overflow_policies );
	vec4 tc_tr = texelFetch( sampler, tex_coords, lod );

	f_uv = full_uv + vec2( -h_tx_size_in_uv.x, h_tx_size_in_uv.y );
	tex_coords =
	texel_coords( tex_size,
	              f_uv, tex_data.uv_origin,
	              tex_data.uv_limits, tex_data.overflow_policies );
	vec4 tc_tl = texelFetch( sampler, tex_coords, lod );

	f_uv = full_uv + vec2( h_tx_size_in_uv.x, -h_tx_size_in_uv.y );
	tex_coords =
	texel_coords( tex_size,
	              f_uv, tex_data.uv_origin,
	              tex_data.uv_limits, tex_data.overflow_policies );
	vec4 tc_br = texelFetch( sampler, tex_coords, lod );

	vec2 alpha = intra_f_uv / tx_size_in_uv;
	vec4 c = mix( mix( tc_bl, tc_br, alpha.x ),
	              mix( tc_tl, tc_tr, alpha.x ), alpha.y );
	c = mix(
	         mix( tc_bl, tc_br, alpha.x ),
	         mix( tc_tl, tc_tr, alpha.x ), alpha.y );
	return c;
}

vec4
texture_nearest( sampler2D sampler, int lod, vec2 full_uv )
{
	ivec2 tex_size = textureSize( sampler, lod );
	ivec2 tex_coords =
	texel_coords( tex_size,
	              full_uv, tex_data.uv_origin,
	              tex_data.uv_limits, tex_data.overflow_policies );
	vec4 c = texelFetch( sampler, tex_coords, lod );
	return c;
}

vec4
sample_texture( sampler2D sampler )
{
	float mipmap_lvl_f =
	   mip_map_level( sampler,
	                  _uv * tex_data.uv_limits
	                  /* + tex_data.uv_origin */ );

	vec2 full_uv = _uv * tex_data.uv_limits + tex_data.uv_origin;

	vec4 tc = vec4(0);
	if ( mipmap_lvl_f > 0 &&
	     tex_data.min_filter == LEPRE_MIPMAP_NEAREST )
	{
		int mipmap_lvl = int( round( mipmap_lvl_f ) );
		tc = texture_nearest( sampler, mipmap_lvl,
		                      full_uv );
	} else
	if ( mipmap_lvl_f > 0 &&
	     tex_data.min_filter == LEPRE_MIPMAP_BILINEAR )
	{
		int mipmap_lvl = int( round( mipmap_lvl_f ) );
		tc = texture_bilinear( sampler, mipmap_lvl,
		                       full_uv );
	} else
	if ( mipmap_lvl_f > 0 &&
	     tex_data.min_filter == LEPRE_MIPMAP_LINEAR )
	{
		int mipmap_lvl = int( floor( mipmap_lvl_f ) );
		vec4 tc0 = texture_nearest(  sampler,
		                             mipmap_lvl, full_uv );

		mipmap_lvl = int( ceil( mipmap_lvl_f ) );
		vec4 tc1 = texture_nearest( sampler,
		                            mipmap_lvl, full_uv );

		tc = mix( tc0, tc1, fract( mipmap_lvl_f ) );
	} else
	if ( mipmap_lvl_f > 0 &&
	     tex_data.min_filter == LEPRE_MIPMAP_TRILINEAR )
	{
		int mipmap_lvl = int( floor( mipmap_lvl_f ) );
		vec4 tc0 = texture_bilinear( sampler,
		                             mipmap_lvl, full_uv );

		mipmap_lvl = int( ceil( mipmap_lvl_f ) );
		vec4 tc1 = texture_bilinear( sampler,
		                             mipmap_lvl, full_uv );

		tc = mix( tc0, tc1, fract( mipmap_lvl_f ) );
	} else
	if ( ( mipmap_lvl_f > 0 &&
	       tex_data.min_filter == LEPRE_BILINEAR ) ||
	     ( mipmap_lvl_f <= 0 &&
	       tex_data.mag_filter == LEPRE_BILINEAR )
	   )
	{
		tc = texture_bilinear( sampler, 0, full_uv );
	} else
	if ( mipmap_lvl_f == 0 ||
	     ( mipmap_lvl_f > 0 &&
	       tex_data.min_filter == LEPRE_NEAREST ) ||
	     ( mipmap_lvl_f <= 0 &&
	       tex_data.mag_filter == LEPRE_NEAREST )
	   )
	{
		tc = texture_nearest( sampler, 0, full_uv );
	} else
	{
		tc = vec4(0,1,1,1);
	}

	return tc;
}

void main( void )
{
#if STENCIL_ENABLED
	uint stencil_value = 1u;
	if ( stencil_id != 255u )
	{
		uint stencil_tex_id = stencil_id / 128u;
		#define stencil_fetch( s ) texelFetch( s, ivec2(gl_FragCoord.xy), 0 )
		uvec4 stencil_texel = sample_stencil_id( stencils, stencil_tex_id, stencil_fetch );
		uint stencil_32bit_id = stencil_id - stencil_tex_id * 128u;
		if ( stencil_id < 32u )
		{
			stencil_value = ( stencil_texel.r >> stencil_32bit_id ) & 1u;
		} else
		if ( stencil_id < 64u )
		{
			stencil_value = ( stencil_texel.g >> ( stencil_32bit_id - 32u ) ) & 1u;
		} else
		if ( stencil_id < 96u )
		{
			stencil_value = ( stencil_texel.b >> ( stencil_32bit_id - 64u ) ) & 1u;
		} else
		if ( stencil_id < 128u )
		{
			stencil_value = ( stencil_texel.a >> ( stencil_32bit_id - 96u ) ) & 1u;
		}
	}
	if ( stencil_value == 0u )
	{
		discard;
	}
#endif

	vec4 c = color;
	if ( texture_id == uint(0xFF) )
	{
		out_color = c;
	} else
	{
		out_color = c * sample_texture_id( textures, texture_id, sample_texture );
	}
}
