#version 330 core
layout(row_major) uniform;

in  vec2 vertex;
in  vec4 color;

out vec4 _color;

uniform mat3 VP;


void main( void )
{
	gl_Position = vec4( (vec3(vertex.xy, 1.0) * VP).xy, 0.0, 1.0 );

	_color = color;

	_color.rgb = pow( _color.rgb, vec3(2.2,2.2,2.2) ) * _color.a; // NOTE: premultiply alpha

}
