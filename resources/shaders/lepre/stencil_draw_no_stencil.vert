#version 330 core
layout(row_major) uniform;
#define STENCIL_ENABLED 0

in  vec2 vertex;
in  vec2 uv;

flat out uvec4 f_texture_mask;
#if STENCIL_ENABLED
flat out uvec4 f_stencil_mask;
#endif
out vec2 f_uv;

uniform uint draw_mode;
uniform uint target_stencil_id;
#if STENCIL_ENABLED
	uniform uint stencil_id;
#endif

uniform mat3 VP;

uvec4
stencil_mask( uint id )
{
	uvec4 ret = uvec4(0u);
	if ( id < 32u )
	{
		ret.r = 0x1u << id;
	} else
	if ( id < 64u )
	{
		ret.g = 0x1u << ( id - 32u );
	} else
	if ( id < 96u )
	{
		ret.b = 0x1u << ( id - 64u );
	} else
	if ( id < 128u )
	{
		ret.a = 0x1u << ( id - 96u );
	}

	return ret;
}

void main( void )
{
	vec3 p3 = vec3( vertex, 1.0 ) * VP;

	gl_Position = vec4( p3.xy, 0.0, 1.0 );

	f_uv = uv;

#if STENCIL_ENABLED
	if ( stencil_id == 255u )
	{
		f_stencil_mask = uvec4(0xFFFFFFFF);
	} else
	{
		f_stencil_mask = stencil_mask( stencil_id );
	}
#endif
	f_texture_mask = stencil_mask( target_stencil_id );
}
